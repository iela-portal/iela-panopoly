const fs = require("fs");
const proxy = require("http-proxy");
const path = require("path");
// const http = require("http");
// const https = require("https");
// const express = require("express");
// const parse = require("url-parse");
// const nextInit = require("next");

// const port = parseInt(process.env.PORT, 10) || 3000;
// const httpsPort = parseInt(process.env.HTTPS_PORT, 10) || 8443;
// const dev = process.env.NODE_ENV !== "production";
// const app = nextInit({ dev });
// const handle = app.getRequestHandler();

const target = process.env.PROXY_TARGET || "http://localhost:8080";

const key = path.resolve(__dirname, "./certificates/local.host.key");
const cert = path.resolve(__dirname, "./certificates/local.host.crt");

const server = proxy.createServer({
  target: target,
  ssl: {
    key: fs.readFileSync(key),
    cert: fs.readFileSync(cert)
  }
});

server.on("proxyReq", function(proxyReq) {
  proxyReq.setHeader("X-Forwarded-Proto", "https");
});

server.on("error", function(err, req, res) {
  console.log(err);

  res.writeHead(500, {
    "Content-Type": "text/plain"
  });

  res.end("Something went wrong. And we are reporting a custom error message.");
});

server.listen(8443, () =>
  console.log("Access SSL safe site at: https://local.host:8443")
);
