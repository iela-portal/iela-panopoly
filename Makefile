.PHONY: build

run:
	touch docker/.bash_history
	docker-compose run --rm -p 8080:80 -p 8443:8443 dev

in:
	docker exec -it $(shell docker-compose ps -q dev) /bin/bash

mysql:
	docker exec -it iela-db mysql -h localhost -u root -ppassword drupal

mysql--reset:
	docker exec -it $(shell docker-compose ps -q dev) /bin/bash -l -c \
		"cd /iela/app/drupal && gunzip -kc /initial-database.sql.gz | drush sqlc"

css--compile:
	docker exec -it $(shell docker-compose ps -q dev) /bin/bash -l -c \
		"cd /iela/app/drupal/sites/all/themes/iela_theme/sass/ && bundle exec compass compile"

css--watch:
	docker exec -it $(shell docker-compose ps -q dev) /bin/bash -l -c \
		"cd /iela/app/drupal/sites/all/themes/iela_theme/sass/ && bundle exec compass watch -e development"

stop:
	docker-compose stop

clean:
	docker-compose down

build:
	docker-compose build dev

https:
	docker exec -it $(shell docker-compose ps -q dev) /bin/bash -l -c \
		"cd /iela/app/https-proxy && PROXY_TARGET=http://localhost:80 node server.js"
