#!/bin/bash -l

set -e

source $NVM_DIR/nvm.sh

# Enforce services are logging.
sudo service php5-fpm restart > /tmp/php.log
sudo service nginx restart > /tmp/nginx.log

# Await database.
while ! nc -q 1 database-host 3306 </dev/null; do sleep 3; done

echo ""
echo "--------------------------------------"
echo "--------- Database connected ---------"
echo "--------------------------------------"
echo ""

# Provision minimal database, if not already set.
# -----------------------------------------------

if [ ! -f /iela/app/drupal/sites/default/settings.local.php ]
then
  echo ""
  echo "------- starting fresh install -------"
  echo ""

  echo "1 - Create basic files and ensure permissions."
  mkdir -p /iela/app/drupal/sites/default/files

  echo "2 - Copy configuration files."
  sudo cp /iela/app/drupal/sites/default/template.settings.local.php /iela/app/drupal/sites/default/settings.local.php
  sudo chmod -R 777 /iela/app/drupal/sites/default/settings.local.php

  echo "3 - Inject initial database."
  cd /iela/app/drupal && gunzip -kc /initial-database.sql.gz | drush sqlc

  echo "4 - Compile CSS."
  cd /iela/app/drupal/sites/all/themes/iela_theme/sass && bundle exec compass compile

  echo "5 - Install HTTPS proxy"
  cd /iela/app/https-proxy && yarn
fi

# Ensure permissions are correct.
sudo chmod -R 777 /iela/app/drupal/sites/default/files
sudo chmod 777 /iela/app/drupal/sites/default/settings.local.php
sudo chmod +w -R /iela/app/drupal/sites/default

echo ""
echo "--------------------------------------"
echo "--- Virtual Machine ready to work! ---"
echo "--------------------------------------"

echo ""
echo "Access your Drupal site at: http://$(hostname -i):8080"
echo ""

exec "$@"
