FROM lucasconstantino/iela:latest

# Configure Ruby related environment.
# -----------------------------------
RUN apt-get install -y openssl
RUN gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
RUN \curl -L https://get.rvm.io | bash -s stable
RUN /bin/bash -l -c "rvm requirements"
RUN /bin/bash -l -c "rvm install 2.2"
RUN /bin/bash -l -c "gem install rb-inotify -v '0.10.1'"


# Configure non-sudo user.
# ------------------------

ENV PERM_GROUP_ID 1001
ENV PERM_USER_ID 1000
RUN groupadd --gid ${PERM_GROUP_ID} iela

RUN echo "%sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
  useradd -u ${PERM_USER_ID} -G users,www-data,sudo -g iela -d /iela --shell /bin/bash -m iela && \
  echo "secret\nsecret" | passwd iela


# Configure server.
# -----------------
COPY ./docker/nginx.conf    /etc/nginx/nginx.conf
COPY ./docker/fastcgi.conf  /etc/nginx/fastcgi.conf
COPY ./docker/fpm.conf      /etc/php5/fpm/php-fpm.conf
COPY ./docker/www.conf      /etc/php5/fpm/pool.d/www.conf
COPY ./docker/fpm.ini       /etc/php5/fpm/php.ini
COPY ./docker/cli.ini       /etc/php5/cli/php.ini
COPY ./docker/xdebug.ini    /etc/php5/mods-available/xdebug.ini


# Configure SSL certificate.
# --------------------------

COPY ./docker/local.host.crt /usr/local/share/ca-certificates/local.host.crt
RUN update-ca-certificates


# Configure Node related environment.
# -----------------------------------

ENV NVM_DIR /usr/local/nvm
ARG VERSION=stable
ENV VERSION ${VERSION}

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash \
    && [ -s "$NVM_DIR/nvm.sh" ]                                                         \
    && . "$NVM_DIR/nvm.sh"                                                              \
    && nvm install ${VERSION}                                                           \
    && npm install -g yarn

RUN echo "\n# Source NVM scripts\nsource $NVM_DIR/nvm.sh" >> /etc/bash.bashrc


# Make sure any sudoer can control libs.
# --------------------------------------

RUN sudo chmod a+w -R /usr/local


# Pre-install bundle gems
# -----------------------

COPY ./theme/sass/Gemfile /warmup/Gemfile
COPY ./theme/sass/Gemfile.lock /warmup/Gemfile.lock
RUN /bin/bash -l -c "cd /warmup && bundle install"


# Setup runtime user & dir.
# -------------------------

USER iela
WORKDIR /iela/app


CMD ["/bin/bash", "-l"]
ENTRYPOINT ["/etc/entrypoint.sh"]
