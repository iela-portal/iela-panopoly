<?php

/**
 * IELA Simple Layout
 * Layout with many simple rows an no columns.
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('IELA Simple'),
  'icon' => 'iela-simple.png',
  'category' => t('IELA'),
  'theme' => 'iela_simple',
  'weight' => -10,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'status'             => t('Status'),
    'highlighted'        => t('Highlighted'),
    'highlighted_second' => t('Highlighted Second'),
    'header'             => t('Header'),
    'header_second'      => t('Header Second'),
    'header_third'       => t('Header Third'),
    'header_fourth'      => t('Header Fourth'),
    'content'            => t('Content'),
    'content_second'     => t('Content Second'),
    'content_third'      => t('Content Third'),
    'content_fourth'     => t('Content Fourth'),
    'content_fifth'      => t('Content Fifth'),
    'footer'             => t('Footer'),
    'footer_second'      => t('Footer Second'),
    'footer_third'       => t('Footer Third'),
    'footer_fourth'      => t('Footer Fourth'),
  ),
);

/**
 * Preprocess variables for IELA Simple.
 */
function template_preprocess_iela_simple(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'iela-simple', 'clearfix'),
    ),
  );

  // Construct renderable regions.
  foreach ($variables['layout']['regions'] as $name => $title) {
    $variables['content'][$name] = _iela_panels_layout_single_region_wrapper($variables, $name);
    $variables['containers'][$name][] = $name;
  }
}

/**
 * Returns HTML for the IELA Simple layout.
 */
function theme_iela_simple($vars) {
  return drupal_render($vars['content']);
}
