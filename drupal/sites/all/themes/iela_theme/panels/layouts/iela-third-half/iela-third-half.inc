<?php

/**
 * IELA Third-Half Layout
 * Layout with two third and one half divided columns.
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('IELA Third-Half'),
  'icon' => 'iela-third-half.png',
  'category' => t('IELA'),
  'theme' => 'iela_third_half',
  'weight' => 60,
  // 'css' => 'iela-third-half.css',
  'regions' => array(
    'status'             => t('Status'),
    'highlighted'        => t('Highlighted'),
    'highlighted_second' => t('Highlighted Second'),
    'header'             => t('Header'),
    'header_second'      => t('Header Second'),
    'header_third'       => t('Header Third'),
    'header_fourth'      => t('Header Fourth'),
    'content'            => t('Content'),
    'content_second'     => t('Content Second'),
    'content_third'      => t('Content Third'),
    'content_fourth'     => t('Content Fourth'),
    'content_fifth'      => t('Content Fifth'),
    'footer'             => t('Footer'),
    'footer_second'      => t('Footer Second'),
    'footer_third'       => t('Footer Third'),
    'footer_fourth'      => t('Footer Fourth'),
  ),
);

/**
 * Preprocess variables for IELA Third-Half.
 */
function template_preprocess_iela_third_half(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'iela-third-half', 'clearfix'),
    ),
  );

  // Status:
  $variables['content']['status'] = _iela_panels_layout_single_region_wrapper($variables, 'status');
  $variables['containers']['status'][] = 'status';

  // Highlighted:
  $variables['content']['highlighted'] = _iela_panels_layout_single_region_wrapper($variables, 'highlighted');
  $variables['containers']['highlighted'][] = 'highlighted';

  // Highlighted Second:
  $variables['content']['highlighted_second'] = _iela_panels_layout_single_region_wrapper($variables, 'highlighted_second');
  $variables['containers']['highlighted_second'][] = 'highlighted_second';


  // Headers
  // --------------

  // Header:
  $variables['content']['header'] = _iela_panels_layout_single_region_wrapper($variables, 'header');
  $variables['containers']['header'][] = 'header';

  // Header Second, Third and Fourth (third):
  $variables['content']['header_second_third_fourth'] = _iela_panels_layout_third_region_wrapper($variables, array(
    'header_second', 'header_third', 'header_fourth'
  ));
  $variables['containers']['header_second_third_fourth'][] = 'header_second';
  $variables['containers']['header_second_third_fourth'][] = 'header_third';
  $variables['containers']['header_second_third_fourth'][] = 'header_fourth';


  // Contents
  // --------------

  // Content:
  $variables['content']['content'] = _iela_panels_layout_single_region_wrapper($variables, 'content');
  $variables['containers']['content'][] = 'content';

  // Content Second:
  $variables['content']['content_second'] = _iela_panels_layout_single_region_wrapper($variables, 'content_second');
  $variables['containers']['content_second'][] = 'content_second';

  // Contend Third and Fourth (half):
  $variables['content']['content_third_fourth'] = _iela_panels_layout_half_region_wrapper($variables, array(
    'content_third', 'content_fourth'
  ));
  $variables['containers']['content_third_fourth'][] = 'content_third';
  $variables['containers']['content_third_fourth'][] = 'content_fourth';

  // Content Fifth:
  $variables['content']['content_fifth'] = _iela_panels_layout_single_region_wrapper($variables, 'content_fifth');
  $variables['containers']['content_fifth'][] = 'content_fifth';


  // Footers
  // --------------

  // Footer:
  $variables['content']['footer'] = _iela_panels_layout_single_region_wrapper($variables, 'footer');
  $variables['containers']['footer'][] = 'footer';

  // Footer Second, Third and Fourth (third):
  $variables['content']['footer_second_third_fourth'] = _iela_panels_layout_third_region_wrapper($variables, array(
    'footer_second', 'footer_third', 'footer_fourth'
  ));
  $variables['containers']['footer_second_third_fourth'][] = 'footer_second';
  $variables['containers']['footer_second_third_fourth'][] = 'footer_third';
  $variables['containers']['footer_second_third_fourth'][] = 'footer_fourth';
}

/**
 * Returns HTML for the IELA Third-Half layout.
 */
function theme_iela_third_half($vars) {
  return drupal_render($vars['content']);
}
