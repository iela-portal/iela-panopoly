<?php

/**
 * IELA Sidebar Mixed Right Layout
 * Layout with two sidebars columns on the right and one on the left.
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('IELA Sidebar Mixed Right'),
  'icon' => 'iela-sidebar-mixed-right.png',
  'category' => t('IELA'),
  'theme' => 'iela_sidebar_mixed_right',
  'weight' => 80,
  // 'css' => 'iela-sidebar-mixed-right.css',
  'regions' => array(
    'status'             => t('Status'),
    'highlighted'        => t('Highlighted'),
    'highlighted_second' => t('Highlighted Second'),
    'header'             => t('Header'),
    'header_second'      => t('Header Second'),
    'header_third'       => t('Header Third'),
    'header_fourth'      => t('Header Fourth'),
    'content'            => t('Content'),
    'content_second'     => t('Content Second'),
    'content_third'      => t('Content Third'),
    'content_fourth'     => t('Content Fourth'),
    'content_fifth'      => t('Content Fifth'),
    'footer'             => t('Footer'),
    'footer_second'      => t('Footer Second'),
    'footer_third'       => t('Footer Third'),
    'footer_fourth'      => t('Footer Fourth'),
  ),
);

/**
 * Preprocess variables for IELA Sidebar Mixed Right.
 */
function template_preprocess_iela_sidebar_mixed_right(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'iela-sidebar-mixed-right', 'clearfix'),
    ),
  );

  // Status:
  $variables['content']['status'] = _iela_panels_layout_single_region_wrapper($variables, 'status');
  $variables['containers']['status'][] = 'status';

  // Highlighted:
  $variables['content']['highlighted'] = _iela_panels_layout_single_region_wrapper($variables, 'highlighted');
  $variables['containers']['highlighted'][] = 'highlighted';

  // Highlighted Second:
  $variables['content']['highlighted_second'] = _iela_panels_layout_single_region_wrapper($variables, 'highlighted_second');
  $variables['containers']['highlighted_second'][] = 'highlighted_second';


  // Headers
  // --------------

  // Header:
  $variables['content']['header'] = _iela_panels_layout_single_region_wrapper($variables, 'header');
  $variables['containers']['header'][] = 'header';

  // Header Second and Third (sidebar left):
  $variables['content']['header_second_third'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'header_second' => array('xs' => 12, 'sm' => 12, 'md' => 8, 'lg' => 9),
    'header_third' => array('xs' => 12, 'sm' => 12, 'md' => 4, 'lg' => 3),
  ));
  $variables['containers']['header_second_third'][] = 'header_second';
  $variables['containers']['header_second_third'][] = 'header_third';

  // Header Fourth:
  $variables['content']['header_fourth'] = _iela_panels_layout_single_region_wrapper($variables, 'header_fourth');
  $variables['containers']['header_fourth'][] = 'header_fourth';


  // Contents
  // --------------

  // Content:
  $variables['content']['content'] = _iela_panels_layout_single_region_wrapper($variables, 'content');
  $variables['containers']['content'][] = 'content';

  // Content Second:
  $variables['content']['content_second'] = _iela_panels_layout_single_region_wrapper($variables, 'content_second');
  $variables['containers']['content_second'][] = 'content_second';

  // Contend Third and Fourth (sidebar right):
  $variables['content']['content_third_fourth'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'content_third' => array('xs' => 12, 'sm' => 12, 'md' => 4, 'lg' => 3),
    'content_fourth' => array('xs' => 12, 'sm' => 12, 'md' => 8, 'lg' => 9),
  ));
  $variables['containers']['content_third_fourth'][] = 'content_third';
  $variables['containers']['content_third_fourth'][] = 'content_fourth';

  // Content Fifth:
  $variables['content']['content_fifth'] = _iela_panels_layout_single_region_wrapper($variables, 'content_fifth');
  $variables['containers']['content_fifth'][] = 'content_fifth';


  // Footers
  // --------------

  // Footer:
  $variables['content']['footer'] = _iela_panels_layout_single_region_wrapper($variables, 'footer');
  $variables['containers']['footer'][] = 'footer';

  // Footer Second and Third (sidebar left):
  $variables['content']['footer_second_third'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'footer_second' => array('xs' => 12, 'sm' => 12, 'md' => 8, 'lg' => 9),
    'footer_third' => array('xs' => 12, 'sm' => 12, 'md' => 4, 'lg' => 3),
  ));
  $variables['containers']['footer_second_third'][] = 'footer_second';
  $variables['containers']['footer_second_third'][] = 'footer_third';

  // Footer Fourth:
  $variables['content']['footer_fourth'] = _iela_panels_layout_single_region_wrapper($variables, 'footer_fourth');
  $variables['containers']['footer_fourth'][] = 'footer_fourth';
}

/**
 * Returns HTML for the IELA Sidebar Mixed Right layout.
 */
function theme_iela_sidebar_mixed_right($vars) {
  return drupal_render($vars['content']);
}
