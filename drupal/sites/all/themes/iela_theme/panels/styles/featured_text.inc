<?php

/**
 * @file
 * Definition of the 'Featured Text' panel style.
 */

$plugin = array(
  'title' => t('Featured text'),
  'description' => t('Use this to make a text be featured.'),
  'render pane' => 'featured_text_pane_style',
  // 'pane settings form' => 'featured_text_pane_style_settings_form',
  'multiple style' => TRUE,
  'type access' => 'featured_text_pane_style_access',
  // 'defaults' => array(
  //   'width' => '100%',
  //   'height' => '0.6',
  // ),
);

/**
 * Template preprocess function for the plugin style.
 */
function template_preprocess_featured_text_pane_style(&$variables) {
  $variables['pane_wrapper_attributes']['class'][] = 'featured-text';
}

/**
 * Theme function for the plugin style.
 */
function theme_featured_text_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Defines access to this style based on pane.
 */
function featured_text_pane_style_access(&$pane, $subtype) {
  $access_map = array(
    'entity_field' => array(
      'node:body'
    ),
  );

  foreach ($access_map as $type => $subtypes) {
    if ($pane->type === $type && in_array($pane->subtype, $subtypes)) {
      return TRUE;
    }
  }

  return FALSE;
}
