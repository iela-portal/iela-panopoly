<?php

/**
 * @file
 * Definition of the User Grid panel style.
 * This style allows to configure a user list to be presented as a grid.
 */

module_load_include('inc', 'iela', 'includes/iela.functions');

$plugin = array(
  'title' => t('Users grid'),
  'description' => t('Make a grid with the a list of users.'),
  'render pane' => 'user_grid_pane_style',
  'pane settings form' => 'user_grid_pane_style_settings_form',
  'multiple style' => true,
  'weight' => 0,
  'type access' => 'user_grid_pane_style_access',
  'defaults' => array(
    'columns' => array(
      'xs' => 12,
      'sm' => 6,
      'md' => 4,
      'lg' => 3,
    ),
    'reduce' => false,
    'masonry' => false,
  ),
);

/**
 * Template preprocess for the region style.
 */
function template_preprocess_user_grid_pane_style(&$variables) {

  $settings = $variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  $variables['pane_wrapper_attributes']['class'][] = 'user-grid';

  foreach ($settings['columns'] as $breakpoint => $amount) {
    $variables['pane_wrapper_attributes']['class'][] = 'user-grid-' . $breakpoint . '-' . $amount;
  }

  if (!empty($settings['reduce'])) {
    $variables['pane_wrapper_attributes']['class'][] = 'user-grid-reduce';
  }

  if (!empty($settings['masonry'])) {
    libraries_load('masonry');

    // Make sure pane wrapper id is set.
    $css_id = iela_fulfil_pane_wrapper_id($variables);

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-masonry.js', array(
      'weight' => 100,
    ));

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaMasonry' => array(
        '#' . $css_id => array(
          'containerSelector' => '.field-name-field-users > .field-items',
          'itemSelector' => '.user-profile'
        ),
      ),
    ), 'setting');
  }
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_user_grid_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Configuration form.
 */
function user_grid_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $breakpoints = iela_get_breakpoints_theme_tree();

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  foreach ($breakpoints as $machine_name => $breakpoint) {
    $form['columns'][$breakpoint->name] = array(
      '#type' => 'select',
      '#title' => "{$breakpoint->name} breakpoint",
      '#descripion' => "{$breakpoint->breakpoint}",
      '#options' => array(),
      '#default_value' => $conf['columns'][$breakpoint->name],
      '#wrapper_attributes' => array('class' => array(
        'inline-form-item',
        'views-grid-breakpoint-label'
      )),
      '#label_attributes' => array('style' => array('width: 50%;')),
    );

    if (!empty($icons[$breakpoint->name])) {
      $form['columns'][$breakpoint->name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    // Add options.
    for($i = 1; $i <= 12; $i++) {
      $form['columns'][$breakpoint->name]['#options'][$i] = format_plural($i, '1 column per item', '@count columns per item');
    }
  }

  $form['reduce'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reduce text font size'),
    '#default_value' => $conf['reduce'],
  );

  // Simple break.
  $form['break_1'] = array(
    '#type' => 'html_tag',
    '#tag' => 'hr',
  );

  // Use masonry plugin.
  $form['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['masonry'],
  );

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function user_grid_pane_style_access(&$pane, $subtype) {
  return $pane->type == 'fieldable_panels_pane' && !empty($subtype) && $subtype->bundle == 'user_list';
}
