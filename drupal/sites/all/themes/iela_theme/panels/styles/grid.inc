<?php

/**
 * @file
 * Definition of the Grid panel region style.
 */

$plugin = array(
  'title' => t('Grid'),
  'description' => t('Configure the grid of a region\'s panes.'),
  'render region' => 'grid_region_style',
  'settings form' => 'grid_region_style_settings_form',
  'multiple style' => true,
  'defaults' => array(
    'same_content_height' => false,
    'same_title_height' => false,
    'masonry' => false,
  ),
);

/**
 * Preprocess wrapping container.
 */
function grid_region_style_preprocess_container($variables) {

  // Easy access.
  $region = &$variables['container']['container inside']['row']['row inside'][$variables['region_id']];
  $theme_path = drupal_get_path('theme', 'iela_theme');
  $settings = $variables['settings'];
  $same_height = array();

  if (!empty($settings['same_content_height'])) $same_height[] = '.pane-content';
  if (!empty($settings['same_title_height'])) $same_height[] = '.pane-title';

  // Any same height.
  if (!empty($same_height)) {
    
    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-same-height.js');

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaSameHeight' => array(
        '#' . $region['#attributes']['id'] => $same_height,
      ),
    ), 'setting');
  }

  // Handle Masonry.
  if (!empty($settings['masonry'])) {
    $region['#attributes']['class'][] = 'masonry-region-style';

    // Load Masonry.
    libraries_load('masonry');

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-masonry.js', array(
      'weight' => 100,
    ));

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaMasonry' => array(
        '#' . $region['#attributes']['id'] => array(
          'containerSelector' => '.panel-panel-inner',
          'itemSelector' => '.panel-pane',
        ),
      ),
    ), 'setting');
  }
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_grid_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('grid_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_grid_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}

/**
 * Configuration form.
 */
function grid_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();

  // Keep same height amoung panes.
  $form['same_content_height'] = array(
    '#type' => 'checkbox',
    '#title' => t('Same content height'),
    '#description' => t('Force all pane contents to have the same height'),
    '#default_value' => $conf['same_content_height'],
  );

  // Keep same height amoung pane titles.
  $form['same_title_height'] = array(
    '#type' => 'checkbox',
    '#title' => t('Same title height'),
    '#description' => t('Force all pane contents to have the same title height'),
    '#default_value' => $conf['same_title_height'],
  );

  // Use masonry plugin.
  $form['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['masonry'],
  );

  return $form;
}
