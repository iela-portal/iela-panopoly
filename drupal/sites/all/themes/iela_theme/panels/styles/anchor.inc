<?php

/**
 * @file
 * Definition of the Anchor region style.
 * This style allows to configure an anchor id to a region or pane.
 */

$plugin = array(
  'title' => t('Anchor'),
  'description' => t('Configure an anchor id.'),

  // Region style.
  'render region' => 'anchor_region_style',
  'settings form' => 'anchor_style_settings_form',

  // Pane style.
  'render pane' => 'anchor_pane_style',
  'pane settings form' => 'anchor_style_settings_form',

  'multiple style' => array(
    'wrapper' => true,
    'region' => false,
  ),
  'weight' => -10,
  'defaults' => array(
    'id' => '',
    'update' => false,
    'update_distance' => '60',
  ),
);


// --------------------------------------------------------
// General
// --------------------------------------------------------

/**
 * Configuration form.
 */
function anchor_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $multiple = !empty($form_state['style']['name']) && $form_state['style']['name'] == 'multiple';
  $base_input_selector = $multiple ? ':input[name="settings[anchor]' : ':input[name="settings';

  module_load_include('inc', 'iela', 'includes/iela.functions');

  // HTML Id field.
  $form['id'] = array(
    '#type' => 'textfield',
    '#title' => t('Anchor ID'),
    '#description' => t('The HTML ID to be added and referenced via # on the url'),
    '#default_value' => $conf['id'],
    '#required' => true,
    '#element_validate' => array('iela_css_id_validate'),
  );

  // Option to update links on reach.
  $form['update'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update link states'),
    '#description' => t('Check this to update anchor links state on the same page'),
    '#default_value' => $conf['update'],
  );

  // Distance from top for the update.
  $form['update_distance'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance from top'),
    '#description' => t('Define the activating distance from the top of the page'),
    '#default_value' => $conf['update_distance'],
    '#states' => array(
      'invisible' => array(
        $base_input_selector . '[update]"]' => array('checked' => false),
      ),
    ),
  );

  return $form;
}

// --------------------------------------------------------
// Panel Region
// --------------------------------------------------------

/**
 * Preprocess wrapping container.
 */
function anchor_region_style_preprocess_container(&$variables) {

  // Easy access.
  $region = &$variables['container']['container inside']['row']['row inside'][$variables['region_id']];
  $settings = $variables['settings'];

  // Identify region.
  $region['#attributes']['id'] = $settings['id'];

  if (!empty($settings['update'])) {

    $id = $settings['id'][0] == '#' ? $settings['id'] : '#' . $settings['id'];

    // Provide settings to plugin.
    drupal_add_js(array(
      'anchorUpdate' => array(
        $id => array(
          'distance' => $settings['update_distance'],
        ),
      ),
    ), 'setting');

    // Load activation script.
    drupal_add_js(drupal_get_path('theme', 'iela_theme') . '/js/anchor-update.js');
  }
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_anchor_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('anchor_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_anchor_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}


// --------------------------------------------------------
// Panel Pane
// --------------------------------------------------------

/**
 * Template preprocess for the pane style.
 */
function template_preprocess_anchor_pane_style(&$variables) {

  // Easy access.
  $settings = $variables['settings'];

  // Identify pane.
  $variables['pane_wrapper_attributes']['id'] = $settings['id'];

  if (!empty($settings['update'])) {

    $id = $settings['id'][0] == '#' ? $settings['id'] : '#' . $settings['id'];

    // Provide settings to plugin.
    drupal_add_js(array(
      'anchorUpdate' => array(
        $id => array(
          'distance' => $settings['update_distance'],
        ),
      ),
    ), 'setting');

    // Load activation script.
    drupal_add_js(drupal_get_path('theme', 'iela_theme') . '/js/anchor-update.js');
  }
}

/**
 * Returns HTML for the pane style.
 */
function theme_anchor_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}
