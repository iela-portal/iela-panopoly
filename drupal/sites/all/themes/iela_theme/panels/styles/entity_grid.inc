<?php

/**
 * @file
 * Definition of the Enitity Grid panel style.
 * This style allows to configure an entity reference field to be presented as a grid.
 */

module_load_include('inc', 'iela', 'includes/iela.functions');

$plugin = array(
  'title' => t('Entities grid'),
  'description' => t('Make a grid with the referenced entities.'),
  'render pane' => 'entity_grid_pane_style',
  'pane settings form' => 'entity_grid_pane_style_settings_form',
  'multiple style' => true,
  'weight' => 0,
  'type access' => 'entity_grid_pane_style_access',
  'defaults' => array(
    'columns' => array(
      'xs' => 12,
      'sm' => 6,
      'md' => 4,
      'lg' => 3,
    ),
    'reduce' => false,
    'same_height' => false,
    'masonry' => false,
  ),
);

/**
 * Configuration form.
 */
function entity_grid_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $breakpoints = iela_get_breakpoints_theme_tree();

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  foreach ($breakpoints as $machine_name => $breakpoint) {
    $form['columns'][$breakpoint->name] = array(
      '#type' => 'select',
      '#title' => "{$breakpoint->name} breakpoint",
      '#descripion' => "{$breakpoint->breakpoint}",
      '#options' => array(),
      '#default_value' => $conf['columns'][$breakpoint->name],
      '#wrapper_attributes' => array('class' => array(
        'inline-form-item',
        'views-grid-breakpoint-label'
      )),
      '#label_attributes' => array('style' => array('width: 50%;')),
    );

    if (!empty($icons[$breakpoint->name])) {
      $form['columns'][$breakpoint->name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    // Add options.
    for($i = 1; $i <= 12; $i++) {
      $form['columns'][$breakpoint->name]['#options'][$i] = format_plural($i, '1 column per item', '@count columns per item');
    }
  }

  $form['reduce'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reduce text font size'),
    '#default_value' => $conf['reduce'],
  );

  // Simple break.
  $form['break_1'] = array(
    '#type' => 'html_tag',
    '#tag' => 'hr',
  );

  // Keep same height amoung results.
  $form['same_height'] = array(
    '#type' => 'checkbox',
    '#title' => t('Same height'),
    '#default_value' => $conf['same_height'],
  );

  // Use masonry plugin.
  $form['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['masonry'],
  );

  return $form;
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_entity_grid_pane_style(&$variables) {

  $settings = $variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Make sure pane wrapper id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  $variables['pane_wrapper_attributes']['class'][] = 'entity-grid';

  foreach ($settings['columns'] as $breakpoint => $amount) {
    $variables['pane_wrapper_attributes']['class'][] = 'entity-grid-' . $breakpoint . '-' . $amount;
  }

  if (!empty($settings['reduce'])) {
    $variables['pane_wrapper_attributes']['class'][] = 'entity-grid-reduce';
  }

  if (!empty($settings['same_height'])) {
    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-same-height.js');

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaSameHeight' => array(
        '#' . $css_id => '.field-type-entityreference > .field-items .entity-view',
      ),
    ), 'setting');
  }

  if (!empty($settings['masonry'])) {
    libraries_load('masonry');

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-masonry.js', array(
      'weight' => 100,
    ));

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaMasonry' => array(
        '#' . $css_id => array(
          'containerSelector' => '.field-type-entityreference > .field-items',
          'itemSelector' => '.entity-view'
        ),
      ),
    ), 'setting');
  }
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_entity_grid_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Defines access to this style based on pane.
 */
function entity_grid_pane_style_access(&$pane, $subtype) {
 
  switch ($pane->type) {
    case 'entity_field':

      // Allow entity reference field panes with view formatter.
      if ($pane->configuration['formatter'] == 'entityreference_entity_view') {
        return true;
      }

      break;

    case 'fieldable_panels_pane':

      switch ($subtype->bundle) {
        case 'content_list':
        // case 'other_allowed_bundle':
          return true;
          break;
      }

      break;
  }
}
