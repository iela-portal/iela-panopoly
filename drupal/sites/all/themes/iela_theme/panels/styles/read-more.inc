<?php

/**
 * @file
 * Definition of the 'Read More' panel style.
 */

$plugin = array(
  'title' => t('Read More'),
  'description' => t('Read more style'),
  'render pane' => 'read_more_pane_style',
  'pane settings form' => 'read_more_pane_style_settings_form',
  'multiple style' => true,
  'type access' => 'read_more_pane_style_access',
  'defaults' => array(
    'height' => 6,
    'more' => t('Read more'),
    'less' => t('Close'),
    'open' => false,
  ),
);

/**
 * Template preprocess function for the plugin style.
 */
function template_preprocess_read_more_pane_style(&$variables) {

  // Easy access:
  $conf = &$variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Make sure pane wrapper id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  $settings = array(
    'readMorePaneStyle' => array(
      '#' . $css_id => array(
        'blockSelector' => '.pane-content .field',
        'moreLink' => '<a href="#" class="read-more-pane-style read-more">' . $conf['more'] . '</a>',
        'lessLink' => '<a href="#" class="read-more-pane-style read-less">' . $conf['less'] . '</a>',
        'startOpen' => $conf['open'],
      ),
    ),
  );

  if (strpos($conf['height'], 'px') !== FALSE) {
    $settings['readMorePaneStyle']['#' . $css_id]['collapsedHeight'] = $conf['height'];
  } else {
    $settings['readMorePaneStyle']['#' . $css_id]['lines'] = $conf['height'];
  }

  drupal_add_js($theme_path . '/lib/readmore/readmore.js');
  drupal_add_js($theme_path . '/js/read-more-pane-style.js');
  drupal_add_js($settings, 'setting');
}

/**
 * Theme function for the plugin style.
 */
function theme_read_more_pane_style($variables) {
  return theme('panels_pane', $variables);
}

/**
 * Create form the plugin.
 */
function read_more_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();

  // Shown height.
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Shown height'),
    '#description' => t('How many lines should be shown when collapsed. Use "px" if needed.'),
    '#default_value' => $conf['height'],
  );

  // Read more text.
  $form['more'] = array(
    '#type' => 'textfield',
    '#title' => t('Read more text'),
    '#description' => t('Text to shown on the "read more" button'),
    '#default_value' => $conf['more'],
  );

  // Read les text.
  $form['less'] = array(
    '#type' => 'textfield',
    '#title' => t('Close text'),
    '#description' => t('Text to shown on the "close" button'),
    '#default_value' => $conf['less'],
  );

  // Open option.
  $form['open'] = array(
    '#type' => 'checkbox',
    '#title' => t('Start opened'),
    '#description' => t('Mark this to start showing the full text'),
    '#default_value' => $conf['open'],
  );

  return $form;
}

/**
 * Access callback.
 */
function read_more_pane_style_access($pane, $subtype) {
  return ($pane->type == 'node_body')
      || ($pane->type == 'entity_field' && strpos($pane->subtype, 'node:body') !== FALSE)
      || ($pane->type == 'fieldable_panels_pane' && !empty($subtype) && $subtype->bundle == 'text');
}
