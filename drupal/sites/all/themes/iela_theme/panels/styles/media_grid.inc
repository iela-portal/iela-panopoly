<?php

/**
 * @file
 * Definition of the Media Grid panel style.
 * This style allows to configure a media list to be presented as a grid.
 */

module_load_include('inc', 'iela', 'includes/iela.functions');

$plugin = array(
  'title' => t('Multimedia grid'),
  'description' => t('Make a grid with the a list of media.'),
  'render pane' => 'media_grid_pane_style',
  'pane settings form' => 'media_grid_pane_style_settings_form',
  'multiple style' => true,
  'weight' => 0,
  'type access' => 'media_grid_pane_style_access',
  'defaults' => array(
    'columns' => array(
      'xs' => 12,
      'sm' => 6,
      'md' => 4,
      'lg' => 3,
    ),
    'item_alignment' => 'left',
    'clickable_item' => true,
    'grid_alignment' => 'center',
    'masonry' => false,
  ),
);

/**
 * Template preprocess for the region style.
 */
function template_preprocess_media_grid_pane_style(&$variables) {

  $content = &$variables['content']->content;
  $settings = $variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  $variables['pane_wrapper_attributes']['class'][] = 'media-list-grid';

  foreach ($settings['columns'] as $breakpoint => $amount) {
    $variables['pane_wrapper_attributes']['class'][] = 'media-list-grid-' . $breakpoint . '-' . $amount;
  }

  if (!empty($settings['item_alignment']) && !empty($content['field_medias']['#items'])) {
    foreach ($content['field_medias']['#items'] as $delta => $item) {
      $content['field_medias'][$delta]['#attributes']['class'][] = 'text-' . $settings['item_alignment'];
    }
  }

  if (!empty($settings['clickable_item']) && !empty($content['field_medias']['#items'])) {
    foreach ($content['field_medias']['#items'] as $delta => $item) {
      $field_collection_item = array_values($content['field_medias'][$delta]['entity']['field_collection_item']);
      $entity = array_shift($field_collection_item);
      if (!empty($entity['field_link']['#items'][0]['url'])) {
        $content['field_medias'][$delta]['#attributes']['data-click'] = $entity['field_link']['#items'][0]['url'];
      }
    }
  }

  if (!empty($settings['grid_alignment'])) {
    $variables['content_attributes_array']['class'][] = 'text-' . $settings['grid_alignment'];
  }

  if (!empty($settings['masonry'])) {
    libraries_load('masonry');

    // Make sure pane wrapper id is set.
    $css_id = iela_fulfil_pane_wrapper_id($variables);

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-masonry.js', array(
      'weight' => 100,
    ));

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaMasonry' => array(
        '#' . $css_id => array(
          'containerSelector' => '.field-name-field-medias > .field-items'
        ),
      ),
    ), 'setting');
  }
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_media_grid_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Configuration form.
 */
function media_grid_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $breakpoints = iela_get_breakpoints_theme_tree();

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  foreach ($breakpoints as $machine_name => $breakpoint) {
    $form['columns'][$breakpoint->name] = array(
      '#type' => 'select',
      '#title' => "{$breakpoint->name} breakpoint",
      '#descripion' => "{$breakpoint->breakpoint}",
      '#options' => array(),
      '#default_value' => $conf['columns'][$breakpoint->name],
      '#wrapper_attributes' => array('class' => array(
        'inline-form-item',
        'views-grid-breakpoint-label'
      )),
      '#label_attributes' => array('style' => array('width: 50%;')),
    );

    if (!empty($icons[$breakpoint->name])) {
      $form['columns'][$breakpoint->name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    // Add options.
    for($i = 1; $i <= 12; $i++) {
      $form['columns'][$breakpoint->name]['#options'][$i] = format_plural($i, '1 column per item', '@count columns per item');
    }
  }

  // Each item's alignment.
  $form['item_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Item\'s content alignment'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#default_value' => $conf['item_alignment'],
  );

  // Make the whole item clickable.
  $form['clickable_item'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clickable item'),
    '#description' => t('If this option is set and the title has a link, the whole item will be clickable.'),
    '#default_value' => $conf['clickable_item'],
  );

  // Simple break.
  $form['break_1'] = array(
    '#type' => 'html_tag',
    '#tag' => 'hr',
  );

  // Grid alignment.
  $form['grid_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Grid\'s alignment'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#default_value' => $conf['grid_alignment'],
  );

  // Use masonry plugin.
  $form['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['masonry'],
  );

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function media_grid_pane_style_access(&$pane, $subtype) {
  return $pane->type == 'fieldable_panels_pane' && !empty($subtype) && $subtype->bundle == 'media_list';
}
