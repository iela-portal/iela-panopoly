<?php

/**
 * @file
 * Definition of the Tabs panel region style.
 * This style allows to configure a region to behave as tabs of panes.
 */

$plugin = array(
  'title' => t('Tabs'),
  'description' => t('Make tabs with the region\'s panes.'),
  'render region' => 'tabs_region_style',
  'settings form' => 'tabs_region_style_settings_form',
  'weight' => 50,
  'multiple style' => array(
    'wrapper' => true,
    'region' => false,
  ),
  'defaults' => array(
    'active' => '1',
    'style' => 'nav-tabs',
    'justify' => false,
    // 'no_gap' => false,
  ),
);

/**
 * Helper function to transform the settings into jQueryUI options.
 */
function _tabs_region_style_transform_settings($settings) {
  $settings['active'] = empty($settings['active']) ? false : intval($settings['active']) - 1;

  return $settings;
}

/**
 * Preprocess wrapping container.
 * We do our thing here to make sure any ID altering has already occured.
 */
function tabs_region_style_preprocess_container(&$variables) {

  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Easy access.
  $region = &$variables['container']['container inside']['row']['row inside'][$variables['region_id']];
  $settings = _tabs_region_style_transform_settings($variables['settings']);

  $css_id = $region['#attributes']['id'];
  $region['#attributes']['class'][] = 'tabs-region-style';

  // Remove gap from header to content.
  // if (!empty($settings['no_gap'])) {
  //   $region['#attributes']['class'][] = 'no-gap';
  // }

  // Load activation script:
  drupal_add_js($theme_path . '/js/tabs-region-style.js');

  // Expose options to JavaScript:
  drupal_add_js(array(
    'tabsRegionStyle' => array(
      '#' . $css_id => $settings,
    ),
  ), 'setting');
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_tabs_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('tabs_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_tabs_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}

/**
 * Configuration form.
 */
function tabs_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();

  // Initial active tab.
  $form['active'] = array(
    '#type' => 'textfield',
    '#title' => t('Active'),
    '#description' => t('Which panel is open on the beginning.'),
    '#default_value' => $conf['active'],
  );

  // Tab style.
  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#options' => array(
      'nav-tabs' => t('Tabs'),
      'nav-pills' => t('Pills'),
    ),
    '#default_value' => $conf['style'],
  );

  // Horizontal justification.
  $form['justify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Justify'),
    '#description' => t('Mark this to horizonatlly justify tabs.'),
    '#default_value' => $conf['justify'],
  );

  // Gap option.
  // $form['no_gap'] = array(
  //   '#type' => 'checkbox',
  //   '#title' => t('Remove gap'),
  //   '#description' => t('Mark this to remove gap between tabs and contents.'),
  //   '#default_value' => $conf['no_gap'],
  // );

  return $form;
}
