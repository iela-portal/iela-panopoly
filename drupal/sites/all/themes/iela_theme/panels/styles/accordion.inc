<?php

/**
 * @file
 * Definition of the Accordion panel region style.
 * This style allows to configure a region to behave as a accordion of panes.
 */

$plugin = array(
  'title' => t('Accordion'),
  'description' => t('Make an accordion with the region\'s panes.'),
  'render region' => 'accordion_region_style',
  'settings form' => 'accordion_region_style_settings_form',
  'multiple style' => array(
    'wrapper' => true,
    'region' => false,
  ),
  'defaults' => array(
    'active' => '1',
    'collapsible' => false,
    'heightStyle' => 'auto',
  ),
);

/**
 * Helper function to transform the settings into jQueryUI options.
 */
function _accordion_region_style_transform_settings($settings) {
  $settings['active'] = empty($settings['active']) ? false : intval($settings['active']) - 1;
  $settings['collapsible'] = filter_var($settings['collapsible'], FILTER_VALIDATE_BOOLEAN);

  return $settings;
}

/**
 * Preprocess wrapping container.
 */
function accordion_region_style_preprocess_container(&$variables) {

  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Easy access.
  $region = &$variables['container']['container inside']['row']['row inside'][$variables['region_id']];
  $settings = _accordion_region_style_transform_settings($variables['settings']);

  $css_id = $region['#attributes']['id'];
  $region['#attributes']['class'][] = 'accordion-region-style';

  // Load activation script:
  drupal_add_js($theme_path . '/js/accordion-region-style.js');

  // Expose options to JavaScript:
  drupal_add_js(array(
    'accordionRegionStyle' => array(
      $css_id => $settings
    ),
  ), 'setting');
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_accordion_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('accordion_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_accordion_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}

/**
 * Configuration form.
 */
function accordion_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();

  // Start activated.
  $form['active'] = array(
    '#type' => 'textfield',
    '#title' => t('Active'),
    '#description' => t('Which panel is open on the beginning.'),
    '#default_value' => $conf['active'],
  );

  // Only one.
  $form['collapsible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsable'),
    '#description' => t('Whether all the panels can be closed at once.'),
    '#default_value' => $conf['collapsible'],
  );

  // Height style.
  $form['heightStyle'] = array(
    '#type' => 'select',
    '#title' => t('Height method'),
    '#description' => t('How the accordion height will be defined.') . '<small>'
      . '<br />' . t('Tallest: all panes will have the height of the tallest one')
      . '<br />' . t('Content: each pane will be only as tall as its content')   
      . '</small>',
    '#options' => array(
      'auto' => t('Tallest'),
      'content' => t('Content'),
    ),
    '#default_value' => $conf['heightStyle'],
  );

  return $form;
}
