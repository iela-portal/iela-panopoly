<?php

/**
 * @file
 * Definition of the 'Main Pane' panel style.
 */

$plugin = array(
  'title' => t('Main pane'),
  'description' => t('Use this identify the most importa pane, augmenting titles and other assets.'),
  'render pane' => 'main_pane_pane_style',
  // 'pane settings form' => 'main_pane_pane_style_settings_form',
  'multiple style' => TRUE,
  // 'type access' => 'main_pane_pane_style_access',
);

/**
 * Template preprocess function for the plugin style.
 */
function template_preprocess_main_pane_pane_style(&$variables) {
  $variables['pane_wrapper_attributes']['class'][] = 'main-pane';
  $variables['title_wrapper'] = array(
    '#type' => 'html_tag',
    '#tag' => 'h3',
  );
}

/**
 * Theme function for the plugin style.
 */
function theme_main_pane_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}
