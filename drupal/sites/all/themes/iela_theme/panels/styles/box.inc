<?php

/**
 * @file
 * Definition of the 'Box' panel style.
 */

$plugin = array(
  'title' => t('Box'),
  'description' => t('Box style'),

  // Region style.
  'render region' => 'box_region_style',
  'settings form' => 'box_region_style_settings_form',

  // Pane style.
  'render pane' => 'box_pane_style',
  'pane settings form' => 'box_pane_style_settings_form',

  'multiple style' => true,
  'defaults' => array(
    'padding' => false,
    'paddings' => array(
      'top' => 'small',
      'right' => 'small',
      'bottom' => 'small',
      'left' => 'small',
    ),
    'full_height' => false,
    'remove_margin_left' => false,
    'remove_margin_right' => false,
    'background' => array(
      'color' => '',
      'reverse' => false
    ),
    'alignment' => 'left',
    'contain_title' => false,
    'title' => array(
      'color' => '',
      'alignment' => 'center',
    ),
  ),
);


// --------------------------------------------------------
// General
// --------------------------------------------------------

/**
 * Basic configuration form.
 */
function box_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Background fieldset:
  $form['background'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background'),
    '#description' => t('Background options'),
    '#collapsible' => true,
    '#collapsed' => false,
  );

  // Load color picker files.
  drupal_add_js($theme_path . '/lib/spectrum/spectrum.js');
  drupal_add_js($theme_path . '/js/color-picker.js');
  drupal_add_css($theme_path . '/lib/spectrum/spectrum.css');

  // Container background color.
  $form['background']['color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color'),
    '#description' => t('Choose the background color of the box'),
    '#attributes' => array('class' => array('color-picker')),
    '#default_value' => $conf['background']['color'],
  );

  // Option to reverse default text.
  $form['background']['reverse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reverse text colors'),
    '#description' => t('If background color is too dark, check this to reverse default text colors'),
    '#default_value' => $conf['background']['reverse'],
  );

  return $form;
}


// --------------------------------------------------------
// Panel Region
// --------------------------------------------------------

/**
 * Region settings form.
 */
function box_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = box_style_settings_form($conf, $display, $pid, $type, $form_state);

  // Full height option.
  $form['full_height'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full height'),
    '#description' => t('Make region as tall as the container'),
    '#default_value' => $conf['full_height'],
  );

  // Margin left option.
  $form['remove_margin_left'] = array(
    '#type' => 'checkbox',
    '#title' => t('Margin left'),
    '#description' => t('Add margin at the box left side'),
    '#default_value' => $conf['remove_margin_left'],
  );

  // Margin right option.
  $form['remove_margin_right'] = array(
    '#type' => 'checkbox',
    '#title' => t('Margin right'),
    '#description' => t('Add margin at the box right side'),
    '#default_value' => $conf['remove_margin_right'],
  );

  return $form;
}

/**
 * Preprocess box region container.
 */
function box_region_style_preprocess_container($variables) {

  // Easy access.
  $outer_row = &$variables['container']['container inside']['row'];
  $outer_row_inside = &$outer_row['row inside'];
  $region = &$outer_row_inside[$variables['region_id']];
  $region_inner = &$region[$variables['region_id'] . ' inner'];
  $settings = $variables['settings'];

  $region['#attributes']['class'][] = 'box-region-style';

  // Set container background color:
  if (!empty($settings['background']['color'])) {
    $region['#attributes']['style'][] = 'background-color: ' . $settings['background']['color'] . ';';
  }

  // Set reverse color option:
  if (!empty($settings['background']['reverse'])) {
    $region_inner['#attributes']['class'][] = 'dark-background';
  }

  // Set full height option:
  if (!empty($settings['full_height'])) {
    $outer_row_inside['#attributes']['style'][] = 'overflow-y: hidden;';
    $region['#attributes']['class'][] = 'full-height-column';
  }

  // Set margin left option:
  if (!empty($settings['remove_margin_left'])) {
    $region_inner['#attributes']['style'][] = 'margin-left: 0;';
  }

  // Set margin right option:
  if (!empty($settings['remove_margin_right'])) {
    $region_inner['#attributes']['style'][] = 'margin-right: 0;';
  }
}

/**
 * Template preprocess function for the region style.
 */
function template_preprocess_box_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('box_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_box_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}


// --------------------------------------------------------
// Panel Pane
// --------------------------------------------------------

/**
 * Template preprocess function for the pane style.
 */
function template_preprocess_box_pane_style(&$variables) {

  // Easy access:
  $conf = &$variables['settings'];

  $variables['pane_wrapper_attributes']['class'][] = 'box-style';

  // Set container background color:
  if (!empty($conf['background']['color'])) {
    $variables['content_attributes_array']['style'][] = 'background-color: ' . $conf['background']['color'] . ';';
  }

  // Set reverse color option:
  if (!empty($conf['background']['reverse'])) {
    $variables['content_attributes_array']['class'][] = 'dark-background';
  }

  // Set padding option:
  if (!empty($conf['padding'])) {
    foreach (array('top', 'right', 'bottom', 'left') as $side) {
      $variables['content_attributes_array']['class'][] = implode('-', array(
        'padding',
        $side,
        empty($conf['paddings'][$side]) ? 'small' : $conf['paddings'][$side],
      ));
    }
  }

  // Set text alignment.
  if (!empty($conf['alignment'])) {
    $variables['content_attributes_array']['class'][] = 'text-' . $conf['alignment'];
  }

  // Set title options.
  if (!empty($conf['contain_title'])) {
    $variables['title_attributes_array']['class'][] = 'boxed-title';
    $variables['title_attributes_array']['class'][] = 'text-' . $conf['title']['alignment'];
    
    // Set title background color:
    if (!empty($conf['title']['color'])) {
      $variables['title_attributes_array']['style'][] = 'background-color: ' . $conf['title']['color'] . ';';
    }
  }
}

/**
 * Theme function for the plugin style.
 */
function theme_box_pane_style($variables) {
  return theme('panels_pane', $variables);
}

/**
 * Pane settings form.
 */
function box_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = box_style_settings_form($conf, $display, $pid, $type, $form_state);
  $multiple = !empty($form_state['style']['name']) && $form_state['style']['name'] == 'multiple';
  $base_input_selector = $multiple ? ':input[name="settings[box]' : ':input[name="settings';

  // Text align.
  $form['alignment'] = array(
    '#type' => 'select',
    '#title' => t('Text alignment'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
      'justify' => t('Justify'),
    ),
    '#default_value' => $conf['alignment'],
    '#weight' => -10,
  );

  // Box contain title.
  $form['contain_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Boxed title'),
    '#description' => t('Mark this to include pane title in the box'),
    '#default_value' => $conf['contain_title'],
  );

  // Title color.
  $form['title']['color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Choose the background color of the title'),
    '#attributes' => array('class' => array('color-picker')),
    '#default_value' => $conf['title']['color'],
    '#states' => array(
      'visible' => array(
        $base_input_selector . '[contain_title]"]' => array('checked' => true),
      ),
    ),
  );

  // Title alignment.
  $form['title']['alignment'] = array(
    '#type' => 'select',
    '#title' => t('Title alignment'),
    '#description' => t('Choose the alignment of the title horizontally.'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#default_value' => $conf['title']['alignment'],
    '#states' => array(
      'visible' => array(
        $base_input_selector . '[contain_title]"]' => array('checked' => true),
      ),
    ),
  );

  // Padding option.
  $form['padding'] = array(
    '#type' => 'checkbox',
    '#title' => t('Padding'),
    '#description' => t('Add space from box content to borders'),
    '#default_value' => $conf['padding'],
  );

  $sides = array(
    'top' => t('Top'),
    'right' => t('Right'),
    'bottom' => t('Bottom'),
    'left' => t('Left'),
  );

  foreach ($sides as $side => $title) {
    $form['paddings'][$side] = array(
      '#type' => 'select',
      '#title' => $title,
      '#default_value' => empty($conf['paddings'][$side]) ? 'small' : $conf['paddings'][$side],
      '#options' => array(
        'none' => t('None'),
        'small' => t('Small'),
        'medium' => t('Medium'),
        'big' => t('Big'),
      ),
      '#states' => array(
        'visible' => array(
          $base_input_selector . '[padding]"]' => array('checked' => true),
        ),
      ),
    );
  }

  return $form;
}
