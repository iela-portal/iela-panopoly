<?php

/**
 * @file
 * Definition of the Views Accordion panel style.
 * This style allows to configure a view to behave as a accordion of row groups.
 */

$plugin = array(
  'title' => t('Views Accordion'),
  'description' => t('Make an accordion with the view\'s grouped results.'),
  'render pane' => 'views_accordion_pane_style',
  'pane settings form' => 'views_accordion_pane_style_settings_form',
  'multiple style' => true,
  'weight' => 0,
  'type access' => 'views_accordion_pane_style_access',
  'defaults' => array(
    'active' => '1',
    'collapsible' => false,
    'heightStyle' => 'auto',
  ),
);

/**
 * Helper function to transform the settings into jQueryUI options.
 */
function _views_accordion_pane_style_transform_settings($settings) {
  $settings['active'] = empty($settings['active']) ? false : intval($settings['active']) - 1;
  $settings['collapsible'] = filter_var($settings['collapsible'], FILTER_VALIDATE_BOOLEAN);

  return $settings;
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_views_accordion_pane_style(&$variables) {

  $theme_path = drupal_get_path('theme', 'iela_theme');
  $settings = _views_accordion_pane_style_transform_settings($variables['settings']);

  $variables['pane_wrapper_attributes']['class'][] = 'views-accordion-pane-style';

  // Make sure pane wrapper id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  // Load activation script:
  drupal_add_js($theme_path . '/js/views-accordion-pane-style.js');

  // Expose options to JavaScript:
  drupal_add_js(array(
    'viewsAccordionPaneStyle' => array(
      '#' . $css_id => $settings
    ),
  ), 'setting');
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_views_accordion_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Configuration form.
 */
function views_accordion_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();

  // Start activated.
  $form['active'] = array(
    '#type' => 'textfield',
    '#title' => t('Active'),
    '#description' => t('Which panel is open on the beginning.'),
    '#default_value' => $conf['active'],
  );

  // Only one.
  $form['collapsible'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsable'),
    '#description' => t('Whether all the panels can be closed at once.'),
    '#default_value' => $conf['collapsible'],
  );

  // Height style.
  $form['heightStyle'] = array(
    '#type' => 'select',
    '#title' => t('Height method'),
    '#description' => t('How the accordion height will be defined.') . '<small>'
      . '<br />' . t('Tallest: all panes will have the height of the tallest one')
      . '<br />' . t('Content: each pane will be only as tall as its content')   
      . '</small>',
    '#options' => array(
      'auto' => t('Tallest'),
      'content' => t('Content'),
    ),
    '#default_value' => $conf['heightStyle'],
  );

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function views_accordion_pane_style_access(&$pane, $subtype) {

  // Early return.
  if ($pane->type != 'views_panes') return false;

  list($name, $display) = explode('-', $pane->subtype);

  $view = views_get_view($name);
  
  // Early return.
  if (empty($view) || !isset($view->display[$display])) return false;

  return !empty($view->display[$display]->display_options['style_options']['grouping']);
}
