<?php

/**
 * @file
 * Definition of the 'Secondary Content' style.
 */

$plugin = array(
  'title' => t('Secondary content'),
  'description' => t('Mark pane/region as secondary content.'),

  // Region style.
  'render region' => 'secondary_content_region_style',

  // Pane style.
  'render pane' => 'secondary_content_pane_style',

  'multiple style' => true,
);


// --------------------------------------------------------
// Panel Region
// --------------------------------------------------------

/**
 * Preprocess secondary content region container.
 */
function secondary_content_region_style_preprocess_container(&$variables) {

  // Easy access.
  $outer_row = &$variables['container']['container inside']['row'];
  $outer_row_inside = &$outer_row['row inside'];
  $region = &$outer_row_inside[$variables['region_id']];

  $region['#attributes']['class'][] = 'secondary-content';
  $region['#attributes']['class'][] = 'secondary-content-pane-style';
}

/**
 * Template preprocess function for the region style.
 */
function template_preprocess_secondary_content_region_style(&$variables) {
    _iela_panels_region_style_add_container_preprocess('secondary_content_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_secondary_content_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}


// --------------------------------------------------------
// Panel Pane
// --------------------------------------------------------

/**
 * Template preprocess function for the pane style.
 */
function template_preprocess_secondary_content_pane_style(&$variables) {
  $variables['pane_wrapper_attributes']['class'][] = 'secondary-content';
  $variables['pane_wrapper_attributes']['class'][] = 'secondary-content-pane-style';
}

/**
 * Theme function for the plugin style.
 */
function theme_secondary_content_pane_style($variables) {
  return theme('panels_pane', $variables);
}
