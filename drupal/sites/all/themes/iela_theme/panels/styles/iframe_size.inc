<?php

/**
 * @file
 * Definition of the 'Iframe Size' panel style.
 * This style is only used with fieldable panes that use iframe, like YouTube
 * videos or Google Maps.
 */

$plugin = array(
  'title' => t('iFrame Size'),
  'description' => t('Use this to define the size of framed content such as YouTube videos or Google Maps.'),
  'render pane' => 'iframe_size_pane_style',
  'pane settings form' => 'iframe_size_pane_style_settings_form',
  'multiple style' => true,
  'type access' => 'iframe_size_pane_style_access',
  'defaults' => array(
    'width' => '100%',
    'height' => '0.6',
    'update_src' => false,
    'src_width' => 'width',
    'src_height' => 'height',
  ),
);

/**
 * Template preprocess function for the plugin style.
 */
function template_preprocess_iframe_size_pane_style(&$variables) {

  $conf = $variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');
  $variables['pane_wrapper_attributes']['class'][] = 'iframe-size-pane-style';

  // Make sure pane wrapper id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  if (empty($conf['width'])) $conf['width'] = '100%';
  if (empty($conf['height'])) $conf['height'] = '0.6';

  // Fullfill non-overriden.
  foreach ($conf['breakpoints'] as &$breakpoint) {
    if (empty($breakpoint['width'])) $breakpoint['width'] = $conf['width'];
    if (empty($breakpoint['height'])) $breakpoint['height'] = $conf['height'];
  }

  // Load activation script:
  drupal_add_js($theme_path . '/js/iframe-size-region-style.js');

  // Expose options to JavaScript:
  drupal_add_js(array(
    'iFrameSizeRegionStyle' => array(
      '#' . $css_id => $conf,
    ),
  ), 'setting');
}

/**
 * Theme function for the plugin style.
 */
function theme_iframe_size_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Create form the plugin.
 */
function iframe_size_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $multiple = !empty($form_state['style']['name']) && $form_state['style']['name'] == 'multiple';
  $base_input_selector = $multiple ? ':input[name="settings[iframe-size]' : ':input[name="settings';

  $form = array(
    'width' => array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#description' => t('Insert the width in any CSS allowed format.'),
      '#default_value' => $conf['width']
    ),
    'height' => array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#description' => t('Insert the height in any CSS allowed format.'),
      '#default_value' => $conf['height']
    ),
    'update_src' => array(
      '#type' => 'checkbox',
      '#title' => t('Update iFrame src'),
      '#description' => t('Mark this to try to dynamically update iFrame\'s src attribute to replace height and width.'),
      '#default_value' => $conf['update_src'],
    ),
    'src_width' => array(
      '#type' => 'textfield',
      '#title' => t('Width attribute'),
      '#description' => t('Identify the width attribute of the iFrame'),
      '#default_value' => $conf['src_width'],
      '#states' => array(
        'visible' => array(
          $base_input_selector . '[update_src]"]' => array('checked' => true),
        ),
      ),
    ),
    'src_height' => array(
      '#type' => 'textfield',
      '#title' => t('Height attribute'),
      '#description' => t('Identify the height attribute of the iFrame'),
      '#default_value' => $conf['src_height'],
      '#states' => array(
        'visible' => array(
          $base_input_selector . '[update_src]"]' => array('checked' => true),
        ),
      ),
    ),
  );

  $breakpoints = breakpoints_breakpoint_load_all_theme();

  if (!empty($breakpoints)) {
    $form['breakpoints'] = array(
      '#type' => 'fieldset',
      '#title' => t('Breakpoint specific settings'),
      '#description' => t('Fullfill the options below to override settings based on the breakpoint.'),
    );

    // FontAwesome icon classes.
    $icons = array(
      'xs' => 'mobile',
      'sm' => 'tablet',
      'md' => 'laptop',
      'lg' => 'desktop',
    );

    // Loop through each breakpoint to construct settings.
    foreach ($breakpoints as $machine_name => $breakpoint) {
      $form['breakpoints'][$machine_name] = array(
        '#type' => 'fieldset',
        '#title' => $breakpoint->name . ' ' . t('breakpoint'),
        '#collapsible' => true,
        '#collapsed' => true,
        'width' => array(
          '#type' => 'textfield',
          '#title' => t('Width'),
          '#description' => t('Insert the width in any CSS allowed format.'),
          '#default_value' => empty($conf['breakpoints'][$machine_name]['width']) ? '' : $conf['breakpoints'][$machine_name]['width'],
        ),
        'height' => array(
          '#type' => 'textfield',
          '#title' => t('Height'),
          '#description' => t('Insert the height in any CSS allowed format.'),
          '#default_value' => empty($conf['breakpoints'][$machine_name]['height']) ? '' : $conf['breakpoints'][$machine_name]['height'],
        ),
      );

      if (!empty($icons[$breakpoint->name])) {
        $form['breakpoints'][$machine_name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
      }
    }
  }

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function iframe_size_pane_style_access(&$pane, $subtype) {
  return $pane->type == 'fieldable_panels_pane' && in_array($subtype->bundle, array(
    'map',
    'video',
    'embed',
  ));
}
