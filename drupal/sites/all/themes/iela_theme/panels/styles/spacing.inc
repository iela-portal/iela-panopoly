<?php

/**
 * @file
 * Definition of the Spacing panel region style.
 * This style allows to set spacing related configuratin of a region wrapper.
 */

$plugin = array(
  'title' => t('Container Spacing'),
  'description' => t('Configure spacing options for the container.'),
  'render region' => 'spacing_region_style',
  'settings form' => 'spacing_region_style_settings_form',
  'multiple style' => array(
    'wrapper' => true,
    'region' => false,
  ),
  'affects container' => true,
  'weight' => -10,
  'defaults' => array(
    'fluid' => false,
    'tighten_panes' => false,
    'padding' => array(
      'top' => 'small',
      'right' => 'default',
      'left' => 'default',
      'bottom' => 'none',
    ),
    'margin' => array(
      'top' => 'none',
      'right' => 'none',
      'left' => 'none',
      'bottom' => 'none',
    ),
    'divisor' => array(
      'top' => array(
        'type' => '',
        'extended' => false,
        'thickness' => 'thin',
        'color' => '',
      ),
      'bottom' => array(
        'type' => '',
        'extended' => false,
        'thickness' => 'thin',
        'color' => '',
      ),
    ),
  ),
);

/**
 * Preprocess wrapping container.
 */
function spacing_region_style_preprocess_container(&$variables) {

  $settings = &$variables['settings'];
  $container = &$variables['container'];
  $container_inner = &$container['container inside'];
  $region = &$container_inner['row']['row inside'][$variables['region_id']];

  // Set fluidness if chosen.
  if (!empty($settings['fluid'])) {
    // Remove 'container' class.
    if (($key = array_search('container', $container_inner['#attributes']['class'])) !== false) {
      unset($container_inner['#attributes']['class'][$key]);
    }

    $container_inner['#attributes']['class'][] = 'container-fluid';
  }

  // Stacked option.
  if (!empty($settings['tighten_panes'])) {
    $region['#attributes']['class'][] = 'tighten-panes';
  }

  // Add padding and margin styles:
  foreach (array('margin', 'padding') as $type) {
    foreach (array('top','right','bottom','left') as $side) {
      if (!empty($settings[$type][$side])) {

        // Handle side padding to attach to inner to outer.
        $subject = &$container;
        if ($type == 'padding') {
          if ($settings[$type][$side] == 'none'  && in_array($side, array('right', 'left'))) continue;
          $subject = &$container_inner;
        }

        $subject['#attributes']['class'][] = implode('-', array(
          $type,
          $side,
          $settings[$type][$side],
        ));
      }
    }
  }

  // Add border styling:
  foreach (array('top', 'bottom') as $side) {
    $side_settings = $settings['divisor'][$side];
    if (!empty($side_settings['type'])) {
      if (!empty($side_settings['extended'])) {
        $placement = &$container;
      } else {
        $placement = &$container_inner;
      }

      $base_class_name = 'divisor-' . $side . '-';

      $placement['#attributes']['class'][] = 'divisor-' . $side;
      $placement['#attributes']['class'][] = $base_class_name . $side_settings['type'];
      $placement['#attributes']['class'][] = $base_class_name . $side_settings['thickness'];

      if (!empty($side_settings['color'])) {
        $placement['#attributes']['style'][] = 'border-color:' . $side_settings['color'] . ';';
      }
    }
  }
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_spacing_region_style(&$variables) {

  // Avoid undefined.
  if (!isset($variables['display']->container_preprocess)) {
    $variables['display']->container_preprocess = array();
  }

  // Save preprocessing function.
  $variables['display']->container_preprocess[$variables['region_id']]['spacing_region_style_preprocess_container'] = $variables['settings'];
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_spacing_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}

/**
 * Configuration form.
 */
function spacing_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $theme_path = drupal_get_path('theme', 'iela_theme');
  $multiple = !empty($form_state['style']['name']) && $form_state['style']['name'] == 'multiple';

  $base_input_selector = $multiple ? ':input[name="settings[spacing]' : ':input[name="settings';

  // Option to make container fluid.
  $form['fluid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fluid container'),
    '#description' => t('Make the contents occupy all the width of the window'),
    '#default_value' => $conf['fluid'],
  );

  // Option to make all region's panes tighten.
  $form['tighten_panes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tighten panes'),
    '#description' => t('Remove spacing between panes'),
    '#default_value' => $conf['tighten_panes'],
  );

  // Padding option.
  $form['padding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Padding'),
    '#description' => t('Space inside container'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#prefix' => '<div class="row"><div class="col-md-6">',
    '#suffix' => '</div>',
  );

  // Margin option.
  $form['margin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Margin'),
    '#description' => t('Space ouside container'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div></div>',
  );

  $sides = array(
    'top' => t('Top'),
    'right' => t('Right'),
    'bottom' => t('Bottom'),
    'left' => t('Left'),
  );

  // Top/Right/Bottom/Left padding options.
  foreach ($sides as $side => $title) {
    foreach (array('padding', 'margin') as $type) {
      $form[$type][$side] = array(
        '#type' => 'select',
        '#title' => $title,
        '#default_value' => $conf[$type][$side],
      );

      // Container padding show follow bootstrap defaults.s
      if ($type == 'padding' && in_array($side, array('right', 'left'))) {
        $form[$type][$side]['#options'] = array(
          'minor' => t('Minor'),
          'default' => t('Small'),
          'medium' => t('Medium'),
          'big' => t('Big'),
          'large' => t('Large'),
          'huge' => t('Huge'),
        );
      } else {
        $form[$type][$side]['#options'] = array(
          'none' => t('None'),
          'minor' => t('Minor'),
          'small' => t('Small'),
          'medium' => t('Medium'),
          'big' => t('Big'),
          'large' => t('Large'),
          'huge' => t('Huge'),
        );
      }
    }
  }

  // Divisor options.
  $form['divisor'] = array(
    '#type' => 'fieldset',
    '#title' => t('Borders'),
    '#collapsible' => true,
    '#collapsed' => true,
  );

  // Top/Bottom divisor options.
  foreach (array('top' => t('Top'), 'bottom' => t('Bottom')) as $side => $title) {
    $form['divisor'][$side] = array(
      '#type' => 'fieldset',
      '#title' => $title,
      '#collapsible' => true,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      'type' => array(
        '#type' => 'select',
        '#title' => t('Type'),
        '#options' => array(
          '' => t('None'),
          'solid' => t('Solid'),
          'dashed' => t('Dashed'),
          'dotted' => t('Dotted'),
        ),
        '#default_value' => $conf['divisor'][$side]['type'],
      ),
      'extended' => array(
        '#type' => 'checkbox',
        '#title' => t('Extended'),
        '#description' => t('Make the divisor span to the full window width.'),
        '#default_value' => $conf['divisor'][$side]['extended'],
        '#states' => array(
          'invisible' => array(
            ':input[name="settings[spacing][divisor][' . $side . '][type]"]' => array('value' => ''),
          ),
        ),
      ),
      'thickness' => array(
        '#type' => 'select',
        '#title' => t('Thickness'),
        '#options' => array(
          'thin' => t('Thin'),
          'medium' => t('Medium'),
          'thick' => t('Thick'),
        ),
        '#default_value' => $conf['divisor'][$side]['thickness'],
        '#states' => array(
          'invisible' => array(
            ':input[name="settings[spacing][divisor][' . $side . '][type]"]' => array('value' => ''),
          ),
        ),
      ),
      'color' => array(
        '#type' => 'textfield',
        '#title' => t('Color'),
        '#description' => t('Leave it empty to use default color'),
        '#default_value' => $conf['divisor'][$side]['color'],
        '#states' => array(
          'invisible' => array(
            ':input[name="settings[spacing][divisor][' . $side . '][type]"]' => array('value' => ''),
          ),
        ),
        '#attributes' => array('class' => array('color-picker')),
        '#attached' => array(
          'js' => array(
            $theme_path . '/lib/spectrum/spectrum.js',
            $theme_path . '/js/color-picker.js'
          ),
          'css' => array(
            $theme_path . '/lib/spectrum/spectrum.css',
          ),
        ),
      ),
    );

    // Open fieldset when there are options selected.
    if (!empty($conf['divisor'][$side]['type'])) {
      $form['divisor']['#collapsed'] = false;
    }
  }

  return $form;
}
