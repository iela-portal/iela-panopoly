<?php

/**
 * @file
 * Definition of the 'Menu' panel style.
 * This style is only used with menu panes or with the links fieldable pane.
 */

$plugin = array(
  'title' => t('Menu Options'),
  'description' => t('Menu presentation options.'),
  'render pane' => 'menu_pane_style',
  'pane settings form' => 'menu_pane_style_settings_form',
  'multiple style' => false,
  'type access' => 'menu_pane_style_access',
  'defaults' => array(
    'style' => 'pills',
    'orientation' => 'vertical',
    'justified' => false,
    'categorized' => false,
    'categories' => array(
      'columns' => array(
        'xs' => 12,
        'sm' => 6,
        'md' => 4,
        'lg' => 4,
      ),
      'masonry' => true,
    ),
    'navbar' => array(
      'type' => 'default',
      'align' => 'nav',
      'stick' => false,
      'sticky_alone' => false,
      'sticky_weight' => '0',
    ),
  ),
);

/**
 * Template preprocess function for the plugin style.
 */
function template_preprocess_menu_pane_style(&$variables) {

  module_load_include('inc', 'iela', 'includes/iela.functions');

  $configuration = $variables['pane']->configuration;
  $settings = $variables['settings'];
  $content = &$variables['content']->content;
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Make sure pane id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  if (!$variables['pane']->type == 'menu_tree') {
    // @todo Transform non-menu links list into the common menu structure.
  }

  $variables['pane_wrapper_attributes']['class'][] = 'menu-style-' . $settings['style'];

  // Default theme wrapper.
  $theme = 'menu_tree';

  switch ($settings['style']) {
    case 'pills':
      // Radix theme:
      $theme .= '__nav_pills';
      if ($settings['orientation'] == 'vertical') $theme .= '_stacked';
      elseif (!empty($settings['justified'])) $theme .= '_justified';
      array_unshift($content['#content']['#theme_wrappers'][0], $theme);
      break;

    case 'tabs':
      // Radix theme:
      $theme .= '__nav_tabs';
      if (!empty($settings['justified'])) $theme .= '_justified';
      array_unshift($content['#content']['#theme_wrappers'][0], $theme);
      break;

    case 'links':
      $theme .= '__links';
      array_unshift($content['#content']['#theme_wrappers'][0], $theme);

      // Update all menu items.
      iela_walk_menu_items($content['#content'], function (&$item) use ($theme) {
        array_unshift($item['#theme'], 'menu_link__link');
        if (!empty($item['#below'])) array_unshift($item['#below']['#theme_wrappers'][0], $theme);
      });

      if (!empty($settings['categorized'])) {
        $content['#content']['#block_wrapper_classes_array'][] = 'row';
        foreach ($content['#content'] as $uuid => &$item) {
          if (strpos($uuid, '#') === 0) continue;
          foreach ($settings['categories']['columns'] as $breakpoint => $columns) {
            $item['#attributes']['class'][] = 'col-' . $breakpoint . '-' . $columns;
          }
        }

        if (!empty($settings['categories']['masonry'])) {
          libraries_load('masonry');

          // Load activation script:
          drupal_add_js($theme_path . '/js/iela-masonry.js', array(
            'weight' => 100,
          ));

          // Expose options to JavaScript:
          drupal_add_js(array(
            'ielaMasonry' => array(
              '#' . $css_id => array(
                'containerSelector' => '.menu-block-wrapper',
                'itemSelector' => '.nav-links > li'
              ),
            ),
          ), 'setting');
        }
      }
      break;

    case 'navbar':

      // Radix theme:
      $theme .= '__navbar_' . $settings['navbar']['align'];
      array_unshift($content['#content']['#theme_wrappers'][0], $theme);

      // Grabe reference for renderable menu.
      $menu = $content['#content'];

      // Replace default renderables.
      $content = array(
        '#theme' => 'bootstrap_navbar',
        'content' => array(
          'menu' => array(
            '#type' => 'container',
            'content' => $menu,
          ),
        ),
        'type' => $settings['navbar']['type'],
      );

      // Handle middle alignment.
      if ($settings['navbar']['align'] == 'nav') {
        $content['content']['menu']['#attributes']['class'][] = 'navbar-nav-middle';
      }

      // Handle sticky.
      if (!empty($settings['navbar']['sticky'])) {
        drupal_add_js(array(
          'ielaStickyNavbar' => array(
            '#' . $css_id => array(
              'weight' => $settings['navbar']['sticky_weight'],
              'stopMain' => $settings['navbar']['sticky_alone'],
            ),
          ),
        ), 'setting');
      }

      if (!empty($variables['content']->title)) {
        if (!empty($configuration['link']) && !empty($configuration['path'])) {
          $content['navbar']['container']['header']['title'] = array(
            '#markup' => l($variables['content']->title, $configuration['path'], array(
              'attributes' => array('class' => array('navbar-brand')),
              'html' => true,
            )),
          );
        } else {
          $content['navbar']['container']['header']['title'] = array(
            '#markup' => '<span class="navbar-brand">' . $variables['content']->title . '</span>',
          );
        }

        unset($variables['content']->title);
      }
      break;
    
    case 'dropdown':
      $theme .= '__dropdown';
      array_unshift($content['#content']['#theme_wrappers'][0], $theme);
      break;
  }
}

/**
 * Theme function for the plugin style.
 */
function theme_menu_pane_style($variables) {
  return theme('panels_pane', $variables);
}

/**
 * Create form the plugin.
 */
function menu_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $theme_path = drupal_get_path('theme', 'iela_theme');
  $breakpoints = iela_get_breakpoints_theme_tree();

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  $form['#attached'] = array(
    'js' => array(
      $theme_path . '/js/menu-style-form.js',
    ),
  );

  $style_description  = t('Choose the menu style type.') . '<br>';
  $style_description .= '<small>';
  $style_description .= t('Select "Navbar dropdown" to use in mini-panel menus.');
  $style_description .= '</small>';

  // Menu style (pills/tabs)
  $form['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => $style_description,
    '#default_value' => $conf['style'],
    '#options' => array(
      'pills' => t('Pills'),
      'tabs' => t('Tabs'),
      'links' => t('Links'),
      'navbar' => t('Navbar'),
      'dropdown' => t('Navbar submenu'),
    ),
  );

  // Orientation option (horizontal/vertical)
  $form['orientation'] = array(
    '#type' => 'select',
    '#title' => t('Orientation'),
    '#description' => t('Choose the menu button distribution direction'),
    '#options' => array(
      'vertical' => t('Vertical'),
      'horizontal' => t('Horizontal'),
    ),
    '#default_value' => $conf['orientation'],
  );

  // Allow usage of justified button variant.
  $form['justified'] = array(
    '#type' => 'checkbox',
    '#title' => t('Justify buttons'),
    '#description' => t('Make the group of buttons stretch at equal sizes to span the entire width of its parent.'),
    '#default_value' => $conf['justified'],
  );

  // Categorized
  $form['categorized'] = array(
    '#type' => 'checkbox',
    '#title' => t('Categorized sub-items'),
    '#description' => t('Make the root links be shown as category blocks for sub-items.'),
    '#default_value' => $conf['categorized'],
  );

  // Categories options.
  $form['categories'] = array(
    '#type' => 'fieldset',
    '#title' => t('Categories options'),
  );

  // Categories columns.
  $form['categories']['columns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Columns used by each category'),
  );

  // Categories columns
  foreach ($breakpoints as $machine_name => $breakpoint) {
    $form['categories']['columns'][$breakpoint->name] = array(
      '#type' => 'select',
      '#title' => "{$breakpoint->name} breakpoint",
      '#descripion' => "{$breakpoint->breakpoint}",
      '#options' => array(),
      '#default_value' => $conf['categories']['columns'][$breakpoint->name],
      '#wrapper_attributes' => array('class' => array(
        'inline-form-item',
        'views-grid-breakpoint-label'
      )),
      '#label_attributes' => array('style' => array('width: 50%;')),
    );

    if (!empty($icons[$breakpoint->name])) {
      $form['categories']['columns'][$breakpoint->name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    // Add options.
    for($i = 1; $i <= 12; $i++) {
      $form['categories']['columns'][$breakpoint->name]['#options'][$i] = format_plural($i, '1 column per item', '@count columns per item');
    }
  }

  // Simple break.
  $form['categories']['break_1'] = array(
    '#type' => 'html_tag',
    '#tag' => 'hr',
  );

  // Use masonry plugin.
  $form['categories']['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['categories']['masonry'],
  );

  // Navbar options.
  $form['navbar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Navbar options'),
  );

  // Allow definition of navbar type.
  $form['navbar']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#default_value' => $conf['navbar']['type'],
    '#options' => array(
      'light' => t('Light'),
      'info' => t('Blue'),
      'default' => t('Green'),
      'danger' => t('Red'),
      'warning' => t('Yellow'),
      'dark' => t('Dark'),
      'info-dark' => t('Dark blue'),
      'default-dark' => t('Dark green'),
      'danger-dark' => t('Dark red'),
      'warning-dark' => t('Dark yellow'),
    ),
  );

  // Allow definition of menu position.
  $form['navbar']['align'] = array(
    '#type' => 'select',
    '#title' => t('Alignment'),
    '#default_value' => $conf['navbar']['align'],
    '#options' => array(
      'left' => t('Left'),
      'nav' => t('Middle'),
      'right' => t('Right'),
    ),
  );

  // Allow sticky navbar option.
  $form['navbar']['sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sticky'),
    '#description' => t('Make this navbar stick at the top after scrolling bellow.'),
    '#default_value' => $conf['navbar']['sticky'],
  );

  // Allow sticky replacement of main navbar.
  $form['navbar']['sticky_alone'] = array(
    '#type' => 'checkbox',
    '#title' => t('Stop main navbar'),
    '#description' => t('Make the main navbar stop sticking while this one is sticking.'),
    '#default_value' => $conf['navbar']['sticky_alone'],
  );

  // Allow sticky weight definition.
  $form['navbar']['sticky_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#description' => t('Lower weight means it will stick above others.') . '<br />'
      . '<small>' . t('Main navbar has a weight of -10.') . '</small>',
    '#default_value' => $conf['navbar']['sticky_weight'],
  );

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function menu_pane_style_access(&$pane, $subtype) {
  return $pane->type == 'menu_tree' ||
    ($pane->type == 'fieldable_panels_pane' && $subtype->bundle == 'quick_links');
}
