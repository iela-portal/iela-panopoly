<?php

/**
 * @file
 * Definition of the Views Grid panel style.
 * This style allows to configure a view to be presented as a grid.
 */

module_load_include('inc', 'iela', 'includes/iela.functions');

$plugin = array(
  'title' => t('Views Grid'),
  'description' => t('Make a grid with the view\'s results.'),
  'render pane' => 'views_grid_pane_style',
  'pane settings form' => 'views_grid_pane_style_settings_form',
  'multiple style' => true,
  'weight' => 0,
  'type access' => 'views_grid_pane_style_access',
  'defaults' => array(
    'columns' => array(
      'xs' => 12,
      'sm' => 6,
      'md' => 4,
      'lg' => 3,
    ),
    'align' => 'left',
    'same_height' => false,
    'masonry' => false,
    'margin' => array(
      'bottom' => 'medium',
    ),
  ),
);

/**
 * Template preprocess for the region style.
 */
function template_preprocess_views_grid_pane_style(&$variables) {

  $settings = $variables['settings'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Make sure pane wrapper id is set.
  $css_id = iela_fulfil_pane_wrapper_id($variables);

  $variables['pane_wrapper_attributes']['class'][] = 'grid-view';

  foreach ($settings['columns'] as $breakpoint => $amount) {
    $variables['pane_wrapper_attributes']['class'][] = 'grid-view-' . $breakpoint . '-' . $amount;
  }

  if (!empty($settings['align'])) {
    $variables['content_attributes_array']['class'][] = 'text-' . $settings['align'];
  }

  if (!empty($settings['same_height'])) {

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-same-height.js');

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaSameHeight' => array(
        '#' . $css_id => '.views-row > article',
      ),
    ), 'setting');
  }

  if (!empty($settings['masonry'])) {
    libraries_load('masonry');

    // Load activation script:
    drupal_add_js($theme_path . '/js/iela-masonry.js', array(
      'weight' => 100,
    ));

    // Expose options to JavaScript:
    drupal_add_js(array(
      'ielaMasonry' => array(
        '#' . $css_id => array(
          'containerSelector' => '.view-content',
          'itemSelector' => '.views-row'
        ),
      ),
    ), 'setting');
  }

  if (!empty($settings['margin']['bottom'])) {
    $variables['content_attributes_array']['class'][] = $settings['margin']['bottom'] . '-row-margin-bottom';
  }
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_views_grid_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}

/**
 * Configuration form.
 */
function views_grid_pane_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  $form = array();
  $breakpoints = iela_get_breakpoints_theme_tree();

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  foreach ($breakpoints as $machine_name => $breakpoint) {
    $form['columns'][$breakpoint->name] = array(
      '#type' => 'select',
      '#title' => "{$breakpoint->name} breakpoint",
      '#descripion' => "{$breakpoint->breakpoint}",
      '#options' => array(),
      '#default_value' => $conf['columns'][$breakpoint->name],
      '#wrapper_attributes' => array('class' => array(
        'inline-form-item',
        'views-grid-breakpoint-label'
      )),
      '#label_attributes' => array('style' => array('width: 50%;')),
    );

    if (!empty($icons[$breakpoint->name])) {
      $form['columns'][$breakpoint->name]['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    // Add options.
    for($i = 1; $i <= 12; $i++) {
      $form['columns'][$breakpoint->name]['#options'][$i] = format_plural($i, '1 column per item', '@count columns per item');
    }
  }

  // Simple break.
  $form['break_1'] = array(
    '#type' => 'html_tag',
    '#tag' => 'hr',
  );

  // Keep same height amoung results.
  $form['align'] = array(
    '#type' => 'select',
    '#title' => t('Alignment'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#default_value' => $conf['align'],
  );

  // Keep same height amoung results.
  $form['same_height'] = array(
    '#type' => 'checkbox',
    '#title' => t('Same height'),
    '#default_value' => $conf['same_height'],
  );

  // Use masonry plugin.
  $form['masonry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Masonry'),
    '#default_value' => $conf['masonry'],
  );

  // Vertical distance between elements.
  $form['margin']['bottom'] = array(
    '#type' => 'select',
    '#title' => t('Vertical gap'),
    '#description' => t('Size of the distance between rows.'),
    '#options' => array(
      'none' => t('None'),
      'small' => t('Small'),
      'medium' => t('Medium'),
      'big' => t('Big'),
    ),
    '#default_value' => empty($conf['margin']['bottom']) ? 'medium' : $conf['margin']['bottom'],
  );

  return $form;
}

/**
 * Defines access to this style based on pane.
 */
function views_grid_pane_style_access(&$pane, $subtype) {
  return $pane->type == 'views_panes';
}
