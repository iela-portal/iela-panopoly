<?php
/**
 * @file
 * Contains the theme's hook implementations.
 */

define('IELA_THEME_PATH', drupal_get_path('theme', 'iela_theme'));

include_once 'includes/functions.php';
include_once 'includes/alter.php';
include_once 'includes/preprocess.php';
// include_once 'includes/process.php';
include_once 'includes/theme.php';
// include_once 'includes/callback.php';
