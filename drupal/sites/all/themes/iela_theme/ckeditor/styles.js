/**
 * @file
 * Registers styles for CKEditor.
 */

var styles = [

  // Titles:
  { name: 'Subtítulo',   element: 'h2' }
, { name: 'Subtítulo 2', element: 'h3' }
, { name: 'Subtítulo 3', element: 'h4' }
, { name: 'Subtítulo 4', element: 'h5' }
, { name: 'Subtítulo 5', element: 'h6' }

  // Quotation:
, { name: 'Rodapé de citação', element: 'footer' }
, { name: 'Autoria de citação', element: 'cite' }

  // Alignment:
, { name: 'Esquerda', element: ['div', 'img', 'blockquote'], attributes: { 'class': 'pull-left' } }
, { name: 'Direita', element: ['div', 'img', 'blockquote'], attributes: { 'class': 'pull-right' } }

  // Table:
, { name : 'Alinhar no topo', element: ['th', 'td'], styles: { 'vertical-align': 'top' } }
, { name : 'Alinhar no meio', element: ['th', 'td'], styles: { 'vertical-align': 'middle' } }
, { name : 'Alinhar no fundo ', element: ['th', 'td'], styles: { 'vertical-align': 'bottom' } }

  // Text:
, { name: 'Letra capitular', element: 'span', attributes: { 'class': 'capitalize' } }

  // Link:
, { name: 'Botão', element: ['a'], attributes: { 'class': 'btn btn-success' } }
, { name: 'Botão Longo', element: ['a'], attributes: { 'class': 'btn btn-success btn-block' } }

  // Disabled:
  // { name : 'Legenda', element : 'span', attributes : { 'class' : 'ckeditor-legend' } },
  // { name : 'Texto escuro', element : 'span', attributes : { 'class' : 'ckeditor-dark-text' } },
  // { name : 'Texto claro', element : 'span', attributes : { 'class' : 'ckeditor-light-text' } },
  // { name : 'Destacar célula', element : 'td', attributes : { 'class' : 'ckeditor-highlight-cell' } },
  // { name : 'Célula branca', element : 'td', attributes : { 'class' : 'ckeditor-white-cell' } },
  // { name : 'Fonte maior', element : 'span', attributes : { 'class' : 'ckeditor-bigger-font' } },
  // { name : 'Fonte grande', element : 'span', attributes : { 'class' : 'ckeditor-big-font' } },
  // { name : 'Fonte menor', element : 'span', attributes : { 'class' : 'ckeditor-smaller-font' } },
  // { name : 'Promoção', element : 'td', attributes : { 'class' : 'ckeditor-promotional-cell' } },
  // { name : 'Tabela', element : 'div', attributes : { 'class' : 'ckeditor-table' } },
];

// Handle CKEditor style inheritance.
if (!(currentStyles = CKEDITOR.stylesSet.get('iela_wysiwyg'))) {
  CKEDITOR.stylesSet.add('iela_wysiwyg', styles);
} else {
  for (s in styles) {
    currentStyles.push(styles[s]);
  }
}
