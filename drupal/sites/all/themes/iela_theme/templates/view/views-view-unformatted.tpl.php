<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="view-content-outer">
  <?php if (!empty($title)): ?>
    <h4 class="view-content-title"><?php print $title; ?></h4>
  <?php endif; ?>
  <div class="view-content-inner">
    <?php foreach ($rows as $id => $row): ?><div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
        <?php print $row; ?>
      </div><?php endforeach; ?>
  </div>
</div>
