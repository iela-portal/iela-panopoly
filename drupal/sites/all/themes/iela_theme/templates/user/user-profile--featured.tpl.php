<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
?>

<article <?php print $attributes; ?>>
  <div <?php print drupal_attributes($user_inner_attributes); ?>>
    <?php if (!empty($has_preview)): ?>
      <div <?php print drupal_attributes($variables['featured_content_attributes']); ?>>
        <div class="featured-user-content-inner">
          <div class="preview-content">
            <div class="preview-content-inner">
              <?php print render($variables['preview_content']); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div <?php print drupal_attributes($variables['user_info_attributes']); ?>>
      <div class="user-info-inner">
        <div class="user-heading">
          <?php if (!empty($user_profile['field_fullname'])): ?>
            <h4><?php print drupal_render($user_profile['field_fullname']); ?></h4>
          <?php endif; ?>

          <?php if (!empty($user_profile['field_iela_user_status'])): ?>
            <div class="user-status">
              <?php print drupal_render($user_profile['field_iela_user_status']); ?>  
            </div>
          <?php endif; ?>
        </div>

        <?php
          // Rendered here to avoid re-rendering of fields.
          $rendered_profile = render($user_profile);
        ?>

        <?php if (!empty($rendered_profile)): ?>
          <div class="content"<?php print $content_attributes; ?>>
            <?php print $rendered_profile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</article>
