<?php

/**
 * @file
 * Common header for pages.
 */
?>

<?php if (!empty($page['header_highlight'])): ?>
  <div id="header-highlight">
    <?php print render($page['header_highlight']); ?>
  </div>
<?php endif; ?>

<header id="header" class="header" role="header">
  <?php print drupal_render($main_navbar); ?>
</header>
