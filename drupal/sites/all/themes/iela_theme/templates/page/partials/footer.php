<?php

/**
 * @file
 * Common footer for pages.
 */
?>

<footer id="site-footer" role="footer">
  <?php print render($page['footer']); ?>
</footer>
