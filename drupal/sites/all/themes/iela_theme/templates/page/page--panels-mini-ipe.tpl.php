<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>

<?php include 'partials/header.php'; ?>

<div id="main-wrapper" <?php print $attributes; ?>>
  <div id="main" class="main is-panel">
    <?php if ($tabs || $action_links || $breadcrumb || $title): ?>
      <div id="page-top">
        <div class="container">
          <?php if ($tabs): ?>
            <div class="tabs" id="local-tasks">
              <?php print render($tabs); ?>
            </div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links">
              <?php print render($action_links); ?>
            </ul>
          <?php endif; ?>
          <?php if ($breadcrumb): ?>
            <div id="breadcrumb" class="visible-desktop">
              <?php print $breadcrumb; ?>
            </div>
          <?php endif; ?>
          <?php if ($title): ?>
            <div class="heading-container text-center">
              <h1 class="title"><?php print $title; ?></h1>
              <?php if (!empty($title_info)): ?>
                <div class="title-info"><p><?php print $title_info; ?></p></div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($messages): ?>
      <div class="container">
        <div id="messages">
          <?php print $messages; ?>
        </div>
      </div>
    <?php endif; ?>
    <div id="content">
      <?php print render($page['content']); ?>
    </div>
  </div> <!-- /#main -->
</div> <!-- /#main-wrapper -->

<?php include 'partials/footer.php'; ?>
