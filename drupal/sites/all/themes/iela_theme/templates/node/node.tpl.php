<?php

/**
 * @file
 * IELA theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>

<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div <?php print drupal_attributes($node_inner_attributes); ?>>
    <?php if ($variables['has_preview']): ?>
      <div <?php print drupal_attributes($variables['featured_content_attributes']); ?>>
        <div class="featured-node-content-inner">
          <div class="preview-content">
            <div class="preview-content-inner">
              <?php print render($variables['preview_content']); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div <?php print drupal_attributes($variables['node_info_attributes']); ?>>
      <div class="node-info-inner">
        <div class="node-heading">
          <?php print drupal_render($content['field_tags']); ?>

          <?php print render($title_prefix); ?>
          <?php if (!$page): ?>
            <?php print drupal_render($renderable_title); ?>
          <?php endif; ?>
          <?php print render($title_suffix); ?>

          <?php if ($display_submitted): ?>
            <div class="submitted">
              <?php print $submitted; ?>
            </div>
          <?php endif; ?>
        </div>

        <?php
          // Rendered here to avoid re-rendering of fields.
          $rendered_content = render($content);
        ?>

        <?php if (!empty($rendered_content)): ?>
          <div class="content"<?php print $content_attributes; ?>>
            <?php print $rendered_content; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>

    <?php print drupal_render($content['links']); ?>
    <?php print drupal_render($content['comments']); ?>
  </div>
</article>
