<?php
/**
 * @file
 * Contains the theme's helper functions.
 */

/**
 * Returns the absolute path to a resource.
 */
function _iela_theme_path($path) {
  global $base_path;
  return $base_path . IELA_THEME_PATH . '/' . $path;
}
