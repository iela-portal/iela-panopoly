<?php
/**
 * @file
 * Contains the theme's preprocess functions for nodes.
 */

/**
 * Implements template_preprocess_panelizer_view_mode().
 */
function iela_theme_preprocess_panelizer_view_mode(&$variables) {
  $element = &$variables['element'];

  // Use entity_view_mode module's helping information.
  $entity_info = empty($element['#entity_view_mode']) ? array() : $element['#entity_view_mode'];

  // Early return if non entity info found.
  if (empty($entity_info)) return;

  $entity_type = $entity_info['entity_type'];

  // Find entity object.
  if (!empty($element['#' . $entity_type])) $entity = $element['#' . $entity_type];
  elseif (!empty($element['#entity'])) $entity = $element['#entity'];

  // Early return if no entity object found.
  if (empty($entity)) return;

  // Make sure to fall title back to what it was.
  $title = isset($variables['title']) ? $variables['title'] : NULL;

  // Setup variables as if inside common entity preprocess.
  // @todo: code was built with nodes in mind. Other kinds of entity
  // will probably need a refactoring.
  $variables += (array) $entity;
  $variables['view_mode'] = $entity_info['view_mode'];
  $variables['theme_hook_suggestions'][] = 'panelizer_view_mode';
  $variables['theme_hook_suggestions'][] = 'panelizer_view_mode__' . $entity_info['bundle'];
  $variables['theme_hook_suggestions'][] = 'panelizer_view_mode__' . $entity_info['entity_type'];
  $variables['theme_hook_suggestions'][] = 'panelizer_view_mode__' . $entity_info['entity_type'] . '__' . $entity_info['id'];

  if ($title === NULL) unset($variables['title']);

  $variables['classes_array'][] = 'panelizer-view-mode';

  _iela_theme_common_preprocess_node($variables);
}

/**
 * Implements hook_preprocess_node().
 */
function iela_theme_preprocess_node(&$variables) {
  _iela_theme_common_preprocess_node($variables);

  /*
   * Initiate attribute for distinct template parts.
   */

  $variables['node_inner_attributes']['class'][] = 'node-inner';
  $variables['node_info_attributes']['class'][] = 'node-info';
  $variables['featured_content_attributes']['class'][] = 'featured-node-content';


  /*
   * Run sub-processors.
   */

  // Handle download icon for articles.
  _iela_theme_download_icon($variables);

  // Handle preview content.
  _iela_theme_preview_content($variables);

  // Handle view mode hidden comments/links.
  _iela_theme_hide_comments_and_links($variables);

  // Handle view mode hidden submitted by.
  _iela_theme_hide_submitted_by($variables);

  // Handle clickable view modes.
  _iela_theme_clickable_node($variables);

  // Handle node titles.
  _iela_theme_node_title($variables);


  /*
   * Type/view mode specific processing.
   */

  $processing_suggestions = array(
    "iela_theme_preprocess_{$variables['type']}_node",
    "iela_theme_preprocess_{$variables['view_mode']}_node",
    "iela_theme_preprocess_{$variables['type']}_{$variables['view_mode']}_node",
  );

  foreach ($processing_suggestions as $suggestion) {
    // @todo: cache function existance information.
    if (function_exists($suggestion)) {
      $suggestion($variables);
    }
  }
}

/**
 * Common preprocess for default node and panelizer preprocessing().
 */
function _iela_theme_common_preprocess_node(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  // Force embed nodes to show as stand.
  if ($view_mode == 'node_embed') $view_mode = $variables['view_mode'] = 'stand';

  // Generic entity view class.
  $variables['classes_array'][] = 'entity-view';


  /*
   * Add view mode based class.
   */

  $view_mode_css = drupal_html_class($variables['view_mode']);
  if (!in_array('node-' . $view_mode_css, $variables['classes_array'])) {
    $variables['classes_array'][] = 'node-' . $view_mode_css;
  }


  /*
   * Add view mode based theme suggestions.
   */

  $current_suggestions = array_values($variables['theme_hook_suggestions']);
  array_unshift($current_suggestions, 'node');
  $variables['theme_hook_suggestions'] = array();

  foreach ($current_suggestions as $suggestion) {
    $variables['theme_hook_suggestions'][] = $suggestion;
    $variables['theme_hook_suggestions'][] = $suggestion . '__' . $variables['view_mode'];
  }


  /*
   * Add title classes.
   */

  $variables['title_attributes_array']['class'][] = 'node-title';
  $variables['title_attributes_array']['class'][] = drupal_html_class($variables['type']) . '-title';
}

/**
 * Implements (fake) template_preprocess_video_featured_node().
 */
function iela_theme_preprocess_video_featured_node(&$variables) {
  $variables['node_inner_attributes']['class'][] = 'row';
  $variables['node_info_attributes']['class'][] = 'col-sm-6';
  $variables['featured_content_attributes']['class'][] = 'col-sm-6';
  $variables['featured_content_attributes']['class'][] = 'hidden-xs';
}

/**
 * Implementes (fake) template_preprocess_stacked_node().
 */
function iela_theme_preprocess_stacked_node(&$variables) {

  $theme_path = drupal_get_path('theme', 'iela_theme');

  // @todo: use attachments.
  // @todo : fix selectors.

  // Load activation script:
  drupal_add_js($theme_path . '/js/iela-same-height.js');

  // Same height settings.
  drupal_add_js(array(
    'ielaSameHeight' => array(
      '.node-stacked' => array(
        'selector' => '> .featured-image-cover, > .node-info',
        'byRow' => true,
      ),
    ),
  ), 'setting');
}

/**
 * Implementes (fake) template_preprocess_stacked_node().
 */
function iela_theme_preprocess_teaser_node(&$variables) {
  $variables['node_info_attributes']['class'][] = 'col-xs-12';
  $variables['featured_content_attributes']['class'][] = 'col-sm-4';
  $variables['featured_content_attributes']['class'][] = 'hidden-xs';

  if (!empty($variables['has_preview'])) {
    $variables['node_info_attributes']['class'][] = 'col-sm-8';
  }
}

/**
 * Implementes (fake) template_preprocess_stacked_node().
 */
function iela_theme_preprocess_mini_teaser_node(&$variables) {
  $variables['node_info_attributes']['class'][] = 'col-xs-12';
  $variables['featured_content_attributes']['class'][] = 'col-sm-3';
  $variables['featured_content_attributes']['class'][] = 'hidden-xs';

  if (!empty($variables['has_preview'])) {
    $variables['node_info_attributes']['class'][] = 'col-sm-9';
  }
}

/**
 * Custom function to handle download icon for documents.
 */
function _iela_theme_download_icon(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  $article_types = array(
    'article',
    'photo_essay',
    'interview',
    'review',
  );

  // Map view modes that use preview content.
  $use_map = array();

  foreach ($article_types as $article_type) {
    $use_map[$article_type] = array(
      'view_modes' => array('stacked')
    );
  }

  // Handle preview content.
  if (!empty($use_map[$type]) && in_array($view_mode, $use_map[$type]['view_modes'])) {
    $variables['classes_array'][] = 'has-icon-action';
  }
}

/**
 * Custom function to get field_video thumbnails' url.
 */
function _iela_theme_field_video_get_thumbnail_url(&$variables) {
  hide($variables['content']['field_video']);

  // Early return for empty field.
  if(empty($variables['field_video'][0])) return null;

  // Get preview style from media.
  $preview = media_get_thumbnail_preview((object) $variables['field_video'][0]);

  return !empty($preview['#path']) ? file_create_url($preview['#path']) : null;
}

/**
 * Custom function to handle preview content for nodes.
 */
function _iela_theme_preview_content(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  $preview_field = $type == 'video' ? 'field_video' : 'field_featured_image';

  // Map types that never have preview content.
  $use_map = array();

  // Find preview content.
  if (!in_array($type, $use_map) && !empty($variables['content'][$preview_field])) {
    // Make sure it is only rendered when forced.
    hide($variables['content'][$preview_field]);

    $variables['preview_content'] = $variables['content'][$preview_field];
  }

  $variables['has_preview'] = !empty($variables['preview_content']);
}

/**
 * Custom function to hide comments/links for nodes.
 */
function _iela_theme_hide_comments_and_links(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  // Map allowing view modes.
  $use_map = array();

  if (empty($use_map[$type]) || !in_array($view_mode, $use_map[$type])) {
    hide($variables['content']['comments']);
    hide($variables['content']['links']);
  }
}

/**
 * Custom function to hide submitted by for nodes.
 */
function _iela_theme_hide_submitted_by(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  // Map disallowed view modes.
  $use_map = array(
    'news' => array('stand', 'stacked'),
    'video' => array('stand', 'stacked'),
  );

  if (!empty($use_map[$type]) && in_array($view_mode, $use_map[$type])) {
    $variables['display_submitted'] = FALSE;
  }
}

/**
 * Custom funcition to make node clickable.
 */
function _iela_theme_clickable_node(&$variables) {
  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  // Map clickable view modes.
  $use_map = array(
    'news' => array('featured', 'banner', 'poster'),
    'panopoly_page' => array('featured', 'banner', 'poster'),
    'video' => array('banner', 'poster'),
  );

  if (!empty($use_map[$type]) && in_array($view_mode, $use_map[$type])) {
    $variables['attributes_array']['data-click'] = $variables['node_url'];
  }
}

/**
 * Custom function to handle node titles.
 */
function _iela_theme_node_title(&$variables) {

  $type = $variables['type'];
  $view_mode = $variables['view_mode'];

  // Map title sizes.
  $use_map = array(
    'default' => array(
      'banner' => 'h4',
      'featured' => 'h2',
      'mini_teaser' => 'h5',
      'poster' => 'h4',
      'stacked' => 'h4',
      'teaser' => 'h3',
      'stand' => 'h4',
      'default' => 'h2',
    ),
    'panopoly_page' => array(
      'poster' => 'h3',
    ),
  );

  $node_url = drupal_get_path_alias('node/' . $variables['nid']);

  $variables['renderable_title'] = array(
    '#type' => 'html_tag',
    '#tag' => 'h1',
    '#attributes' => $variables['title_attributes_array'],
    '#value' => l(decode_entities($variables['title']), $node_url),
  );

  if (!empty($use_map[$type][$view_mode])) {
    $variables['renderable_title']['#tag'] = $use_map[$type][$view_mode];
  } elseif (!empty($use_map['default'][$view_mode])) {
    $variables['renderable_title']['#tag'] = $use_map['default'][$view_mode];
  }
}
