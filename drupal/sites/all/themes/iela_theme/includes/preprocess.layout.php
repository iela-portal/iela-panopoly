<?php
/**
 * @file
 * Contains the theme's preprocess functions for layouts.
 */

/**
 * Implements hook_preprocess_panels_layout(&$variables).
 * @todo : panels does not have a general preprocess function. A better
 * implementation would find all IELA layouts and automatically add
 * preprocess functions for them.
 */
function _iela_theme_preprocess_panels_layout(&$variables) {

  // Panels frames suffix for regions.
  $frame = isset($variables['display']->current_frame) ? $variables['display']->current_frame . '__' : '';

  // Build contained map.
  $variables['contained'] = array();
  foreach ($variables['containers'] as $container => $regions) {
    foreach ($regions as $region) {
      $variables['contained'][$region] = $container;
    }
  }

  // Handle region attributes.
  if (!empty($variables['display']->region_attributes)) {
    foreach ($variables['display']->region_attributes as $region => $attributes) {
      $region_content = &$variables['content'][$variables['contained'][$region]]['container inside']['row']['row inside'][$region];
      $region_content['#attributes'] = array_merge_recursive($region_content['#attributes'], $attributes);
    }
  }

  // Handle container preprocessors.
  if (!empty($variables['display']->container_preprocess)) {
    foreach ($variables['display']->container_preprocess as $region => $preprocesses) {

      // Run only current frame preprocesses.
      if (!empty($frame) && strpos($region, $frame) !== 0) continue;
      
      // Remove frame name, if any.
      $region = substr($region, strlen($frame));

      // Find region's container.
      $container = $variables['contained'][$region];

      // Apply settings.
      if ($container) {
        foreach ($preprocesses as $function => $options) {
          $container_variables = array(
            'container' => &$variables['content'][$container],
            'settings' => &$options,
            'layout_variables' => &$variables,
            'container_id' => $container,
            'region_id' => $region,
          );

          $function($container_variables);
        }
      }
    }
  }

  // Add display owner classes.
  if (isset($variables['display']->owner) && isset($variables['display']->owner->table)) {
    $variables['content']['#attributes']['class'][] = 'panels-owner-' . str_replace('_', '-', $variables['display']->owner->table);
    if ($variables['display']->owner->table == 'panels_mini') {
      $variables['content']['#attributes']['class'][] = 'row';
    }
  }
}

function iela_theme_preprocess_iela_simple(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_half(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_third(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_half_third(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_third_half(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_sidebar_left(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_sidebar_right(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_sidebar_right_third(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_sidebar_mixed_left(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_iela_sidebar_mixed_right(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_single_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_half_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_third_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_quarter_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_sidebar_left_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_sidebar_right_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_double_sidebar_left_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

function iela_theme_preprocess_double_sidebar_right_frame(&$variables) {
  _iela_theme_preprocess_panels_layout($variables);
}

