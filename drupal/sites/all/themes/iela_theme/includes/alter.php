<?php
/**
 * @file
 * Contains the theme's alter functions.
 */

/**
 * Implements hook_form_alter(&$form, &$form_state, $form_id).
 */
function iela_theme_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#entity_type']) && $form['#entity_type'] === 'entityform' && !path_is_admin(current_path())) {
    if (!empty($form['actions'])) {
      foreach ($form['actions'] as $index => &$element) {
        if (substr($index, 0, 1) != '#') {
          $element['#prefix'] = '<div class="form-action form-action-' . preg_replace('/[ _]/i', '-', $index) . '">';
          $element['#suffix'] = '</div>';
        }
      }
    }
  }
}

/**
 * Implements hook_form_search_form_alter().
 */
function iela_theme_form_search_form_alter(&$form, &$form_state) {

  /*
   * Remove inline form specification.
   */
  if (!empty($form['basic']['#attributes']['class']) && ($key = array_search('container-inline', $form['basic']['#attributes']['class'])) !== false) {
    unset($form['basic']['#attributes']['class'][$key]);
  }

  /*
   * Make block submit button.
   */
  if (!empty($form['basic']['submit'])) {
    $form['basic']['submit']['#attributes']['class'][] = 'btn-block';
  }
}

/**
 * Implements hook_css_alter().
 */
function iela_theme_css_alter(&$css) {
  $filter_map = array(
    drupal_get_path('module', 'system') => array(
      '/system.base.css',
      // '/system.messages.css',
      '/system.theme.css',
    ),
    drupal_get_path('module', 'panels_mini_ipe') => array(
      '/panels-mini-ipe.css',
    ),
  );

  foreach ($filter_map as $module_path => $stylesheets) {
    foreach ($stylesheets as $stylesheet) {
      unset($css[$module_path . $stylesheet]);
    }
  }
}

/**
 * Implements hook_js_alter().
 */
function iela_theme_js_alter(&$js) {

  /*
   * Handle views_timelinejs overrides.
   */

  $views_timelinejs = drupal_get_path('module', 'views_timelinejs') . '/js/views_timelinejs.js';

  if (!empty($js[$views_timelinejs])) {
    $js[$views_timelinejs]['data'] = drupal_get_path('theme', 'iela_theme') . '/js/iela-timeline.js';
  }
}

/**
 * Implements hook_views_timelinejs_rows().
 */
function iela_theme_views_timelinejs_data_alter(&$rows, &$view) {

  $result = $view->result;
  $field_map = $view->style_options['timeline_fields'];
  $media_field = 'field_' . $field_map['media'];

  foreach ($rows as $delta => &$row) {
    if (!empty($row['asset']['media'])) {

      // Grab media content.
      $media = !empty($result[$delta]->{$media_field}[0]) ? $result[$delta]->{$media_field}[0] : NULL;

      // Modify thumbnail path.
      if (!empty($row['asset']['thumbnail']) && !empty($media['raw']['uri'])) {
        $row['asset']['thumbnail'] = image_style_url('square_extra_small', $media['raw']['uri']);
      }

      // Make use of rendered file, if it is an image.
      if (!empty($media['rendered']['file']['#theme']) && strpos($media['rendered']['file']['#theme'], 'image') === 0) {
        $row['asset']['media'] = render($media['rendered']['file']);
      }
    }
  }
}
