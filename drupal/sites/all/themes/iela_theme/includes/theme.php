<?php
/**
 * @file
 * Contains the theme's theme replacements.
 */

include_once 'theme.pager.php';
include_once 'theme.form.php';
include_once 'theme.navbar.php';

/**
 * Implements hook_theme().
 */
function iela_theme_theme() {
  return array(
    'bootstrap_navbar' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Overrides theme_breadcrumb(&$variables).
 */
function iela_theme_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="breadcrumb-title">' . t('You are here') . '</h2>';

    $output .= '<ol class="breadcrumb"><li>' . implode('</li><li>', $breadcrumb) . '</li></ol>';
    return $output;
  }
}

/**
 * Overrides theme_menu_link(&$variables).
 */
function iela_theme_menu_link(&$variables) {

  $element = $variables['element'];
  $sub_menu = '';

  if (!empty($element['#below']) || !empty($element['#localized_options']['minipanel']['name'])) {
    // Wrap in dropdown-menu.
    unset($element['#below']['#theme_wrappers']);
    $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
    $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

    $sub_menu = array(
      '#type' => 'html_tag',
      '#tag' => 'ul',
      '#attributes' => array('class' => array('dropdown-menu')),
    );

    if (!empty($element['#localized_options']['minipanel']['name'])) {
      $minipanel_options = $element['#localized_options']['minipanel'];
      $minipanel = panels_mini_block_view($minipanel_options['name']);

      $sub_menu['#tag'] = 'div';
      $sub_menu['#value'] = $minipanel['content'];
      $sub_menu['#attributes']['class'][] = 'dropdown-mini-panel-menu';

      $element['#attributes']['class'][] = 'mini-panel-item';

      // Parse and set size.
      if (!empty($minipanel_options['size'])) {
        $minipanel_options['size'] = array(
          'raw' => str_replace(',', '.', $minipanel_options['size']),
        );
        $minipanel_options['size']['number'] = floatval($minipanel_options['size']['raw']);
        $minipanel_options['size']['unit'] = str_replace($minipanel_options['size']['number'], '', $minipanel_options['size']['raw']);
      
        $sub_menu['#attributes']['style'][] = 'width: ' . $minipanel_options['size']['raw'] . ';';
      }

      // Set alignment.
      switch ($minipanel_options['align']) {
        case 'left':
        case 'right':
          $sub_menu['#attributes']['class'][] = 'dropdown-align-' . $minipanel_options['align'];
          break;
        case 'center':
          if (!empty($minipanel_options['size']['number'])) {
            $margin_left  = 'margin-left: -';
            $margin_left .= $minipanel_options['size']['number'] / 2;
            $margin_left .= $minipanel_options['size']['unit'];

            $sub_menu['#attributes']['style'][] = 'left: 50%;';
            $sub_menu['#attributes']['style'][] = $margin_left;
          }
          break;
      }

      // Set relative parent.
      if ($minipanel_options['relative_to'] == 'navbar') {
        $element['#attributes']['class'][] = 'dropdown-static';
      }

      // Clean configuration.
      unset($element['#localized_options']['minipanel']);
    } else {
      $sub_menu['#value'] = drupal_render($element['#below']);
    }

    // Check if element is nested.
    if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
      $element['#attributes']['class'][] = 'dropdown-submenu';
    }
    else {
      $element['#attributes']['class'][] = 'dropdown';
      $element['#localized_options']['html'] = TRUE;
      $element['#title'] .= '<span class="caret"></span>';
    }

    $sub_menu = drupal_render($sub_menu);

    $element['#localized_options']['attributes']['data-target'] = '#';
  }

  // Fix for active class.
  if (($element['#href'] == current_path() || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
    $element['#attributes']['class'][] = 'active';
  }

  if (!empty($element['#localized_options']['fragment'])) {
    foreach (array('active', 'active-trail') as $class_name) {
      foreach (array_keys($element['#attributes']['class'], $class_name) as $key) {
        unset($element['#attributes']['class'][$key]);
      }
    }
  }

  // Add active class to li if active trail.
  if (in_array('active-trail', $element['#attributes']['class'])) {
    $element['#attributes']['class'][] = 'active';
  }

  // Add a unique class using the title.
  $title = strip_tags($element['#title']);
  $element['#attributes']['class'][] = 'menu-link-' . drupal_html_class($title);

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Overrides theme_field_collection_item__field_social_links__horizontal().
 */
function iela_theme_field_collection_item__field_social_links__horizontal($variables) {

  $social_network_term = !empty($variables['field_social_network']) ? $variables['field_social_network'][0]['taxonomy_term'] : NULL;
  $social_network_name = !empty($social_network_term->name) ? $social_network_term->name : NULL;
  $social_network_icon = !empty($social_network_term->field_icon[LANGUAGE_NONE][0]) ? $social_network_term->field_icon[LANGUAGE_NONE][0] : NULL;
  $url = !empty($variables['field_url']) ? $variables['field_url'][0]['value'] : NULL;

  if (!empty($social_network_icon)) {
    $social_network_css_name = strtolower(str_replace(' ', '-', $social_network_name));

    $renderable_image = array(
      '#theme' => 'image_formatter',
      '#item' => array(
        'uri' => file_create_url($social_network_icon['uri']),
        'width' => $social_network_icon['metadata']['width'],
        'height' => $social_network_icon['metadata']['height'],
        'attributes' => array('class' => array(
          'social-network',
          'social-network-' . $social_network_css_name,
        )),
      ),
    );

    if (!empty($url)) {
      $renderable_image['#path'] = array(
        'path' => $url,
        'options' => array(
          'attributes' => array('class' => array(
            'social-network-link',
            'social-network-link-' . $social_network_css_name,
          )),
        ),
      );
    }

    $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];
    foreach ($variables['classes_array'] as $class_name) {
      if (empty($attributes['class']) || (is_array($attributes['class']) && !in_array($class_name, $attributes['class']))) {
        $attributes['class'][] = $class_name;
      }
    }

    $renderable_item = array(
      '#theme' => 'container',
      '#attributes' => $attributes,
      '#children' => drupal_render($renderable_image),
    );

    return drupal_render($renderable_item);
  }

  return '';
}

/**
 * Implements theme_menu_tree__dropdown().
 */
function iela_theme_menu_tree__dropdown(&$variables) {
  return '<ul class="dropdown-menu standalone">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_tree__links().
 */
function iela_theme_menu_tree__links(&$variables) {
  return '<ul class="menu nav-links">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_link__link(&$variables).
 */
function iela_theme_menu_link__link(&$variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if (!empty($element['#below'])) {
    unset($element['#below']['#theme_wrappers']);
    $sub_menu = '<ul class="submenu">' . drupal_render($element['#below']) . '</ul>';
  }

  // Fix for active class.
  if (($element['#href'] == current_path() || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
    $element['#attributes']['class'][] = 'active';
  }

  if (!empty($element['#localized_options']['fragment'])) {
    foreach (array('active', 'active-trail') as $class_name) {
      foreach (array_keys($element['#attributes']['class'], $class_name) as $key) {
        unset($element['#attributes']['class'][$key]);
      }
    }
  }

  // Add active class to li if active trail.
  if (in_array('active-trail', $element['#attributes']['class'])) {
    $element['#attributes']['class'][] = 'active';
  }

  // Add a unique class using the title.
  $title = strip_tags($element['#title']);
  $element['#attributes']['class'][] = 'menu-link-' . drupal_html_class($title);

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_button().
 */
function iela_theme_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  $element['#attributes']['class'][] = 'btn';

  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  $button_style = 'btn-default';

  // Handle custom types.
  if (!empty($element['#button_style'])) {
    $button_style = $element['#button_style'];
  } else if (isset($element['#parents']) && ($element['#parents'][0] == 'submit')) {
    $button_style = 'btn-primary';
  }

  $element['#attributes']['class'][] = $button_style;

  if (!empty($element['#button_tag'])) {
    $prefix = empty($element['#value_prefix']) ? '' : $element['#value_prefix'];
    $suffix = empty($element['#value_suffix']) ? '' : $element['#value_suffix'];
    $value  = isset($element['#value_replace']) ? $element['#value_replace'] : $element['#value'];

    $markup  = '<button';
    $markup .= drupal_attributes($element['#attributes']);
    $markup .= '>' . $prefix . $value . $suffix;
    $markup .= '</button>';
  } else {
    $markup = '<input' . drupal_attributes($element['#attributes']) . ' />';
  }

  return $markup;
}

/**
 * Overrides theme_file_entity_download_link().
 */
function iela_theme_file_entity_download_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];

  $uri = url_is_external($file->uri) ? array(
    'path' => $file->uri
  ) : file_entity_download_uri($file);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $uri['options']['attributes']['type'] = $file->filemime . '; length=' . $file->filesize;

  // Provide the default link text.
  if (!isset($variables['text'])) {
    $variables['text'] = t('Download [file:name]');
  }

  // Peform unsanitized token replacement if $uri['options']['html'] is empty
  // since then l() will escape the link text.
  $variables['text'] = token_replace($variables['text'], array('file' => $file), array('clear' => TRUE, 'sanitize' => empty($uri['options']['html'])));

  $output = '<span class="file">' . l($variables['text'], $uri['path'], $uri['options']) . '</span>';

  return $output;
}

/**
 * Implements theme_menu_local_tasks().
 */
function iela_theme_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="nav nav-pills primary">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="nav nav-pills secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Implements theme_status_messages().
 */
function iela_theme_status_messages($variables) {
  $output = '';
  $display = $variables['display'];
  $message_types = empty($variables['messages']) ? drupal_get_messages($display) : $variables['messages'];
  $skip_filters = empty($variables['skip_filters']) ? array() : $variables['skip_filters'];

  // Filter messages if filtering is enabled.
  if (variable_get('disable_messages_enable', '1')) {
    $message_types = disable_messages_apply_filters($message_types, $skip_filters);
  }

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );

  foreach ($message_types as $type => $messages) {
    // Add Bootstrap classes.
    $type = ($type == 'status') ? 'success' : $type;
    $type = ($type == 'error') ? 'danger' : $type;
    $type_output = '';

    foreach ($messages as $message) {
      $type_output .= "<div class=\"alert alert-$type alert-dismissable\">\n";

      // Add a close button.
      $type_output .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

      if (!empty($status_heading[$type])) {
        $type_output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
      }

      $type_output .= $message . "</div>\n";
    }

    if (!empty($type_output)) {
      $output .= "<div class=\"alert-$type-messages alert-messages\">" . $type_output . '</div>';
    }
  }

  if (!empty($output)) {
    $output = '<div class="alert-messages-container">' . $output . '</div>';
  }
  
  return $output;
}
