<?php
/**
 * @file
 * Contains the theme's preprocess functions.
 */

include_once 'preprocess.layout.php';
include_once 'preprocess.node.php';

/**
 * Implements hook_preprocess_html(&$variables).
 */
function iela_theme_preprocess_html(&$variables) {

  // Add some script/style adjustments for administrative interfaces.
  if ($variables['logged_in'] == TRUE) {
    drupal_add_css(IELA_THEME_PATH . '/css/admin.css');
    drupal_add_js(IELA_THEME_PATH . '/js/admin.js');
  }

  // Force unique navbar sticky at home.
  if (drupal_is_front_page()) {
    $variables['classes_array'][] = 'stick-main-navbar';
  }
}

/**
 * Implements hook_preprocess_page(&$variables);
 */
function iela_theme_preprocess_page(&$variables) {
  $theme_path = drupal_get_path('theme', 'iela_theme');
  $current_path = current_path();
  $menu_item = menu_get_item();
  $status = drupal_get_http_header("status");
  $blocked_pages = array(
    '404 Not Found',
    '403 Forbidden'
  );

  // PANELS ADJUSTMENTS.
  // -------------------

  $current_panels_page = page_manager_get_current_page();

  if (!empty($variables['is_panel']) || !empty($current_panels_page)) {
    // Add custom page template override.
    $variables['theme_hook_suggestions'][] = 'page__panels';
  }


  // PANELS MINI ADJUSTMENTS.
  // ------------------------

  $arg_3 = arg(3);
  if (!in_array($status, $blocked_pages) && $menu_item['page_callback'] === '_panels_mini_ipe_editor' && !empty($menu_item['page_arguments'][0])) {
    $panel_mini = panels_mini_load($menu_item['page_arguments'][0]);
    if (!empty($panel_mini)) {
      $variables['theme_hook_suggestions'][] = 'page__panels_mini_ipe';
      $variables['classes_array'][] = 'panels-mini-ipe-page';
      $variables['title'] = t('"@panel" Mini Panel', array(
        '@panel' => $panel_mini->admin_title,
      ));
      $variables['title_info'] = t('Click "@customize" in the bottom toolbar to edit the content of this mini panel.', array(
        '@customize' => t('Customize this page'),
      ));
    }
  }


  // LOGO ADJUSTMENTS.
  // -----------------

  $logo_classes = array();
  $variables['logo_path'] = $theme_path . '/images/logo-wide.png';

  if (empty($variables['header_promotion'])) {
    $logo_classes[] = 'logo-solo';
  }

  $variables['logo_class'] = implode(' ', $logo_classes);


  // MAIN NAVBAR.
  // ------------

  // Create basic main navbar with logo:
  $theme_navbar = theme_get_setting('navbar');
  $variables['main_navbar'] = array(
    '#theme' => 'bootstrap_navbar',
    'type' => empty($theme_navbar) ? 'dark' : $theme_navbar,
    'header' => array(
      'logo' => array(
        '#type' => 'link',
        '#title' => theme('image', array(
          'path' => $theme_path . '/images/logo-mini.png',
        )),
        '#href' => '<front>',
        '#options' => array(
          'html' => true,
          'attributes' => array(
            'id' => 'navbar-logo',
            'class' => array('navbar-brand'),
          ),
        ),
      ),
    ),
  );

  // Garantee the menu is present.
  if (empty($variables['main_menu']) || count($variables['main_menu']) == 1) {
    $tree = menu_tree_all_data(variable_get('menu_main_links_source', 'main-menu'));
    menu_tree_add_active_path($tree);
    $variables['main_menu'] = menu_tree_output($tree);
  }

  // Add main menu to main navbar:
  if (!empty($variables['main_menu'])) {
    $variables['main_menu']['#theme_wrappers'] = array('menu_tree__navbar_nav');
    $variables['main_navbar']['content']['main-menu-container'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'main-menu-container',
      ),
      'main-menu' => $variables['main_menu'],
    );
  }

  // Garantee the second menu is present.
  if (empty($variables['secondary_menu']) || count($variables['secondary_menu']) == 1) {
    $tree = menu_tree_all_data(variable_get('menu_secondary_links_source', 'second-menu'));
    menu_tree_add_active_path($tree);
    $variables['secondary_menu'] = menu_tree_output($tree);
  }

  // Add secondary menu to main navbar:
  if (!empty($variables['secondary_menu'])) {
    $variables['secondary_menu']['#theme_wrappers'] = array('menu_tree__navbar_nav');
    $variables['main_navbar']['content']['secondary-menu-container'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'id' => 'secondary-menu-container',
        'class' => array('navbar-right')
      ),
      'secondary-menu' => $variables['secondary_menu'],
    );
  }

  foreach ($variables['classes_array'] as $class) {
    $variables['attributes_array']['class'][] = $class;
  }
}

/**
 * Implements hook_preprocess_breadcrumb(&$variables).
 */
function iela_theme_preprocess_breadcrumb(&$variables) {
  foreach ($variables['breadcrumb'] as $key => $crumb) {
    if (preg_match("/<[^<]+>/", $crumb) == 0) {
      $variables['breadcrumb'][$key] = '<span';
      if ($key === count($variables['breadcrumb']) - 1) {
        $variables['breadcrumb'][$key] .= ' class="current"';
      }
      $variables['breadcrumb'][$key] .= '>' . $crumb . '</span>';
    }
  }
}

/**
 * Implements hook_preprocess_panels_pane(&$variables).
 */
function iela_theme_preprocess_panels_pane(&$variables) {

  $pane = &$variables['pane'];
  $theme_path = drupal_get_path('theme', 'iela_theme');

  // Default classes.
  $variables['content_attributes_array']['class'][] = 'pane-content';
  $variables['pane_wrapper_attributes']['class'][] = 'panel-pane-wrapper';

  /*
   * Make sure an ID is defined.
   */

  if (empty($variables['pane_wrapper_attributes']['id'])) {
    $prefix = 'panel-pane-wrapper-';

    if (!empty($variables['display']->css_id)) {
      $prefix .= str_replace('_', '-', $variables['display']->css_id) . '-';
    }

    $variables['pane_wrapper_attributes']['id'] = $prefix . $pane->pid;
  }

  /*
   * Rendereable titles.
   */

  if (!empty($variables['title'])) {
    $default_title_wrapper = array(
      '#type' => 'html_tag',
      '#tag' => 'h4',
      '#attributes' => empty($variables['title_attributes_array']) ? array() : $variables['title_attributes_array'],
    );

    $variables['title_wrapper'] = empty($variables['title_wrapper']) ? $default_title_wrapper : $variables['title_wrapper'] + $default_title_wrapper;

    if (empty($variables['title_wrapper']['#value'])) {
      $variables['title_wrapper']['#value'] = $variables['title'];
    }

    $variables['title'] = $variables['title_wrapper'];
  }

  /*
   * Type specific processing.
   */

  $processor = "iela_theme_preprocess_{$pane->type}_panels_pane";

  // @todo: cache function existance information.
  if (function_exists($processor)) {
    $processor($variables);
  }
}

/**
 * Implements (fake) template_preprocess_entity_field_panels_pane().
 */
function iela_theme_preprocess_entity_field_panels_pane(&$variables) {
  $pane = &$variables['pane'];

  // Hide default pane title when defined another title showing option.
  if ($pane->configuration['label'] != 'title') {
    $variables['title'] = '';
  }

  // Add identifier for mini panel fields.
  if ($pane->configuration['formatter'] == 'mini_panel_reference_default') {
    $variables['classes_array'][] = 'pane-panels-mini';
  }
}

/**
 * Implements (fake) template_preprocess_views_panes_panels_pane().
 */
function iela_theme_preprocess_views_panes_panels_pane(&$variables) {
  $pane = &$variables['pane'];

  if (!empty($pane->configuration['view_mode'])) {
    $variables['pane_wrapper_attributes']['class'][] = 'pane-views-panes-' . str_replace('_', '-', $pane->configuration['view_mode']);
  }
}

/**
 * Implements (fake) template_preprocess_fieldable_panels_pane_panels_pane().
 */
function iela_theme_preprocess_fieldable_panels_pane_panels_pane(&$variables) {
  $theme_path = drupal_get_path('theme', 'iela_theme');

  switch ($variables['content']['#bundle']) {
    case 'entityform':
      drupal_add_js($theme_path . '/js/entityform.js');
      break;
  }
}

/**
 * Implements (fake) template_preprocess_search_form_panels_pane().
 */
function iela_theme_preprocess_search_box_panels_pane(&$variables) {
  $form = &$variables['content']['basic'];
  if (!empty($form['keys']) && !empty($form['submit'])) {
    $form['input_group'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => 'input-group'),
      'keys' => $form['keys'],
      'submit' => array(
        '#type' => 'container',
        '#attributes' => array('class' => 'input-group-btn'),
        '#weight' => 10,
        'button' => $form['submit'],
      ),
    );

    $form['input_group']['submit']['button']['#button_tag'] = TRUE;
    $form['input_group']['submit']['button']['#button_style'] = 'btn-default';

    // Remove old elements.
    unset($form['keys']);
    unset($form['submit']);
  }
}

/**
 * Implements template_preprocess_entity().
 * Runs a entity specific preprocess function, if they exist.
 */
function iela_theme_preprocess_entity(&$variables, $hook) {

  // Active per-entity-type preprocess.
  $function = __FUNCTION__ . '_' . $variables['entity_type'];
  if (function_exists($function)) {
    $function($variables, $hook . '_' . $variables['entity_type']);
  }

  // Add common view-mode classes.
  if (!empty($variables['view_mode'])) {
    $view_mode_css = str_replace('_', '-', $variables['view_mode']);
    $variables['classes_array'] = empty($variables['classes_array']) ? array() : $variables['classes_array'];

    foreach ($variables['classes_array'] as $class_name) {
      $variables['classes_array'][] = $class_name . '-' . $view_mode_css;
    }
  }

  // Generic entity view class.
  $variables['classes_array'][] = 'entity-view';
}

/**
 * Implements template_preprocess_entity_entityform_type(&$variables).
 */
function iela_theme_preprocess_entity_entityform_type(&$variables, $hook) {

  $view_mode_css = str_replace('_', '-', $variables['view_mode']);

  $variables['classes_array'][] = 'entityform-type-' . $view_mode_css;
  $variables['classes_array'][] = 'entityform-type-' . $variables['entityform_type']->type;
}

/**
 * Implements template_preprocess_user_profile(&$variables).
 */
function iela_theme_preprocess_user_profile(&$variables) {

  $view_mode_css = str_replace('_', '-', $variables['elements']['#view_mode']);

  // Generic entity view class.
  $variables['attributes_array']['class'][] = 'entity-view';
  $variables['attributes_array']['class'][] = 'user-profile';
  $variables['attributes_array']['class'][] = 'user-profile-' . $view_mode_css;

  /*
   * Add view mode based class.
   */

  if (!in_array('node-' . $view_mode_css, $variables['classes_array'])) {
    $variables['classes_array'][] = 'node-' . $view_mode_css;
  }


  /*
   * Add view mode based theme suggestions.
   */

  $variables['theme_hook_suggestions'][] = 'user_profile__' . $variables['elements']['#view_mode'];

  // Hide default user picture.
  $variables['user_profile']['user_picture']['#access'] = FALSE;


  /*
   * Initiate attribute for distinct template parts.
   */

  $variables['user_inner_attributes']['class'][] = 'user-inner';
  $variables['user_info_attributes']['class'][] = 'user-info';
  $variables['featured_content_attributes']['class'][] = 'featured-user-content';

  /*
   * View mode specific processing.
   */

  $suggestion = "iela_theme_preprocess_{$variables['elements']['#view_mode']}_user_profile";

  // @todo: cache function existance information.
  if (function_exists($suggestion)) {
    $suggestion($variables);
  }
}

/**
 * Implements (fake) template_preprocess_featured_user_profile().
 */
function iela_theme_preprocess_featured_user_profile(&$variables) {
  $user_picture_attributes = array(
    'class' => array('featured-image-background'),
  );

  $featured_contents = array();

  if (!empty($variables['user_profile']['field_user_picture'])) {
    $variables['preview_content'][] = $variables['user_profile']['field_user_picture'];
    hide($variables['user_profile']['field_user_picture']);
  }

  if (!empty($variables['user_profile']['field_social_links'])) {
    $variables['preview_content'][] = $variables['user_profile']['field_social_links'];
    hide($variables['user_profile']['field_social_links']);
    $variables['attributes_array']['class'][] = 'has-social-links';
  }

  $variables['has_preview'] = !empty($variables['preview_content']);
}

/**
 * Implements template_preprocess_field(&$variables, $hook).
 */
function iela_theme_preprocess_field(&$variables, $hook) {

  /*
   * Add attached classes.
   */
  if (!empty($variables['element']['#attributes']['class'])) {
    $variables['classes_array'] = array_merge($variables['classes_array'], $variables['element']['#attributes']['class']);
    unset($variables['element']['#attributes']['class']);
  }

  /*
   * Add attached attirbutes.
   */
  if (!empty($variables['element']['#attributes'])) {
    $variables['attributes'] = array_merge($variables['element']['#attributes'], $variables['attributes']);
  }

  /*
   * Add field formatter default class names.
   */

  $variables['classes_array'][] = 'formatter-' . str_replace('_', '-', $variables['element']['#formatter']);

  if (!empty($variables['element']['#items'][0]['format'])) {
    $variables['classes_array'][] = str_replace('_', '-', $variables['element']['#items'][0]['format']);
  }


  /*
   * Field names specific handlers.
   */

  if (!empty($variables['element']['#field_name'])) {
    switch ($variables['element']['#field_name']) {

      // Parse html markup fields to allow original content.
      case 'field_html_markup':
        foreach ($variables['element']['#items'] as $index => $item) {
          if (!empty($item['value'])) {
            $value = $item['value'];
            if (isset($item['safe_value'])) $item['safe_value'] = $value;
            if (isset($variables['items'][$index]['#markup'])) $variables['items'][$index]['#markup'] = $value;
            if (isset($variables['element'][$index]['#markup'])) $variables['element'][$index]['#markup'] = $value;
          }
        }
        break;

      // Add view mode class to user reference field wrapper.
      case 'field_user':
        if (!empty($variables['items'][0]['user'])) {
          $sample_users = array_values($variables['items'][0]['user']);
          $sample_user = array_shift($sample_users);
          $variables['classes_array'][] = 'user-profiles-' . str_replace('_', '-', $sample_user['#view_mode']);
        }
        break;
    }
  }


  /*
   * Formatter specific handlers.
   */

  if (!empty($variables['element']['#formatter'])) {
    switch ($variables['element']['#formatter']) {
      // Add view mode class to field collection wrapper.
      case 'field_collection_view':
      case 'field_collection_fields':
        if (!empty($variables['element'][0]['entity']['field_collection_item'])) {
          $first_item = reset($variables['element'][0]['entity']['field_collection_item']);
          if (!empty($first_item['#view_mode'])) {
            $variables['classes_array'][] = 'field-collection-' . $first_item['#view_mode'];
            $variables['classes_array'][] = str_replace('_', '-', $variables['element']['#field_name']) . '-' . $first_item['#view_mode'];
          }
        }
        break;
    }
  }
}

/**
 * Implements template_preprocess_form_element(&$variables).
 */
function iela_theme_preprocess_textarea(&$variables) {
  if (!path_is_admin(current_path())) {
    $variables['element']['#resizable'] = FALSE;
  }
}

/**
 * Implements template_preprocess_simplenews_block().
 */
function iela_theme_preprocess_simplenews_block(&$variables) {

  $form = &$variables['form'];
  $form['mail']['#attributes']['placeholder'][] = t('Write your e-mail');
  $form['#attributes']['class'][] = 'simplenews';

  if ($form['mail']['#type'] == 'textfield') {
    $mail = $form['mail'];
    $submit = $form['submit'];

    unset($form['mail']);
    unset($form['submit']);

    $submit['#value_prefix'] = '<span class="fa fa-paper-plane"></span>';
    $submit['#value_replace'] = '';
    $submit['#button_tag'] = true;

    $form['mail'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('input-group')
      ),
      'button' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('input-group-btn'),
        ),
        'submit' => $submit,
      ),
      'mail' => $mail,
    );
  }
}

/**
 * Implements hook_preprocess_menu_block_wrapper().
 */
function iela_theme_preprocess_menu_block_wrapper(&$variables) {
  if (!empty($variables['content']['#block_wrapper_classes_array'])) {
    foreach ($variables['content']['#block_wrapper_classes_array'] as $class) {
      $variables['classes_array'][] = $class;
    }
  }
}

/**
 * Implements template_preprocess_link(&$variables).
 */
function iela_theme_preprocess_link(&$variables) {
  if ($variables['path'] == '<nolink>' && !empty($variables['options']['fragment'])) {
    $variables['path'] = '';
    $variables['options']['external'] = TRUE;
  }
}

/**
 * Implements template_preprocess_file().
 */
function iela_theme_preprocess_file_entity(&$variables) {
  if (!empty($variables['content']['file']['#theme'])) {
    $file_content = $variables['content']['file'];
    $theme = $file_content['#theme'];

    // Handle wrapping links.
    if ($theme == 'link' && is_array($file_content['#text']) && !empty($file_content['#text']['#theme'])) {
      $theme = $file_content['#text']['#theme'];
    }

    // Theme identification.
    $variables['classes_array'][] = 'file-content-theme-' . drupal_html_class($theme);
  }
}
