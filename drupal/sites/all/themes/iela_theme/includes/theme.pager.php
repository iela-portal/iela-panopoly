<?php
/**
 * @file
 * Contains the theme's theme replacements for pagers.
 */

/**
 * Overrides theme_pager().
 */
function iela_theme_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Load pager script.
  drupal_add_js(drupal_get_path('theme', 'iela_theme') . '/js/pager.js');

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => '', 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array(
    'text' => '', 
    'element' => $element, 
    'interval' => 1, 
    'parameters' => $parameters,
    'attributes' => array('class' => array('glyphicon', 'glyphicon-chevron-left')),
  ));
  $li_next = theme('pager_next', array(
    'text' => '', 
    'element' => $element, 
    'interval' => 1, 
    'parameters' => $parameters,
    'attributes' => array('class' => array('glyphicon', 'glyphicon-chevron-right')),
  ));
  $li_last = theme('pager_last', array('text' => '', 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {

    $items[] = array(
      'class' => array(
        'pager-first',
        $pager_page_array[$element] > 0 ? '' : 'disabled',
      ),
      'data' => $li_first,
    );

    $items[] = array(
      'class' => array(
        'pager-previous',
        $pager_page_array[$element] > 0 ? '' : 'disabled',
      ),
      'data' => $li_previous,
    );

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('active'),
            'data' => '<span>' . $i . '</span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    $items[] = array(
      'class' => array(
        'pager-next',
        $pager_page_array[$element] < ($pager_total[$element] - 1) ? '' : 'disabled',
      ),
      'data' => $li_next,
    );

    $items[] = array(
      'class' => array(
        'pager-last',
        $pager_page_array[$element] < ($pager_total[$element] - 1) ? '' : 'disabled',
      ),
      'data' => $li_last,
    );

    return '<div class="text-center">' . theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pagination', 'pager')),
    )) . '</div>';
  }
}

/**
 * Overrides theme_pager_first().
 */
function iela_theme_pager_first($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];
  global $pager_page_array;
  $output = '';

  $output = theme('pager_link', array(
    'text' => $text,
    'page_new' => pager_load_array(0, $element, $pager_page_array),
    'element' => $element,
    'parameters' => $parameters,
    'attributes' => $attributes,
  ));

  return $output;
}

/**
 * Overrides theme_pager_previous().
 */
function iela_theme_pager_previous($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];
  global $pager_page_array;
  $output = '';

  $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

  // If the previous page is the first page, mark the link as such.
  if ($page_new[$element] == 0) {
    $output = theme('pager_first', array(
      'text' => $text, 
      'element' => $element, 
      'parameters' => $parameters,
      'attributes' => $attributes,
    ));
  }
  // The previous page is not the first page.
  else {
    $output = theme('pager_link', array(
      'text' => $text, 
      'page_new' => $page_new, 
      'element' => $element, 
      'parameters' => $parameters,
      'attributes' => $attributes,
    ));
  }

  return $output;
}

/**
 * Overrides theme_pager_next().
 */
function iela_theme_pager_next($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $interval = $variables['interval'];
  $parameters = $variables['parameters'];
  $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];
  global $pager_page_array, $pager_total;
  $output = '';

  $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
  // If the next page is the last page, mark the link as such.
  if ($page_new[$element] == ($pager_total[$element] - 1)) {
    $output = theme('pager_last', array(
      'text' => $text, 
      'element' => $element, 
      'parameters' => $parameters,
      'attributes' => $attributes,
    ));
  }
  // The next page is not the last page.
  else {
    $output = theme('pager_link', array(
      'text' => $text, 
      'page_new' => $page_new, 
      'element' => $element, 
      'parameters' => $parameters,
      'attributes' => $attributes,
    ));
  }

  return $output;
}

/**
 * Overrides theme_pager_last().
 */
function iela_theme_pager_last($variables) {
  $text = $variables['text'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];
  global $pager_page_array, $pager_total;
  $output = '';

  $output = theme('pager_link', array(
    'text' => $text, 
    'page_new' => pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), 
    'element' => $element,
    'parameters' => $parameters,
    'attributes' => $attributes,
  ));

  return $output;
}

/**
 * Overrides theme_views_load_more_pager().
 */
function iela_theme_views_load_more_pager($vars) {
  global $pager_page_array, $pager_total;

  $tags = $vars['tags'];
  $element = $vars['element'];
  $parameters = $vars['parameters'];
  $attributes = empty($variables['attributes']) ? array() : $variables['attributes'];

  $pager_classes = array('pagination', 'pager', 'pager-load-more');

  $li_next = theme('pager_next',
    array(
      'text' => (isset($tags[3]) ? $tags[3] : t($vars['more_button_text'])),
      'element' => $element,
      'interval' => 1,
      'parameters' => $parameters,
      'attributes' => $attributes,
    )
  );
  if (empty($li_next)) {
    $li_next = empty($vars['more_button_empty_text']) ? '&nbsp;' : t($vars['more_button_empty_text']);
    $pager_classes[] = 'pager-load-more-empty';
  }
  // Compatibility with tao theme's pager
  elseif (is_array($li_next) && isset($li_next['title'], $li_next['href'], $li_next['attributes'], $li_next['query'])) {
    $li_next = l($li_next['title'], $li_next['href'], array('attributes' => $li_next['attributes'], 'query' => $li_next['query']));
  }

  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => array('pager-next'),
      'data' => $li_next,
    );
    return theme('item_list',
      array(
        'items' => $items,
        'title' => NULL,
        'type' => 'ul',
        'attributes' => array('class' => $pager_classes),
      )
    );
  }
}
