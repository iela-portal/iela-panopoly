<?php
/**
 * @file
 * Contains the theme's Bootstrap navbar theme implementation.
 */

/**
 * Process Bootstrap Navbar.
 */
function iela_theme_process_bootstrap_navbar(&$variables) {

  // Avoid unsupported operations.
  if (empty($variables['element'])) $variables['element'] = array();

  $element = &$variables['element'];

  // Avoid unsupported operation. 
  if (empty($element['navbar'])) $element['navbar'] = array();
  if (empty($element['content'])) $element['content'] = array();
  if (empty($element['header'])) $element['header'] = array();
  if (empty($element['type'])) $element['type'] = 'default';

  $element['navbar'] = array_merge_recursive(array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'navbar',
        'navbar-' . $element['type'],
      ),
    ),
    'container' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container',
        ),
      ),
      'header' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array(
            'navbar-header',
          ),
        ),
        'toggle' => array(
          '#type' => 'html_tag',
          '#tag' => 'button',
          '#attributes' => array(
            'type' => 'button',
            'class' => array('navbar-toggle'),
            'data-toggle' => 'collapse',
            'data-target' => '.navbar-collapse',
            'iela-data-target-context' => '.navbar',
          ),
          '#value' => 
            '<span class="sr-only">' . t('Toggle navigation') . '</span>' .
            '<span class="icon-bar"></span>' .
            '<span class="icon-bar"></span>' .
            '<span class="icon-bar"></span>',
        ),
      ),
      'collapse' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('navbar-collapse', 'collapse'),
        ),
      ),
    ),
  ), $element['navbar']);

  // Merge pre-content.
  $element['navbar']['container']['collapse'] = array_merge_recursive(
    $element['navbar']['container']['collapse'],
    $element['content']
  );

  // Merge pre-content.
  $element['navbar']['container']['header'] = array_merge_recursive(
    $element['navbar']['container']['header'],
    $element['header']
  );

  // Make easy access.
  $element['content'] = &$element['navbar']['container']['collapse'];
  $element['header'] = &$element['navbar']['container']['header'];
}

/**
 * Implements Bootstrap Navbar theme.
 */
function iela_theme_bootstrap_navbar($variables) {
  $element = &$variables['element'];
  $brand   = &$element['navbar']['container']['header']['brand'];

  // Avoid brand part with no link.
  if (empty($brand['#text'])) {
    unset($brand);
  } else {
    if (empty($brand['#path'])) {
      $brand['#type'] = 'html_tag';
      $brand['#tag'] = 'span';
      $brand['#value'] = empty($brand['#text']) ? '' : $brand['#text'];
      $brand['#attributes'] = empty($brand['#options']['attributes']) ? array() : $brand['#options']['attributes'];
    }
  }

  return drupal_render($element['navbar']);
}
