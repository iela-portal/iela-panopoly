/**
 * @file
 * Tabs Region Style JavaScript activation.
 */

;(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.tabsRegionStyle = {
  attach: function (context, settings) {
    if (!settings.tabsRegionStyle) return;

    $.each(settings.tabsRegionStyle, function (regionId, options) {

      var $regionWrapper = $(context).find(regionId)
        , isIPE = Drupal.PanelsIPE && $regionWrapper.find('.panels-ipe-region').length
        , containerSelector = isIPE ? '.panels-ipe-sort-container' : '.panel-panel-inner'
        , paneSelector = isIPE ? '.panels-ipe-portlet-wrapper' : '.panel-pane-wrapper';

      // @alert: region is acctually region's inner.
      var $region      = $regionWrapper.findExclude(containerSelector, '.panel-display')
        , $panes       = $region.findExclude(paneSelector, '.panel-display')
        , $header      = $('<ul />', {
          'class': [
            'nav',
            'tabs-region-tabs',
            options.style || 'nav-tabs',
            options.justify ? 'nav-justified' : ''
          ].join(' ')
        })
        , $content     = $('<div />', { 'class': 'tab-content' })
        , invalid      = false
        , regionIdAttr = regionId.replace('#', '')
        , $tabs;

      // Invalid cases for tabs.
      if (!$region.length || $panes.length <= 1) return;

      $panes.each(function (index) {
        var $pane = $(this)
          , $title = $pane.findExclude('.pane-title', '.panel-display')
          , $titleLink = $title.find('a')
          , id = $pane.attr('id') || ($pane.attr('id', regionIdAttr + '-' + index)).attr('id')
          , $tabLink = $('<a />').attr('href', '#' + id);

        // Handle title links.
        if ($titleLink.length) {
          // Hold anchor DOM and keep only text.
          $title.html($titleLink.detach().contents());

          $tabLink.append($('<span />').addClass('fa-external-link fa').on('click', function (e) {
            fakeClick(e.originalEvent, $titleLink.get(0));
          }));
        }

        $tabLink.prepend($title.contents());

        // Invalid cases for tabs.
        if (!id || !$title.length) return invalid = true;

        // Make reliable copy of title's attributes.
        $.each($title[0].attributes, function (index, attr) {
          $tabLink.attr(attr.nodeName, attr.value || attr.nodeValue);
        });

        $header.append($('<li />').append($tabLink));
      }).addClass('clearfix');

      // Avoid odd behavior.
      if (invalid) return;

      // Remove old titles - but only if we know tabs are valid.
      $panes
        .addClass('tab-pane')
        .findExclude('.pane-title', '.panel-display').detach();

      // Mount markup
      $region
        .append($header)
        .append($content.append($panes));

      // Activate Bootstrap plugin.
      $header.find('a').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).tab('show');
      }).eq(options.active).tab('show');
    });
  }
};

/**
 * Performs a default click.
 */
function fakeClick(event, anchorObj) {
  if (anchorObj.click) {
    anchorObj.click()
  } else if(document.createEvent) {
    if(event.target !== anchorObj) {
      var evt = document.createEvent("MouseEvents"); 
      evt.initMouseEvent("click", true, true, window, 
          0, 0, 0, 0, 0, false, false, false, false, 0, null); 
      var allowDefault = anchorObj.dispatchEvent(evt);
      // you can check allowDefault for false to see if
      // any handler called evt.preventDefault().
      // Firefox will *not* redirect to anchorObj.href
      // for you. However every other browser will.
    }
  }
}

})(jQuery, Drupal, this, this.document);
