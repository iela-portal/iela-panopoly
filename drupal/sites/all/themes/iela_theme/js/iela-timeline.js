/**
 * @file
 * IELA TimelineJS extends.
 */

;(function ($, Drupal, window, document, undefined) {

  /**
   * Override timelineJS behavior.
   */
  Drupal.behaviors.timelineJS = {
    attach: function(context, settings) {
      $.each(Drupal.settings.timelineJS, function(key, timeline) {
        var id = '#' + timeline['embed_id']
          , container = $(context).find(id);

        if (container.length && !container.hasClass('processed-timeline-js')) {
          container.addClass('processed-timeline-js');

          if (context === '#modalContext' || Drupal.PanelsIPE) {
            container.append($('<div />', {
              'class': 'admin-placeholder',
              'text': Drupal.t('Placeholder for @thing.', {
                '@thing': 'TimelineJS pane (id: ' + timeline['embed_id'] +  ')'
              })
            }));
          } else {
            embed_path = timeline['embed_path'];

            if (timeline['processed'] != true) {
              createStoryJS(timeline);
              $('script[src*="timeline-min.js"]').on('load', extendTimelineJS.bind(null, id));
            }
            timeline['processed'] = true;
          }
        }
      });

      $(context).filter('.slider-item').each(function () {
        var $slide = $(this)
          , $image = $slide.find('img[data-original-image]')
          , originalImage;

        // Run after other behaviors.
        if ($image.length) setTimeout(function () {
          if ((originalImage = $image.data('original-image')) && originalImage.src) {
            var maxHeight = parseFloat($image.css('max-height'))
              , maxWidth = parseFloat($image.css('max-width'))
              , width = originalImage.width
              , height = originalImage.height;

            // Default behavior, when no max is defined.
            if (!maxHeight || !maxWidth) return $image.width(width);

            // Handle max height proportion scaling.
            if (maxHeight && height && (maxHeight < height)) {
              width = (width * maxHeight) / height;
              height = maxHeight;
            }

            // Handle max width proportion scaling.
            if (maxWidth && width && (maxWidth < width)) {
              height = (height * maxWidth) / width;
              width = maxWidth;
            }

            // Modify element width.
            if (width) $image.width(width);
          }
        }, 10);
      });
    }
  }

  /**
   * Override core TimelineJS methods.
   */
  function extendTimelineJS(timelineID) {

    var $timeline = $(timelineID)
      , $slider = null;

    VMM.bindEvent(global, onDataReady, 'DATAREADY');

    /**
     * Allow usage of custom html content.
     */
    VMM.MediaElement.create = function(data, uid) {
      var _valid = false,
        //loading_messege     = "<span class='messege'><p>" + VMM.master_config.language.messages.loading + "</p></span>";
        loading_messege     = VMM.MediaElement.loadingmessage(VMM.master_config.language.messages.loading + "...");

      if (data.media != null && data.media != "") {
        var mediaElem = "", captionElem = "", creditElem = "", _id = "", isTextMedia = false, m;
        
        m = VMM.MediaType(data.media); //returns an object with .type and .id
        m.uid = uid;
        _valid = true;
        
      // CREDIT
        if (data.credit != null && data.credit != "") {
          creditElem      = "<div class='credit'>" + VMM.Util.linkify_with_twitter(data.credit, "_blank") + "</div>";
        }
      // CAPTION
        if (data.caption != null && data.caption != "") {
          captionElem     = "<div class='caption'>" + VMM.Util.linkify_with_twitter(data.caption, "_blank") + "</div>";
        }
      // IMAGE
        if (m.type        ==  "image") {

          mediaElem  = '<div class="media-image media-shadow">';
          mediaElem += m.id.indexOf('<img') === 0 ? m.id : '<img src="' + m.id + '" class="media-image">';
          mediaElem += '</div>';

      // FLICKR
        } else if (m.type   ==  "flickr") {
          //mediaElem     = "<div class='media-image media-shadow' id='" + uid + "'>" + loading_messege + "</div>";
          mediaElem     = "<div class='media-image media-shadow'><a href='" + m.link + "' target='_blank'><img id='" + uid + "'></a></div>";
          VMM.ExternalAPI.flickr.get(m);
      // INSTAGRAM
        } else if (m.type   ==  "instagram") {
          mediaElem     = "<div class='media-image media-shadow'><a href='" + m.link + "' target='_blank'><img src='" + VMM.ExternalAPI.instagram.get(m) + "'></a></div>";
      // GOOGLE DOCS
        } else if (m.type   ==  "googledoc") {
          mediaElem     = "<div class='media-frame media-shadow doc' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.googledocs.get(m);
      // YOUTUBE
        } else if (m.type   ==  "youtube") {
          mediaElem     = "<div class='media-shadow'><div class='media-frame video youtube' id='" + m.uid + "'>" + loading_messege + "</div></div>";
          VMM.ExternalAPI.youtube.get(m);
      // VIMEO
        } else if (m.type   ==  "vimeo") {
          mediaElem     = "<div class='media-shadow media-frame video vimeo' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.vimeo.get(m);
      // DAILYMOTION
        } else if (m.type   ==  "dailymotion") {
          mediaElem     = "<div class='media-shadow'><iframe class='media-frame video dailymotion' autostart='false' frameborder='0' width='100%' height='100%' src='//www.dailymotion.com/embed/video/" + m.id + "'></iframe></div>";
      // VINE
        } else if (m.type   ==  "vine") {
          mediaElem     = "<div class='media-shadow media-frame video vine' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.vine.get(m);
      // TWITTER
        } else if (m.type   ==  "twitter"){
          mediaElem     = "<div class='twitter' id='" + m.uid + "'>" + loading_messege + "</div>";
          isTextMedia     = true;
          VMM.ExternalAPI.twitter.get(m);
      // TWITTER
        } else if (m.type   ==  "twitter-ready") {
          isTextMedia     = true;
          mediaElem     = m.id;
      // SOUNDCLOUD
        } else if (m.type   ==  "soundcloud") {
          mediaElem     = "<div class='media-frame media-shadow soundcloud' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.soundcloud.get(m);
      // GOOGLE MAPS
        } else if (m.type   ==  "google-map") {
          mediaElem     = "<div class='media-frame media-shadow map' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.googlemaps.get(m);
      // GOOGLE PLUS
        } else if (m.type   ==  "googleplus") {
          _id         = "googleplus_" + m.id;
          mediaElem     = "<div class='googleplus' id='" + _id + "'>" + loading_messege + "</div>";
          isTextMedia     = true;
          VMM.ExternalAPI.googleplus.get(m);
      // WIKIPEDIA
        } else if (m.type   ==  "wikipedia") {
          mediaElem     = "<div class='wikipedia' id='" + m.uid + "'>" + loading_messege + "</div>";
          isTextMedia     = true;
          VMM.ExternalAPI.wikipedia.get(m);
      // STORIFY
        } else if (m.type   ==  "storify") { 
          isTextMedia     = true;
          mediaElem     = "<div class='plain-text-quote'>" + m.id + "</div>";
      // IFRAME
        } else if (m.type   ==  "iframe") { 
          isTextMedia     = true;
          mediaElem     = "<div class='media-shadow'><iframe class='media-frame video' autostart='false' frameborder='0' width='100%' height='100%' src='" + m.id + "'></iframe></div>";
      // QUOTE
        } else if (m.type   ==  "quote") { 
          isTextMedia     = true;
          mediaElem     = "<div class='plain-text-quote'>" + m.id + "</div>";
      // UNKNOWN
        } else if (m.type   ==  "unknown") { 
          trace("NO KNOWN MEDIA TYPE FOUND TRYING TO JUST PLACE THE HTML"); 
          isTextMedia     = true;
          mediaElem     = "<div class='plain-text'><div class='container'>" + VMM.Util.properQuotes(m.id) + "</div></div>";
      // WEBSITE
        } else if (m.type   ==  "website") { 
          
          mediaElem     = "<div class='media-shadow website' id='" + m.uid + "'>" + loading_messege + "</div>";
          VMM.ExternalAPI.webthumb.get(m);
          //mediaElem     = "<div class='media-shadow website'><a href='" + m.id + "' target='_blank'>" + "<img src='http://api1.thumbalizr.com/?url=" + m.id.replace(/[\./]$/g, "") + "&width=300' class='media-image'></a></div>";
          
      // NO MATCH
        } else {
          trace("NO KNOWN MEDIA TYPE FOUND");
          trace(m.type);
        }
        
      // WRAP THE MEDIA ELEMENT
        mediaElem       = "<div class='media-container' >" + mediaElem + creditElem + captionElem + "</div>";
      // RETURN
        if (isTextMedia) {
          return "<div class='text-media'><div class='media-wrapper'>" + mediaElem + "</div></div>";
        } else {
          return "<div class='media-wrapper'>" + mediaElem + "</div>";
        }
        
      };
    };

    /**
     * Dataready event listener.
     */
    function onDataReady() {
      $slider = $timeline.find('.vco-slider');
      VMM.bindEvent($slider, onMessage, 'MESSAGE');
    }

    /**
     * Update/Load event listener.
     */
    function onMessage(e, message) {
      if (message && message == 'TEST') {
        // Unfortunetly, timelinejs sucks.
        setTimeout(function () {
          $slider.find('.slider-item').filter(function () {
            return this.innerHTML ? true : false;
          }).once('iela-timeline', function () {
            Drupal.attachBehaviors(this, Drupal.settings);
          });
        }, 1200);
      }
    }

    // Override dumb method.
    Array.prototype.remove = function (from, to) {
      if (!this || !this.slice) {
        return this;
      }
      var rest = this.slice((to || from) + 1 || this.length);
      this.length = from < 0 ? this.length + from : from;
      return this.push.apply(this, rest);
    };
  };

})(jQuery, Drupal, this, this.document);
