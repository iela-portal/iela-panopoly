/**
 * @file
 * Views Accordion Pane Style JavaScript activation.
 */

;(function ($, Drupal, window, document, undefined) {

var defaults = {
  header: '.view-content-title',
  icons: false,
  beforeActivate: function (e, ui) {
    ui.newHeader.find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
    ui.oldHeader.find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');

    $('.active-accordion-view-content', this).removeClass('active-accordion-view-content');
    ui.newHeader.closest('.panel-pane').addClass('active-accordion-view-content');
  },
  animate: {
    easing: 'easeOutQuint',
    duration: 600
  }
};

var removeClasses = {
  title: 'ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons',
  content: 'ui-accordion-content ui-accordion ui-widget ui-helper-reset ui-widget-content ui-corner-bottom'
};

Drupal.behaviors.viewsAccordionPaneStyle = {
  attach: function (context, settings) {
    if (!settings.viewsAccordionPaneStyle) return;

    $.each(settings.viewsAccordionPaneStyle, function (id, options) {
      if (!id.match(/^#[a-zA-Z][\w:.-]*$/)) return true;
      
      var $pane = $(context).find(id)
        , $titles = $pane.find('.view-content-title');

      if ($pane.length && $titles.length) {

        // Apply jQuery plugin:
        $pane.accordion($.extend(true, {}, defaults, options));

        // Remove dumb classes:
        $titles.removeClass(removeClasses.title);
        $pane.removeClass(removeClasses.content);
        $pane.find('.view-content-inner').removeClass(removeClasses.content);

        // Add icons:
        $titles.prepend($('<i />', {
          'class': 'fa fa-chevron-down'
        }));

        // Add active icons:
        $titles
          .filter('.ui-state-active')
          .find('i.fa-chevron-down')
          .removeClass('fa-chevron-down')
          .addClass('fa-chevron-up')
          .closest('.view-content-outer').addClass('active-accordion-view-content');
      }
    });
  }
};

})(jQuery, Drupal, this, this.document);
