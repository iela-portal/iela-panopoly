/**
 * @file
 * Inserts Smooth Scroll.
 */

;(function ($, Drupal, window, document, undefined) {

// Map tracked items.
var tracked = {}
  , listening = false
  , url = window.location.origin + window.location.pathname;

/**
 * Starts listening.
 */
function startListener() {
  // Start only once.
  if (!listening) {
    listening = true;
    $(window).on('scroll', scrolled).trigger('scroll');
  }
}

/**
 * Executed on every scrolling.
 */
function scrolled(e) {
  $.each(tracked, update);
}

/**
 * Update anchor states for tracked element.
 */
function update(id, info) {
  var offset = info.element.offset()
    , scrollTop = window.scrollY;

  offset.bottom = offset.top + info.element.height();
  offset.top -= parseInt(info.options.distance) || 0;

  var inside = scrollTop >= offset.top && scrollTop < offset.bottom;

  // Inside
  if (!info.active && inside) {
    info.active = true;
    info.anchors.addClass('active').parent('li').addClass('active');
  }
  // Outside
  else if (info.active && !inside) {
    info.active = false;
    info.anchors.removeClass('active').parent('li').removeClass('active');
  }
}


Drupal.behaviors.anchorUpdate = {
  attach: function (context, settings) {

    // Avoid dumb processing.
    if (settings.anchorUpdate) {

      // Parse context.
      context = $(context);

      // Activate new settings.
      $.each(settings.anchorUpdate, function (id, options) {
        if (!id.match(/^#[a-zA-Z][\w:.-]*$/)) return true;

        var element = context.find(id);
        if (element.length) {
          tracked[id] = {
            options: options,
            element: element.addClass('clearfix'),
            anchors: $('a[href*="' + id + '"]').filter(function () {
              return this.href.replace(url, '') == this.hash;
            })
          }
        }
      });

      // Start heavy listener.
      startListener();
    }
  }
};

})(jQuery, Drupal, this, this.document);
