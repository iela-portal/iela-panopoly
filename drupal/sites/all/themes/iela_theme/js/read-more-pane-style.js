/**
 * @file
 * Enabled Read More jQuery plugin on panes.
 */

;(function ($, Drupal, window, document, undefined) {

var defaults = {
  speed: 250
, collapsedHeight: 200
, heightMargin: '1em'
, moreLink: Drupal.t('Read more')
, lessLink: Drupal.t('Close')
};

Drupal.behaviors.readMorePaneStyle = {
  attach: function (context, settings) {

    if (!settings.readMorePaneStyle || !$.fn.readmore) return;

    // Parse context.
    var $context = $(context);

    $.each(settings.readMorePaneStyle, function (selector, options) {
      $context.find(selector).each(function () {
        var $collapstible = $(options.blockSelector, this)
          , configuration = $.extend({}, defaults, options);

        if (options.lines) {
          var lineHeight = parseFloat($collapstible.css('line-height'))
            , maxHeight = lineHeight * parseInt(options.lines) - (lineHeight / 3);

          if (maxHeight) configuration.collapsedHeight = maxHeight;
        }

        $collapstible.readmore(configuration);

        window.addEventListener('load', function() {
          var $link = $collapstible.next('.read-more')
            , $linkWrapper = $('<div />', {
                'class': 'read-more-wrapper'
              });

          $link.before($linkWrapper).appendTo($linkWrapper);
        });
      });
    });
  }
};

})(jQuery, Drupal, this, this.document);
