/**
 * @file
 * Generic JavaScript adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.ielaDraggableTable = {
  attach: function (context, settings) {

    // Parse context.
    var $context = $(context);

    $context.find('.field-multiple-drag').each(function () {
      var $td = $(this);
      setTimeout(function () {
        $td.find('.tabledrag-handle .handle').addClass('glyphicon glyphicon-move');
      }, 10);
    });
  }
};

Drupal.behaviors.ielaAdminPanelsMiniIpeLink = {
  attach: function (context, settings) {

    // Add Mini Panels edit to pane settings bar.
    $('.panels-ipe-display-container .edit-mini-panel-link', context).once('iela-admin-panels-mini-ipe-link', function () {
      var $button         = $(this)
        , $portlet        = $button.closest('.panels-ipe-portlet-wrapper')
        , $settingsList   = $portlet.find('.panels-ipe-handlebar-wrapper .panels-ipe-linkbar')
        , $listItem       = $('<li />', {
            'class': 'mini-panel last',
          });

      // Adjust button.
      $button
        .text($button.text() + ' ' + Drupal.t('Mini Panel'))
        .attr('target', '_blank');

      // Update class.
      $settingsList.find('> .last').removeClass('last');

      // Append new item.
      $settingsList.append($listItem.append($button));
    });

    // Adjust appearence of Mini Panels edit button for blocks.
    $('.block-panels-mini .edit-mini-panel-link', context).once('iela-admin-panels-mini-ipe-link', function () {
      $(this).addClass('btn btn-default btn-sm');
    });
  }
};

})(jQuery, Drupal, this, this.document);
