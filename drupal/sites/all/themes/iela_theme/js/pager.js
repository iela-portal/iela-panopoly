/**
 * @file
 * Pager JavaScript adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.ielaPager = {
  attach: function (context, settings) {
    $(context).find('.pagination .disabled a').each(function () {
      var $link = $(this)
        , $span = $('<span />');

      // Copy all attributes.
      $.each($link.prop('attributes'), function () {
        $span.attr(this.name, this.value);
      });

      $link.replaceWith($span);
    });
  }
};

})(jQuery, Drupal, this, this.document);
