/**
 * @file
 * iFrame Size Region Style JavaScript activation.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Resizer.
 */
function resize(iframe, width, height, updateSrc) {

  var src = iframe.attr('src')
    , regex;

  // First set heights.
  iframe.width(width);
  iframe.height(height);

  var curr = {
    width: iframe.width(),
    height: iframe.height()
  };

  try {
    if (parseFloat(width) == width) iframe.width(parseFloat(width) * curr.height);
    if (parseFloat(height) == height) iframe.height(parseFloat(height) * curr.width);

    for (var a = 0, attrs = ['height', 'width']; a < attrs.length; a++) {
      if (updateSrc && updateSrc[attrs[a]]) {
        regex = new RegExp('(' + updateSrc[attrs[a]] + '=)[^&]+');
        src = src.replace(regex, '$1' + iframe[attrs[a]]());
      }
    }

    if (iframe.attr('src') != src) iframe.attr('src', src);
  } catch(e) {
    // Avoid breaking errors.
  }
}

Drupal.behaviors.iFrameSizeRegionStyle = {
  attach: function (context, settings) {
    
    // Parse context.
    var context = $(context);

    if (Drupal.Breakpoints) {
      $.each(Drupal.settings.iFrameSizeRegionStyle, function(id, conf) {

        // Get element.
        var iframe = context.find(id).find('iframe')
          , updateSrc = conf.update_src ? {
              width: conf.src_width
            , height: conf.src_height
            } : null;

        // Only make heavy logic if found element.
        if (iframe.length) {
          resize(iframe, conf.width, conf.height, updateSrc);

          var reversedConf = [];

          $.each(conf.breakpoints, function (breakpoint, conf) {
            reversedConf.unshift({
              breakpoint: breakpoint,
              conf: conf
            });
          });

          $.each(reversedConf, function (i, conf) {
            Drupal.Breakpoints.register(conf.breakpoint, function () {
              resize(iframe, conf.conf.width, conf.conf.height, updateSrc);
            });
          });
        }
      });
    }
  }
};

})(jQuery, Drupal, this, this.document);
