/**
 * @file
 * Entityform pane.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Dismiss alert cover.
 */
function dismissCover() {
  $(this).closest('.alert-messages-cover').fadeOut(250, function () {
    $(this).addClass('hidden');
  });
}

Drupal.behaviors.ielaEntityFormAlertCover = {
  attach: function (context, settings) {
    $(context).filter('.alert-messages-container').each(function () {    
      var $container  = $(this)
        , $groups     = $container.find('.alert-messages')
        , $entityForm = $container.closest('.entity-entityform-type')
        , $dismiss;

      // Early return.
      if (!$entityForm.length) return;

      // Identify type of alert messages.
      $container.addClass('cover alert-messages-cover');

      // Add close button if all messages can be closes.
      if ($container.find('.alert').length == ($dismiss = $container.find('.close')).length) {
        $dismiss.on('click', function () {
          if ($container.find('.alert').length <= 1) dismissCover.call(this);
        });

        var $containerDismiss = $('<button type="button" class="close" aria-hidden="true">×</button>');
        $containerDismiss.on('click', dismissCover);
        $container.prepend($containerDismiss);
      }

      // Add a cover background.
      var color = $container.closestCss('background-color', ['rgba(0, 0, 0, 0)', 'transparent'], 'white');
      $container.prepend('<div class="cover background-cover" style="background-color: ' + color + ';"></div>');
    });
  }
};

})(jQuery, Drupal, this, this.document);
