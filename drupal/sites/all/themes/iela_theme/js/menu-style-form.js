/**
 * @file
 * Menu style form adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.menuPaneStyleForm = {
  attach: function (context, settings) {
    $('#edit-general-settings', context).once(function () {
      var $settings           = $(this)
        , $style              = $settings.find('#edit-settings-style')
        , $orientation        = $settings.find('#edit-settings-orientation')
        , $orientationItem    = $orientation.closest('.form-item')
        , $justify            = $settings.find('#edit-settings-justified')
        , $justifyItem        = $justify.closest('.form-item')
        , $categorized        = $settings.find('#edit-settings-categorized')
        , $categorizedItem    = $categorized.closest('.form-item')
        , $categoriesFieldset = $settings.find('#edit-settings-categories')
        , $navbarFieldset     = $settings.find('#edit-settings-navbar')
        , $sticky             = $settings.find('#edit-settings-navbar-sticky')
        , $stickyAlone        = $settings.find('#edit-settings-navbar-sticky-alone')
        , $stickyAloneItem    = $stickyAlone.closest('.form-item')
        , $stickyWeight       = $settings.find('#edit-settings-navbar-sticky-weight')
        , $stickyWeightItem   = $stickyWeight.closest('.form-item');

      /**
       * Update visibilities.
       */
      function updateFields() {
        var style       = $style.find('option:selected').val()
          , orientation = $orientation.find('option:selected').val()
          , categorized = $categorized.is(':checked')
          , sticky      = $sticky.is(':checked');

        // Default state.
        $orientationItem.hide();
        $justifyItem.hide();
        $categorizedItem.hide();
        $categoriesFieldset.detach();
        $navbarFieldset.detach();
        $stickyAloneItem.hide();
        $stickyWeightItem.hide();

        if (style == 'pills') $orientationItem.show();
        if (style == 'tabs' || (style == 'pills' && orientation == 'horizontal')) $justifyItem.show();
        if (style == 'links') $categorizedItem.show();
        if (style == 'links' && categorized) $categorizedItem.after($categoriesFieldset);
        if (style == 'navbar') $justifyItem.after($navbarFieldset);
        if (sticky) {
          $stickyAloneItem.show();
          $stickyWeightItem.show();
        }
      }

      $style.on('change', updateFields);
      $orientation.on('change', updateFields);
      $categorized.on('change', updateFields);
      $sticky.on('change', updateFields);

      updateFields();
    });
  }
};

})(jQuery, Drupal, this, this.document);
