/**
 * @file
 * Accordion Region Style JavaScript activation.
 */

;(function ($, Drupal, window, document, undefined) {

var defaults = {
  header: '.pane-title',
  icons: false,
  beforeActivate: function (e, ui) {
    ui.newHeader.find('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
    ui.oldHeader.find('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');

    ui.newHeader.closest('.panel-pane').addClass('active-accordion-pane');
    ui.oldHeader.closest('.panel-pane').removeClass('active-accordion-pane');
  },
  animate: {
    easing: 'easeOutQuint',
    duration: 600
  }
};

var removeClasses = {
  title: 'ui-accordion-header ui-helper-reset ui-state-default ui-corner-all ui-accordion-icons',
  content: 'ui-accordion-content ui-accordion ui-widget ui-helper-reset ui-widget-content ui-corner-bottom'
};

Drupal.behaviors.accordionRegionStyle = {
  attach: function (context, settings) {
    if (!settings.accordionRegionStyle) return;

    $.each(settings.accordionRegionStyle, function (region, options) {
      var $region = $(context).find('#' + region)
        , $titles = $region.findExclude('.pane-title', '.panel-display');

      // Handle title links.
      $titles.each(function () {
        var $title = $(this)
          , $link = $title.find('a')
          , href = $link.attr('href');

        if (!$link.length) return;

        // Hold link DOM and keep only text.
        $title.html($link.detach().contents());

        $title.append($('<span />').addClass('fa-external-link fa').on('click', function (e) {
          fakeClick(e.originalEvent, $link.get(0));
        }));
      });

      if ($region.length) {

        // Apply jQuery plugin:
        $region.accordion($.extend(true, {}, defaults, options));

        // Remove dumb classes:
        $titles.removeClass(removeClasses.title);
        $region.removeClass(removeClasses.content);
        $region.findExclude('.pane-content', '.panel-display').removeClass(removeClasses.content);

        // Add icons:
        $titles.prepend($('<i />', {
          'class': 'fa fa-chevron-down'
        }));

        // Add active icons:
        $titles
          .filter('.ui-state-active')
          .find('i.fa-chevron-down')
          .removeClass('fa-chevron-down')
          .addClass('fa-chevron-up')
          .closest('.panel-pane').addClass('active-accordion-pane');
      }
    });
  }
};

/**
 * Performs a default click.
 */
function fakeClick(event, anchorObj) {
  if (anchorObj.click) {
    anchorObj.click()
  } else if(document.createEvent) {
    if(event.target !== anchorObj) {
      var evt = document.createEvent("MouseEvents"); 
      evt.initMouseEvent("click", true, true, window, 
          0, 0, 0, 0, 0, false, false, false, false, 0, null); 
      var allowDefault = anchorObj.dispatchEvent(evt);
      // you can check allowDefault for false to see if
      // any handler called evt.preventDefault().
      // Firefox will *not* redirect to anchorObj.href
      // for you. However every other browser will.
    }
  }
}

})(jQuery, Drupal, this, this.document);
