<?php
/**
 * @file
 * Theme settings.
 */

/**
 * Implements theme_settings().
 */
function iela_theme_form_system_theme_settings_alter(&$form, &$form_state) {
  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('theme', 'default') . '/theme-settings.php';

  // Add theme settings here.
  $form['iela_theme_settings'] = array(
    '#title' => t('IELA theme Settings'),
    '#type' => 'fieldset',
    '#weight' => -10,
  );

  $navbar = theme_get_setting('navbar');
  $form['iela_theme_settings']['navbar'] = array(
    '#title' => t('Navbar color scheme'),
    '#type' => 'select',
    '#description' => t('Choose the default navbar color scheme'),
    '#options' => array(
      'light' => t('Light'),
      'info' => t('Blue'),
      'default' => t('Green'),
      'danger' => t('Red'),
      'warning' => t('Yellow'),
      'dark' => t('Dark'),
      'info-dark' => t('Dark blue'),
      'default-dark' => t('Dark green'),
      'danger-dark' => t('Dark red'),
      'warning-dark' => t('Dark yellow'),
    ),
    '#default_value' => empty($navbar) ? 'dark' : $navbar,
  );

  $color_scheme = theme_get_setting('color_scheme');
  $form['iela_theme_settings']['color_scheme'] = array(
    '#title' => t('Default color scheme'),
    '#type' => 'select',
    '#description' => t('Choose the default color scheme for the site'),
    '#options' => array(
      'info' => t('Blue'),
      'default' => t('Green'),
      'danger' => t('Red'),
      'warning' => t('Yellow'),
    ),
    '#disabled' => true,
    '#default_value' => empty($color_scheme) ? 'default' : $color_scheme,
  );

  // Copyright.
  $copyright = theme_get_setting('copyright');
  $form['iela_theme_settings']['copyright'] = array(
    '#title' => t('Copyright'),
    '#type' => 'text_format',
    '#format' => $copyright['format'],
    '#default_value' => $copyright['value'] ? $copyright['value'] : t('Drupal is a registered trademark of Dries Buytaert.'),
  );

  // Return the additional form widgets.
  return $form;
}
