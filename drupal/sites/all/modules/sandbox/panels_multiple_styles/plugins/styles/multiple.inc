<?php

/**
 * Panels Multiple Styles plugin.
 */

$plugin = array(
  'title' => t('Multiple Styles'),

  // Region style.
  'render region' => 'multiple_region_style',
  'settings form' => 'multiple_style_settings_form',

  // Pane style.
  'render pane' => 'multiple_pane_style',
  'pane settings form' => 'multiple_style_settings_form',
  'pane preprocess alter' => 'multiple_pane_style_preprocess_alter',
  'pane process alter' => 'multiple_pane_style_process_alter',

  // Container preprocessing.
  'container preprocess' => 'multiple_style_preprocess_container',
);


// --------------------------------------------------------
// General
// --------------------------------------------------------

/**
 * Configuration form.
 */
function multiple_style_settings_form(&$settings, $display, $pid, $type, &$form_state) {

  if (!empty($_SESSION['pane_previous_style_settings'])) {
    $display->previous_style_settings = $_SESSION['pane_previous_style_settings'];
    unset($_SESSION['pane_previous_style_settings']);
  }

  // Handle previous single style settings.
  if (empty($settings) && !empty($display->previous_style_settings)) {
    foreach ($display->previous_style_settings as $style => $settings) {
      $settings[$style] = $settings;
    }

    // Make sure it is not kept when leaving.
    unset($display->previous_style_settings);
  }

  $styles = $form_state['input']['style'];

  if (empty($styles)) {
    foreach ($form_state['input']['settings'] as $name => $options) {
      $styles[$name] = $name;
    }
  }

  // Prepare basic form.
  $form = array();

  // Find the target's settings form property (region or pane style).
  $settings_form = $type == 'pane' ? 'pane settings form' : 'settings form';

  // Make it easy to understand concurrent styles.
  $form_state['multiple style'] = NULL;
  $form_state['multiple styles'] = array();

  // Fulfil form states and load plugins.
  foreach ($styles as $name) {
    $form_state['multiple styles'][$name] = array(
      'name' => $name,
      'plugin' => panels_get_style($name),
      'settings' => empty($settings[$name]) ? array() : $settings[$name],
    );
  }

  // Hold multiple_style info.
  $multiple_style_plugin_info = $form_state['style'];

  // Use each style's settings form, if they have one.
  foreach ($form_state['multiple styles'] as $name => &$info) {
    if (!empty($info['plugin'])) {

      // Identify current style.
      $form_state['multiple style'] = $name;
      $form_state['style'] = $info['plugin'];

      // Find the form function.
      $function = panels_plugin_get_function('styles', $info['plugin'], $settings_form);

      if ($function && function_exists($function)) {

        // Get current style configuration.
        $style_settings = empty($settings[$name]) ? array() : $settings[$name];

        // Construct style form inside a fieldset.
        $form[$name] = $function($style_settings, $display, $pid, $type, $form_state);
        $form[$name]['#type'] = 'fieldset';
        $form[$name]['#title'] = $info['plugin']['title'];
      } else {
        $form[$name] = array(
          '#type' => 'hidden',
          '#value' => null,
        );
      }
    }
  }

  // Cleaup states. Is it necessary?
  $form_state['style'] = $multiple_style_plugin_info;
  unset($form_state['multiple style']);
  // unset($form_state['multiple styles']);

  return $form;
}

/**
 * Implements 'fake' template_multiple_style_preprocess_container().
 */
function multiple_style_preprocess_container(&$variables) {

  global $theme;
  $region = $variables['region_id'];

  // Fullfill missing attribute.
  if (empty($variables['style'])) {
    $variables['style'] = panels_get_style('multiple');
  }

  // Fullfill missing attribute.
  if (empty($variables['settings'])) {
    $variables['settings'] = $variables['display']->panel_settings['style_settings'][$region];
  }

  // Hold loaded styles.
  $styles = array();

  // Load all styles.
  foreach ($variables['settings'] as $name => $config) {

    // Load style defition.
    $style = panels_get_style($name);

    if (!empty($style) && !empty($style['container preprocess'])) {
      $styles[] = array(
        'style' => $style,
        'config' => empty($config) ? array() : $config,
      );
    }
  }

  // Sort styles by weight, if given.
  usort($styles, function ($a, $b) {
    $weight_a = intval(empty($a['style']['weight']) ? 0 : $a['style']['weight']);
    $weight_b = intval(empty($b['style']['weight']) ? 0 : $b['style']['weight']);

    return $weight_a - $weight_b;
  });

  // Keep the original attributes.
  $original_style = $variables['style'];
  $original_settings = $variables['settings'];

  // Iterate and execute each style.
  foreach ($styles as $info) {

    // Easy access.
    $style = $info['style'];
    $name = $style['name'];

    // Make sure if a style settings is empty it is an empty array
    // and not an empty string, boolean, or whatever.
    if (empty($original_settings[$name])) {
      $original_settings[$name] = array();
    }

    // Prepare data to apply the template preprocessing.
    $variables['style'] = $style;
    $variables['settings'] = $original_settings[$name];

    // Construct the template preprocessing function name.
    $function = $style['container preprocess'];

    // Execute main template function.
    if (function_exists($function)) $function($variables);
  }

  // Reset original attributes.
  $variables['style'] = $original_style;
  $variables['settings'] = $original_settings;
}


// --------------------------------------------------------
// Panel Region
// --------------------------------------------------------

/**
 * Template preprocess for the region style.
 */
function template_preprocess_multiple_region_style(&$variables) {

  global $theme;
  $region = $variables['region_id'];

  // Fullfill missing attribute.
  if (empty($variables['style'])) {
    $variables['style'] = panels_get_style('multiple');
  }

  // Fullfill missing attribute.
  if (empty($variables['settings'])) {
    $variables['settings'] = $variables['display']->panel_settings['style_settings'][$region];
  }

  // Hold loaded styles.
  $styles = array();

  // Load all styles.
  foreach ($variables['settings'] as $name => $config) {

    // Load style defition.
    $style = panels_get_style($name);

    if (!empty($style)) {
      $styles[] = array(
        'style' => $style,
        'config' => empty($config) ? array() : $config,
      );
    }
  }

  // Sort styles by weight, if given.
  usort($styles, function ($a, $b) {
    $weight_a = intval(empty($a['style']['weight']) ? 0 : $a['style']['weight']);
    $weight_b = intval(empty($b['style']['weight']) ? 0 : $b['style']['weight']);

    return $weight_a - $weight_b;
  });

  // Keep the original attributes.
  $original_style = $variables['style'];
  $original_settings = $variables['settings'];

  // Iterate and execute each style.
  foreach ($styles as $info) {

    // Easy access.
    $style = $info['style'];
    $name = $style['name'];

    // Make sure if a style settings is empty it is an empty array
    // and not an empty string, boolean, or whatever.
    if (empty($original_settings[$name])) {
      $original_settings[$name] = array();
    }

    // Prepare data to apply the template preprocessing.
    $variables['style'] = $style;
    $variables['settings'] = $original_settings[$name];

    // Construct the template preprocessing function name.
    $function = 'template_preprocess_' . $style['render region'];

    // Execute main template function.
    if (function_exists($function)) $function($variables);

    // Execute implemented hooks.
    foreach (module_implements('preprocess_' . $style['render region']) as $module) {
      $function = $module . '_preprocess_' . $style['render region'];
      $function($variables);
    }

    // Execute implemented theme hooks.
    $theme_preprocess = $theme . '_preprocess_' . $style['render region'];
    if (function_exists($theme_preprocess)) $theme_preprocess($variables);
  }

  // Reset original attributes.
  $variables['style'] = $original_style;
  $variables['settings'] = $original_settings;
}

/**
 * Returns HTML for the region style.
 */
function theme_multiple_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}


// --------------------------------------------------------
// Panel Pane
// --------------------------------------------------------

/**
 * Template preprocess for the pane style.
 */
function template_preprocess_multiple_pane_style(&$variables) {

  global $theme;
  $pane = $variables['pane'];

  // Fullfill missing attribute.
  if (empty($variables['style'])) {
    $variables['style'] = panels_get_style('multiple');
  }

  // Fullfill missing attribute.
  if (empty($variables['settings'])) {
    $variables['settings'] = $pane->style['settings'];
  }

  $styles = array();

  // Load all styles.
  foreach ($variables['settings'] as $name => $config) {

    // Load style defition.
    $style = panels_get_style($name);

    if (!empty($style)) {
      $styles[] = array(
        'style' => $style,
        'config' => empty($config) ? array() : $config,
      );
    }
  }

  // Save for further usage.
  $variables['multiple_style_plugins'] = $styles;

  // Sort styles by weight, if given.
  usort($styles, function ($a, $b) {
    $weight_a = intval(empty($a['style']['weight']) ? 0 : $a['style']['weight']);
    $weight_b = intval(empty($b['style']['weight']) ? 0 : $b['style']['weight']);

    return $weight_a - $weight_b;
  });

  // Keep the original attributes.
  $original_style = $variables['style'];
  $original_settings = $variables['settings'];
  $original_pane_style = $pane->style;

  // Iterate and execute each style.
  foreach ($styles as $info) {

    // Easy access.
    $style = $info['style'];
    $name = $style['name'];

    // Make sure if a style settings is empty it is an empty array
    // and not an empty string, boolean, or whatever.
    if (empty($original_settings[$name])) {
      $original_settings[$name] = array();
    }

    // Prepare data for new preprocess cicle.
    $variables['style'] = $style;
    $variables['settings'] = $original_settings[$name];
    $variables['pane']->style['style'] = $name;
    $variables['pane']->style['settings'] = $original_settings[$name];

    // Construct the template preprocessing function name.
    $function = 'template_preprocess_' . $style['render pane'];

    // Execute main template function.
    if (function_exists($function)) $function($variables);

    // Execute implemented module hooks.
    foreach (module_implements('preprocess_' . $style['render pane']) as $module) {
      $function = $module . '_preprocess_' . $style['render pane'];
      $function($variables);
    }

    // Execute implemented theme hooks.
    $theme_preprocess = $theme . '_preprocess_' . $style['render pane'];
    if (function_exists($theme_preprocess)) $theme_preprocess($variables);
  }

  // Reset original attributes.
  $variables['style'] = $original_style;
  $variables['settings'] = $original_settings;
  $variables['pane']->style = $original_pane_style;
}

/**
 * Implement 'fake' template_preprocess_panels_pane_alter().
 */
function multiple_pane_style_preprocess_alter(&$variables) {

  // Make sure the pane will still have style-specific classes.
  foreach ($variables['multiple_style_plugins'] as $info) {

    // Easy access.
    $style = $info['style'];
    $name = $style['name'];

    $variables['classes_array'][] = 'style-' . drupal_html_class($name);
  }

  // Perform style's alters.
  _multiple_pane_style_processor_alter('preprocess', $variables);
}

/**
 * Implement 'fake' template_process_panels_pane_alter().
 */
function multiple_pane_style_process_alter(&$variables) {

  // Perform style's alters.
  _multiple_pane_style_processor_alter('process', $variables);
}

/**
 * Helper method to execut each style's custom alters.
 */
function _multiple_pane_style_processor_alter($type = 'process', &$variables) {

  $pane = $variables['pane'];
  $hook = "pane {$type} alter";

  // Keep the original attributes.
  $original_style = $variables['style'];
  $original_settings = $variables['settings'];
  $original_pane_style = $pane->style;

  foreach ($variables['multiple_style_plugins'] as $info) {

    // Easy access.
    $style = $info['style'];
    $name = $style['name'];
    if (!empty($style[$hook]) && function_exists($style[$hook])) {

      // Make sure if a style settings is empty it is an empty array
      // and not an empty string, boolean, or whatever.
      if (empty($original_settings[$name])) {
        $original_settings[$name] = array();
      }

      // Prepare data for new preprocess cicle.
      $variables['style'] = $style;
      $variables['settings'] = $original_settings[$name];
      $variables['pane']->style['style'] = $name;
      $variables['pane']->style['settings'] = $original_settings[$name];

      // Execute custom processor.
      $style[$hook]($variables);
    }
  }

  // Reset original attributes.
  $variables['style'] = $original_style;
  $variables['settings'] = $original_settings;
  $variables['pane']->style = $original_pane_style;
}

/**
 * Returns HTML for the pane style.
 */
function theme_multiple_pane_style(&$variables) {
  return theme('panels_pane', $variables);
}
