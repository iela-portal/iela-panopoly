<?php

/**
 * @file
 * Taller Forms alter implementation.
 */

/**
 * Implements hook_form_set_cache_form_state_alter().
 */
function taller_forms_form_set_cache_form_state_alter($type, &$data) {
  if (!empty($data['build_info']['form_id'])) {
    $safe_keys = array();

    switch ($data['build_info']['form_id']) {

      // Avoid caching Panels style plugins configuration.
      case 'panels_edit_style_settings_form':
        $safe_keys = array('conf');
        break;

      // Avoid caching CTools access plugins configuration.
      case 'ctools_access_ajax_edit_item':
      case 'ctools_access_ajax_add_item':
        $safe_keys = array('access', 'test');
        break;
    }

    foreach ($safe_keys as $safe_key) {
      if (!empty($data[$safe_key])) {
        $data['build_info'][$safe_key] = $data[$safe_key];
        unset($data[$safe_key]);
      }
    }
  }
}
