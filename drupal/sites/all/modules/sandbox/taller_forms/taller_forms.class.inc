<?php

/**
 * @file
 * Database Cache implementation for forms.
 */

class TallerFormCache extends DrupalDatabaseCache {

  /**
   * Possible target cid prefixes.
   * P.s.: should be ordered by length, so
   * that only the first match is processed.
   */
  protected $cid_prefixes = array(
    'form_state',
    'form',
  );

  /**
   * Implements DrupalCacheInterface::set().
   */
  function set($cid, $data, $expire = CACHE_PERMANENT) {
    foreach ($this->cid_prefixes as $prefix) {
      if (strpos($cid, $prefix) === 0) {
        $context = array(
          'cid' => $cid,
          'expire' => &$expire,
        );

        drupal_alter('form_set_cache', $prefix, $data, $context);
        drupal_alter('form_set_cache_' . $prefix, $prefix, $data, $context);
        break;
      }
    }

    parent::set($cid, $data, $expire);
  }
}
