<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Load all the information panels pane by id or load info. in certain fields.
 */
function iela_adaptative_panels_pane_load($pid, $fields = array()) {

  $query = db_select('panels_pane', 'pp')
    ->condition('pid', $pid, '=');

  if (!empty($fields)) {
    $query->fields('pp', $fields);
  }

  $pane = $query->execute()->fetchAssoc();

  if (array_search('configuration', $fields) && !empty($pane['configuration'])) {
    $pane['configuration'] = unserialize($pane['configuration']);
  }

  return $pane;
}

/**
 * Create fields for breakpoints.
 */
function iela_adaptative_panels_create_fields_settings_form($pid) {

  $form = array();

  // Load data adaptative_pane to set in default value.
  // The pid is numeric when updated a pane. When create
  // a new pane the pid isn't numeric. Eg.: pid = new-1.
  if (is_numeric($pid)) {
    $pane = iela_adaptative_panels_pane_load($pid, array('pid', 'configuration'));
  }
  else {
    $pane['configuration']['adaptative_pane'] = variable_get('iela_adaptative_panels_columns_' . $pid);
  }

  // FontAwesome icon classes.
  $icons = array(
    'xs' => 'mobile',
    'sm' => 'tablet',
    'md' => 'laptop',
    'lg' => 'desktop',
  );

  // Loop through each breakpoint and check for existing settings.
  foreach (breakpoints_breakpoint_load_all_theme() as $machine_name => $breakpoint) {

    $breakpoint_options = array(
      '#type' => 'fieldset',
      '#title' => "{$breakpoint->name} breakpoint",
      '#description' => "{$breakpoint->breakpoint}",
      '#tree' => TRUE,
      'columns' => array(
        '#type' => 'select',
        '#title' => t('Columns'),
        '#description' => t('Set how many columns (of 12) it will occupy. Choose "12" to occupy all available space.'),
        '#options' => array(
          '<none>' => t('Hidden'),
        ),
        '#default_value' => 12,
      ),
      'new_line' => array(
        '#type' => 'checkbox',
        '#title' => t('Force new line'),
        '#default_value' => FALSE,
        '#states' => array(
          'invisible' => array(
            ':input[name="' . $breakpoint->name . '[columns]"]' => array('value' => 12),
          ),
        ),
      ),
      'bleed' => array(
        '#type' => 'checkbox',
        '#title' => t('Bleed to the margins.'),
        '#default_value' => FALSE,
      ),
      'bottom_margin' => array(
        '#type' => 'checkbox',
        '#title' => t('Space below'),
        '#default_value' => TRUE,
      ),
      'ordering' => array(
        '#type' => 'select',
        '#title' => t('Ordering'),
        '#description' => t('Push or pull the column in some direction.'),
        '#options' => array(
          'push-0' => t('Default'),
        ),
        '#default_value' => 'push-0',
      ),
    );

    if (!empty($icons[$breakpoint->name])) {
      $breakpoint_options['#title'] .= '<i class="fa fa-' . $icons[$breakpoint->name] . '"></i>';
    }

    for ($c = 1; $c <= 12; $c++) {
      // Unshift pull.
      $breakpoint_options['ordering']['#options'] = array(
        'pull-' . $c => t('Pull @amount', array('@amount' => $c))
      ) + $breakpoint_options['ordering']['#options'];

      // Add push.
      $breakpoint_options['ordering']['#options']['push-' . $c] = t('Push @amount', array('@amount' => $c));

      // Add options.
      $breakpoint_options['columns']['#options'][$c] = format_plural($c, '1 column', '@count columns');
    }

    // Set default values.
    if (!empty($pane['configuration']['adaptative_pane'][$machine_name])) {

      // Simplify variables.
      $current_config = $pane['configuration']['adaptative_pane'][$machine_name];

      // Walk recursively on configuration to set defaults.
      iela_array_walk_recursive_with_path($current_config, function ($item, $path) use (&$breakpoint_options) {

        $element = &$breakpoint_options;

        foreach ($path as $key) {
          $element = &$element[$key];
        }

        if (!empty($element)) {
          $element['#default_value'] = $item;
        }
      });
    }

    // @TODO: The human name of the breakpoints should work with space.
    $name_bp = str_replace(' ', '_', $breakpoint->name);
    $form[$machine_name][$name_bp] = $breakpoint_options;

  }

  return $form;
}
