<?php

/**
 * @file
 * Code for the Iela Adaptative Panels Preprocess functions.
 */

/**
 * Implements hook_preprocess_panels_pane().
 */
function iela_adaptative_panels_preprocess_panels_pane(&$vars) {

  $module_path = drupal_get_path('module', 'iela_adaptative_panels');

  $pane = $vars['pane'];
  $breakpoints = breakpoints_breakpoint_load_all_theme();
  // $adaptative_form = iela_adaptative_panels_create_fields_settings_form($pane->pid);

  // Set pane breakpoint appearence settings.
  foreach ($breakpoints as $machine_name => $breakpoint) {

    // Handle default setting for a breakpoint with no configuration.
    // @todo: This logic should be done also in a layout-save moment, so that
    // we don't enter a recursive logic always for panes that have no changes to
    // breakpoint settings.
    if (empty($pane->configuration['adaptative_pane'][$machine_name])) {
    // if (empty($pane->configuration['adaptative_pane'][$machine_name]) && !empty($adaptative_form[$machine_name][$breakpoint->name])) {
            
      // This is a simpler solution that avoids too much processing loss and
      // works until the @todo above is resolved.  
      $pane->configuration['adaptative_pane'][$machine_name]['columns'] = 12;

      // Initiate config.
      // $pane->configuration['adaptative_pane'][$machine_name] = array();

      // // Alias only.
      // $breakpoint_config = &$pane->configuration['adaptative_pane'][$machine_name];
      // $breakpoint_form = $adaptative_form[$machine_name][$breakpoint->name];

      // // Walk form to get defaults.
      // iela_array_walk_recursive_with_path($breakpoint_form, function ($item, $path) use (&$breakpoint_config) {

      //   if (array_pop($path) == '#default_value') {
          
      //     $value = &$breakpoint_config;

      //     for ($i = 0; $i < count($path) - 1; $i++) {
      //       $value = &$value[$path[$i]];
      //       if (empty($value)) {
      //         $value = array();
      //       }
      //     }

      //     $value[end($path)] = $item;
      //   }
      // });
    }


    // Bellow, we set the configuration as classes in the pane.

    $value = $pane->configuration['adaptative_pane'][$machine_name];
    $machine_name_parts = explode('.', $machine_name);
    $css_name = array_pop($machine_name_parts);

    $hidden = empty($value['columns']) || $value['columns'] == '<none>' || !is_numeric($value['columns']);

    // Column width.
    if ($hidden) {
      $vars['classes_array'][] = 'hidden-' . $css_name;
    } else {
      $vars['classes_array'][] = 'col-' . $css_name . '-' . $value['columns'];
    }

    // Force new line option.
    $vars['classes_array'][] = 'col-' . $css_name . (empty($value['new_line']) ? '-no' : '') . '-clear-left';

    // Bleed option.
    $vars['classes_array'][] = 'col-' . $css_name . (empty($value['bleed']) ? '-no' : '') . '-bleed';

    // Bottom margin option;
    $vars['classes_array'][] = 'col-' . $css_name . (!empty($value['bottom_margin']) ? '-no' : '') . '-margin-bottom';

    // Left/Right span.
    $vars['classes_array'][] = 'col-' . $css_name . '-' . (!empty($value['ordering']) ? $value['ordering'] : 'push-0');
  }
}
