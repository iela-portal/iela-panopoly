<?php
/**
 * @file
 * Code for integrating to IPE's interface.
 */

module_load_include('inc', 'iela', 'includes/iela.functions');

/**
 * Implements hook_panels_ipe_pane_links_alter().
 *
 * Displays the adaptative configuration button in the IPE.
 */
function iela_adaptative_panels_panels_ipe_pane_links_alter(&$links, $context, $display) {

  global $theme_key;

  // Skips if the current theme or its parents doesn't
  // implements any breakpoint.
  if (!$breakpoints = iela_get_breakpoints_theme_tree($theme_key)) {
    return;
  }

  if (!empty($context['pane']->pid)) {

    $links['adaptative'] = array(
      'title' => '<span>' . t('Adaptative Settings') . '</span>',
      'html' => TRUE,
      'href' => 'iela_adaptative_panels/nojs/settings/form/' . $context['pane']->pid . '/' . 'node_view',
      'attributes' => array(
        'title' => t('Adaptative Settings'),
        'class' => 'ctools-use-modal ctools-modal-modal-popup-small',
      ),
    );

    drupal_add_css(drupal_get_path('module', 'iela_adaptative_panels') . '/css/iela-adaptative-panels.css');
  }
}
