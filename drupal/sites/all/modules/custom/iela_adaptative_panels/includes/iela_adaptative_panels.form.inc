<?php
/**
 * @file
 * Form related code.
 */

/**
 * Settings form callback.
 *
 * Create the setting form displayed in the modal popup when configuring
 * adaptative pane settings.
 */
function iela_adaptative_panels_pane_settings_form($style_settings, &$form_state) {

  $form = array();

  $pid = $form_state['pid'];

  // Create fields adaptatives.
  $form = iela_adaptative_panels_create_fields_settings_form($pid);

  // Set hidden fieds.
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $pid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function iela_adaptative_panels_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'panels_ipe_edit_control_form') {

    // Change the order of submit functions.
    // The iela_adaptative_panels_ipe_form_submit passes the upgraded pane
    // to the form_state.
    array_unshift($form['buttons']['submit']['#submit'], 'iela_adaptative_panels_ipe_form_submit');
  }
}

/**
 * Form submit panels_pane_settings_form().
 */
function iela_adaptative_panels_pane_settings_form_submit(&$form, &$form_state) {
  $bps = breakpoints_breakpoint_load_all_theme();

  // The pid is numeric when updated a pane. When create
  // a new pane the pid isn't numeric. Eg.: pid = new-1.
  if (is_numeric($form_state['values']['pid'])) {

    // Load pane configurations.
    $did = iela_adaptative_panels_pane_load($form_state['values']['pid'], array('pid', 'did'));

    $display = panels_load_display($did['did']);
    $pane = $display->content[$form_state['values']['pid']];
    $pane->configuration['adaptative_pane'] = array();

    // Inserts the values of breakpoints chosen in form.
    foreach ($bps as $bp) {

      // @TODO: The human name of the breakpoints should work with space.
      $name_bp = str_replace(' ', '_', $bp->name);
      $pane->configuration['adaptative_pane'][$bp->machine_name] = $form_state['values'][$name_bp];
    }

    // Insert the pane upgraded in display.
    $display->content[$form_state['values']['pid']] = $pane;

    // Save the display with the new breakpoint settings.
    panels_save_display($display);
  }
  else {

    $columns = array();

    // Inserts the values of breakpoints chosen in form.
    foreach ($bps as $bp) {
      $columns[$bp->machine_name]['columns'] = $form_state['values'][$bp->name]['columns'];
    }

    // Create variable with breakpoints values.
    variable_set('iela_adaptative_panels_columns_' . $form_state['values']['pid'], $columns);
  }
}

/**
 * Submit function the form panels_ipe_edit_control_form().
 *
 * Set breakpoints values in form_state['diplay']
 */
function iela_adaptative_panels_ipe_form_submit(&$form, &$form_state) {

  foreach ($form_state['display']->content as $pid => $pane) {

    // The pid is numeric when updated a pane. When create
    // a new pane the pid isn't numeric. Eg.: pid = new-1
    if (is_numeric($pid)) {

      $fields = iela_adaptative_panels_pane_load($pid, array('pid', 'configuration'));
      $form_state['display']->content[$pid]->configuration['adaptative_pane'] = $fields['configuration']['adaptative_pane'];
    }
    else {

      $adaptative_pane = variable_get('iela_adaptative_panels_columns_' . $pid);
      $form_state['display']->content[$pid]->configuration['adaptative_pane'] = $adaptative_pane;
      variable_del('iela_adaptative_panels_columns_' . $pid);
    }
  }
}
