/**
 * @file
 * Icon select adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Show icon rendered when possible.
 */
Drupal.behaviors.ielaMenuIconSelector = {
  attach: function (context, settings) {

    // Early return.
    if (!settings.ielaMenuIconSelect) return;

    $(context).find('.icon-selector').each(function () {
      var $select  = $(this)
        , $label   = $select.siblings('label')
        , $options = $select.find('option')
        , $icon    = $newIcon = bundle = icon = info = markup = null;

      $select.on('change', function () {
        info   = $options.filter(':selected').val().split('|');
        bundle = info[0]
        icon   = info[1]
        markup = settings.ielaMenuIconSelect[bundle] && settings.ielaMenuIconSelect[bundle][icon];
        $newIcon = markup && $(markup).css({
          'float': 'right',
          'font-size': '2.5em'
        });

        // Remove old icon.
        if ($icon) $icon.remove();

        if ($newIcon) {
          $icon = $newIcon;
          $label.append($icon);
        }
      });
    });
  }
};

})(jQuery, Drupal, this, this.document);
