<?php

/**
 * @file
 * Install, uninstall, and update functions for libraries.module.
 */

function iela_install() {
  iela_update_7201();
  iela_update_7203();
  iela_update_7204();
  iela_update_7205();
  iela_update_7206();
  iela_update_7207();
  iela_update_7208();
  iela_update_7210();
  iela_update_7211();
  iela_update_7212();
}

/**
 * Enabled Xautoload and Configuration Management.
 */
function iela_update_7201() {
  module_enable(array(
    'xautoload',
    'configuration',
    'configuration_ui',
  ));
}

/**
 * Install Adminimal theme.
 */
function iela_update_7202() {
  theme_enable(array('adminimal'));

  // Set Adminimal as the default administrative theme.
  variable_set('admin_theme', 'adminimal');
}

/**
 * Install IELA sub-theme and disable unused themes.
 */
function iela_update_7203() {

  // Enable IELA theme.
  theme_enable(array(
    'radix',
    'iela_theme',
  ));

  // Disable unused themes.
  theme_disable(array(
    'bartik',
    'seven',
    'garland',
    'responsive_bartik',
    'stark',
  ));

  // Set IELA theme as the default.
  variable_set('theme_default', 'iela_theme');
}

/**
 * Modify default config path.
 */
function iela_update_7204() {
  variable_set('configuration_config_path', 'sites/default/config');
}

/**
 * Install iela_wysiwyg module.
 */
function iela_update_7205() {
  module_enable(array('iela_wysiwyg'));
}

/**
 * Install block module.
 */
function iela_update_7206() {
  module_enable(array('block'));
}

/**
 * Install IELA Adaptative Panels module.
 */
function iela_update_7207() {
  module_enable(array('iela_adaptative_panels'));
}

/**
 * Install Breakpoints JS module.
 */
function iela_update_7208() {
  module_enable(array('breakpoints_js'));
}

/**
 * Install IELA Panels module.
 */
function iela_update_7209() {
  module_enable(array('iela_panels'));
}

/**
 * Install @font-your-face module and sub-modules.
 */
function iela_update_7210() {
  module_enable(array(
    'fontyourface',
    'fontyourface_ui',
    'google_fonts_api',
    'iela_fontyourface',
  ));
}

/**
 * Install Special Menu Items module.
 */
function iela_update_7211() {
  module_enable(array(
    'special_menu_items',
  ));
}

/**
 * Install Views TimelineJS module and it's feature.
 */
function iela_update_7212() {
  module_enable(array(
    'views_timelinejs',
    'views_timelinejs_feature',
  ));
}

/**
 * Install Name and IELA Users modules.
 */
function iela_update_7213() {
  module_enable(array(
    'name',
    'iela_users',
  ));
}

/**
 * Install modules: Field Collection, URL.
 */
function iela_update_7214() {
  module_enable(array(
    'field_collection',
    'url',
  ));
}

/**
 * Install Countries module.
 */
function iela_update_7215() {
  module_enable(array(
    'countries',
  ));
}

/**
 * Install Flexslider and subordinate modules.
 */
function iela_update_7216() {
  module_enable(array(
    'flexslider',
    'flexslider_views',
    'flexslider_field_collection',
  ));

  // Force flexslider version detection.
  variable_set('flexslider_version', '2.2');
  define("FLEXSLIDER_VERSION", variable_get('flexslider_version', '2.2'));
}

/**
 * Install i18n related modules.
 */
function iela_update_7217() {
  module_enable(array(
    'countries_i18n',
    'variable',
    'i18n',
    'locale',
    'i18n_string',
    'better_countries',
    'l10n_update',
  ));
}

/**
 * Install Entity Form module.
 */
function iela_update_7218() {
  module_enable(array(
    'entityform',
  ));
}

/**
 * Install E-mail module.
 */
function iela_update_7219() {
  module_enable(array(
    'email',
  ));
}

/**
 * Install Ajax Form Entity module.
 */
function iela_update_7220() {
  module_enable(array(
    'ajax_form_entity',
  ));
}

/**
 * Install Node Embed module.
 */
function iela_update_7221() {
  module_enable(array(
    'node_embed',
  ));
}

/**
 * Install entityform_send_mail.
 */
function iela_update_7222() {
  module_enable(array(
    'entityform_send_mail'
  ));
}

/**
 * Install improved_multi_select.
 */
function iela_update_7223() {
  module_enable(array(
    'improved_multi_select'
  ));
}

/**
 * Install Field Formatter Settings & View Mode Field modules.
 */
function iela_update_7224() {
  module_enable(array(
    'field_formatter_settings',
    'view_mode_field',
  ));
}

/**
 * Force enabling some forgotten modules.
 */
function iela_update_7225() {
  module_enable(array(
    'iela_panels',
    'panels_multiple_styles',
  ));
}

/**
 * Install Masonry.
 */
function iela_update_7226() {
  module_enable(array(
    'masonry',
  ));
}

/**
 * Install Disqus.
 */
function iela_update_7227() {
  module_enable(array(
    'disqus',
  ));
}

/**
 * Install Simple HTML Dom.
 */
function iela_update_7228() {
  module_enable(array(
    'simplehtmldom',
  ));
}

/**
 * Install Smart Trim formatter.
 */
function iela_update_7229() {
  module_enable(array(
    'smart_trim',
  ));
}

/**
 * Install Taxonomy tools.
 */
function iela_update_7230() {
  module_enable(array(
    'taxonomy_tools',
    'taxonomy_tools_redirect',
  ));
}

/**
 * Install Taxonomy tools.
 */
function iela_update_7231() {
  module_enable(array(
    'disable_messages',
  ));
}

/**
 * Install Views Load More.
 */
function iela_update_7232() {
  module_enable(array(
    'views_load_more',
  ));
}

/**
 * Install Timeago module.
 */
function iela_update_7233() {
  module_enable(array(
    'timeago',
  ));

  variable_set('timeago_node', 0);
  variable_set('timeago_comment', 0);
}

/**
 * Install Statistics Modules.
 */
function iela_update_7234() {
  module_enable(array(
    'statistics',
    'statistics_counter',
    'statistics_filter',
  ));
}

/**
 * Install Statistics Modules.
 */
function iela_update_7235() {
  module_enable(array(
    'radioactivity',
  ));
}

/**
 * Install E-mail Registration Module.
 */
function iela_update_7236() {
  module_enable(array(
    'email_registration',
  ));
}

/**
 * Install Google Analytics Module.
 */
function iela_update_7237() {
  module_enable(array(
    'googleanalytics',
  ));
}

/**
 * Install Taxonomy Entity Index.
 */
function iela_update_7238() {
  module_enable(array(
    'taxonomy_entity_index',
  ));
}

/**
 * Install Simplenews.
 */
function iela_update_7239() {
  module_enable(array(
    'simplenews',
    'scs',
    'scs_views',
  ));
}

/**
 * Install Panels Mini.
 */
function iela_update_7240() {
  module_enable(array(
    'panels_mini',
    'panels_mini_ipe',
  ));
}

/**
 * Install Panels Frame Builder.
 */
function iela_update_7241() {
  module_enable(array(
    'panels_frame',
  ));
}

/**
 * Install Textformatter.
 */
function iela_update_7242() {
  module_enable(array(
    'textformatter',
    'textformatter_contrib',
  ));
}

/**
 * Install Remote Media.
 */
function iela_update_7243() {
  module_enable(array(
    'remote_stream_wrapper',
  ));
}

/**
 * Install IELA Media.
 */
function iela_update_7244() {
  module_enable(array(
    'iela_media',
  ));
}

/**
 * Install Views Distinct module.
 */
function iela_update_7245() {
  module_enable(array(
    'views_distinct',
  ));
}

/**
 * Install Inline Entity Form.
 */
function iela_update_7246() {
  module_enable(array(
    'inline_entity_form',
  ));
}

/**
 * Install CER related modules.
 */
function iela_update_7247() {
  module_enable(array(
    'cer',
  ));
}

/**
 * Install Entity Token module.
 */
function iela_update_7248() {
  module_enable(array(
    'entity_token',
  ));
}

/**
 * Install Mini Panel Reference module.
 */
function iela_update_7249() {
  module_enable(array(
    'mini_panel_reference',
  ));
}

/**
 * Install Views Watchdog module.
 */
function iela_update_7250() {
  module_enable(array(
    'views_watchdog',
    'views_contextual_filter_query',
  ));
}

/**
 * Install IELA Menu.
 */
function iela_update_7251() {
  module_enable(array(
    'iela_menu',
  ));
}

/**
 * Install Icon APIs.
 */
function iela_update_7252() {
  module_enable(array(
    'icon',
    'icon_menu',
    'fontawesome',
  ));
}

/**
 * Add secondary menu.
 */
function iela_update_7253() {

  // Create secondary menu, if not there yet.
  $second_menu = menu_load('second-menu');
  if (empty($second_menu)) {
    $second_menu = array(
      'menu_name' => 'second-menu',
      'title' => 'Second menu',
      'description' => 'Secondary navbar menu. Shown in the right of the navbar.',
    );
    menu_save($second_menu);
  }

  // Set default menus.
  variable_set('menu_main_links_source', variable_get('menu_main_links_source', 'main-menu'));
  variable_set('menu_secondary_links_source', variable_get('menu_secondary_links_source', 'second-menu'));
}

/**
 * Disable useless modules.
 */
function iela_update_7254() {
  module_disable(array(
    'apps',
  ));
}

/**
 * Install Imagecache External module.
 */
function iela_update_7255() {
  module_enable(array(
    'imagecache_external',
  ));

  variable_set('imagecache_external_use_whitelist', 0);
}

/**
 * Enable View Modes Display module.
 */
function iela_media_update_7256() {
  module_enable(array(
    'view_modes_display',
  ));
}

/**
 * Enable Token Insert Entity module.
 */
function iela_media_update_7257() {
  module_enable(array(
    'token_insert_entity',
  ));
}

/**
 * Enable Redirecting modules.
 */
function iela_media_update_7258() {
  module_enable(array(
    'globalredirect',
    'redirect',
  ));
}

/**
 * Enable Metatag related modules.
 */
function iela_media_update_7259() {
  module_enable(array(
    'metatag',
    'metatag_opengraph',
    'metatag_panels',
  ));
}

/**
 * Enable Environment modules.
 */
function iela_update_7260() {
  module_enable(array(
    'iela_environment',
    'environment',
  ));
}

/**
 * Enable entity_view_mode module.
 */
function iela_update_7261() {
  module_enable(array(
    'entity_view_mode',
  ));
}

/**
 * Replace multiple styles module.
 */
function iela_update_7262() {
  module_disable(array(
    'panels_multiple_styles',
  ));

  module_enable(array(
    'panels_multiple_styles',
  ));  
}
