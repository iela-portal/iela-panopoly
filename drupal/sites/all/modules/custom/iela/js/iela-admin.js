/**
 * @file
 * Generic JavaScript adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Executed when a new ui-dialog appears.
 */
function addedDialog() {
  var dialog = $(this)
    , body = $('body');

  if (body.hasClass('ui-modal-open')) {
    dialog.attr('data-had-modal', 'true');
  }

  if (dialog.is(':visible')) {
    body.addClass('ui-modal-open');
  }
}

/**
 * Executed when a ui-dialog closes.
 */
function removedDialog() {
  if (!$(this).attr('data-had-modal')) {
    $('body').removeClass('ui-modal-open');
  }
}

$('.ui-dialog').livequery(addedDialog, removedDialog);

/**
 * Parent iFrame resizer.
 */
Drupal.behaviors.iFrameResize = {
  attach: function (context, settings) {
    if (window !== window.parent && context == document && window.parent_iframe) {
      try {
        var body = $('body', context)
          , iframe = $(parent_iframe);

        iframe.height(body.height());

        $('body', context).resize(function () {
          iframe.height(body.height());
        });
      } catch(e) {}
    }
  }
};

/**
 * Avoid crop image close on UI dialog.
 */
Drupal.behaviors.ielaUIDialog = {
  attach: function (context, settings) {
    var $context = $(context)
      , $body = $context.find('body.page-media-browser');

    if ($body.length && window.parent !== window && window.frameElement) {
      try  {
        var $parentJQuery = window.parent.jQuery
          , $dialog = $parentJQuery('.ui-dialog.media-wrapper')
          , $iframe = $parentJQuery('iframe.media-modal-frame');

        $iframe.on('dialogbeforeclose', function (e, ui) {
          var $manualCrop = $body.find('.manualcrop-overlay');

          if ($manualCrop.length && $manualCrop.is(':visible')) {
            e.preventDefault();
            $manualCrop.find('.manualcrop-cancel').trigger('mousedown');
          }
        });
      }  

      // Possible crossdomain exception.
      catch(e) { debugger; return; }
    }
  }
};

})(jQuery, Drupal, this, this.document);
