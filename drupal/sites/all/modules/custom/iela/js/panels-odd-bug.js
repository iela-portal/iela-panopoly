/**
 * Somethings - dunno why - panels display editor is executed
 * before panels base script creates the default Panels property.
 * We simply make sure it exists on both cases.
 */

if (Drupal) Drupal.Panels = Drupal.Panels || {};
