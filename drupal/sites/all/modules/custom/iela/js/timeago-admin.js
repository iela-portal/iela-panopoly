/**
 * @file
 * Timeago Form adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

var confirmChangeAll   = 'Are you sure you want to @state all options?'
  , checkTranslation   = Drupal.t('check')
  , uncheckTranslation = Drupal.t('uncheck');

/**
 * Parent iFrame resizer.
 */
Drupal.behaviors.timeagoBundleTable = {
  attach: function (context, settings) {
    var $table = $('#timeago-bundle-table', context)
      , $rows  = $table.find('tbody tr')
      , $checkboxes = $rows.find('input[type="checkbox"]');

    $checkboxes.on('change', function () {
      var $checkbox = $(this)
        , $col      = $checkbox.closest('th,td')
        , $row      = $checkbox.closest('tr')
        , $changed  = $([])
        , isLastCol = $col.is(':last-child')
        , isLastRow = $row.is(':last-child')
        , isLast    = isLastCol && isLastRow
        , colIndex  = $row.find('th,td').index($col) + 1
        , newState  = $checkbox.is(':checked') ? checkTranslation : uncheckTranslation
        , sure      = Drupal.t(confirmChangeAll, {
            '@state': newState
          });

      // When modifying all options, confirm first.
      if (isLast && !confirm(sure)) return;

      // Bundle automatization.
      if (isLastCol) {
        $changed = $changed.add($row
          .find('input[type="checkbox"]')
          .not($checkbox)
          .prop('checked', $checkbox.is(':checked'))
        );
      }

      // View mode automatization.
      if (isLastRow) {
        $changed = $changed.add($rows
          .not($row)
          .find('th,td')
          .filter(':nth-child(' + colIndex + ')')
          .find('input[type="checkbox"]')
          .prop('checked', $checkbox.is(':checked'))
        );
      }

      if (isLastCol && isLastRow) $changed.trigger('change');
    });
  }
};

})(jQuery, Drupal, this, this.document);
