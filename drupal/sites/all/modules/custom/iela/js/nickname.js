/**
 * @file
 * Nicknam adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Fullfill nick name dynamically.
 */
Drupal.behaviors.nickname = {
  attach: function (context, settings) {
    $('#edit-field-fullname', context).once(function () {
      var $wrapper  = $(this)
        , $given    = $wrapper.find('.name-given')
        , $family   = $wrapper.find('.name-family')
        , $names    = $given.add($family)
        , $nickname = $wrapper.find('.name-credentials')
        , $nickItem = $nickname.closest('.form-item')
        , $form     = $nickname.closest('form')
        , maxLength = $nickname.attr('maxlength') || 255
        , custom;

      // Soon stop when no elements found.
      if (!$given.length || !$family.length || !$nickname.length || !$nickItem.length) return;

      /**
       * Initiate customization.
       */
      function initCustom() {

        // Stop listeners.
        $nickItem.off('click', nicknameClick);
        $names.off('keydown keyup change', updateNickname);

        // Enable field and focus.
        $nickname.removeAttr('disabled').removeClass('autofill');

        // Stop automatization.
        custom = true;
      }

      /**
       * Disable automatization and start custom state.
       */
      function nicknameClick(e) {
        if ($nickname.is(e.srcElement)) {
          initCustom();
          $nickname.focus();
        }
      }

      /**
       * Update nickname value.
       */
      function updateNickname() {
        if (!custom) {
          var values      = []
            , givenValue  = $.trim($given.val()).split(' ').shift()
            , lastName    = $.trim($family.val()).split(' ').pop();

          if (givenValue) values.push(givenValue);
          if (lastName) values.push(lastName);

          $nickname.val(values.join(' ')).trigger('change');
        }
      }

      /**
       * Limit nickname length to max-length.
       */
      function limitNicknameLength() {
        var value = $nickname.val()
          , newValue = value.substring(0, maxLength);

        if (value !== newValue) $nickname.val(newValue);
      }

      /**
       * Initiate automatization.
       */
      function initListeners() {
        if (!$nickname.val()) {

          custom = false;
          updateNickname();

          // Initial state.
          $nickname.attr('disabled', 'disabled').addClass('autofill');

          // Start listeners.
          $nickItem.on('click', nicknameClick);
          $names.on('keydown keyup change', updateNickname);
        }
      }

      // Always limit nickname size.
      initListeners();
      $form.on('submit', initCustom);
      $nickname.on('keyup change', limitNicknameLength).on('blur', initListeners);
    });
  }
};

})(jQuery, Drupal, this, this.document);
