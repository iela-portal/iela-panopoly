/**
 * @file
 * IELA preview view modes script.
 */

;(function ($, Drupal, window, document, undefined) {

var defaultColumns = {
  'node': {
    'featured': 12,
    'teaser': 6
  }
};

Drupal.behaviors.ielaPreviewViewModes = {
  attach: function (context) {

    $('#content', context).removeClass('container');

    $(context).find('.view-mode-list-item').each(function () {
      var $item = $(this)
        , $options = $item.find('.adaptative-options')
        , $columnOption = $options.find('.column-option')
        , $fluidOption = $options.find('[name="fluid-option"]')
        , $bleedOption = $options.find('[name="bleed-option"]')
        , $container = $item.find('.preview-container')
        , $column = $container.find('> .row > div')
        , viewMode = $item.attr('data-view-mode')
        , type = $item.attr('data-entity-type')
        , bundle = $item.attr('data-bundle')
        , currColumns = defaultColumns[type] && defaultColumns[type][viewMode] || 4;

      $column.removeClass('col-xs-4').addClass('col-xs-' + currColumns);

      $columnOption.slider({
        min: 1,
        max: 12,
        animate: false,
        value: currColumns,
        slide: function (e, ui) {
          $column.removeClass('col-xs-' + currColumns).addClass('col-xs-' + (currColumns = ui.value));
        }
      });

      $fluidOption.on('change', function () {
        $container.removeClass('container').removeClass('container-fluid');
        $container.addClass('container' + ($(this).is(':checked') ? '-fluid' : ''));
      });

      $bleedOption.on('change', function () {
        $column.toggleClass('col-xs-bleed', $(this).is(':checked'));
      });
    });
  }
};

})(jQuery, Drupal, this, this.document);
