<?php
/**
 * @file
 * IELA module alter implementations.
 */

/**
 * Implements hook_module_implements_alter().
 */
function iela_module_implements_alter(&$implementations, $hook) {
  $hook_map = array(
    'form_alter',
    'entity_info_alter',
  );

  if (in_array($hook, $hook_map)) {
    // Move our hook implementation to the bottom.
    $group = $implementations['iela'];
    unset($implementations['iela']);
    $implementations['iela'] = $group;
  }
}

/**
 * Implements hook_entity_info_alter(&$entities);
 */
function iela_entity_info_alter(&$entities) {

  $entities['user']['access callback'] = 'iela_metadata_user_access';

  // Stacked view mode for nodes.
  $entities['node']['view modes']['stacked'] = array(
    'label' => t('Stacked'),
    'custom settings' => TRUE,
  );

  // Poster view mode for nodes.
  $entities['node']['view modes']['poster'] = array(
    'label' => t('Poster'),
    'custom settings' => TRUE,
  );

  // Banner view mode for nodes.
  $entities['node']['view modes']['banner'] = array(
    'label' => t('Banner'),
    'custom settings' => TRUE,
  );

  // Mini teaser view mode for nodes.
  $entities['node']['view modes']['mini_teaser'] = array(
    'label' => t('Mini Teaser'),
    'custom settings' => TRUE,
  );

  // Stand view mode for nodes.
  $entities['node']['view modes']['stand'] = array(
    'label' => t('Stand'),
    'custom settings' => TRUE,
  );

  // Full name view mode for users.
  $entities['user']['view modes']['full_name'] = array(
    'label' => t('Full name'),
    'custom settings' => TRUE,
  );

  // Side-by-side view mode for field collections.
  $entities['field_collection_item']['view modes']['horizontal'] = array(
    'label' => t('Horizontal'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function iela_theme_registry_alter(&$theme_registry) {

  // Reset to defaults.
  $theme_registry['status_messages']['iela_previous_function'] = $theme_registry['status_messages']['function'];
  $theme_registry['status_messages']['function'] = 'iela_status_messages';
  $theme_registry['status_messages']['variables']['messages'] = NULL;
  $theme_registry['status_messages']['variables']['skip_filters'] = array();
}

/**
 * Custom status message theme implementation.
 */
function iela_status_messages(&$variables) {
  global $theme;
  $func = 'iela_theme_status_messages';

  // Map to iela theme first, if possible.
  if ($theme == 'iela_theme' && function_exists($func)) return $func($variables);

  // Map to previous method, otherwise.
  $theme_registry = theme_get_registry();
  $theme_info = $theme_registry['status_messages'];
  $func = $theme_info['iela_previous_function'];

  return call_user_func((function_exists($func) ? $func : 'theme_status_messages'), $variables);
}

/**
 * Implements hook_page_alter(&$page).
 */
function iela_page_alter(&$page) {

  // Load administrative improvement stylesheet and scripts.
  if (path_is_admin(current_path()) || user_access('administer_panels_layouts')) {

    $module_path = drupal_get_path('module', 'iela');
    $theme_path = drupal_get_path('theme', 'iela_theme');

    // Load main theme administrative styles.
    $page['content']['#attached']['css'][$theme_path . '/css/admin.css'] = array(
      'group' => CSS_THEME,
    );

    // Load administrative styles.
    $page['content']['#attached']['css'][$module_path . '/css/iela-admin.css'] = array(
      'group' => CSS_THEME,
    );

    // Load administrative scripts.
    $page['content']['#attached']['js'][] = $module_path . '/js/lib/jquery.livequery.js';
    $page['content']['#attached']['js'][] = $module_path . '/js/lib/jquery.resize.js';
    $page['content']['#attached']['js'][$module_path . '/js/iela-admin.js'] = array(
      'group' => JS_THEME,
    );
    $page['content']['#attached']['js'][$module_path . '/js/panels-odd-bug.js'] = array(
      'weight' => -1000,
    );
  }
}

/**
 * Implements hook_library_alter(&$libraries).
 */
function iela_library_alter(&$libraries) {
  if (!empty($libraries['navbar.tableheader'])) {
    // Is this a bug? JS files defined by this library do not exist.
    unset($libraries['navbar.tableheader']['js']);
  }
}

/**
 * Implements hook_libraries_info_alter().
 */
function iela_libraries_info_alter(&$libraries) {
  // Flexslider minified version has a bug. Use default one.
  if (!empty($libraries['flexslider'])) {
    $libraries['flexslider']['files']['js'][0] = 'jquery.flexslider.js';
  }
}

/**
 * Implements hook_field_formatter_info_alter().
 */
function iela_field_formatter_info_alter(&$info) {
  $info['entityreference_entity_view']['settings']['view_mode_field'] = NULL;
}

/**
 * Implements hook_field_formatter_settings_form_alter().
 */
function iela_field_formatter_settings_form_alter(&$form, $context) {
  // Act on entityreference's entity view formatter settings form only:
  if ($context['module'] == 'entityreference') {
    if ($context['instance']['display'][$context['view_mode']]['type'] == 'entityreference_entity_view') {
      $view_mode_fields = array();

      foreach ($context['form']['#fields'] as $field_name) {
        if ($field_name !== $context['field']['field_name']) {
          $field_info = field_info_field($field_name);

          if ($field_info['type'] == 'view_mode_field_view_mode' && $field_info['settings']['entity_type'] == $context['field']['settings']['target_type']) {
            $view_mode_fields[$field_name] = $field_info;
          }
        }
      }

      // Add options if there are view mode fields available.
      if (!empty($view_mode_fields)) {
        $settings = $context['instance']['display'][$context['view_mode']]['settings'];

        $form['view_mode']['#weight'] = -2;
        $form['view_mode']['#options']['view_mode_field'] = t('View mode field');

        $view_mode_selector = ':input[name="fields[' . $context['field']['field_name'] . '][settings_edit_form][settings][view_mode]"]';

        $form['view_mode_field'] = array(
          '#type' => 'select',
          '#title' => t('View mode field'),
          '#description' => t('Choose the field from where to get the view mode.'),
          '#default_value' => !empty($settings['view_mode_field']) ? $settings['view_mode_field'] : null,
          '#options' => array(),
          '#access' => TRUE,
          '#weight' => -1,
          '#states' => array(
            'visible' => array(
              $view_mode_selector => array('value' => 'view_mode_field'),
            ),
          ),
        );

        foreach ($view_mode_fields as $field_name => $field_info) {
          $form['view_mode_field']['#options'][$field_name] = $context['form']['fields'][$field_name]['human_name']['#markup'];
        }
      }
    }
  }
}

/**
 * Implements hook_field_formatter_settings_summary_alter().
 */
function iela_field_formatter_settings_summary_alter(&$summary, $context) {

  // Easy access to variables.
  $instance = $context['instance'];
  $current_display = $instance['display'][$context['view_mode']];
  $settings = $current_display['settings'];

  if ($current_display['type'] == 'entityreference_entity_view' && $settings['view_mode'] == 'view_mode_field') {
    $field_instance_info = field_info_instance($instance['entity_type'], $settings['view_mode_field'], $instance['bundle']);
    $replace_with = t('using') . ' "' . $field_instance_info['label'] . '" ' . t('configuration');
    $summary = str_replace('as view_mode_field', $replace_with, $summary);
  }
}

/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 *
 * Add placeholder field for text formatters.
 */
function iela_form_field_ui_field_edit_form_alter(&$form, $form_state, $form_id) {
  if (!empty($form_state['build_info']['args'][0])) {
    $conf = $form_state['build_info']['args'][0];
    $accepted_widgets = array(
      'text_long',
      'textfield',
      'text',
    );

    if (in_array($form['#field']['type'], $accepted_widgets)) {
      $form['instance']['placeholder_text'] = array(
        '#type' => 'textfield',
        '#title' => t('Placeholder'),
        '#description' => t('Optional text to appear inside text box when field is empty'),
        '#weight' => -5,
        '#default_value' => !empty($conf['placeholder_text']) ? $conf['placeholder_text'] : '',
      );
    }
  }
}

/**
 * Implements hook_form_taxonomy_form_term_alter().
 */
function iela_form_taxonomy_form_term_alter(&$form, &$form_state) {
  // Never allow for multiple taxonomy parent terms.
  $form['relations']['parent']['#multiple'] = false;
}

/**
 * Implements hook_date_popup_pre_validate_alter().
 */
function iela_date_popup_pre_validate_alter($element, $form_state, &$input) {
  if (isset($input['date']) && isset($input['time'])) {
    if ($input['date'] != '' && $input['time'] == '') {
      $input['time'] = '12:00';
    }
  }
}

/**
 * Implements hook_form_timeago_admin_alter().
 */
function iela_form_timeago_admin_alter(&$form, &$form_state) {
  $form['#submit'][] = 'iela_form_timeago_admin_submit';

  $entity_info  = entity_get_info('node');
  $bundles      = $entity_info['bundles'];
  $view_modes   = $entity_info['view modes'];

  $form_addition = array();
  $form_addition['info'] = $form['info'];
  unset($form['info']);
  unset($form['timeago_node']);

  $form_addition['bundle_view_mode_table'] = array(
    '#theme' => 'table',
    '#empty' => t('No content types available'),
    '#multiple' => TRUE,
    '#header' => array(''),
    '#rows' => array(),
    '#attributes' => array('id' => 'timeago-bundle-table'),
  );

  $rows = &$form_addition['bundle_view_mode_table']['#rows'];
  $header = &$form_addition['bundle_view_mode_table']['#header'];

  // Initiate top header.
  foreach($view_modes as $machine_view_mode => $view_mode) {
    $header[] = array(
      'data' => $view_mode['label'],
      'style' => array('text-align: center;'),
      'data-view-mode' => $machine_view_mode,
    );
  }

  // Add 'All' column.
  $header[] = t('All');

  // Initiate left header.
  foreach ($bundles as $machine_bundle => $bundle) {
    $rows[] = array(
      array(
        'data' => $bundle['label'],
        'header' => TRUE,
        'data-bundle' => $machine_bundle,
      ),
    );
  }

  // Add 'All' row.
  $rows[] = array(
    array(
      'data' => t('All'),
      'header' => TRUE,
    ),
  );

  $last_row = &$rows[count($rows) - 1];

  $row = 0;
  $col = 1;

  // Build cells.
  foreach ($bundles as $machine_bundle => $bundle) {
    $bundle_variable_name = "timeago_node_{$machine_bundle}";
    $bundle_variable = variable_get($bundle_variable_name, 1);

    foreach ($view_modes as $machine_view_mode => $view_mode) {
      $bundle_view_mode_variable_name = $bundle_variable_name . "_{$machine_view_mode}";
      $view_mode_variable_name = "timeago_node_{$machine_view_mode}";

      if ($row == 0) {
        $view_mode_variable = variable_get($view_mode_variable_name, 1);

        $last_row[$col] = array(
          'data' => array(
            '#type' => 'checkbox',
            '#name' => $view_mode_variable_name,
            '#checked' => $view_mode_variable,
            '#return_value' => 1,
            '#default_value' => 0,
          ),
          'header' => TRUE,
          'style' => array('text-align: center;'),
        );
      }

      $default_bundle_view_mode_variable = !empty($bundle_variable) && !empty($view_mode_variable) ? 1 : 0;

      $rows[$row][$col] = array(
        'data' => array(
          '#type' => 'checkbox',
          '#name' => $bundle_view_mode_variable_name,
          '#checked' => variable_get($bundle_view_mode_variable_name, $default_bundle_view_mode_variable),
          '#return_value' => 1,
          '#default_value' => 0,
        ),
        'style' => array('text-align: center;'),
      );

      $col++;
    }

    // Fullfill 'All'.
    $rows[$row][$col] = array(
      'data' => array(
        '#type' => 'checkbox',
        '#name' => $bundle_variable_name,
        '#checked' => $bundle_variable,
        '#return_value' => 1,
        '#default_value' => 0,
      ),
      'header' => TRUE,
      'style' => array('text-align: center;'),
    );

    $row++;
  }

  $last_row[$col - 1] = array(
    'data' => array(
      '#type' => 'checkbox',
      '#name' => 'timeago_node',
      '#checked' => variable_get('timeago_node', 1),
      '#return_value' => 1,
      '#default_value' => 0,
    ),
    'header' => TRUE,
    'style' => array('text-align: center;'),
  );

  $form_addition['#attached']['js'][] = drupal_get_path('module', 'iela') . '/js/timeago-admin.js';

  // Add new options to beggining of form.
  $form = $form_addition + $form;
}

function iela_form_timeago_admin_submit(&$form, &$form_state) {

  $entity_info  = entity_get_info('node');
  $bundles      = $entity_info['bundles'];
  $view_modes   = $entity_info['view modes'];

  // Never allow timeago to be true.
  variable_set('timeago_node', 0);

  foreach ($bundles as $machine_bundle => $bundle) {
    $bundle_variable_name = "timeago_node_{$machine_bundle}";

    variable_set($bundle_variable_name, empty($form_state['input'][$bundle_variable_name]) ? 0 : 1);

    foreach ($view_modes as $machine_view_mode => $view_mode) {
      $bundle_view_mode_variable_name = $bundle_variable_name . "_{$machine_view_mode}";
      $view_mode_variable_name = "timeago_node_{$machine_view_mode}";

      variable_set($bundle_view_mode_variable_name, empty($form_state['input'][$bundle_view_mode_variable_name]) ? 0 : 1);

      if (!isset($form_state['values'][$view_mode_variable_name])) {
        variable_set($view_mode_variable_name, empty($form_state['input'][$view_mode_variable_name]) ? 0 : 1);
      }
    }
  }
}

/**
 * Implements hook_form_ctools_search_form_content_type_edit_form_alter().
 */
function iela_form_ctools_search_form_content_type_edit_form_alter(&$form, $form_state) {
  iela_search_form_content_type_alter($form, $form_state);
}

/**
 * Implements hook_form_panopoly_search_search_box_content_type_edit_form_alter().
 */
function iela_form_panopoly_search_search_box_content_type_edit_form_alter(&$form, $form_state) {
  iela_search_form_content_type_alter($form, $form_state);
}

/**
 * Alters both search content types.
 */
function iela_search_form_content_type_alter(&$form, $form_state) {
  $conf = $form_state['conf'] + array(
    'placeholder' => '',
  );

  $form['placeholder'] = array(
    '#type' => 'textfield',
    '#title' => t('Placeholder text'),
    '#description' => t('Text to show inside the term search field before any value is inserted.'),
    '#default_value' => $conf['placeholder'],
  );
}

/**
 * Implements (fake) hook_search_form_alter().
 */
function iela_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'search_form' || $form_id == 'search_theme_form') {
    $form['#submit'] = array('search_form_submit');
  }
}

/**
 * Implements hook_ctools_plugin_pre_alter().
 */
function iela_ctools_plugin_pre_alter(&$plugin, &$info) {
  if ($info['type'] == 'content_types' && in_array($plugin['name'], array('search_form', 'search_box'))) {
    $plugin['defaults']['placeholder'] = '';
  }
}

/**
 * Implements hook_field_widget_WIDGET_NAME_form_alter().
 */
function iela_field_widget_name_widget_form_alter(&$element, &$form_state, $context) {
  if ($element['#field_name'] == 'field_fullname') {
    $element['#attached']['js'][] = drupal_get_path('module', 'iela') . '/js/nickname.js';
  }
}

/**
 * Implements hook_field_widget_url_external_form_alter(&$element, &$form_state, $context).
 */
function iela_field_widget_url_external_form_alter(&$element, &$form_state, $context) {
  if ($element['#bundle'] == 'field_social_links' && $element['#field_name'] == 'field_url') {
    $element['#type'] = 'container';
    unset($element['#title']);
  }
}

/**
 * Implements hook_query_alter().
 * Alter entity queries to always allow access to bloqued users.
 */
function iela_query_alter(&$query) {
  if (isset($query->alterTags) && !empty($query->alterTags['entityreference']) && isset($query->alterTags['user_access'])) {
    $where = &$query->conditions();
    foreach ($where as $index => $condition) {
      if (strpos($index, '#') === 0) continue;
      if (is_array($condition) && !empty($condition['field']) && is_string($condition['field']) && $condition['field'] == 'users.status') {
        unset($where[$index]);
      }
    }
  }
}

/**
 * Access callback to user entities.
 */
function iela_metadata_user_access($op, $entity = NULL, $account = NULL, $entity_type) {
  $access = entity_metadata_user_access($op, $entity, $account, $entity_type);
  if (!$access && $op == 'view' && user_access('access user profiles', $account)) {
    $access = true;
  }
  return $access;
}

/**
 * Implements hook_inline_entity_form_entity_form_alter(&$entity_form, &$form_state)
 */
function iela_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
  module_load_include('inc', 'cer', 'cer.cer');

  $inline_entity_form = $form_state['inline_entity_form'][$entity_form['#ief_id']];

  $parent_entity_type = $inline_entity_form['instance']['entity_type'];
  $parent_bundle      = $inline_entity_form['instance']['bundle'];
  $parent_field_name  = $inline_entity_form['instance']['field_name'];
  $parent_field_info  = field_info_field($parent_field_name);

  $cer_fields = cer_cer_fields();

  // We will check if parent's field responsible for generating this inline
  // entity form has a "cer" definition for it.
  $parent_field_is_cer = FALSE;
  foreach ($cer_fields as $cer_field_name => $cer_field_info) {
    list($cer_field_entity_type, $cer_field_bundle, $cer_field_field_name) = explode(':', $cer_field_name);
    if (
      $cer_field_entity_type == $parent_entity_type &&
      $cer_field_bundle == $parent_bundle &&
      $cer_field_field_name == $parent_field_name
    ) {
      $parent_field_is_cer = TRUE;
      break;
    }
  }

  // Now, we will check if any of the inline entity's fields is a reference
  // field that might point to the parent entity. We will then check if the
  // field also has a "cer" definition.
  if (!$parent_field_is_cer) {
    return; // If parent check failed, no bother to continue processing.
  } else {
    $fields = field_info_instances($entity_form['#entity_type'], $entity_form['#bundle']);
    foreach ($fields as $field_name => $field_instance) {
      $field_info = field_info_field($field_name);
      if (
        $field_info['type'] == 'entityreference' &&
        $field_info['settings']['target_type'] == $parent_entity_type &&
        in_array($parent_bundle, $field_info['settings']['handler_settings']['target_bundles'])
      ) {
        foreach ($cer_fields as $cer_field_name => $cer_field) {
          list($cer_field_entity_type, $cer_field_bundle, $cer_field_field_name) = explode(':', $cer_field_name);
          if (
            $cer_field_entity_type == $entity_form['#entity_type'] &&
            $cer_field_bundle == $entity_form['#bundle'] &&
            $cer_field_field_name == $field_name
          ) {
            // Hide parent field.
            $entity_form[$field_name]['#access'] = FALSE;

            // Remove field required status.
            $entity_form[$field_name][LANGUAGE_NONE]['#required'] = 0;

            if (isset($entity_form[$field_name][LANGUAGE_NONE]['#max_delta'])) {
              $delta = $entity_form[$field_name][LANGUAGE_NONE]['#max_delta'];
              if (is_numeric($delta) && $delta >= 0) {
                for(; $delta >= 0; $delta--) {
                  $entity_form[$field_name][LANGUAGE_NONE][$delta]['target_id']['#required'] = FALSE;
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_ctools_render_alter().
 *
 * Temporary solution to load meta tags on entity pages that are driven by
 * CTools display handlers.
 */
function iela_ctools_render_alter(&$info, $page, $context) {

  // Only proceed if this is a full page (don't process individual panes) and
  // there's an 'admin path' for the current task.
  if ($page && !empty($context['args'][0]) && is_numeric($context['args'][0])) {
    $admin_path = NULL;

    if (!empty($context['task']['admin path'])) {
      $admin_path = $context['task']['admin path'];
    } elseif (!empty($context['subtask']['admin path'])) {
      $admin_path = $context['subtask']['admin path'];
    }

    if (empty($admin_path)) {
      return;
    }

    // Check if this is an entity display page, if so trigger
    // hook_entity_view().
    foreach (entity_get_info() as $entity_type => $entity_info) {
      // Entity paths will include an auto-loader that matches the entity's
      // name, thus the path will be 'some/path/%entity_name'.
      if (isset($entity_info['default path']) && $admin_path == $entity_info['default path']) {
        // Only proceed if this entity type supports meta tags.
        if (metatag_entity_supports_metatags($entity_type)) {
          $entities = entity_load($entity_type, array($context['args'][0]));
          $entity = array_pop($entities);
          metatag_entity_view($entity, $entity_type, 'full', NULL);
        }

        break;
      }
    }
  }
}

/**
 * Implements hook_form_ctools_entity_field_content_type_formatter_styles_alter().
 */
function iela_form_ctools_entity_field_content_type_formatter_styles_alter(&$form, &$form_state) {
  // Add 'delta suffix' formatting option.
  if (!empty($form['delta_limit'])) {
    list($prefix, $suffix) = explode('@suffix', t('Then add the suffix @suffix after the last item'));
    $form['delta_suffix'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#field_prefix' => $prefix,
      '#field_suffix' => $suffix,
      '#description' => t('You can use the tokens "[remaining]", "[shown]", and "[total]".'),
      '#default_value' => !empty($form_state['conf']['formatter_settings']['delta_suffix']) ? $form_state['conf']['formatter_settings']['delta_suffix'] : '',
    );

    $form['#submit'][] = 'iela_form_ctools_entity_field_content_type_formatter_styles_submit';
  }
}

/**
 * Custom submit for ctools fields style formatters.
 */
function iela_form_ctools_entity_field_content_type_formatter_styles_submit(&$form, &$form_state) {
  if (isset($form_state['values']['delta_suffix'])) {
    $form_state['conf']['formatter_settings']['delta_suffix'] = $form_state['values']['delta_suffix'];
  }
}

/**
 * Implements hook_field_attach_view_alter().
 */
function iela_field_attach_view_alter(&$output, $context) {
  if (!empty($output) && is_array($context['display']) && !empty($context['display']['settings']['delta_suffix'])) {
    $field_name = array_keys($output)[0];
    $element = &$output[$field_name];

    $entity = entity_metadata_wrapper($context['entity_type'], $context['entity']);
    $entity_id = $entity->getIdentifier();
    $reloaded = entity_load($context['entity_type'], array($entity_id), array(), TRUE);
    $field_real_values = field_get_items($context['entity_type'], $reloaded[$entity_id], $field_name);

    $shown = count($element['#items']);
    $total = count($field_real_values);
    $remaining = $total - $shown;

    $search = array('[shown]','[total]','[remaining]');
    $replace = array($shown, $total, $remaining);
    $suffix = str_replace($search, $replace, $context['display']['settings']['delta_suffix']);

    if (!empty($suffix)) {
      $element[] = array('#markup' => $suffix);
      $element['#attributes']['class'][] = 'delta-suffix';
    }
  }
}
