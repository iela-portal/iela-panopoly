<?php
/**
 * @file
 * Preprocess functions.
 */

/**
 * Implements template_preprocess_html().
 */
function iela_preprocess_html(&$variables) {

  // Add class to page.
  if (path_is_admin(current_path())) {
    $variables['classes_array'][] = 'page-admin';
  }
}

/**
 * Implements template_preprcess_node().
 */
function iela_preprocess_node(&$variables) {
  $variable_name = "timeago_node_{$variables['type']}_{$variables['view_mode']}";
  $variable_value = variable_get($variable_name, 1);

  if (!empty($variable_value)) {
    $node = $variables['node'];
    $variables['date'] = timeago_format_date($node->created, $variables['date']);
    if (variable_get('node_submitted_' . $node->type, TRUE)) {
      $variables['submitted'] = t('Submitted by !username !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
    }

    global $language;
    if ($language->language !== 'en') {
      drupal_add_js(libraries_get_path('timeago') . '/locales/jquery.timeago.' . $language->language . '.js', array(
        'group' => JS_THEME,
      ));
      drupal_add_js(drupal_get_path('module', 'iela') . '/js/timeago.js', array(
        'group' => JS_THEME,
      ));
    }
  }
}

/**
 * Implements template_preprocess_panels_pane().
 */
function iela_preprocess_panels_pane(&$variables) {

  $content = &$variables['content'];
  $pane = &$variables['pane'];

  switch ($pane->type) {
    case 'search_box':
    case 'search_form':

      /*
       * Configure placeholder option.
       */
      if (!empty($pane->configuration['placeholder']) && !empty($content['basic']['keys'])) {
        $content['basic']['keys']['#attributes']['placeholder'] = $pane->configuration['placeholder'];
      }

      break;
  }
}

/**
 * Implements template_preprocess_textfield().
 */
function iela_preprocess_textfield(&$variables) {
  if (!empty($variables['element']['#entity_type'])) {
    iela_preprocess_add_placeholder_text($variables);
  }
}

/**
 * Implements template_preprocess_textarea().
 */
function iela_preprocess_textarea(&$variables) {
  if (!empty($variables['element']['#entity_type'])) {
    iela_preprocess_add_placeholder_text($variables);
  }
}

/**
 * Implements template_preprocess_status_messages().
 */
function iela_preprocess_status_messages(&$variables) {

  // Load the currently logged in user.
  global $user;

  // Avoid heavy processing when user should always access all messages.
  // if ($user->uid === 1 || in_array('administrator', $user->roles)) return;

  // Hidden messages:
  $excludes = array(
    array('panelizer_panelizer_task_render()', '240', 'profiles/panopoly/modules/contrib/panelizer/plugins/task_handlers/panelizer_node.inc'),
    array(t('Click "Customize this page" in the bottom black toolbar to edit the content of this region.')),
  );

  // Load-up all current messages to iterate.
  foreach (drupal_get_messages($variables['display'], true) as $type => $messages) {
    foreach ($messages as $message) {
      $match = false;

      foreach ($excludes as $exclude) {

        // Try parts.
        foreach ($exclude as $part) if (strpos($message, $part) === FALSE) continue 2;

        $match = true;
        break;
      }

      // If messages was not matched against excludes, put it back:
      if (!$match) drupal_set_message($message, $type, TRUE);
    }
  }
}

/**
 * Implements template_preprocess_views_view().
 */
function iela_preprocess_views_view(&$variables) {
  if (!empty($variables['view']->tag) && strpos($variables['view']->tag, 'admin') !== FALSE) {
    drupal_add_css(drupal_get_path('module', 'admin_views') . '/admin_views.css');
  }
}
