<?php

/*
 * @file
 * Migration functions
 */

module_load_include('inc', 'iela', 'includes/migrate/iela.migrate.node.news');
module_load_include('inc', 'iela', 'includes/migrate/iela.migrate.node.news.images');
module_load_include('inc', 'iela', 'includes/migrate/iela.migrate.node.news.files');

/**
 * IELA migration options form.
 */
function iela_migration_options() {
  return array(
    'type' => array(
      '#type' => 'select',
      '#title' => t('Content type'),
      '#description' => t('Choose the content type to migrate from the old database.'),
      '#options' => array(
        'node_news' => t('Import news'),
        'node_news_images' => t('Download news images'),
        'node_news_files' => t('Download news files'),
      ),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Start'),
    ),
  );
}

/**
 * IELA migration submit handler.
 */
function iela_migration_options_submit($form, &$form_state) {
  $function = 'iela_migration_' . $form_state['values']['type'];
  if (function_exists($function)) {
    batch_set($function());
  }
}

/**
 * IELA migration node import finish.
 */
function iela_migration_node_finished($success, $results, $operations) {
  if ($success) {
    if (!empty($results['created'])) {
      drupal_set_message(t('Successfully created @count nodes.', array('@count' => $results['created'])));
    }
    if (empty($results['failed'])) {
      foreach ($results['failed'] as $title) {
        drupal_set_message(t('Failed to create "@title" node.', array(
          '@title' => $title
        )), 'error');
      }
    }
    // if (!empty($results['updated'])) {
    //   drupal_set_message(t('Successfully updated @count nodes.', array('@count' => $results['updated'])));
    // }
  }
}

/**
 * Helper function to download external file and manage it.
 */
function download_external_file($url, $uri, $save_mode = FILE_EXISTS_RENAME, $manage_file = TRUE) {
 
  $url_info = parse_url($url);
  $url_path_info = pathinfo($url_info['path']);
   
  //This helps with filenames with spaces
  $url = $url_info['scheme'] . '://' . $url_info['host']  . $url_path_info['dirname'] .'/'. rawurlencode($url_path_info['basename']);
 
  //Need to remove the filename from the uri
  $uri_target = file_uri_target($uri);
  $uri_scheme = file_uri_scheme($uri);
  $uri_path_info = pathinfo($uri_target);
  $directory = file_stream_wrapper_uri_normalize($uri_scheme . "://" . $uri_path_info['dirname']);

  if(file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
    $drupal_result = drupal_http_request($url);
    if(!empty($drupal_result->data)) {
      $path = file_stream_wrapper_uri_normalize($uri);
      if($manage_file) {
        $new_file = file_save_data($drupal_result->data, $path, $save_mode);
      } else {
        return file_unmanaged_save_data($drupal_result->data, $path, $save_mode);
      }
    } else {
      drupal_set_message("Error downloading file, no data recieved for " . $url);
      return FALSE;
    }
 
    $new_file->display = 1;
    return (array)$new_file;
  } else {
    drupal_set_message("Could not create directory");
  }
}
