<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Get a theme's tree of base themes info.
 */
function iela_theme_tree($theme_key = NULL) {
  if (empty($theme_key)) {
    $theme_key = variable_get('theme_default');
  }

  $themes = system_list('theme');
  $theme_tree = $themes[$theme_key]->base_themes;
  foreach ($theme_tree as $key => $theme_name) {
    $theme_tree[$key] = $themes[$key];
  }
  $theme_tree[$theme_key] = $themes[$theme_key];

  return $theme_tree;
}

/**
 * Get currently breakpoints.
 */
function iela_get_breakpoints_theme_tree($theme, $enabled_only = TRUE) {

  $breakpoints = array();
  $themes = iela_theme_tree($theme);

  foreach ($themes as $theme_key => $theme) {
    foreach (breakpoints_breakpoint_load_all_theme($theme_key) as $key => $bps) {
      $breakpoints[$bps->machine_name] = $bps;
    }
  }

  // Filter disabled, if set to.
  if ($enabled_only == TRUE) {
    $breakpoints = array_filter($breakpoints, function ($breakpoint) {
      return is_object($breakpoint) && !empty($breakpoint->status);
    });
  }

  return $breakpoints;
}

/**
 * Recursively walk an array keeping a path reference.
 */
function iela_array_walk_recursive_with_path($array, $callback, $path = array(), &$root_parent = NULL) {

  if (empty($root_parent)) $root_parent = $array;

  foreach ($array as $key => $value) {
    if (is_array($value)) {
      $current_path = $path;
      $current_path[] = $key;
      iela_array_walk_recursive_with_path($value, $callback, $current_path, $root_parent);
    }
    else {
      $current_path = $path;
      $current_path[] = $key;
      $callback($value, $current_path, $root_parent);
    }
  }
}

/**
 * Adds placeholder text to a field template.
 */
function iela_preprocess_add_placeholder_text(&$variables) {
  $element = &$variables['element'];

  if (!empty($element['#entity_type'])
    && !empty($element['#field_name'])
    && !empty($element['#bundle'])) {
    $instance = field_info_instance(
      $element['#entity_type'],
      $element['#field_name'],
      $element['#bundle']
    );

    if (!empty($instance['placeholder_text'])) {
      $element['#attributes']['placeholder'] = $instance['placeholder_text'];
    }
  }
}

/**
 * Custom validator for CSS ID values.
 */
function iela_css_id_validate($element, &$form_state) {
  $parents = !empty($element['#parents']) ? $element['#parents'] : array('id');
  $value = &$form_state['values'];
  $count = 0;

  while ($count < count($parents)) {

    // Avoid undefined.
    if (empty($value[$parents[$count]])) return;

    $value = &$value[$parents[$count]];
    $count++;
  }

  // Avoid further processing.
  if (empty($value) || !is_string($value)) return;

  if (preg_match('/^[a-zA-Z][\w:.-]*$/', $value) !== 1) {
    $error = t('You have to enter a valid ID. Please refer to the <a href="@url">W3c id and name specification</a> for further information.', array(
      '@url' => url('http://www.w3.org/TR/html4/types.html#type-id'),
    ));
    form_set_error(implode('][', $parents), $error);
  }
}

/**
 * Walks every item in a menu tree and applies the callback to each.
 */
function iela_walk_menu_items(&$items, $callback) {
  foreach ($items as $uuid => &$item) {
    if (strpos($uuid, '#') === 0) return;
    $callback($item);
    if (!empty($item['#below'])) iela_walk_menu_items($item['#below'], $callback);
  }
}

/**
 * Mount a token data array from a context structure.
 */
function iela_contexts_to_token_array($contexts) {
  $token_data = array();
  if (is_object($contexts) && isset($contexts->keyword) && isset($contexts->data)) {
    $token_data[$contexts->keyword] = $contexts->data;
  } elseif (is_array($contexts)) {
    foreach ($contexts as $context) {
      foreach (iela_contexts_to_token_array($context) as $keyword => $data) {
        $token_data[$keyword] = $data;
      }
    }
  }
  return $token_data;
}

/**
 * Field all fields that would be available to the context joined by their label.
 */
function iela_get_context_available_fields_by_label($context) {
  $available_fields = array();

  if (is_object($context) && is_array($context->type) && in_array('entity', $context->type)) {
    // Grab some meta and info about the context entity.
    $entity_type = end($context->type);
    $entity_info = entity_get_info($entity_type);
    $bundle_fields = field_info_instances($entity_type);
    $restrictions = isset($context->restrictions) ? $context->restrictions : array();

    // Search bundle fields.
    if (!empty($bundle_fields)) {
      foreach ($bundle_fields as $bundle => $field_intances) {
        if (!empty($restrictions['type']) && !in_array($bundle, $restrictions['type'])) {
          continue;
        }

        $bundle_label = $entity_info['bundles'][$bundle]['label'];
        foreach ($field_intances as $field_name => $field_instance) {
          $available_fields[$field_name][$field_instance['label']][$bundle] = $bundle_label;
        }
      }
    }
  }

  return $available_fields;
}

/**
 * Generates a pane wrapper id.
 */
function iela_get_pane_wrapper_id($pane, $display = null) {
  $prefix = 'panel-pane-wrapper-';

  if (!empty($display->css_id)) {
    $prefix .= str_replace('_', '-', $display->css_id) . '-';
  }

  return $prefix . $pane->pid;
}

/**
 * Fulfil pane wrapper id on preprocessors.
 */
function iela_fulfil_pane_wrapper_id(&$variables) {
  if (empty($variables['pane_wrapper_attributes']['id'])) {
    $variables['pane_wrapper_attributes']['id'] = iela_get_pane_wrapper_id($variables['pane'], $variables['display']);
  }

  return $variables['pane_wrapper_attributes']['id'];
}
