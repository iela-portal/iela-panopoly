<?php

/*
 * @file
 * File download for news content-type.
 */

/**
 * IELA news files migration batch definition.
 */
function iela_migration_node_news_files() {

  // Prepare query object.
  $query = new EntityFieldQuery();

  $result = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'news')
    ->execute();

  $news = $result['node'];
  $news_fields = field_info_instances('node', 'news');

  field_attach_load('node', $news, FIELD_LOAD_CURRENT, array(
    'field_id' => $news_fields['body']['field_id'],
  ));

  // Operation registries.
  $batch = array(
    'operations' => array(),
    'finished' => 'iela_migration_node_news_files_finished',
    'title' => t('Downloading files from @count news.', array(
      '@count' => count($news)
    )),
    'init_message' => t('Downloads are starting.'),
    'progress_message' => t('Downloaded @current out of @total.'),
    'error_message' => t('Failed to download news files'),
  );

  foreach ($news as $nid => $entity) {
    if (!empty($entity->body[LANGUAGE_NONE][0]['value'])) {
      $batch['operations'][] = array(
        'iela_migration_node_news_files_save_record',
        array(
          'nid' => $nid,
          'entity' => $entity
        ),
      );
    }
  }

  return $batch;
}

/**
 * IELA news files migration operation.
 */
function iela_migration_node_news_files_save_record($nid, $node, &$context) {

  $destination = 'public://iela-legacy-files';
  $iela_legacy_url = 'http://antigo.iela.ufsc.br';

  $html = str_get_html($node->body[LANGUAGE_NONE][0]['value']);
  $migrate_files = array_filter($html->find('a'), function ($anchor) {
    // Old site puts dynamic files inside '/uploads', and use relative paths.
    return isset($anchor->href) && strpos($anchor->href, '/uploads/') === 0;
  });

  if (!empty($migrate_files)) {
    foreach ($migrate_files as $migrating_file) {

      $file_url = $iela_legacy_url . $migrating_file->href;
      $file_destination = $destination . $migrating_file->href;

      if (file_exists($file_destination)) {
        // Force managed existing file.
        $file = file_uri_to_object($file_destination);
      } else {
        // Download new file.
        $file = download_external_file($file_url, $file_destination, FILE_EXISTS_REPLACE);
      }

      // Avoid breaking process.
      if (empty($file)) continue;

      // Make sure file is an object.
      $file = (object) $file;

      if (!isset($file->fid) || empty($file->fid)) continue;

      $context['results']['files'] += 1;

      // Make sure file does not get removed for no usage.
      file_usage_add($file, 'file', 'node', $node->nid);

      $migrating_file_url = file_create_url($file->uri);
      $parsed_url = parse_url($migrating_file_url);

      // Use relative path, for it to work on all environments.
      $migrating_file->href = $parsed_url['path'];
    }

    // Update body;
    $node->body[LANGUAGE_NONE][0]['value'] = $html->save();

    node_save($node);

    // Node got change.
    $context['results']['nodes'] += 1;
  }
}

/**
 * IELA news files download finish.
 */
function iela_migration_node_news_files_finished($success, $results, $operations) {
  if ($success) {
    if (!empty($results['files'])) {
      drupal_set_message(t('Successfully download @files files from @nodes news.', array(
        '@files' => $results['files'],
        '@nodes' => $results['nodes'],
      )));
    }
  }
}
