<?php

/*
 * @file
 * Content-type news migration.
 */

/**
 * IELA news node migration batch definition.
 */
function iela_migration_node_news() {

  // Change database.
  db_set_active('hpiela');

  // Load content records.
  $records = db_select('noticias', 'n')
    ->fields('n')
    ->execute();

  // Return database.
  db_set_active();

  // Operation registries.
  $batch = array(
    'operations' => array(),
    'finished' => 'iela_migration_node_finished',
    'title' => t('Importing @count news.', array(
      '@count' => count($records)
    )),
    'init_message' => t('Import is starting.'),
    'progress_message' => t('Imported @current out of @total.'),
    'error_message' => t('Import has encountered an error.'),
  );

  foreach ($records as $index => $record) {
    $batch['operations'][] = array(
      'iela_migration_node_news_save_record',
      array(
        'record' => $record,
        'content_type' => 'news'
      ),
    );
  }

  return $batch;
}

/**
 * IELA migration node import operation.
 */
function iela_migration_node_news_save_record($record, $content_type, &$context) {

  $query = new EntityFieldQuery();
  $entities = $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'news')
    ->propertyCondition('title', $record->titulo)
    ->range(0, 1)
    ->execute();

  // Avoid modifying current nodes.
  if (empty($entities['node'])) {
    $node = (object) NULL;
    $node->uid = 0;
    $node->is_new = TRUE;
    $node->type = 'news';
    $node->title = $record->titulo;
    $node->created = strtotime($record->data_insert);
    $node->changed = strtotime("now");
    $node->status = 0;
    $node->comment = 0;
    $node->promote = 0;
    $node->moderate = 0;
    $node->sticky = 0;
    $node->language = 'und';
    $node->body[$node->language][0]['value']   = $record->texto;
    $node->body[$node->language][0]['summary'] = text_summary($record->chamada);
    $node->body[$node->language][0]['format']  = 'filtered_html';

    // Fix negative dates.
    if (((int) $node->created) <= 0) {
      $node->created = $node->changed;
    }

    node_save($node);

    if (isset($node->nid) && !empty($node->nid)) {
      $context['results']['created'] += 1;
    } else {
      $context['results']['failed'][] = $record->titulo;
    }
  }
}
