<?php

/*
 * @file
 * Image download for news content-type.
 */

/**
 * IELA news images migration batch definition.
 */
function iela_migration_node_news_images() {

  // Prepare query object.
  $query = new EntityFieldQuery();

  $result = $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'news')
    ->execute();

  $news = $result['node'];
  $news_fields = field_info_instances('node', 'news');

  field_attach_load('node', $news, FIELD_LOAD_CURRENT, array(
    'field_id' => $news_fields['body']['field_id'],
  ));

  field_attach_load('node', $news, FIELD_LOAD_CURRENT, array(
    'field_id' => $news_fields['field_featured_image']['field_id'],
  ));

  // Operation registries.
  $batch = array(
    'operations' => array(),
    'finished' => 'iela_migration_node_news_images_finished',
    'title' => t('Downloading images from @count news.', array(
      '@count' => count($news)
    )),
    'init_message' => t('Downloads are starting.'),
    'progress_message' => t('Downloaded @current out of @total.'),
    'error_message' => t('Failed to download news images'),
  );

  foreach ($news as $nid => $entity) {
    if (!empty($entity->body[LANGUAGE_NONE][0]['value'])) {
      $batch['operations'][] = array(
        'iela_migration_node_news_images_save_record',
        array(
          'nid' => $nid,
          'entity' => $entity
        ),
      );
    }
  }

  return $batch;
}

/**
 * IELA news images migration operation.
 */
function iela_migration_node_news_images_save_record($nid, $node, &$context) {

  $destination = 'public://iela-legacy-files';
  $iela_legacy_url = 'http://antigo.iela.ufsc.br';

  $html = str_get_html($node->body[LANGUAGE_NONE][0]['value']);
  $migrate_images = array_filter($html->find('img'), function ($image) {
    // Old site puts dynamic images inside '/uploads', and use relative paths.
    return isset($image->src) && strpos($image->src, '/uploads/') === 0;
  });

  if (!empty($migrate_images)) {
    $featured = null;

    foreach ($migrate_images as $image) {

      $file_url = $iela_legacy_url . $image->src;
      $file_destination = $destination . $image->src;

      if (file_exists($file_destination)) {
        // Force managed existing file.
        $file = file_uri_to_object($file_destination);
      } else {
        // Download new file.
        $file = download_external_file($file_url, $file_destination, FILE_EXISTS_REPLACE);
      }

      // Avoid breaking process.
      if (empty($file)) continue;

      // Make sure file is an object.
      $file = (object) $file;

      if (!isset($file->fid) || empty($file->fid)) continue;

      // Image will get changed.
      $context['results']['images'] += 1;

      // Make sure file does not get removed for no usage.
      file_usage_add($file, 'file', 'node', $node->nid);

      $image_url = file_create_url($file->uri);
      $parsed_url = parse_url($image_url);

      // Use relative path, for it to work on all environments.
      $image->src = $parsed_url['path'];

      // Save biggest image as featured one.
      if (!$featured || $featured->metadata['height'] < $file->metadata['height']) {
        $featured = $file;
      }
    }

    // Update body;
    $node->body[LANGUAGE_NONE][0]['value'] = $html->save();

    // Update feature image.
    if ($featured && empty($node->field_featured_image[LANGUAGE_NONE][0])) {
      $node->field_featured_image[LANGUAGE_NONE][0] = (array) $featured;
    }

    node_save($node);

    // Node got change.
    $context['results']['nodes'] += 1;
  }
}

/**
 * IELA news images download finish.
 */
function iela_migration_node_news_images_finished($success, $results, $operations) {
  if ($success) {
    if (!empty($results['images'])) {
      drupal_set_message(t('Successfully download @images images from @nodes news.', array(
        '@images' => $results['images'],
        '@nodes' => $results['nodes'],
      )));
    }
  }
}
