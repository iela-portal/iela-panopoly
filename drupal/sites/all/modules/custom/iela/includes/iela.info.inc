<?php
/**
 * @file
 * IELA module info and derivative implementations.
 */

/**
 * Implements hook_action_info().
 */
function iela_action_info() {
  return array(
    'iela_path_alias_recreate' => array(
      'type' => 'entity',
      'label' => t('Recreate path alias'),
      'behavior' => array('changes_property'),
      'configurable' => FALSE,
      'vbo_configurable' => TRUE,
      'triggers' => array('any'),
    ),
  );
}

/**
 * Implements custom action form.
 */
function iela_path_alias_recreate_form($settings, &$form_state) {
  return array(
    'force' => array(
      '#type' => 'checkbox',
      '#title' => t('Override custom paths'),
      '#description' => t('Mark this i you wish to override any custom paths on the selected entities.'),
      '#default_value' => isset($settings['settings']['force']) ? $settings['settings']['force'] : FALSE,
    ),
  );
}

function iela_path_alias_recreate_submit($form, $form_state) {
  $return = array();
  foreach ($form_state['values'] as $name => $value) {
    $return[$name] = $value;
  }
  return $return;
}

/**
 * Action to reset path alias.
 */
function iela_path_alias_recreate(&$entity, $context) {
  $recreate_path = !empty($context['force']) ? TRUE : FALSE;

  if (!$recreate_path) {
    // Find entity IDs.
    $ids = entity_extract_ids($context['entity_type'], $entity);
    $state = pathauto_entity_state_load($context['entity_type'], $ids[0]);
    $recreate_path = !empty($state) ? TRUE : FALSE;
  }

  if ($recreate_path) {
    $entity->path = array(
      'pathauto' => TRUE,
    );
  }

  // $message = t('Node title is %title. Sincerely, %hero', array(
  //   '%title' => $node->title,
  //   '%hero' => $context['hero'],
  // ));
  // drupal_set_message($message);
}

/**
* Implements hook_token_info().
*/
function iela_token_info() {
  return array(
    'tokens' => array(
      'node' => array(
        'project_name' => array(
          'name' => t('Project name'),
          'description' => t('The project associated with this node, if available.'),
        ),
        'project_hierarchy' => array(
          'name' => t('Project hierarchy'),
          'description' => t('The full hierarchy for the associated project term.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function iela_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  if ($type == 'node' && !empty($data['node'])) {
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      switch($name) {
        case 'project_name':
          if (!empty($node->field_project[LANGUAGE_NONE][0]['tid']) && count($node->field_project[LANGUAGE_NONE]) == 1) {
            $term = taxonomy_term_load($node->field_project[LANGUAGE_NONE][0]['tid']);

            if (!empty($term)) {
              $replacements[$original] = pathauto_cleanstring($term->name) . '/';
            }
          }
          break;

        case 'project_hierarchy':
          if (!empty($node->field_project[LANGUAGE_NONE][0]['tid']) && count($node->field_project[LANGUAGE_NONE]) == 1) {
            $terms = taxonomy_get_parents_all($node->field_project[LANGUAGE_NONE][0]['tid']);
            $names = array();

            foreach (array_reverse($terms) as $term) {
              $names[] = pathauto_cleanstring($term->name);
            }

            if (!empty($names)) {
              $replacements[$original] = join('/', $names);
            }
          }
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_field_formatter_info().
 */
function iela_field_formatter_info() {
  return array(
    'download_icon' => array(
      'label' => t('Download Icon'),
      'field types' => array(
        'file',
        'image'
      ),
      'settings' => array(
        'text' => '',
        'title' => 'Download: [file:name]',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 * @todo : allow choosing the font-awesome icon.
 */
function iela_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = array();

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'download_icon':
      $element['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#description' => t('This text will appear when a user mouses over.'),
        '#default_value' => $settings['title'],
      );

      $element['text'] = array(
        '#type' => 'textfield',
        '#title' => t('Text'),
        '#description' => t('This text will appear next to the icon. Leave empty to show only the title.'),
        '#default_value' => $settings['text'],
      );

      $element['tokens'] = array(
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => '<i>' . t('Your can use !tokens on the fields above.', array(
          '!tokens' => l('tokens', 'https://www.drupal.org/node/390482', array(
            'attributes' => array('target' => '_blank')
          )),
        )) . '</i>',
      );
      break;
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function iela_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'download_icon':
      if (!empty($settings['title'])) return '<strong>' . t('Title') . ':</strong> "<i>' . $settings['title'] . '</i>"';
      break;
  }

  return '';
}

/**
* Implements hook_field_formatter_view().
*/
function iela_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = empty($display['settings']) ? array() : $display['settings'];

  switch ($display['type']) {
    case 'download_icon':
      foreach ($items as $delta => $item) {
        $title = !empty($settings['title']) ? $settings['title'] : (!empty($item['description']) ? $item['description'] : $item['filename']);
        $text = empty($settings['title']) ? '' : $settings['title'];

        $file = file_load($item['fid']);

        $token_data = array($entity_type => $entity, 'file' => $file);
        $token_options = array('clear' => true, 'sanitize' => false);

        $title = token_replace($title, $token_data, $token_options);
        $text = token_replace($text, $token_data, $token_options);

        $element[$delta] = array(
          '#theme' => 'download_icon', 
          '#title' => $title, 
          '#text' => $text,
          '#url' => file_create_url($item['uri']),
        );
      }
      break;
  }

  return $element;
}
