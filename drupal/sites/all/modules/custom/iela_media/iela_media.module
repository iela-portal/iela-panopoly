<?php
/**
 * @file
 * Code for the IELA Media feature.
 */

include_once 'iela_media.features.inc';

/**
 * @file
 * Code for the IELA Media main module.
 */

module_load_include('inc', 'iela_media', 'includes/iela_media.form');

/**
 * Implements hook_module_implements_alter().
 */
function iela_media_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'media_wysiwyg_wysiwyg_allowed_view_modes_alter') {
    $group = $implementations['iela_media'];
    unset($implementations['iela_media']);
    $implementations['iela_media'] = $group;
  }
}

/**
 * Implements hook_filter_info().
 */
function iela_media_filter_info() {
  $filters['anchored_media_filter'] = array(
    'title' => t('Parses anchors arround media'),
    'description' => t('This will make sure tat surrounding anchor tags are properly parsed to media elements.'),
    'process callback' => 'anchored_media_filter',
    'weight' => 1,
    // @TODO not implemented
    'tips callback' => 'anchored_media_filter_tips',
  );

  return $filters;
}

/**
 * Filter callback for anchored media filter.
 */
function anchored_media_filter($text) {
  $text = preg_replace_callback('/<a[^>]*[^>]*>(\[\[.*?\]\])<\/a>/s', 'anchored_media_filter_anchored_media', $text);
  return $text;
}

function anchored_media_filter_anchored_media($match, $wysiwyg = FALSE) {
  $html = new simple_html_dom();
  $html->load($match[0]);

  // Even if it is only one a, grab ona loop to easy access it.
  foreach ($html->find('a') as $anchor) {
    if (!empty($anchor->attr)) {
      return str_replace('}]]', ',"wrapping_anchor":' . json_encode($anchor->attr) . '}]]', $match[1]);
    }
  }

  return $match[1];
}

/**
 * Implements hook_entity_load().
 */
function iela_media_entity_load($entities, $type) {
  foreach ($entities as $entity) {
    if (!isset($entity->entity_type)) {
      $entity->entity_type = $type;
    }
  }
}

/**
 * Implements hook_entity_info_alter().
 */
function iela_media_entity_info_alter(&$entity_info) {

  // Featured view mode for files.
  $entity_info['file']['view modes']['featured'] = array(
    'label' => t('Featured'),
    'custom settings' => TRUE,
  );

  // Poster view mode for files.
  $entity_info['file']['view modes']['poster'] = array(
    'label' => t('Poster'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements hook_image_styles_alter(). 
 */
function iela_media_image_styles_alter(&$styles) {
  foreach ($styles as $name => &$style) {
    if (!empty($style['storage']) && $style['storage'] === IMAGE_STORAGE_DEFAULT) {
      if ($style['module'] !== 'iela_media') {
        $style['label'] = '[administrative] ' . $style['label'];
      }
    }
  }
}

/**
 * Implements hook_file_formatter_info().
 */
function iela_media_file_formatter_info() {
  $formatters['adaptative_image'] = array(
    'label' => t('Adaptative Image'),
    'default settings' => array(
      'srcset' => array(),
      'fallback' => '',
      'alter_results' => FALSE,
      'override_style' => FALSE,
      'image_link' => '',
    ),
    'view callback' => 'iela_media_file_formatter_adaptative_image_view',
    'settings callback' => 'iela_media_file_formatter_adaptative_image_settings',
  );

  return $formatters;
}

/**
 * Implements settings callback for file formatter.
 */
function iela_media_file_formatter_adaptative_image_settings($form, &$form_state, $settings) {
  $image_styles = image_style_options(FALSE);

  $element['srcset'] = array(
    '#type' => 'select',
    '#title' => t('Image styles'),
    '#description' => t('Select the image styles you wish to make available for adaptativeness.'),
    '#required' => TRUE,
    '#options' => $image_styles,
    '#number' => 2,
    '#add_more' => TRUE,
    '#default_values' => $settings['srcset'],
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
  );

  $element['fallback'] = array(
    '#type' => 'select',
    '#title' => t('Fallback'),
    '#description' => t('Select the image style to be used as default, if no JavaScript is enabled.'),
    '#empty_option' => t('None (original image)'),
    '#default_value' => $settings['fallback'],
    '#options' => $image_styles,
  );

  $element['alter_results'] = array(
    '#type' => 'checkbox',
    '#title' => t('Apply to results'),
    '#description' => t('Enable this to try to handle adaptative image after other formatters, when they output images.'),
    '#default_value' => $settings['alter_results'],
  );

  $element['override_style'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override image style'),
    '#description' => t('When "Apply to results" is enabled, check this to also override the result\'s image style with the one defined on Fallback.'),
    '#default_value' => $settings['override_style'],
  );

  $element['image_link'] = array(
    '#title' => t('Link image to'),
    '#type' => 'select',
    '#default_value' => $settings['image_link'],
    '#empty_option' => t('Nothing'),
    '#options' => array(
      'content' => t('Content'),
      'file' => t('File'),
    ),
  );

  return $element;
}

/**
 * Implements hook_media_wysiwyg_format_form_prepare_alter().
 */
function iela_media_media_wysiwyg_format_form_prepare_alter(&$form, &$form_state, $file) {
  if ($file->type == 'image') {
    $query_fields = isset($_GET['fields']) ? drupal_json_decode($_GET['fields']) : array();

    $form['options']['legend_position'] = array(
      '#type' => 'select',
      '#title' => t('Legend position'),
      '#options' => array(
        'hidden' => t('Hidden'),
        'left-above' => t('Above') . '/' . t('Left'),
        'center-above' => t('Above') . '/' . t('Center'),
        'right-above' => t('Above') . '/' . t('Right'),
        'left-below' => t('Below') . '/' . t('Left'),
        'center-below' => t('Below') . '/' . t('Center'),
        'right-below' => t('Below') . '/' . t('Right'),
      ),
      '#default_value' => isset($query_fields['legend_position']) ? $query_fields['legend_position'] : 'center-below',
    );
  }
}

/**
 * Implements form_file_entity_file_display_form_alter().
 */
function iela_media_form_file_entity_file_display_form_alter(&$form, &$form_state, $form_id) {

  // Executed here so that parent tree is already built.
  taller_forms_multifield(array('displays', 'settings', 'adaptative_image', 'srcset'), $form, $form_state);
  array_unshift($form['#validate'], 'iela_media_form_file_entity_file_display_form_validate');
}

/**
 * Implements iela_media_form_file_entity_file_display_form validation.
 */
function iela_media_form_file_entity_file_display_form_validate(&$form, &$form_state) {
  if (!empty($form_state['values']['displays']['adaptative_image']['settings']['srcset']['fieldset_multifield_srcset'])) {
    $srcset_values = &$form_state['values']['displays']['adaptative_image']['settings']['srcset'];
    $srcset_input = &$form_state['input']['displays']['adaptative_image']['settings']['srcset'];
    $values = $srcset_values['fieldset_multifield_srcset']['multifield_srcset'];

    // Sort by weights.
    usort($values, 'drupal_sort_weight');

    $srcset_values = $srcset_input = $values;

    $image_styles = array();
    foreach ($values as $value) {
      if (in_array($value['value'], $image_styles)) {
        form_set_error('displays][adaptative_image][settings][srcset', t('No repeated image styles allowed'));
        continue;
      } else {
        $image_styles[] = $value['value'];
      }
    }
  }
}

/**
 * Implements view callback for custom file formatter.
 */
function iela_media_file_formatter_adaptative_image_view($file, $display, $langcode) {
  // Prevent PHP notices when trying to read empty files.
  // @see http://drupal.org/node/681042
  if (!$file->filesize) {
    return;
  }

  // Do not bother proceeding if this file does not have an image mime type.
  if (strpos($file->filemime, 'image/') !== 0) {
    return;
  }

  $menu_item = menu_get_item();
  if (!empty($menu_item['page_callback']) && $menu_item['page_callback'] == 'file_entity_view_page') {
    return;
  }

  $scheme = file_uri_scheme($file->uri);
  $wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_READ);

  if (!empty($wrappers[$scheme])) {
    $result = array(
      '#theme' => 'image',
      '#path' => $file->uri,
    );

    // Modify renderable as necessary.
    _iela_media_process_image_element($result, $display, $file);

    return $result;
  }
}

/**
 * Implements hook_file_view().
 */
function iela_media_file_view($file, $view_mode, $langcode) {

  // Load file type displays.
  $displays = file_displays($file->type, $view_mode);
  drupal_alter('file_displays', $displays, $file, $view_mode);
  _file_sort_array_by_weight($displays);

  // Find a case where adaptative image formatter is configured to alter results
  // of other formatters and the resulting content is indeed an image.
  if (!empty($file->content['file']) && isset($displays['adaptative_image']) && !empty($displays['adaptative_image']['settings']['alter_results'])) {
    _iela_media_process_image_element($file->content['file'], $displays['adaptative_image'], $file, TRUE);
  }
}

/**
 * Implements template_process_link().
 */
function iela_media_process_link(&$variables) {
  if (!empty($variables['text']) && is_array($variables['text'])) {
    $variables['text'] = render($variables['text']);
  }
}

/**
 * Implements hook_media_wysiwyg_token_to_markup_alter().
 */
function iela_media_media_wysiwyg_token_to_markup_alter(&$element, $tag_info, $settings) {

  /*
   * Handle legend positioning.
   */
  
  if (!empty($tag_info['fields']['legend_position'])) {
    if ($tag_info['fields']['legend_position'] == 'hidden') {
      $element['content']['field_file_image_alt_text']['#access'] = false;
    } else {
      $element['content']['field_file_image_alt_text']['#attributes']['class'][] = $tag_info['fields']['legend_position'];

      if (strpos($tag_info['fields']['legend_position'], 'above') !== FALSE) {
        $element['content']['field_file_image_alt_text']['#weight'] = -10;
      }
    }
  }

  // Handle legend hiding option.
  if (!empty($tag_info['fields']['hide_legend']) && !empty($element['content']['field_file_image_alt_text'])) {
    $element['content']['field_file_image_alt_text']['#access'] = false;
  }

  /*
   * Handle attribute inheritance.
   */

  if (
    !empty($element['content']['file']) &&
    !empty($tag_info['file']) &&
    isset($tag_info['file']->override) &&
    !empty($tag_info['file']->override['attributes'])
  ) {
    $attributes = &$element['content']['file']['#attributes'];
    $attributes = $tag_info['file']->override['attributes'] + (empty($attributes) ? array() : $attributes);

    // Process classes.
    if (!empty($attributes['class'])) {
      foreach (array('right', 'left') as $side) {
        if (($key = array_search('pull-' . $side, $attributes['class'])) !== false) {
          unset($attributes['class'][$key]);
          $element['content']['#attributes']['class'][] = 'pull-' . $side;
        }
      }
    }
  }

  /**
   * Anchored Media Filter handler.
   */
  if (!empty($tag_info['wrapping_anchor']['href'])) {
    foreach ($tag_info['wrapping_anchor'] as $attr => $value) {
      $tag_info['wrapping_anchor'][$attr] = array($value);
    }
 
    $element['content']['file']['#prefix'] = '<a ' . drupal_attributes($tag_info['wrapping_anchor']) . '>';
    $element['content']['file']['#suffix'] = '</a>';
  }
}

/**
 * Implements hook_media_wysiwyg_wysiwyg_allowed_view_modes_alter().
 */
function iela_media_media_wysiwyg_wysiwyg_allowed_view_modes_alter(&$options, $context) {
  if ($context->type == 'image') {
    $entity_info = entity_get_info('file');
    foreach ($options as $name => $option) {
      if (!empty($options[$name]['label']) && !empty($entity_info['view modes'][$name]['label'])) {
        $options[$name]['label'] = t($entity_info['view modes'][$name]['label']);
      }
    }
  }
}

/**
 * Helper method to process an image element and set adaptative attributes.
 */
function _iela_media_process_image_element(&$element, $display, $file, $alter = FALSE) {

  // Allow overrides.
  if (isset($file->override) && !empty($file->override['adaptative_image'])) {
    $display['settings'] = $file->override['adaptative_image'] + $display['settings'];
  }

  // Make sure we do nothing when no entity is set.
  if (!isset($file->referencing_entity) && !empty($display['settings']['image_link']) && $display['settings']['image_link'] == 'content') {
    return;
  }

  // Don't do reprocessing.
  if (!empty($content['#attributes']['data-srcset'])) {
    return;
  }

  // Make sure we are processing an image.
  if (empty($element['#theme']) && strpos($element['#theme'], 'image') !== 0 && empty($element['#path'])) {
    return;
  }

  // Safeguard external images.
  if (url_is_external($element['#path'])) {
    $element['#path'] = imagecache_external_generate_path($element['#path']);
  }

  // Prepare srcset images.
  $srcset = array();
  foreach ($display['settings']['srcset'] as $src) {
    $info = _iela_media_get_image_style_info($src['value'], $element['#path']);

    if (!empty($info)) {
      $srcset[] = $info;
    }
  }

  // Handle alt/title.
  foreach (array('title', 'alt') as $attribute) {
    if (empty($element['#' . $attribute]) && (isset($file->$attribute) && !empty($file->$attribute))) {
      $element['#' . $attribute] = $file->$attribute;
    }
  }

  // Attach necessary attributes to the element, if srcsets are found.
  if (!empty($srcset)) {

    // Handle fallback preparation.
    if ((!$alter || !empty($display['settings']['override_style'])) && !empty($display['settings']['fallback'])) {
      $element['#theme'] = 'image_style';
      $element['#style_name'] = $display['settings']['fallback'];
    }

    // Append fallback to srcset as first resort src.
    if (!empty($element['#style_name'])) {
      $info = _iela_media_get_image_style_info($element['#style_name'], $element['#path']);

      if (!empty($info) && !in_array($info, $srcset)) {
        array_unshift($srcset, $info);
      }
    }

    // Append original image info.
    $original_file_info = image_get_info($element['#path']);
    if (!empty($original_file_info)) {
      $element['#attributes']['data-original-image'] = implode(' ', array(
        file_create_url($element['#path']),
        $original_file_info['width'] . 'w',
        $original_file_info['height'] . 'h',
      ));

      // @todo: Should be configurable.
      if (empty($element['#attributes']['width'])) {
        $element['#attributes']['width'] = $original_file_info['width'] . 'px';
        $element['#attributes']['forced-width'] = NULL;
      }
    }

    $element['#attributes']['data-srcset'] = implode(',', $srcset);
    $element['#attached']['js'][] = drupal_get_path('module', 'iela') . '/js/lib/jquery.resize.js';
    $element['#attached']['js'][] = drupal_get_path('module', 'iela_media') . '/js/auto-srcset.js';
    $element['#attached']['js'][] = drupal_get_path('module', 'iela_media') . '/js/adaptative-image-formatter.js';

    if (!empty($display['settings']['image_link'])) {
      switch ($display['settings']['image_link']) {
        case 'content':
          $uri = entity_uri($file->referencing_entity->entity_type, $file->referencing_entity);
          break;

        default:
          $uri = entity_uri('file', $file);
          break;
      }

      $uri['options']['html'] = TRUE;

      $element = array(
        '#theme' => 'link',
        '#text' => $element,
        '#path' => $uri['path'],
        '#options' => $uri['options'],
      );
    }
  }
}

/**
 * Helper method to generate image file for image style and return metadata.
 */
function _iela_media_get_image_style_info($view_mode, $file_uri) {
  $path = image_style_path($view_mode, $file_uri);

  // Generate image style file, if not generated yet.
  if (!file_exists($path)) {
    $style = image_style_load($view_mode);
    image_style_create_derivative($style, $file_uri, $path);
  }

  // Load image style info.
  $info = image_get_info($path);

  return empty($info) ? array() : implode(' ', array(
    file_create_url($path),
    $info['width'] . 'w',
    $info['height'] . 'h',
  ));
}
