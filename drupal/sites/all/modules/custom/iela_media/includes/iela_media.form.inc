<?php
/**
 * @file
 * IELA Media module form implementations and alters.
 */

/**
 * Implements hook_form_remote_stream_wrapper_file_add_form_alter().
 */
function iela_media_form_remote_stream_wrapper_file_add_form_alter(&$form, &$form_state) {

  // Register custom element validation.
  $form['url']['#element_validate'][] = 'iela_media_validate_remote_stream_file_type';

  // Get options, if any.
  $options = empty($form_state['build_info']['args'][0]) ? array() : $form_state['build_info']['args'][0];

  // Add validation information.
  if (!empty($options)) {

    // Mount up description.
    $descriptions = array();

    // Scheme validation.
    if (!empty($options['schemes'])) {
      $schemes = array();
      $remote_schemes = array('http', 'https', 'feed');

      foreach ($options['schemes'] as $scheme) {
        if (in_array($scheme, $remote_schemes)) {
          $schemes[] = $scheme;
        }
      }

      if (!empty($schemes)) {
        $descriptions[] = t('Allowed scheme types: !schemes', array(
          '!schemes' => '<strong>' . check_plain(implode(' ', $schemes)) . '</strong>',
        ));
      }
    }

    // File type validation.
    if (!empty($options['file_extensions'])) {
      $descriptions[] = t('Allowed file types: !extensions.', array(
        '!extensions' => '<strong>' . check_plain($options['file_extensions']) . '</strong>'
      ));
    }

    // Mount descriptions up, if any.
    if (!empty($descriptions)) {

      // Make sure to add previous descriptions too.
      if (!empty($form['url']['#description'])) {
        array_unshift($descriptions, $form['url']['#description']);
      }

      $form['url']['#description'] = implode('<br />', $descriptions);
    }
  }

  // Register custom form submittion.
  array_unshift($form['#submit'], 'iela_media_form_remote_stream_wrapper_file_add_form_submit');
}

/**
 * Custm submit handler for the remote stream wrapper file form.
 */
function iela_media_form_remote_stream_wrapper_file_add_form_submit($form, &$form_state) {
  $url = $form_state['values']['url'];

  try {
    $file = remote_stream_wrapper_file_load_by_uri($url);

    // Make sure we only create the file if it is non existing.
    if (!$file) {
      $uri = file_stream_wrapper_uri_normalize($url);
      $info = iela_media_remote_stream_file_get_info($url);

      $file = new stdClass();
      $file->fid = NULL;
      $file->uri = $uri;
      $file->filename = !empty($info['filename']) ? $info['filename'] : basename($file->uri);
      $file->filemime = !empty($info['filemime']) ? $info['filemime'] : file_get_mimetype($file->uri);
      $file->uid = $GLOBALS['user']->uid;
      $file->status = FILE_STATUS_PERMANENT;

      file_save($file);
    }
  }
  catch (Exception $e) {
    form_set_error('url', $e->getMessage());
    return;
  }
}

/**
 * Custom file type validation for remote files.
 */
function iela_media_validate_remote_stream_file_type($element, &$form_state) {

  // Get options, if any.
  $allowed_types = explode(' ', empty($form_state['build_info']['args'][0]['file_extensions']) ? array() : $form_state['build_info']['args'][0]['file_extensions']);

  // Only do heavy stuff if there are allowed file types defined.
  if (!empty($allowed_types)) {

    // Grab file information.
    $info = iela_media_remote_stream_file_get_info($element['#value']);

    if (!empty($info['filemime'])) {
      $type = explode('/', $info['filemime'], 2);
      if (!empty($type[1]) && !in_array($type[1], $allowed_types)) {
        $message = t('%name: Only remote files with the following types are allowed: %types-allowed.', array(
          '%name' => t($element['#title']),
          '%types-allowed' => implode(', ', $allowed_types)
        ));

        $message .= ' (' . t('found file type: %file-type', array(
          '%file-type' => $type[1]
        )) . ')';

        form_error($element, $message);
      }
    }
  }
}

/**
 * Helper function to get information from a remote file.
 */
function iela_media_remote_stream_file_get_info($urlOrRequest) {

  // Grab request information.
  $request = is_object($urlOrRequest) ? $urlOrRequest : drupal_http_request($urlOrRequest, array('method' => 'HEAD'));
  $headers = isset($request->headers) ? $request->headers : array();

  $info = array(
    'filename' => null,
    'filemime' => null,
  );

  foreach ($headers as $header => $value) {

    // Filename search.
    if (empty($info['filename'])) {
      // Try to get filename from location.
      if ($header == 'location') {
        $exploded = explode('/', $value);
        if ($exploded && count($exploded) > 1) {
          $info['filename'] = $exploded[count($exploded) - 1];
        }
      }

      // Try to get filename from content-disposition.
      elseif ($header == 'content-disposition') {
        preg_match('/filename="(.+?)"/', $value, $matches);
        if (!empty($matches[1])) {
          $info['filename'] = trim($matches[1]);
        } else {
          preg_match('/filename=([^; ]+)/', $value, $matches);
          if (!empty($matches[1])) {
            $info['filename'] = trim($matches[1]);
          }
        }
      }
    }

    // Filemime search.
    if (empty($info['filemime'])) {
      if ($header == 'content-type') {
        preg_match('/([a-z0-9_\-]+\/[a-z0-9_\-]+)/i', $value, $matches);
        if (!empty($matches[1])) {
          $info['filemime'] = $matches[1];
        }
      }
    }
  }

  return $info;
}
