/**
 * @file
 * Background Style Behaviors.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Background Image Height Behavior.
 */
Drupal.behaviors.backgroundImageHeight = {
  attach: function (context, settings) {
    $('.background-image-height-behavior', context).each(function () {
      var $this = $(this)
        , height = $this.attr('data-height');

      if (height) $this.css('min-height', height + 'px');
    });
  }
};

})(jQuery, Drupal, this, this.document);
