/**
 * @file
 * Heading Settings Form.
 */

;(function ($, Drupal, window, document, undefined) {

/**
 * Background Image Height Behavior.
 */
Drupal.behaviors.headingSettingsForm = {
  attach: function (context, settings) {
    $('#iela-title-content-type-edit-form', context).once(function () {
      var $form = $(this)
        , $descriptionType = $form.find('#edit-description-handler')
        , $tokens = $form.find('.token-tree')
                      .css('margin-left', '0')
                      .parent('.table-responsive');

      function updateTokensState() {
        $tokens.toggle($descriptionType.find('option:selected').val() == 'token');
      }

      $descriptionType.on('change', updateTokensState).trigger('change');
    });
  }
};

})(jQuery, Drupal, this, this.document);
