/**
 * @file
 * Views Content Views Panes adjustments.
 */

;(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.ielaViewsContentViewsPanes = {
  attach: function (context, settings) {
    var duplicatedViewMode = $('#edit-view-mode--2', context);

    if (duplicatedViewMode.length && !duplicatedViewMode.siblings().length) {
      duplicatedViewMode.closest('#edit-content-settings').css('cssText', 'display: none !important');
    }
  }
};

})(jQuery, Drupal, this, this.document);
