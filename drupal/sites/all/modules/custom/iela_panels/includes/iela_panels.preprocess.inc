<?php

/**
 * @file
 * IELA Panels module preprocess implementations.
 */

/**
 * Implements template_preprocess_panels_add_content_modal().
 */
function iela_panels_preprocess_panels_add_content_modal(&$variables) {
  if (!empty($variables['categories']['menus']['content'])) {
    foreach ($variables['categories']['menus']['content'] as $title => $pane) {
      if (!empty($pane['type_name'])) {
        if ($pane['type_name'] === 'block') {
          unset($variables['categories']['menus']['content'][$title]);
        }

        if ($pane['type_name'] === 'menu_tree') {
          if ($pane['subtype_name'] === '_active') {
            $new_title = '<em>' . t('Menu selected by the page') . '</em>';
          } else {
            $new_title = $pane['menu_title'];
          }

          $variables['categories']['menus']['content'][$new_title] = $pane;
          unset($variables['categories']['menus']['content'][$title]);
        }
      }
    }

    if (empty($variables['categories']['menus']['content'])) {
      unset($variables['categories']['menus']);
    }
  }
}

/**
 * Implements 
 */
function iela_panels_preprocess_field(&$variables) {
  $field = $variables['element'];
  if ($field['#field_type'] == 'mini_panel_reference' && $field['#formatter'] == 'mini_panel_reference_default') {
    if (user_access('change layouts in place editing')) {
      foreach ($field['#items'] as $delta => $item) {
        if ($mini_panel = panels_mini_load($item['name'])) {
          $url = 'admin/config/mini-panels-ipe/'. $item['name'];
          $title = $mini_panel->admin_title;

          // Prepend edit link to content
          $variables['items'][$delta]['#markup'] = l('Edit '. $title, $url, array('attributes' => array('class' => array('edit-mini-panel-link')))) . $variables['items'][$delta]['#markup'];
        }
      }
    }
  }
}
