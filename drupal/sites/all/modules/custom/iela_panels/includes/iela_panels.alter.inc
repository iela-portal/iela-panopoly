<?php

/**
 * @file
 * IELA Panels module alters implementations.
 */

/**
 * Implements hook_module_implements_alter().
 */
function iela_panels_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'custom_theme') {
    // Move our hook implementation to the bottom.
    $group = $implementations['iela_panels'];
    unset($implementations['iela_panels']);
    $implementations['iela_panels'] = $group;
  }
}

/**
 * Implements hook_entity_info_alter(&$entitties).
 */
function iela_panels_entity_info_alter(&$entities) {

  // User listing pane.
  $entities['fieldable_panels_pane']['bundles']['user_list'] = array(
    'label' => t('Add user list'),
    'pane category' => t('Views'),
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/user-list',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Node listing pane.
  $entities['fieldable_panels_pane']['bundles']['content_list'] = array(
    'label' => t('Add content list'),
    'pane category' => t('Views'),
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/content-list',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Entity Form pane.
  $entities['fieldable_panels_pane']['bundles']['entityform'] = array(
    'label' => t('Add form'),
    'pane category' => t('Forms'),
    'pane top level' => TRUE,
    'pane icon' => drupal_get_path('module', 'iela_panels') . '/images/entityform.png',
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/entityform',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Social Links pane.
  $entities['fieldable_panels_pane']['bundles']['social_links'] = array(
    'label' => t('Add social links'),
    'pane category' => t('Social'),
    'pane top level' => TRUE,
    'pane icon' => drupal_get_path('module', 'iela_panels') . '/images/social.png',
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/social-links',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Flexslider pane.
  $entities['fieldable_panels_pane']['bundles']['flexslider'] = array(
    'label' => t('Add slider'),
    'pane category' => t('IELA'),
    'pane top level' => TRUE,
    'pane icon' => drupal_get_path('module', 'iela_panels') . '/images/images.png',
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/flexslider',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Media list pane.
  $entities['fieldable_panels_pane']['bundles']['media_list'] = array(
    'label' => t('Add media list'),
    'pane category' => t('IELA'),
    'pane top level' => TRUE,
    'pane icon' => drupal_get_path('module', 'iela_panels') . '/images/images.png',
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/media-list',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );

  // Embed code pane.
  $entities['fieldable_panels_pane']['bundles']['embed'] = array(
    'label' => t('Add code'),
    'pane category' => t('IELA'),
    'pane top level' => TRUE,
    'pane icon' => drupal_get_path('module', 'iela_panels') . '/images/embed.png',
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/embed',
      'access arguments' => array('administer fieldable panels panes'),
    ),
  );
}

/**
 * Implements hook_page_alter(&$page).
 */
function iela_panels_page_alter(&$page) {
  $access_to_panels = user_access('administer_panels_layouts');
  $is_admin_path = path_is_admin(current_path());

  if ($access_to_panels) {
    if ($is_admin_path) {
      unset($_SESSION['iela_panels']['page_title']);
    } else {
      $_SESSION['iela_panels']['page_title'] = drupal_get_title();
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function iela_panels_form_panelizer_edit_content_form_alter(&$form, &$form_state) {
  if (strpos($form_state['display']->layout, 'iela') !== FALSE) {
    drupal_add_css(drupal_get_path('module', 'iela_panels') . '/css/bootstrap-grid-admin.css');
  }
}

/**
 * Implements hook_form_alter().
 */
function iela_panels_form_panels_panel_context_edit_content_alter(&$form, &$form_state) {
  if (strpos($form_state['display']->layout, 'iela') !== FALSE) {
    drupal_add_css(drupal_get_path('module', 'iela_panels') . '/css/bootstrap-grid-admin.css');
  }
}

function iela_panels_form_panels_panel_context_edit_move_alter(&$form, &$form_state) {
  if (strpos($form_state['display']->layout, 'iela') !== FALSE) {
    drupal_add_css(drupal_get_path('module', 'iela_panels') . '/css/bootstrap-grid-admin.css');
  }
}

/**
 * Implements hook_form_alter().
 */
function iela_panels_form_panels_edit_style_settings_form_alter(&$form, &$form_state) {
  $form['#attributes']['class'][] = $form_state['style']['name'] . '-settings-form';
}

/**
 * Implements hook_form_alter().
 */
function iela_panels_form_panels_edit_style_type_form_alter(&$form, &$form_state, $form_id) {

  // Verify here if this form is panels_edit and if arg is a numeric, because if is true then this form is a pane and not a region
  $param = arg(6);

  if (is_numeric($param) || (strpos($param, 'new-') !== FALSE)) {

    // arg(6) has the pane id
    // $form_state['display'] has all pane objects this current page
    // $form['style']['#options']); has all styles of style form pane
    $filtered = _iela_panels_access_style($form_state['display']->content[$param], $form['style']['#options']);

    foreach ($filtered as $key => $style) {
      // Remove the style does not permisson
      unset($form['style']['#options'][$style]);
    }
  } else {
    foreach ($form['style']['#options'] as $name => $title) {
      $style = panels_get_style($name);
      if (!empty($style) && !empty($style['affects container'])) {
        unset($form['style']['#options'][$name]);
        $form['style']['#options'][$name] = $title . ' <small>(' . t('affects container') . ')</small>';
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function iela_panels_form_fieldable_panels_panes_fieldable_panels_pane_content_type_edit_form_alter(&$form, &$form_state) {
  if (!empty($form['buttons']['return'])) {
    $form['buttons']['return']['#attributes']['class'][] = 'btn-success';
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function iela_panels_field_widget_form_alter(&$element, &$form_state, $context) {

  $field        = !empty($context['field']) ? $context['field'] : array();
  $type         = !empty($field['type']) ? $field['type'] : '';
  $settings     = !empty($field['settings']) ? $field['settings'] : array();
  $element_type = !empty($element['#type']) ? $element['#type'] : '';

  if ($type == 'entityreference' && $element_type == 'select') {
    $target = !empty($settings['target_type']) ? $settings['target_type'] : '';
    $handler = !empty($settings['handler']) ? $settings['handler'] : '';

    if ($target == 'user' && $handler == 'base') {
      // Remove anonymous and admin user.
      unset($element['#options'][0]);
      unset($element['#options'][1]);

      foreach ($element['#options'] as $uid => $username) {
        $user = user_load($uid);

        if (!empty($user->field_fullname[LANGUAGE_NONE][0]['safe'])) {
          $new_option_title = implode(' ', $user->field_fullname[LANGUAGE_NONE][0]['safe']);

          if (!empty($new_option_title)) {
            if (!empty($user->picture)) {
              $picture = (array) $user->picture;

              $picture['path'] = $picture['uri'];
              $picture['title'] = $new_option_title;
              $picture['attributes'] = array();

              $new_option_title = theme_image($picture) . $new_option_title;
            }

            $element['#options'][$uid] = $new_option_title;
          }
        }
      }
    }
  }
}

/**
 * Implements hook_form_ctools_export_ui_list_form_alter();
 */
function iela_panels_form_ctools_export_ui_list_form_alter(&$form, &$form_state) {
  if ($form_state['plugin']['name'] === 'panels_mini' && isset($form_state['object'])) {
    $plugin = &$form_state['object']->plugin;
    $plugin['allowed operations']['ipe']['title'] = t('Edit on IPE');
    $plugin['menu']['items']['ipe']['path'] = 'list/%ctools_export_ui/ipe';
  }
}

/**
 * Implements hook_ctools_render_alter().
 */
function iela_panels_ctools_render_alter(&$info, &$page, &$context) {
  if (isset($context['handler']) && is_object($context['handler'])) {
    $handler = $context['handler'];
    $conf = $handler->conf;
    if ($handler->handler == 'panel_context' && isset($conf['pipeline']) && $conf['pipeline'] == 'ipe') {
      if (!empty($info['content'])) {
        $info['content'] .= 
          '<div class="alert alert-warning alert-dismissible variant-alert" role="alert">' .
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' .
            '<strong class="fa fa-exclamation-triangle"></strong> ' . t('Editing all pages with variant "%variant"', array(
              '%variant' => $conf['title'],
            )) .
          '</div>';

        drupal_add_js(array(
          'ielaPanels' => array(
            'variantName' => $conf['title'],
          ),
        ), 'setting');

        drupal_add_css(drupal_get_path('module', 'iela_panels') . '/css/iela-panels-ipe.css');
        drupal_add_js(drupal_get_path('module', 'iela_panels') . '/js/iela-panels-ipe.js');
      }
    }
  }
}

/**
 * Implements hook_form_views_content_views_panes_content_type_edit_form_alter().
 */
function iela_panels_form_views_content_views_panes_content_type_edit_form_alter(&$form, &$form_state, $form_id) {
  
  // Load JavaScript adjustments file.
  drupal_add_js(drupal_get_path('module', 'iela_panels') . '/js/views-content-views-panes.js');

  // Modify default sort order to be date.
  // @todo: this has to happen due to a bug on views interface that consider
  // the last sort machine name only.
  if (!empty($form['exposed']['sort_by']) && isset($form['exposed']['sort_by']['#options']['created'])) {
    $order = array('created' => $form['exposed']['sort_by']['#options']['created']);

    foreach ($form['exposed']['sort_by']['#options'] as $name => $title) {
      if ($name != 'created') $order[$name] = $title;
    }

    $form['exposed']['sort_by']['#options'] = $order;
    $form['exposed']['sort_by']['#default_value'] = 'created';
  }

  // Add link title option.
  if (!empty($form['override_title'])) {
    $conf = $form_state['conf'];

    $form['exposed']['link'] = array(
      '#title' => t('Make title a link'),
      '#type' => 'checkbox',
      '#default_value' => $entity->link,
      '#description' => t('Check here to make the title of the menu be a link.'),
      '#id' => 'edit-link',
      '#weight' => -90,
      '#default_value' => empty($conf['link']) ? false : $conf['link'],
    );

    $form['exposed']['link_path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('The path for this link. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page.', array('%front' => '<front>', '%add-node' => 'node/add', '%drupal' => 'http://drupal.org')),
      '#dependency' => array('edit-link' => array(1)),
      '#default_value' => '',
      '#weight' => -89,
      '#default_value' => empty($conf['path']) ? '' : $conf['path'],
    );

    $form['#submit'][] = 'iela_panels_title_link_form_submit';
  }
}

/**
 * Implements hook_form_menu_block_menu_tree_content_type_edit_form_alter().
 */
function iela_panels_form_menu_block_menu_tree_content_type_edit_form_alter(&$form, &$form_state, $form_id) {

  $conf = $form_state['conf'];

  $form['override_title']['#weight'] = -10;
  $form['override_title_text']['#weight'] = -9;

  $form['link'] = array(
    '#title' => t('Make title a link'),
    '#type' => 'checkbox',
    '#description' => t('Check here to make the title of the menu be a link.'),
    '#id' => 'edit-link',
    '#weight' => -8,
    '#default_value' => empty($conf['link']) ? false : $conf['link'],
  );

  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('The path for this link. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page.', array('%front' => '<front>', '%add-node' => 'node/add', '%drupal' => 'http://drupal.org')),
    '#dependency' => array('edit-link' => array(1)),
    '#weight' => -7,
    '#default_value' => empty($conf['path']) ? '' : $conf['path'],
  );

  $form['#submit'][] = 'iela_panels_title_link_form_submit';
}

function iela_panels_title_link_form_submit(&$form, &$form_state) {

  // Handle strange views pane.
  if ($form['#form_id'] == 'views_content_views_panes_content_type_edit_form') {
    if (!empty($form_state['values']['exposed']['link'])) {
      $form_state['values']['link'] = $form_state['values']['exposed']['link'];
      unset($form_state['values']['exposed']['link']);
    }

    if (!empty($form_state['values']['exposed']['link_path'])) {
      $form_state['values']['path'] = $form_state['values']['exposed']['link_path'];
      unset($form_state['values']['exposed']['link_path']);
    }
  }

  $form_state['conf']['link'] = $form_state['values']['link'];
  $form_state['conf']['path'] = $form_state['values']['path'];
}
