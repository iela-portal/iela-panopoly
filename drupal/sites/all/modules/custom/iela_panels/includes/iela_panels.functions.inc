<?php

/**
 * @file
 * IELA Panels module custom functions.
 */

/**
 * [_iela_panels_access_style This function validate if
 * a pane can see determinate styles]
 * @param  [object]  $pane    [pane object]
 * @param  [array]   $styles  [ object]
 * @return [array]            [return an array with the styles to block]
 */
function _iela_panels_access_style($pane, $styles) {

  // Filtered styles.
  $filtered = array();
  $fieldable_pane = NULL;

  // I need to flip to use the keys in the foreach
  $styles = array_flip($styles);

  // Get all panels styles
  $panel_styles = panels_get_styles();

  if ($pane->type == 'fieldable_panels_pane') {
    ctools_include('fieldable_panels_pane', 'fieldable_panels_panes', 'plugins/content_types');

    // Load $fieldable_pane
    $fieldable_pane = _fieldable_panels_panes_load_entity($pane->subtype);
  }

  foreach ($styles as $key => $style) {
    if (!empty($panel_styles[$style]['type access'])) {
      $access = $panel_styles[$style]['type access'];

      // First try to use 'type access' as a callback.
      if (function_exists($access)) {
        if (!$access($pane, $fieldable_pane)) $filtered[] = $style;
      }

      // First: Fieldable pane if exist
      // Second: Verify if the array fieldable_panels_pane has at least element
      // Third: Verify if $fieldable_pane->bundle exist in array
      elseif (!empty($fieldable_pane) && count($access['fieldable_panels_pane']) > 0 && !in_array($fieldable_pane->bundle, $access['fieldable_panels_pane'])) {
        $filtered[] = $style;
      }

      // if not a fieldable pane
      elseif (empty($fieldable_pane) && $pane->type != 'entity_field') {

        // All sentences are being denied
        // First: Verify if $access is not equal 1
        // Second: Verify if the first key is equal 'fieldable_panels_pane'
        // Third: Verify if $pane->type exist in array
        if (!count($access) != 1 && !key($access) == 'fieldable_panels_pane' && !in_array($pane->type, $access)) {
          $filtered[] = $style;
        }
      }

      elseif ($pane->type == 'entity_field') {

        // All sentences are being denied
        // First: Verify if $access is not equal 1
        // Second: Verify if the first key is equal 'fieldable_panels_pane'
        // Third: Verify if $pane->subtype exist in array
        if (!count($access) != 1 && !key($access) == 'fieldable_panels_pane' && !in_array($pane->subtype, $access['entity_field'])) {

          $filtered[] = $style;
        }
      }
    }
  }

  return $filtered;
}

/**
 * Helper function to get a renderable array for regions wrapper.
 */
function _iela_panels_layout_region_wrapper($regions, $variables) {

  if (isset($variables['display']->renderer_handler) && !empty($variables['display']->renderer_handler->plugin)) {
    $plugin_name = $variables['display']->renderer_handler->plugin['name'];
  } elseif (isset($variables['renderer']) && !empty($variables['renderer']->plugin['name'])) {
    $plugin_name = $variables['renderer']->plugin['name'];
  }

  $editing = !empty($plugin_name) && in_array($plugin_name, array(
    'ipe',
    'editor',
  ));

  $container = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('container-outside'),
    ),
    '#access' => user_access('administer panels layouts') && $editing,
    'container inside' => array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container',
          'panel-panel-container',
        ),
      ),
      'row' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('row')
        ),
        'row inside' => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('row-inside'),
          ),
        ),
      ),
    ),
  );

  // Alias.
  $row = &$container['container inside']['row']['row inside'];

  // Display identifier.
  $display_id = !empty($variables['display']->css_id) ? str_replace('_', '-', $variables['display']->css_id) : '';

  // Used to identify container.
  $regions_html_names = array('container');
  $has_content = false;
  
  if (!empty($display_id)) {
    $regions_html_names[] = $display_id;
    $display_id .= '-';
  }

  foreach ($regions as $name => $region) {
    if ($region['empty'] === false) $has_content = $container['#access'] = true;

    if (empty($region['size'])) {
      $region['size'] = array(
        'xs' => 12,
        'sm' => 12,
        'md' => 12,
        'lg' => 12,
      );
    }

    $regions_html_names[] = $region['css_id'];
    $region['css_id'] = $display_id . $region['css_id'];

    $container['#attributes']['class'][] = 'container-outside-' . $region['css_id'];
    $container['container inside']['#attributes']['class'][] = 'panel-panel-container-' . $region['css_id'];

    $row[$name] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('panel-panel', 'panel-panel-' . $region['css_id']),
        'id' => 'region-' . $region['css_id'],
      ),
      $name . ' inner' => array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('row', 'panel-panel-inner', 'panel-panel-inner-' . $region['css_id'])
        ),
        'content' => array(
          '#markup' => $region['content']
        ),
      ),
    );

    foreach ($region['size'] as $screen => $size) {
      $row[$name]['#attributes']['class'][] = 'col-' . $screen . '-' . $size;
    }
  }

  if (!$has_content) $container['#attributes']['class'][] = 'empty-container';

  // Fulfill container id.
  $container['#attributes']['id'] = implode('-', $regions_html_names);

  return $container;
}

/**
 * Helper function to get a renderable array for a variable amount of regions.
 */
function _iela_panels_layout_multiple_region_wrapper($variables, $names) {
  $regions = array();
  $frame_prefix = isset($variables['display']->current_frame) ? $variables['display']->current_frame . '__' : '';

  foreach($names as $name => $size) {

    // Panels frames identification.
    $framed_name = $frame_prefix . $name;

    // Contruct a valid css id attribute.
    $css_id = str_replace('_', '-', $framed_name);

    $regions[$name] = array(
      'content' => $variables['panel_contents'][$name],
      'empty' => empty($variables['display']->panels[$framed_name]),
      'css_id' => $css_id,
      'size' => $size,
    );
  }

  return _iela_panels_layout_region_wrapper($regions, $variables);
}

/**
 * Helper function to get a renderable array for a single region wrapper.
 */
function _iela_panels_layout_single_region_wrapper($variables, $name) {

  // Panels frames identification.
  $frame_prefix = isset($variables['display']->current_frame) ? $variables['display']->current_frame . '__' : '';
  $framed_name = $frame_prefix . $name;

  // Contruct a valid css id attribute.
  $css_id = str_replace('_', '-', $framed_name);

  return _iela_panels_layout_region_wrapper(array(
    $name => array(
      'content' => $variables['panel_contents'][$name],
      'empty' => empty($variables['display']->panels[$framed_name]),
      'css_id' => $css_id,
    ),
  ), $variables);
}

/**
 * Helper function to get a renderable array for half-divided regions wrapper.
 */
function _iela_panels_layout_half_region_wrapper($variables, $names) {
  $regions = array();
  $frame_prefix = isset($variables['display']->current_frame) ? $variables['display']->current_frame . '__' : '';
  
  foreach($names as $name) {

    // Panels frames identification.
    $framed_name = $frame_prefix . $name;

    // Contruct a valid css id attribute.
    $css_id = str_replace('_', '-', $framed_name);

    $regions[$name] = array(
      'content' => $variables['panel_contents'][$name],
      'empty' => empty($variables['display']->panels[$framed_name]),
      'css_id' => $css_id,
      'size' => array(
        'xs' => 12,
        'sm' => 12,
        'md' => 6,
        'lg' => 6,
      ),
    );
  }

  return _iela_panels_layout_region_wrapper($regions, $variables);
}

/**
 * Helper function to get a renderable array for third-divided regions wrapper.
 */
function _iela_panels_layout_third_region_wrapper($variables, $names) {
  $regions = array();
  $frame_prefix = isset($variables['display']->current_frame) ? $variables['display']->current_frame . '__' : '';
  
  foreach($names as $name) {

    // Panels frames identification.
    $framed_name = $frame_prefix . $name;

    // Contruct a valid css id attribute.
    $css_id = str_replace('_', '-', $framed_name);

    $regions[$name] = array(
      'content' => $variables['panel_contents'][$name],
      'empty' => empty($variables['display']->panels[$framed_name]),
      'css_id' => $css_id,
      'size' => array(
        'xs' => 12,
        'sm' => 12,
        'md' => 4,
        'lg' => 4,
      ),
    );
  }

  return _iela_panels_layout_region_wrapper($regions, $variables);
}

/**
 * Helper function to add a container preprocess.
 */
function _iela_panels_region_style_add_container_preprocess($name, &$variables) {

  // Avoid undefined.
  if (!isset($variables['display']->container_preprocess)) {
    $variables['display']->container_preprocess = array();
  }

  // Save preprocessing function.
  $variables['display']->container_preprocess[$variables['region_id']][$name] = $variables['settings'];
}
