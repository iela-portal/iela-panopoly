<?php

/**
 * Double Sidebar Left frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Double Sidebar Left'),
  'icon' => 'double-sidebar-left.png',
  'category' => t('Frames'),
  'theme' => 'double_sidebar_left_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
    'content_third' => t('Content Third'),
  ),
);

/**
 * Preprocess variables for Double Sidebar Left frame.
 */
function template_preprocess_double_sidebar_left_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'double-sidebar-left-frame', 'clearfix'),
    ),
  );

  $variables['content']['content_first_second_third'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'content' => array('xs' => 12, 'sm' => 12, 'md' => 3, 'lg' => 3),
    'content_second' => array('xs' => 12, 'sm' => 12, 'md' => 3, 'lg' => 3),
    'content_third' => array('xs' => 12, 'sm' => 12, 'md' => 6, 'lg' => 6),
  ));
  $variables['containers']['content_first_second_third'][] = 'content';
  $variables['containers']['content_first_second_third'][] = 'content_second';
  $variables['containers']['content_first_second_third'][] = 'content_third';

}

/**
 * Returns HTML for the Double Sidebar Left frame layout.
 */
function theme_double_sidebar_left_frame($vars) {
  return drupal_render($vars['content']);
}
