<?php

/**
 * Half frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Half'),
  'icon' => 'half.png',
  'category' => t('Frames'),
  'theme' => 'half_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
  ),
);

/**
 * Preprocess variables for Half frame.
 */
function template_preprocess_half_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'half-frame', 'clearfix'),
    ),
  );

  // Contend Third and Fourth (half):
  $variables['content']['content_first_second'] = _iela_panels_layout_half_region_wrapper($variables, array(
    'content', 'content_second'
  ));
  $variables['containers']['content_first_second'][] = 'content';
  $variables['containers']['content_first_second'][] = 'content_second';
}

/**
 * Returns HTML for the Half frame layout.
 */
function theme_half_frame($vars) {
  return drupal_render($vars['content']);
}
