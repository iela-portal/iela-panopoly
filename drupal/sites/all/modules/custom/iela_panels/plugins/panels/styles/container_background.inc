<?php

/**
 * @file
 * Definition of the Container Background panel region style.
 * This style allows to configure the background of a container.
 */

$plugin = array(
  'title' => t('Container Background'),
  'description' => t('Configure the background of the container.'),
  'render region' => 'container_background_region_style',
  'settings form' => 'container_background_region_style_settings_form',
  'multiple style' => array(
    'wrapper' => true,
    'region' => false,
  ),
  'affects container' => true,
  'weight' => -10,
  'defaults' => array(
    'color' => '',
    'reverse' => false,
    'background_image' => array(
      'behavior' => 'cover',
      'position' => 'center-center',
      'source' => 'upload',
      'field' => null,
      'images' => array(0 => ''),
    ),
  ),
);

/**
 * Preprocess wrapping container.
 */
function container_background_region_style_preprocess_container(&$variables) {

  $display = &$variables['layout_variables']['display'];
  $container = &$variables['container'];
  $conf = $variables['settings'];

  // Set container background color:
  if (!empty($conf['color'])) {
    $container['#attributes']['style'][] = 'background-color: ' . $conf['color'] . ';';
  }

  // Set reverse color option:
  if (!empty($conf['reverse'])) {
    $container['#attributes']['class'][] = 'dark-background';
  }

  // Background image options.
  if (!empty($conf['use_background_images'])) {

    // Consider previous configuration.
    $conf['background_image']['source'] = empty($conf['background_image']['source']) ? 'upload' : $conf['background_image']['source'];
    $file = null;

    switch ($conf['background_image']['source']) {
      case 'upload':
        if (!empty($conf['background_image']['images'][0])) {
          $file = file_load($conf['background_image']['images'][0]);
        }
        break;

      case 'field':
        if (!empty($conf['background_image']['field'])) {
          list($context_name, $field_name) = preg_split('/\((.*)\)/', $conf['background_image']['field'], null, PREG_SPLIT_DELIM_CAPTURE);
          if (!empty($context_name) && !empty($field_name) && isset($display->context[$context_name])) {
            $context_data = $display->context[$context_name]->data;
            if (!empty($context_data) && !empty($context_data->$field_name)) {
              $field_value = $context_data->$field_name;
              if (!empty($field_value[LANGUAGE_NONE][0]['fid'])) {
                $file = file_load($field_value[LANGUAGE_NONE][0]['fid']);
              }
            }
          }
        }
        break;
    }

    if (!empty($file) && $file->uri) {
      $container['#attached']['js'][] = drupal_get_path('module', 'iela_panels') . '/js/background-style.js';

      $sizes = array(
        'cover' => 'cover',
        'contain' => 'contain',
        'pattern' => 'initial',
        'height' => 'initial',
      );

      switch ($conf['background_image']['behavior']) {
        case 'cover':
        case 'contain':
          $file->override['adaptative_image']['image_link'] = '';
          $file_view = file_view_file($file);

          // Merge attributes.
          if (!empty($file_view['#attributes'])) {
            foreach ($file_view['#attributes'] as $attr => $value) {
              if (strpos($attr, 'data-') === 0) {
                $container['#attributes'][$attr] = $value;
              }
            }
          }

          // Merge attachments.
          if (!empty($file_view['#attached'])) {
            $container['#attached'] = array_merge_recursive(
              empty($container['#attached']) ? array() : $container['#attached'],
              $file_view['#attached']
            );
          }

          $path = $file_view['#path'];
          if (!empty($file_view['#style_name'])) {
            list($path) = explode(' ', _iela_media_get_image_style_info($file_view['#style_name'], $file_view['#path']));
          }

          break;

        default:
          $path = file_create_url($file->uri);
          break;
      }

      $container['#attributes']['class'][] = 'background-image';
      $container['#attributes']['style'][] = 'background-image: url("' . $path . '");';
      $container['#attributes']['style'][] = 'background-size: ' . $sizes[$conf['background_image']['behavior']] . ';';

      // Handle image positioning:
      if (!empty($conf['background_image']['position'])) {
        list($y, $x) = explode('-', $conf['background_image']['position']);

        $container['#attributes']['style'][] = 'background-position-x: ' . $x . ';';
        $container['#attributes']['style'][] = 'background-position-y: ' . $y . ';';
      }

      // Handle image patterns:
      if ($conf['background_image']['behavior'] == 'pattern') {
        $container['#attributes']['style'][] = 'background-repeat: repeat;';
      }

      // Handle height behavior images:
      if ($conf['background_image']['behavior'] == 'height') {
        $container['#attributes']['class'][] = 'background-image-height-behavior';
        foreach ($file->metadata as $meta => $data) {
          $container['#attributes']['data-' . $meta] = $data;
        }
      }
    }
  }
}

/**
 * Template preprocess for the region style.
 */
function template_preprocess_container_background_region_style(&$variables) {
  _iela_panels_region_style_add_container_preprocess('container_background_region_style_preprocess_container', $variables);
}

/**
 * Theme function for the region style.
 * @info when using multiple styles, this theme will not be used. Instead,
 * the style that implements theme based configuration will be the one to have
 * it's theme function used.
 */
function theme_container_background_region_style(&$variables) {
  return theme('panels_default_style_render_region', $variables);
}

/**
 * Configuration form.
 */
function container_background_region_style_settings_form(&$conf, $display, $pid, $type, &$form_state) {

  module_load_include('inc', 'iela', 'includes/iela.functions');

  $form = array();
  $theme_path = drupal_get_path('theme', 'iela_theme');
  $multiple = !empty($form_state['style']['name']) && $form_state['style']['name'] == 'multiple';
  $base_input_selector = $multiple ? ':input[name="settings[container_background]' : ':input[name="settings';

  // form_load_include($form_state, 'inc', 'iela_panels','plugins/panels/styles/background');
  // $form_state['rebuild'] = TRUE;

  // Load color picker files.
  drupal_add_js($theme_path . '/lib/spectrum/spectrum.js');
  drupal_add_js($theme_path . '/js/color-picker.js');
  drupal_add_css($theme_path . '/lib/spectrum/spectrum.css');

  // Container background color.
  $form['color'] = array(
    '#type' => 'textfield',
    '#title' => t('Color'),
    '#description' => t('Choose the background color of the container'),
    '#attributes' => array('class' => array('color-picker')),
    '#default_value' => $conf['color'],
  );

  // Option to reverse default text.
  $form['reverse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reverse text colors'),
    '#description' => t('If background color is too dark, check this to reverse default text colors'),
    '#default_value' => $conf['reverse'],
  );

  // Checkbox to release image options.
  $form['use_background_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Define background image'),
    '#default_value' => $conf['use_background_images'],
  );

  // Container background image options.
  $form['background_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background image'),
    '#states' => array(
      'visible' => array(
        $base_input_selector . '[use_background_images]"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Container background type.
  $form['background_image']['behavior'] = array(
    '#type' => 'select',
    '#title' => t('Behavior'),
    '#default_value' => $conf['background_image']['behavior'],
    '#options' => array(
      'cover' => t('Cover'),
      'contain' => t('Contain'),
      'pattern' => t('Pattern'),
      'height' => t('Use image height on container'),
    ),
  );

  // Container background image position.
  $form['background_image']['position'] = array(
    '#type' => 'radios',
    '#title' => t('Positioning'),
    '#default_value' => $conf['background_image']['position'],
    '#options' => array(
      'top-left'      => t('Top left'),
      'top-center'    => t('Top middle'),
      'top-right'     => t('Top right'),
      'center-left'   => t('Center left'),
      'center-center' => t('Center middle'),
      'center-right'  => t('Center right'),
      'bottom-left'   => t('Bottom left'),
      'bottom-center' => t('Bottom middle'),
      'bottom-right'  => t('Bottom right'),
    ),
    '#attributes' => array('class' => array('position-field')),
  );

  // Container background images.
  $form['background_image']['images'] = array(
    '#weight' => 10,
    0 => array(
      '#type' => 'media',
      '#title' => t('Image'),
      '#description' => t('Choose a optional background image for the container'),
      '#default_value' => $conf['background_image']['images'][0],
      '#media_options' => array(
        'global' => array(
          'enabledPlugins' => array(
            'upload' => 'upload',
            'media_internet' => 'media_internet',
            'media_default--media_browser_1' => 'media_default--media_browser_1',
            'media_default--media_browser_my_files' => 'media_default--media_browser_my_files',
          ),
          'schemes' => array('public', 'private'),
          'file_directory' => 'pane_region_covers',
          'file_extensions' => 'png jpg jpeg',
        ),
      ),
      '#array_parents' => array('background_image', 'image', 0),
    ),
  );

  if (isset($display->context)) {
    $image_fields = array();

    foreach ($display->context as $context_name => $context) {

      // Fields available throught this context data.
      $available_fields = iela_get_context_available_fields_by_label($context);

      foreach ($available_fields as $field_name => $label_info) {
        $field_info = field_info_field($field_name);
        if ($field_info['type'] === 'image') {
          $image_fields[$context_name]['fields'][$field_name] = $label_info;
        }
      }

      // Identify context.
      if (!empty($image_fields[$context_name])) {
        $image_fields[$context_name]['context'] = $context->identifier;
      }
    }

    if (!empty($image_fields)) {
      $form['background_image']['source'] = array(
        '#type' => 'select',
        '#title' => t('Image source'),
        '#default_value' => $conf['background_image']['source'],
        '#options' => array(
          'upload' => t('Upload'),
          'field' => t('Field'),
        ),
      );

      $form['background_image']['field'] = array(
        '#type' => 'radios',
        '#title' => t('Field image source'),
        '#description' => t('Choose the field to use as image source.'),
        '#default_value' => $conf['background_image']['field'],
        '#weight' => 8,
        '#options' => array(),
        '#attributes' => array(
          'class' => array('radios-hover-aid')
        ),
        '#states' => array(
          'visible' => array(
            $base_input_selector . '[background_image][source]"]' => array('value' => 'field'),
          ),
        ),
      );

      foreach ($image_fields as $context_name => $context_fields) {
        foreach ($context_fields['fields'] as $field_name => $labels) {
          $attributes = array('class' => array('help-block'));
          $descriptions = array();

          foreach ($labels as $label => $bundles) {
            $descriptions[] = join(' ' . t('and') . ' ', array_filter(
              array_merge(
                array(join(', ', array_slice($bundles, 0, -1))),
                array_slice($bundles, -1))
              )
            ) . ': <strong>' . $label . '</strong>';
          }

          $radio_title = '<strong>' . $field_name . '</strong>';

          if (!empty($context_fields['context'])) {
            $radio_title = $context_fields['context'] . ': ' . $radio_title;
          }

          $help_block = array(
            'element' => array(
              '#tag' => 'small',
              '#attributes' => array('class' => array('help-block')),
              '#value' => join('; ', $descriptions),
            ),
          );

          $form['background_image']['field']['#options'][$context_name . '(' . $field_name . ')'] = $radio_title . theme('html_tag', $help_block);
        }
      }

      $form['background_image']['images'][0]['#states'] = array(
        'visible' => array(
          $base_input_selector . '[background_image][source]"]' => array('value' => 'upload'),
        ),
      );
    }
  }

  return $form;
}
