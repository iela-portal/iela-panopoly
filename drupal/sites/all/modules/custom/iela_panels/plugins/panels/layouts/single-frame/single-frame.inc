<?php

/**
 * Single frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Single'),
  'icon' => 'single.png',
  'category' => t('Frames'),
  'theme' => 'single_frame',
  'weight' => -10,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
  ),
);

/**
 * Preprocess variables for Single Frame.
 */
function template_preprocess_single_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'single-frame', 'clearfix'),
    ),
  );

  $variables['content']['content'] = _iela_panels_layout_single_region_wrapper($variables, 'content');
  $variables['containers']['content'][] = 'content';
}

/**
 * Returns HTML for thfie Single Frame layout.
 */
function theme_single_frame($vars) {
  return drupal_render($vars['content']);
}
