<?php

/**
 * Quarter frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Quarter'),
  'icon' => 'quarter.png',
  'category' => t('Frames'),
  'theme' => 'quarter_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
    'content_third' => t('Content Third'),
    'content_quarter' => t('Content Quarter'),
  ),
);

/**
 * Preprocess variables for Quarter frame.
 */
function template_preprocess_quarter_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'quarter-frame', 'clearfix'),
    ),
  );

  $variables['content']['content_first_second_third_quarter'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'content' => array('xs' => 12, 'sm' => 6, 'md' => 3, 'lg' => 3),
    'content_second' => array('xs' => 12, 'sm' => 6, 'md' => 3, 'lg' => 3),
    'content_thrid' => array('xs' => 12, 'sm' => 6, 'md' => 3, 'lg' => 3),
    'content_quarter' => array('xs' => 12, 'sm' => 6, 'md' => 3, 'lg' => 3),
  ));
  $variables['containers']['content_first_second_third_quarter'][] = 'content';
  $variables['containers']['content_first_second_third_quarter'][] = 'content_second';
  $variables['containers']['content_first_second_third_quarter'][] = 'content_third';
  $variables['containers']['content_first_second_third_quarter'][] = 'content_quarter';
}

/**
 * Returns HTML for the Quarter frame layout.
 */
function theme_quarter_frame($vars) {
  return drupal_render($vars['content']);
}
