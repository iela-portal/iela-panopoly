<?php

/**
 * Sidebar Left frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Sidebar left'),
  'icon' => 'sidebar-left.png',
  'category' => t('Frames'),
  'theme' => 'sidebar_left_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
  ),
);

/**
 * Preprocess variables for Sidebar Left frame.
 */
function template_preprocess_sidebar_left_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'sidebar-left-frame', 'clearfix'),
    ),
  );

  $variables['content']['content_first_second'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'content' => array('xs' => 12, 'sm' => 12, 'md' => 4, 'lg' => 4),
    'content_second' => array('xs' => 12, 'sm' => 12, 'md' => 8, 'lg' => 8),
  ));
  $variables['containers']['content_first_second'][] = 'content';
  $variables['containers']['content_first_second'][] = 'content_second';

}

/**
 * Returns HTML for the Sidebar Left frame layout.
 */
function theme_sidebar_left_frame($vars) {
  return drupal_render($vars['content']);
}
