<?php

/**
 * Third frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Third'),
  'icon' => 'third.png',
  'category' => t('Frames'),
  'theme' => 'third_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
    'content_third' => t('Content Third'),
  ),
);

/**
 * Preprocess variables for Third frame.
 */
function template_preprocess_third_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'third-frame', 'clearfix'),
    ),
  );

  $variables['content']['content_first_second_third'] = _iela_panels_layout_third_region_wrapper($variables, array(
    'content', 'content_second', 'content_third'
  ));
  $variables['containers']['content_first_second_third'][] = 'content';
  $variables['containers']['content_first_second_third'][] = 'content_second';
  $variables['containers']['content_first_second_third'][] = 'content_third';
}

/**
 * Returns HTML for the Third frame layout.
 */
function theme_third_frame($vars) {
  return drupal_render($vars['content']);
}
