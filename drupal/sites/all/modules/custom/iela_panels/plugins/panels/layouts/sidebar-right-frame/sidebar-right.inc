<?php

/**
 * Sidebar Right frame
 */

module_load_include('inc', 'iela_panels', 'includes/iela_panels.functions');

$plugin = array(
  'title' => t('Sidebar Right'),
  'icon' => 'sidebar-right.png',
  'category' => t('Frames'),
  'theme' => 'sidebar_right_frame',
  'weight' => -9,
  // 'css' => 'iela-simple.css',
  'regions' => array(
    'content' => t('Content'),
    'content_second' => t('Content Second'),
  ),
);

/**
 * Preprocess variables for Sidebar Right frame.
 */
function template_preprocess_sidebar_right_frame(&$variables, $theme) {

  // Save original contents.
  $variables['panel_contents'] = $variables['content'];

  // Replace layout contents with renderable array.
  $variables['content'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('panel-display', 'sidebar-right-frame', 'clearfix'),
    ),
  );

  $variables['content']['content_first_second'] = _iela_panels_layout_multiple_region_wrapper($variables, array(
    'content' => array('xs' => 12, 'sm' => 12, 'md' => 8, 'lg' => 8),
    'content_second' => array('xs' => 12, 'sm' => 12, 'md' => 4, 'lg' => 4),
  ));
  $variables['containers']['content_first_second'][] = 'content';
  $variables['containers']['content_first_second'][] = 'content_second';

}

/**
 * Returns HTML for the Sidebar Right frame layout.
 */
function theme_sidebar_right_frame($vars) {
  return drupal_render($vars['content']);
}
