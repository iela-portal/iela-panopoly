<?php

/**
 * @file
 * Definition of the Heading content type plugin.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Title'),
  'description' => t('A page or section title'),
  'category' => array(t('Page'), -20),
  'edit form' => 'iela_title_content_type_edit_form',
  'render callback' => 'iela_title_content_type_render',
  'defaults' => array(
    'use_page_title'      => TRUE,
    'title'               => NULL,
    'link'                => false,
    'path'                => TRUE,
    'description_handler' => 'custom',
    'description_token'   => '',
    'description'         => '',
    'tag'                 => 'h1',
    'alignment'           => 'center',
  ),
  'all contexts' => TRUE,
);

/**
 * Pane configuration form.
 */
function iela_title_content_type_edit_form($form, &$form_state) {

  // Avoid showing default override title field.
  $form['override_title']['#access'] = FALSE;
  $form['override_title_text']['#access'] = FALSE;
  $form['override_title_markup']['#access'] = FALSE;

  // Get current configuration.
  $conf = $form_state['conf'];

  // Wheter page title is used or not.
  $form['use_page_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use current page title'),
    '#default_value' => $conf['use_page_title'],
  );

  // Custom title text.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title text'),
    '#default_value' => $conf['title'],
    '#states' => array(
      'invisible' => array(
        ':input[name="use_page_title"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Linking title.
  $form['link'] = array(
    '#title' => t('Make title a link'),
    '#type' => 'checkbox',
    '#default_value' => $entity->link,
    '#description' => t('Check here to make the title be a link.'),
    '#id' => 'heading-link-title',
    '#default_value' => empty($conf['link']) ? false : $conf['link'],
  );

  // Custom path.
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('The path for this link. This can be an internal Drupal path such as %add-node or an external URL such as %drupal. Enter %front to link to the front page.', array('%front' => '<front>', '%add-node' => 'node/add', '%drupal' => 'http://drupal.org')),
    '#default_value' => empty($conf['path']) ? '' : $conf['path'],
    '#states' => array(
      'visible' => array(
        '#heading-link-title' => array('checked' => true),
      ),
    ),
  );

  // Size of title.
  $form['tag'] = array(
    '#type' => 'select',
    '#title' => t('Size of the title'),
    '#description' => t('Choose the size of title.'),
    '#options' => array(
      'h1' => t('Page title'),
      'h2' => t('Section title'),
      'h3' => t('3th title'),
      'h4' => t('4th title'),
      'h5' => t('5th title'),
      'h6' => t('6th title'),
    ),
    '#default_value' => $conf['tag'],
  );

  // Title alignment.
  $form['alignment'] = array(
    '#type' => 'select',
    '#title' => t('Align'),
    '#description' => t('Choose the alignment of the title horizontally.'),
    '#options' => array(
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
    ),
    '#default_value' => $conf['alignment'],
  );

  // Title description.
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Title description'),
    '#default_value' => $conf['description'],
    '#rows' => 4,
    '#weight' => 10,
  );

  $form['description_handler'] = array(
    '#type' => 'select',
    '#title' => t('Description type'),
    '#options' => array(
      'custom'    => t('Custom')
    ),
    '#description' => t('Choose how the description should be fullfilled'),
    '#weight' => 5,
    '#default_value' => $conf['description_handler'],
  );
        
  $form['tokens'] = array(
    '#type' => 'html_tag',
    '#tag' => 'p',
    '#value' => '<i>' . t('Your can use !tokens on the fields above.', array(
      '!tokens' => l('tokens', 'https://www.drupal.org/node/390482', array(
        'attributes' => array('target' => '_blank')
      )),
    )) . '</i>',
    '#weight' => 99,
  );

  // Customization for node context.
  if (!empty($form_state['contexts'])) {
    foreach ($form_state['contexts'] as $context) {
      if (is_object($context) && $context->plugin == 'entity:node' && !empty($context->data)) {
        $form['description_handler']['#options']['authoring'] = t('Authoring information');
        $form['description']['#states']['visible'] = array(
          '#edit-description-handler' => array('value' => 'custom'),
        );
        break;
      }
    }
  }

  if (count($form['description_handler']['#options']) == 1) {
    $form['description_handler']['#access'] = false;
  }

  return $form;
}

/**
 * Configuration submit handler.
 */
function iela_title_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if ($key != 'tokens' && isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function iela_title_content_type_render($subtype, $conf, $args, $contexts) {
  module_load_include('inc', 'iela', 'includes/iela.functions');

  $block = new stdClass();
  $menu_item = menu_get_item();
  $title = !empty($menu_item['title']) ? $menu_item['title'] : drupal_get_title();

  // Check if a node context is available.
  $token_data = iela_contexts_to_token_array($contexts);
  $token_options = array('clear' => true, 'sanitize' => false);

  // Get settings for the administration preview.
  if (empty($title) && !empty($_SESSION['iela_panels'])) {
    $title = $_SESSION['iela_panels']['page_title'];
  }

  // Custom title usage.
  if (empty($conf['use_page_title'])) {
    $title = $conf['title'];
  }

  // Avoid undefined warnings.
  if (!empty($title)) {
    $title = token_replace($title, $token_data, $token_options);

    if (!empty($conf['link']) && !empty($conf['path'])) {
      $title = l($title, $conf['path'], array(
        'html' => true,
      ));
    }

    $block->content = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'heading-container',
          'text-' . $conf['alignment']
        ),
      ),
    );

    $block->content['title'] = array(
      '#type' => 'html_tag',
      '#tag' => $conf['tag'],
      '#attributes' => array(
        'class' => 'title'
      ),
      '#value' => $title
    );

    // Parse description from legacy fields.
    $conf['description'] = !empty($conf['description_token']) ? $conf['description_token'] : $conf['description'];

    if (!empty($conf['description'])) {

      $title_info_value = '';

      if (!empty($conf['description_handler'])) {
        switch ($conf['description_handler']) {

          // Default custom title-info:
          case 'token':
          case 'custom':
            $title_info_value = token_replace($conf['description'], $token_data, $token_options);
            break;

          // Authoring information:
          case 'authoring':
            // Grab node object.
            $node = $args[1][0];

            // Render user link:
            $user = user_load($node->uid);
            $user_name = theme('username', array('account' => $user));

            // Render date:
            $date = format_date($node->created);

            $title_info_value = t('Submitted by !username on !datetime', array('!username' => $user_name, '!datetime' => $date));
            break;
        }
      }

      if (!empty($title_info_value)) {
        $block->content['title-info'] = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => 'title-info'
          ),
          'description' => array(
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $title_info_value,
          ),
        );
      }
    }
  }

  // Return a renderable array.
  return $block;
}
