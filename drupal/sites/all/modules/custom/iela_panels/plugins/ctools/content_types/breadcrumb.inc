<?php

/**
 * @file
 * Definition of the Breadcrumb content type plugin.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Breadcrumb'),
  'description' => t('The current page\'s breadcrumb'),
  'category' => array(t('Page'), -20),
  'render callback' => 'iela_breadcrumb_content_type_render',
  'defaults' => array(),
  'all contexts' => TRUE,
);

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info.
 */
function iela_breadcrumb_content_type_render($subtype, $conf, $args, $contexts) {

  $block = new stdClass();
  $breadcrumb = drupal_get_breadcrumb();
  $current_page = page_manager_get_current_page();
  $panelizer_conf = panels_breadcrumbs_get_panelizer_breadcrumb_conf();
  $configuration = null;

  // Find breadcrumb configuration.
  if (!empty($panelizer_conf)) {
    $configuration = $panelizer_conf;
  } elseif (!empty($current_page)) {
    $configuration = $current_page['handler']->conf;
  }

  // If no breadcrumb configuration was found, fallback to Drupal's default.
  if (isset($configuration) && $configuration['panels_breadcrumbs_state']) {
    $breadcrumb = panels_breadcrumbs_build_breadcrumb($configuration, $contexts);
  }

  $block->content = array(
    '#theme' => 'breadcrumb',
    '#breadcrumb' => $breadcrumb
  );

  // Return a renderable array.
  return $block;
}
