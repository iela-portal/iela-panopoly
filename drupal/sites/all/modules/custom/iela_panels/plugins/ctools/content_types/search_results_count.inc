<?php

/**
 * @file
 * Definition of the Search Count Results panel content type
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('All search results count'),
  'description' => t('The count of results for current search for every search panes.'),
  'required context' => new ctools_context_required(t('Keywords'), 'string'),
  'category' => t('Search'),
  'render last' => TRUE,
);

/**
 * Render the custom content type.
 */
function iela_panels_search_results_count_content_type_render($subtype, $conf, $panel_args, $context) {

  $keys =  (empty($context) || empty($context->data)) ? '' : $context->data;

  $count = 0;
  $searches = search_api_current_search();

  // Sum up all current searches results.
  foreach ($searches as $search) {
    if (!empty($search[1]['result count'])) {
      $count += intval($search[1]['result count']);
    }
  }

  $block = new stdClass();
  $block->content = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('search-results-count'),
    ),
    'content' => array(
      '#type' => 'html_tag',
      '#tag' => 'h4',
      '#value' => t('!count matched !keys', array(
        '!count' => '<em class="search-count">' . format_plural($count, '1 item', '@count items') . '</em>',
        '!keys' => '<em class="search-keys">' . check_plain($keys) . '</em>'
      )),
    ),
  );

  return $block;
}
