/**
 * @file
 * CKEditor fonts configuration.
 */

CKEDITOR.config.font_names = CKEDITOR.config.font_names;

;(function (window, document, $, Drupal, undefined) {

  var loadedFonts = false;

  Drupal.behaviors.fontYourFace = {
    attach: function (context, settings) {

      // Parse context.
      var $context = $(context);

      if (!loadedFonts) {
        // Register @font-your-face enabled fonts to CKEditor.
        var fonts = (settings.fontYourFace || []).map(function (font) {
          var fallbacks = font.css_fallbacks ? ', ' + font_css_fallbacks : '';
          return font.css_family + '/' + font.css_family + fallbacks;
        }).join(';');
        
        if (fonts.length) {
          loadedFonts = true;
          CKEDITOR.config.font_names = fonts + ';{font-your-face};' + CKEDITOR.config.font_names;
          CKEDITOR.on('instanceReady', function (event) {
            $context.find('.cke_button__fontyourface').hide();
          });
        }
      }

      $context.on('click', '.cke_combo__font:not(".font-your-face")', function () {
        $(this).addClass('font-your-face');
        setTimeout(function () {
          $('.cke_combopanel iframe').each(function () {
            $('.cke_panel_list a span', this.contentDocument).filter(function () {
              return $(this).text() === '{font-your-face}';
            }).each(function() {
              var $break  = $(this)
                , $li     = $break.closest('li')
                , $block  = $li.closest('.cke_panel_block')
                , $lis    = $li.prevAll();

              $li.hide();

              $block.prepend($('<ul class="cke_panel_list" />').append($lis));
              $block.prepend('<h1 class="cke_panel_grouptitle">@font-your-face</h1>');
            });
          });
          
        });
      });
    }
  };


})(window, document, jQuery, Drupal);
