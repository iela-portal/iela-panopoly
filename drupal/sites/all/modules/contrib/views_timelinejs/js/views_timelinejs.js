var embed_path;

(function ($) {
  Drupal.behaviors.timelineJS = {
    attach: function(context, settings) {
      $.each(Drupal.settings.timelineJS, function(key, timeline) {
        var container = $(context).find('#' + timeline['embed_id']);
        if (container.length && !container.hasClass('processed-timeline-js')) {
          container.addClass('processed-timeline-js');

          if (context === '#modalContext' || Drupal.PanelsIPE) {
            container.append($('<div />', {
              'class': 'admin-placeholder',
              'text': Drupal.t('Placeholder for @thing.', {
                '@thing': 'TimelineJS pane (id: ' + timeline['embed_id'] +  ')'
              })
            }));
          } else {
            embed_path = timeline['embed_path'];

            if (timeline['processed'] != true) {
              createStoryJS(timeline);
            }
            timeline['processed'] = true;
          }
        }
      });
    }
  }
})(jQuery);
