<?php

/**
 * Database connection.
 */
$databases['default']['default'] = array(
  'database' => 'drupal',
  'username' => 'drupal',
  'password' => 'password',
  'host' => 'database-host',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
);

// Hack to allow local proxy simulated HTTPS.
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
  $_SERVER['HTTPS'] = 'on';
}
