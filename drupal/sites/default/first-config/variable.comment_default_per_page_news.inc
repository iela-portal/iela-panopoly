<?php
/**
 * @file
 * variable.comment_default_per_page_news.inc
 */

$api = '2.0.0';

$data = array(
  'content' => '50',
  'name' => 'comment_default_per_page_news',
);

$dependencies = array();

$optional = array();

$modules = array();
