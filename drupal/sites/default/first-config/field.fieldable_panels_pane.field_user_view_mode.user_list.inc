<?php
/**
 * @file
 * field.fieldable_panels_pane.field_user_view_mode.user_list.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'view_mode' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_user_view_mode',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => '0',
    'module' => 'view_mode_field',
    'settings' => array(
      'entity_type' => 'user',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_user_view_mode' => array(
              'view_mode' => 'field_user_view_mode_view_mode',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_user_view_mode' => array(
              'view_mode' => 'field_user_view_mode_view_mode',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'view_mode_field_view_mode',
  ),
  'field_instance' => array(
    'bundle' => 'user_list',
    'default_value' => array(
      0 => array(
        'view_mode' => 'featured',
      ),
    ),
    'deleted' => '0',
    'description' => 'Selecione o modo de exibição com que os usuários selecionados serão apresentados.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '3',
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_user_view_mode',
    'label' => 'Modo de Exibição',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => '-3',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'view_mode_field',
  2 => 'options',
);
