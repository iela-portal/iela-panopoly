<?php
/**
 * @file
 * field.node.body.timeline_date.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'format' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'summary' => array(
        'not null' => FALSE,
        'size' => 'big',
        'type' => 'text',
      ),
      'value' => array(
        'not null' => FALSE,
        'size' => 'big',
        'type' => 'text',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(
      0 => 'node',
    ),
    'field_name' => 'body',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => '0',
    'module' => 'text',
    'settings' => array(),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_body' => array(
              'format' => 'body_format',
              'summary' => 'body_summary',
              'value' => 'body_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_body' => array(
              'format' => 'body_format',
              'summary' => 'body_summary',
              'value' => 'body_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'text_with_summary',
  ),
  'field_instance' => array(
    'bundle' => 'timeline_date',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'Texto de apresentação do evento, exibido quando o evento é selecionado na linha do tempo.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => '0',
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'poster' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stacked' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stand' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Corpo de Texto',
    'required' => 0,
    'settings' => array(
      'display_summary' => 0,
      'text_processing' => '1',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => '10',
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => '5',
    ),
  ),
);

$dependencies = array(
  'content_type.timeline_date' => 'content_type.timeline_date',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
