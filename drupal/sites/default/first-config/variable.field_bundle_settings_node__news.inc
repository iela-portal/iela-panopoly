<?php
/**
 * @file
 * variable.field_bundle_settings_node__news.inc
 */

$api = '2.0.0';

$data = array(
  'content' => array(
    'extra_fields' => array(
      'display' => array(),
      'form' => array(
        'title' => array(
          'weight' => '2',
        ),
      ),
    ),
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
    ),
  ),
  'name' => 'field_bundle_settings_node__news',
);

$dependencies = array();

$optional = array();

$modules = array();
