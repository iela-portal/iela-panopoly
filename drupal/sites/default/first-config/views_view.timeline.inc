<?php
/**
 * @file
 * views_view.timeline.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'timeline';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Linha do Tempo';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Timeline Teaser';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'timelinejs';
$handler->display->display_options['style_options']['timeline_theme'] = array(
  'width' => '100',
  'width_unit' => '1',
  'height' => '600',
  'height_unit' => '0',
);
$handler->display->display_options['style_options']['timeline_fields'] = array(
  'headline' => 'title',
  'bodytext' => 'body',
  'date' => 'field_timeline_dates',
  'media' => 'field_timeline_media_image',
  'credit' => 'field_timeline_credit_text',
  'caption' => 'field_timeline_caption_text',
  'tag' => '0',
);
$handler->display->display_options['style_options']['timeline_config'] = array(
  'link_to_entity' => 0,
  'link_text_enabled' => 0,
  'link_text' => 'Read more',
  'strip_tags' => '0',
  'hash_bookmark' => '0',
  'start_at_end' => '0',
  'start_zoom_adjust' => '-9',
);
/* Field: Content: Corpo de Texto */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Field: Content: Descrição da Multimídia */
$handler->display->display_options['fields']['field_timeline_caption_text']['id'] = 'field_timeline_caption_text';
$handler->display->display_options['fields']['field_timeline_caption_text']['table'] = 'field_data_field_timeline_caption_text';
$handler->display->display_options['fields']['field_timeline_caption_text']['field'] = 'field_timeline_caption_text';
/* Field: Content: Créditos da Multimídia */
$handler->display->display_options['fields']['field_timeline_credit_text']['id'] = 'field_timeline_credit_text';
$handler->display->display_options['fields']['field_timeline_credit_text']['table'] = 'field_data_field_timeline_credit_text';
$handler->display->display_options['fields']['field_timeline_credit_text']['field'] = 'field_timeline_credit_text';
/* Field: Content: Data */
$handler->display->display_options['fields']['field_timeline_dates']['id'] = 'field_timeline_dates';
$handler->display->display_options['fields']['field_timeline_dates']['table'] = 'field_data_field_timeline_dates';
$handler->display->display_options['fields']['field_timeline_dates']['field'] = 'field_timeline_dates';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'timeline_date' => 'timeline_date',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Has taxonomy terms (with depth) */
$handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
$handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['group'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator_id'] = 'term_node_tid_depth_op';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Projeto';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'term_node_tid_depth';
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['term_node_tid_depth']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
$handler->display->display_options['filters']['term_node_tid_depth']['vocabulary'] = 'projects';
$handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
$handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '10';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['path'] = 'timeline';

/* Display: Timeline Pane */
$handler = $view->new_display('panel_pane', 'Timeline Pane', 'panel_pane_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Linha do Tempo';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'timelinejs';
$handler->display->display_options['style_options']['timeline_theme'] = array(
  'width' => '100',
  'width_unit' => '1',
  'height' => '600',
  'height_unit' => '0',
);
$handler->display->display_options['style_options']['timeline_fields'] = array(
  'headline' => 'title',
  'bodytext' => 'body',
  'date' => 'field_timeline_dates',
  'media' => 'field_multimedia',
  'credit' => 'field_timeline_credit_text',
  'caption' => 'field_timeline_caption_text',
  'tag' => '0',
);
$handler->display->display_options['style_options']['timeline_config'] = array(
  'link_to_entity' => 0,
  'link_text_enabled' => 0,
  'link_text' => '',
  'strip_tags' => '0',
  'hash_bookmark' => '0',
  'start_at_end' => '0',
  'start_zoom_adjust' => '-2',
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Corpo de Texto */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
/* Field: Content: Descrição da Multimídia */
$handler->display->display_options['fields']['field_timeline_caption_text']['id'] = 'field_timeline_caption_text';
$handler->display->display_options['fields']['field_timeline_caption_text']['table'] = 'field_data_field_timeline_caption_text';
$handler->display->display_options['fields']['field_timeline_caption_text']['field'] = 'field_timeline_caption_text';
$handler->display->display_options['fields']['field_timeline_caption_text']['label'] = '';
$handler->display->display_options['fields']['field_timeline_caption_text']['element_label_colon'] = FALSE;
/* Field: Content: Créditos da Multimídia */
$handler->display->display_options['fields']['field_timeline_credit_text']['id'] = 'field_timeline_credit_text';
$handler->display->display_options['fields']['field_timeline_credit_text']['table'] = 'field_data_field_timeline_credit_text';
$handler->display->display_options['fields']['field_timeline_credit_text']['field'] = 'field_timeline_credit_text';
$handler->display->display_options['fields']['field_timeline_credit_text']['label'] = '';
$handler->display->display_options['fields']['field_timeline_credit_text']['element_label_colon'] = FALSE;
/* Field: Content: Data */
$handler->display->display_options['fields']['field_timeline_dates']['id'] = 'field_timeline_dates';
$handler->display->display_options['fields']['field_timeline_dates']['table'] = 'field_data_field_timeline_dates';
$handler->display->display_options['fields']['field_timeline_dates']['field'] = 'field_timeline_dates';
$handler->display->display_options['fields']['field_timeline_dates']['label'] = '';
$handler->display->display_options['fields']['field_timeline_dates']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_timeline_dates']['settings'] = array(
  'format_type' => 'long',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Field: Multimedia */
$handler->display->display_options['fields']['field_multimedia']['id'] = 'field_multimedia';
$handler->display->display_options['fields']['field_multimedia']['table'] = 'field_data_field_multimedia';
$handler->display->display_options['fields']['field_multimedia']['field'] = 'field_multimedia';
$handler->display->display_options['fields']['field_multimedia']['label'] = '';
$handler->display->display_options['fields']['field_multimedia']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_multimedia']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_multimedia']['type'] = 'file_url_plain';
$handler->display->display_options['pane_category']['name'] = 'Views';
$handler->display->display_options['pane_category']['weight'] = '0';
$handler->display->display_options['allow']['use_pager'] = 0;
$handler->display->display_options['allow']['items_per_page'] = 0;
$handler->display->display_options['allow']['offset'] = 0;
$handler->display->display_options['allow']['link_to_view'] = 0;
$handler->display->display_options['allow']['more_link'] = 0;
$handler->display->display_options['allow']['path_override'] = 0;
$handler->display->display_options['allow']['title_override'] = 'title_override';
$handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
$handler->display->display_options['allow']['fields_override'] = 0;
$translatables['timeline'] = array(
  t('Master'),
  t('Timeline Teaser'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Corpo de Texto'),
  t('Descrição da Multimídia'),
  t('Créditos da Multimídia'),
  t('Data'),
  t('Title'),
  t('Projeto'),
  t('Page'),
  t('Timeline Pane'),
  t('Linha do Tempo'),
  t('Views'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'node',
  2 => 'text',
  3 => 'date',
  4 => 'views_content',
  5 => 'file',
);
