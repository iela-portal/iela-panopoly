<?php
/**
 * @file
 * page_manager_pages.homepage.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'homepage';
$page->task = 'page';
$page->admin_title = 'Página Inicial';
$page->admin_description = 'The IELA portal homepage';
$page->path = 'entrada';
$page->access = array();
$page->menu = array(
  'type' => 'normal',
  'title' => 'Entrada',
  'name' => 'main-menu',
  'weight' => '-99',
  'parent' => array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
  ),
);
$page->arguments = array();
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_homepage_panel_context' => 'page_manager_handlers.page_homepage_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
