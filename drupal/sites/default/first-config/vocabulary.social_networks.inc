<?php
/**
 * @file
 * vocabulary.social_networks.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Vocabulário de redes sociais.',
  'hierarchy' => '0',
  'machine_name' => 'social_networks',
  'module' => 'taxonomy',
  'name' => 'Redes Sociais',
  'vid' => '6',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
