<?php
/**
 * @file
 * vocabulary.tags.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Use tags to group articles on similar topics into categories.',
  'hierarchy' => '0',
  'machine_name' => 'tags',
  'module' => 'taxonomy',
  'name' => 'Tags',
  'vid' => '2',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
