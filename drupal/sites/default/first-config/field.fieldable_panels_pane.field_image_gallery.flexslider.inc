<?php
/**
 * @file
 * field.fieldable_panels_pane.field_image_gallery.flexslider.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '-1',
    'columns' => array(
      'revision_id' => array(
        'description' => 'The field collection item revision id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
      'value' => array(
        'description' => 'The field collection item id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_image_gallery',
    'foreign keys' => array(),
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
    ),
    'locked' => '0',
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'path' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_image_gallery' => array(
              'revision_id' => 'field_image_gallery_revision_id',
              'value' => 'field_image_gallery_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_image_gallery' => array(
              'revision_id' => 'field_image_gallery_revision_id',
              'value' => 'field_image_gallery_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'field_collection',
  ),
  'field_instance' => array(
    'bundle' => 'flexslider',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'flexslider_field_collection',
        'settings' => array(
          'optionset' => 'simple',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_entity_flexslider',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_image_gallery',
    'label' => 'Slides',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '-3',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'field_collection',
);
