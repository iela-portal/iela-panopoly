<?php
/**
 * @file
 * page_manager_existing_pages.node_page.inc
 */

$api = '2.0.0';

$data = $pm_existing_page = new stdClass();
$pm_existing_page->api_version = 1;
$pm_existing_page->name = 'node_page';
$pm_existing_page->label = 'Página de Node';
$pm_existing_page->context = 'entity|node|nid';
$pm_existing_page->paths = 'node/%node';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'pm_existing_pages',
);
