<?php
/**
 * @file
 * vocabulary.university.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Vocabulário de universidades',
  'hierarchy' => '0',
  'machine_name' => 'university',
  'module' => 'taxonomy',
  'name' => 'Universidade',
  'vid' => '7',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
