<?php
/**
 * @file
 * content_type.timeline_date.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>Evento de Linha do Tempo</i> para criar um evento que será usado na ferramenta de linha do tempo.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Evento de Linha do Tempo',
  'title_label' => 'Chamada',
  'type' => 'timeline_date',
);

$dependencies = array();

$optional = array(
  'field.node.body.timeline_date' => 'field.node.body.timeline_date',
  'field.node.field_multimedia.timeline_date' => 'field.node.field_multimedia.timeline_date',
  'field.node.field_project.timeline_date' => 'field.node.field_project.timeline_date',
  'field.node.field_tags.timeline_date' => 'field.node.field_tags.timeline_date',
  'field.node.field_timeline_caption_text.timeline_date' => 'field.node.field_timeline_caption_text.timeline_date',
  'field.node.field_timeline_credit_text.timeline_date' => 'field.node.field_timeline_credit_text.timeline_date',
  'field.node.field_timeline_dates.timeline_date' => 'field.node.field_timeline_dates.timeline_date',
  'permission.create_timeline_date_content' => 'permission.create_timeline_date_content',
  'permission.delete_any_timeline_date_content' => 'permission.delete_any_timeline_date_content',
  'permission.delete_own_timeline_date_content' => 'permission.delete_own_timeline_date_content',
  'permission.edit_any_timeline_date_content' => 'permission.edit_any_timeline_date_content',
  'permission.edit_own_timeline_date_content' => 'permission.edit_own_timeline_date_content',
);

$modules = array(
  0 => 'node',
);
