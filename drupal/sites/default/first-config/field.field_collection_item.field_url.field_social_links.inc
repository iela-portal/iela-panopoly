<?php
/**
 * @file
 * field.field_collection_item.field_url.field_social_links.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'attributes' => array(
        'description' => 'The serialized array of attributes of the URL.',
        'not null' => FALSE,
        'serialize' => TRUE,
        'size' => 'big',
        'type' => 'blob',
      ),
      'title' => array(
        'description' => 'The title of the URL.',
        'length' => 1024,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'value' => array(
        'description' => 'The URL string.',
        'not null' => FALSE,
        'size' => 'big',
        'type' => 'text',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_url',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => '0',
    'module' => 'url',
    'settings' => array(),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_url' => array(
              'attributes' => 'field_url_attributes',
              'title' => 'field_url_title',
              'value' => 'field_url_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_url' => array(
              'attributes' => 'field_url_attributes',
              'title' => 'field_url_title',
              'value' => 'field_url_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'url',
  ),
  'field_instance' => array(
    'bundle' => 'field_social_links',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'Enlace para a página da rede social.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '1',
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_url',
    'label' => 'Enlace',
    'required' => 0,
    'settings' => array(
      'title_fetch' => 0,
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'url',
      'settings' => array(
        'size' => '60',
      ),
      'type' => 'url_external',
      'weight' => '2',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'url',
);
