<?php
/**
 * @file
 * permission.create_book_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'create book content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.book' => 'content_type.book',
);

$optional = array();

$modules = array(
  0 => 'node',
);
