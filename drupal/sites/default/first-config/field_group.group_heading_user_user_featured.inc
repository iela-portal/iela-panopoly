<?php
/**
 * @file
 * field_group.group_heading_user_user_featured.inc
 */

$api = '2.0.0';

$data = $field_group = new stdClass();
$field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
$field_group->api_version = 1;
$field_group->identifier = 'group_heading|user|user|featured';
$field_group->group_name = 'group_heading';
$field_group->entity_type = 'user';
$field_group->bundle = 'user';
$field_group->mode = 'featured';
$field_group->parent_name = '';
$field_group->data = array(
  'label' => 'Heading',
  'weight' => '1',
  'children' => array(
    0 => 'field_fullname',
    1 => 'field_iela_user_status',
  ),
  'format_type' => 'div',
  'format_settings' => array(
    'label' => 'Heading',
    'instance_settings' => array(
      'id' => '',
      'classes' => 'group-heading field-group-div',
      'description' => '',
      'show_label' => '0',
      'label_element' => 'h3',
      'effect' => 'none',
      'speed' => 'fast',
    ),
    'formatter' => 'open',
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'field_group',
);
