<?php
/**
 * @file
 * field_group.group_profile_picture_user_user_featured.inc
 */

$api = '2.0.0';

$data = $field_group = new stdClass();
$field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
$field_group->api_version = 1;
$field_group->identifier = 'group_profile_picture|user|user|featured';
$field_group->group_name = 'group_profile_picture';
$field_group->entity_type = 'user';
$field_group->bundle = 'user';
$field_group->mode = 'featured';
$field_group->parent_name = '';
$field_group->data = array(
  'label' => 'Profile picture',
  'weight' => '0',
  'children' => array(
    0 => 'field_user_picture',
    1 => 'field_social_links',
  ),
  'format_type' => 'div',
  'format_settings' => array(
    'label' => 'Profile picture',
    'instance_settings' => array(
      'id' => '',
      'classes' => 'group-profile-picture field-group-div',
      'description' => '',
      'show_label' => '0',
      'label_element' => 'h3',
      'effect' => 'none',
      'speed' => 'none',
    ),
    'formatter' => 'open',
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'field_group',
);
