<?php
/**
 * @file
 * variable.disqus_nodetypes.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'disqus_nodetypes';
$strongarm->value = array(
  'article' => 'article',
  'book' => 'book',
  'news' => 'news',
  'video' => 'video',
  'panopoly_page' => 0,
  'timeline_date' => 0,
  'panopoly_news_article' => 0,
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
