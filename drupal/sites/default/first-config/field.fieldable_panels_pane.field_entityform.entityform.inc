<?php
/**
 * @file
 * field.fieldable_panels_pane.field_entityform.entityform.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'target_id' => array(
        'description' => 'The id of the target entity.',
        'not null' => TRUE,
        'type' => 'int',
        'unsigned' => TRUE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_entityform',
    'foreign keys' => array(
      'entityform_type' => array(
        'columns' => array(
          'target_id' => 'id',
        ),
        'table' => 'entityform_type',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => '0',
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'entityform_type',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_entityform' => array(
              'target_id' => 'field_entityform_target_id',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_entityform' => array(
              'target_id' => 'field_entityform_target_id',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'entityreference',
  ),
  'field_instance' => array(
    'bundle' => 'entityform',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'links' => 0,
          'view_mode' => 'default',
          'view_mode_field' => NULL,
        ),
        'type' => 'entityreference_entity_view',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_entityform',
    'label' => 'Formulário',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => '-4',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'entityreference',
  2 => 'options',
);
