<?php
/**
 * @file
 * image_style.media_thumbnail.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(
    0 => array(
      'data' => array(
        'height' => 100,
        'width' => 100,
      ),
      'dimensions callback' => 'image_resize_dimensions',
      'effect callback' => 'image_scale_and_crop_effect',
      'form callback' => 'image_resize_form',
      'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
      'label' => 'Scale and crop',
      'module' => 'image',
      'name' => 'image_scale_and_crop',
      'summary theme' => 'image_resize_summary',
      'weight' => 0,
    ),
  ),
  'label' => 'Media thumbnail (100x100)',
  'name' => 'media_thumbnail',
  'storage' => 4,
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
