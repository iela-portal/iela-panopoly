<?php
/**
 * @file
 * variable.field_bundle_settings_node__article.inc
 */

$api = '2.0.0';

$data = array(
  'content' => array(
    'extra_fields' => array(
      'display' => array(),
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
      ),
    ),
    'view_modes' => array(),
  ),
  'name' => 'field_bundle_settings_node__article',
);

$dependencies = array();

$optional = array();

$modules = array();
