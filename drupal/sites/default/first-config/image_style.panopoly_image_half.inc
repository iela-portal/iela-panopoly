<?php
/**
 * @file
 * image_style.panopoly_image_half.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(
    0 => array(
      'data' => array(
        'reuse_crop_style' => 'panopoly_image_full',
      ),
      'effect callback' => 'manualcrop_reuse_effect',
      'form callback' => 'manualcrop_reuse_form',
      'help' => 'Reuse a crop selection from another Manual Crop enabled image style.',
      'label' => 'Manual Crop: Reuse cropped style',
      'module' => 'manualcrop',
      'name' => 'manualcrop_reuse',
      'summary theme' => 'manualcrop_reuse_summary',
      'weight' => 0,
    ),
    1 => array(
      'data' => array(
        'height' => '',
        'upscale' => 0,
        'width' => 480,
      ),
      'dimensions callback' => 'image_scale_dimensions',
      'effect callback' => 'image_scale_effect',
      'form callback' => 'image_scale_form',
      'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
      'label' => 'Scale',
      'module' => 'image',
      'name' => 'image_scale',
      'summary theme' => 'image_scale_summary',
      'weight' => 1,
    ),
  ),
  'label' => 'panopoly_image_half',
  'name' => 'panopoly_image_half',
  'storage' => 4,
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'manualcrop',
  1 => 'image',
);
