<?php
/**
 * @file
 * field_group.group_user_profile_user_user_form.inc
 */

$api = '2.0.0';

$data = $field_group = new stdClass();
$field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
$field_group->api_version = 1;
$field_group->identifier = 'group_user_profile|user|user|form';
$field_group->group_name = 'group_user_profile';
$field_group->entity_type = 'user';
$field_group->bundle = 'user';
$field_group->mode = 'form';
$field_group->parent_name = '';
$field_group->data = array(
  'label' => 'User Profile',
  'weight' => '1',
  'children' => array(
    0 => 'field_user_about',
    1 => 'field_user_picture',
    2 => 'field_fullname',
    3 => 'field_social_links',
    4 => 'field_country',
    5 => 'field_university',
    6 => 'field_iela_user_status',
  ),
  'format_type' => 'fieldset',
  'format_settings' => array(
    'formatter' => 'collapsible',
    'instance_settings' => array(
      'description' => '',
      'classes' => '',
      'required_fields' => 1,
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'field_group',
);
