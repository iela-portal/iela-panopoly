<?php
/**
 * @file
 * variable.ajax_form_entity_entityform_contact.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'ajax_form_entity_entityform_contact';
$strongarm->value = array(
  'id' => 'entityform_id',
  'activate' => 1,
  'edit_activate' => 0,
  'delete_activate' => 0,
  'message' => 1,
  'reload' => 1,
  'view_mode_region' => '0',
  'view_mode' => 'full',
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
