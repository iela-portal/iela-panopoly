<?php
/**
 * @file
 * panelizer_defaults.node_basic_page_panelizer_default.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:basic_page_panelizer:default';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'basic_page_panelizer';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'standard';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'page_manager';
$panelizer->css_class = '';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = array();
$display = new panels_display();
$display->layout = 'flexible';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '753b2492-a7da-4684-8cde-99ac8fbbd6be';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-815fefb7-55bb-4b83-b715-123ce8c09179';
  $pane->panel = 'center';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '815fefb7-55bb-4b83-b715-123ce8c09179';
  $display->content['new-815fefb7-55bb-4b83-b715-123ce8c09179'] = $pane;
  $display->panels['center'][0] = 'new-815fefb7-55bb-4b83-b715-123ce8c09179';
  $pane = new stdClass();
  $pane->pid = 'new-b417dc43-29c1-41fb-b82e-140edd24960e';
  $pane->panel = 'center';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'page_manager',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b417dc43-29c1-41fb-b82e-140edd24960e';
  $display->content['new-b417dc43-29c1-41fb-b82e-140edd24960e'] = $pane;
  $display->panels['center'][1] = 'new-b417dc43-29c1-41fb-b82e-140edd24960e';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-b417dc43-29c1-41fb-b82e-140edd24960e';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
