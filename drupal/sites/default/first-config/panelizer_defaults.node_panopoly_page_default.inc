<?php
/**
 * @file
 * panelizer_defaults.node_panopoly_page_default.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:panopoly_page:default';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'panopoly_page';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = 'page-page';
$panelizer->css = '';
$panelizer->pipeline = 'ipe';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = '';
$panelizer->view_mode = 'page_manager';
$panelizer->css_class = '';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = '';
$display = new panels_display();
$display->layout = 'iela-half';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'column1' => NULL,
    'column2' => NULL,
    'sidebar' => NULL,
    'contentmain' => NULL,
    'highlighted' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'header' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'header_second' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'header_third' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'header_fourth' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'content' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'content_second' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'content_third' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'content_fourth' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'content_fifth' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'footer' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'footer_second' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'footer_third' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
    'footer_fourth' => array(
      'region' => array(
        'color' => '',
        'reverse' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
      'container' => array(
        'color' => FALSE,
        'images' => array(
          'files' => array(
            0 => NULL,
          ),
        ),
      ),
    ),
  ),
  'style' => 'background',
  'highlighted' => array(
    'style' => 'background',
  ),
  'header' => array(
    'style' => 'background',
  ),
  'header_second' => array(
    'style' => 'background',
  ),
  'header_third' => array(
    'style' => 'background',
  ),
  'header_fourth' => array(
    'style' => 'background',
  ),
  'content' => array(
    'style' => 'background',
  ),
  'content_second' => array(
    'style' => 'background',
  ),
  'content_third' => array(
    'style' => 'background',
  ),
  'content_fourth' => array(
    'style' => 'background',
  ),
  'content_fifth' => array(
    'style' => 'background',
  ),
  'footer' => array(
    'style' => 'background',
  ),
  'footer_second' => array(
    'style' => 'background',
  ),
  'footer_third' => array(
    'style' => 'background',
  ),
  'footer_fourth' => array(
    'style' => 'background',
  ),
);
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '16346c80-6fcf-4cee-bf7a-9c0173fc2916';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-8e91650e-602a-45f1-bba2-e2de67ae9070';
  $pane->panel = 'content';
  $pane->type = 'node_body';
  $pane->subtype = 'node_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8e91650e-602a-45f1-bba2-e2de67ae9070';
  $display->content['new-8e91650e-602a-45f1-bba2-e2de67ae9070'] = $pane;
  $display->panels['content'][0] = 'new-8e91650e-602a-45f1-bba2-e2de67ae9070';
  $pane = new stdClass();
  $pane->pid = 'new-34465c03-7770-4599-881e-1932b853e43e';
  $pane->panel = 'header';
  $pane->type = 'page_title';
  $pane->subtype = 'page_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_page_title' => 0,
    'title' => '',
    'show_breadcrumb' => 1,
    'override_title' => '',
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '34465c03-7770-4599-881e-1932b853e43e';
  $display->content['new-34465c03-7770-4599-881e-1932b853e43e'] = $pane;
  $display->panels['header'][0] = 'new-34465c03-7770-4599-881e-1932b853e43e';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
