<?php
/**
 * @file
 * variable.language_default.inc
 */

$api = '2.0.0';

$data = array(
  'content' => (object) array(
    'direction' => '0',
    'domain' => '',
    'enabled' => '1',
    'formula' => '($n!=1)',
    'javascript' => 'AdDiOsDFQdLWmPbiTC1c4CaHRPLoarbXyNhA7kt3gj0',
    'language' => 'pt-br',
    'name' => 'Portuguese, Brazil',
    'native' => 'Português',
    'plurals' => '2',
    'prefix' => 'pt-br',
    'provider' => 'language-default',
    'weight' => '-10',
  ),
  'name' => 'language_default',
);

$dependencies = array();

$optional = array();

$modules = array();
