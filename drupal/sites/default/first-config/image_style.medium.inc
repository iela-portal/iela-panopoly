<?php
/**
 * @file
 * image_style.medium.inc
 */

$api = '2.0.0';

$data = array(
  'effects' => array(
    0 => array(
      'data' => array(
        'height' => 220,
        'upscale' => 1,
        'width' => 220,
      ),
      'dimensions callback' => 'image_scale_dimensions',
      'effect callback' => 'image_scale_effect',
      'form callback' => 'image_scale_form',
      'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
      'label' => 'Scale',
      'module' => 'image',
      'name' => 'image_scale',
      'summary theme' => 'image_scale_summary',
      'weight' => 0,
    ),
  ),
  'label' => 'Medium (220x220)',
  'name' => 'medium',
  'storage' => 4,
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'image',
);
