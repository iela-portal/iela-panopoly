<?php
/**
 * @file
 * variable.theme_default.inc
 */

$api = '2.0.0';

$data = array(
  'content' => 'iela_theme',
  'name' => 'theme_default',
);

$dependencies = array();

$optional = array();

$modules = array();
