<?php
/**
 * @file
 * views_view.users.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'users';
$view->description = 'View de listagem de usuários';
$view->tag = 'default';
$view->base_table = 'users';
$view->human_name = 'Usuários';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Usuários';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access user profiles';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'entity';
$handler->display->display_options['row_options']['view_mode'] = 'featured';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
/* Filter criterion: User: Active */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: User: The user ID */
$handler->display->display_options['filters']['uid_raw']['id'] = 'uid_raw';
$handler->display->display_options['filters']['uid_raw']['table'] = 'users';
$handler->display->display_options['filters']['uid_raw']['field'] = 'uid_raw';
$handler->display->display_options['filters']['uid_raw']['operator'] = '!=';
$handler->display->display_options['filters']['uid_raw']['value']['value'] = '1';

/* Display: Listagem por Universidade */
$handler = $view->new_display('panel_pane', 'Listagem por Universidade', 'university_related_list');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Listagem por Universidade';
$handler->display->display_options['display_description'] = 'Lista usuários agrupados por suas universidades';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_university',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: User: Universidade (field_university) */
$handler->display->display_options['relationships']['field_university_tid']['id'] = 'field_university_tid';
$handler->display->display_options['relationships']['field_university_tid']['table'] = 'field_data_field_university';
$handler->display->display_options['relationships']['field_university_tid']['field'] = 'field_university_tid';
$handler->display->display_options['relationships']['field_university_tid']['required'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: User: Rendered User */
$handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_user';
$handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['label'] = '';
$handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
$handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
$handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full_name';
$handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
/* Field: User: Universidade */
$handler->display->display_options['fields']['field_university']['id'] = 'field_university';
$handler->display->display_options['fields']['field_university']['table'] = 'field_data_field_university';
$handler->display->display_options['fields']['field_university']['field'] = 'field_university';
$handler->display->display_options['fields']['field_university']['label'] = '';
$handler->display->display_options['fields']['field_university']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_university']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_university']['type'] = 'taxonomy_term_reference_plain';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: User: Universidade (field_university) */
$handler->display->display_options['sorts']['field_university_tid']['id'] = 'field_university_tid';
$handler->display->display_options['sorts']['field_university_tid']['table'] = 'field_data_field_university';
$handler->display->display_options['sorts']['field_university_tid']['field'] = 'field_university_tid';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: User: Active */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: User: Universidade (field_university) */
$handler->display->display_options['filters']['field_university_tid']['id'] = 'field_university_tid';
$handler->display->display_options['filters']['field_university_tid']['table'] = 'field_data_field_university';
$handler->display->display_options['filters']['field_university_tid']['field'] = 'field_university_tid';
$handler->display->display_options['filters']['field_university_tid']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_university_tid']['expose']['operator_id'] = 'field_university_tid_op';
$handler->display->display_options['filters']['field_university_tid']['expose']['label'] = 'Universidade';
$handler->display->display_options['filters']['field_university_tid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['field_university_tid']['expose']['operator_label'] = 'Tipo de filtro';
$handler->display->display_options['filters']['field_university_tid']['expose']['operator'] = 'field_university_tid_op';
$handler->display->display_options['filters']['field_university_tid']['expose']['identifier'] = 'field_university_tid';
$handler->display->display_options['filters']['field_university_tid']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['field_university_tid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['field_university_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_university_tid']['vocabulary'] = 'university';
$handler->display->display_options['filters']['field_university_tid']['hierarchy'] = 1;
$handler->display->display_options['pane_title'] = 'Usuários listados por universidade';
$handler->display->display_options['pane_category']['name'] = 'Views';
$handler->display->display_options['pane_category']['weight'] = '0';
$handler->display->display_options['allow']['use_pager'] = 0;
$handler->display->display_options['allow']['items_per_page'] = 0;
$handler->display->display_options['allow']['offset'] = 0;
$handler->display->display_options['allow']['link_to_view'] = 0;
$handler->display->display_options['allow']['more_link'] = 0;
$handler->display->display_options['allow']['path_override'] = 0;
$handler->display->display_options['allow']['title_override'] = 'title_override';
$handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
$handler->display->display_options['allow']['fields_override'] = 0;

/* Display: Listagem por País */
$handler = $view->new_display('panel_pane', 'Listagem por País', 'country_related_list');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Listagem por País';
$handler->display->display_options['display_description'] = 'Lista os usuários agrupados por país';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'name',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Field: País (field_country) */
$handler->display->display_options['relationships']['field_country_iso2']['id'] = 'field_country_iso2';
$handler->display->display_options['relationships']['field_country_iso2']['table'] = 'field_data_field_country';
$handler->display->display_options['relationships']['field_country_iso2']['field'] = 'field_country_iso2';
$handler->display->display_options['relationships']['field_country_iso2']['required'] = TRUE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: User: Rendered User */
$handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_user';
$handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
$handler->display->display_options['fields']['rendered_entity']['label'] = '';
$handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
$handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
$handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'full_name';
$handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
/* Field: Countries: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'countries_country';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'field_country_iso2';
$handler->display->display_options['fields']['name']['label'] = '';
$handler->display->display_options['fields']['name']['exclude'] = TRUE;
$handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Countries: Name */
$handler->display->display_options['sorts']['name']['id'] = 'name';
$handler->display->display_options['sorts']['name']['table'] = 'countries_country';
$handler->display->display_options['sorts']['name']['field'] = 'name';
$handler->display->display_options['sorts']['name']['relationship'] = 'field_country_iso2';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: User: Active */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'users';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Countries: Continent - list */
$handler->display->display_options['filters']['continent_list']['id'] = 'continent_list';
$handler->display->display_options['filters']['continent_list']['table'] = 'countries_country';
$handler->display->display_options['filters']['continent_list']['field'] = 'continent_list';
$handler->display->display_options['filters']['continent_list']['relationship'] = 'field_country_iso2';
$handler->display->display_options['filters']['continent_list']['exposed'] = TRUE;
$handler->display->display_options['filters']['continent_list']['expose']['operator_id'] = 'continent_list_op';
$handler->display->display_options['filters']['continent_list']['expose']['label'] = 'Continente';
$handler->display->display_options['filters']['continent_list']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['continent_list']['expose']['operator_label'] = 'Tipo de filtro';
$handler->display->display_options['filters']['continent_list']['expose']['operator'] = 'continent_list_op';
$handler->display->display_options['filters']['continent_list']['expose']['identifier'] = 'continent_list';
$handler->display->display_options['filters']['continent_list']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['continent_list']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
/* Filter criterion: Countries: Name - list */
$handler->display->display_options['filters']['name_list']['id'] = 'name_list';
$handler->display->display_options['filters']['name_list']['table'] = 'countries_country';
$handler->display->display_options['filters']['name_list']['field'] = 'name_list';
$handler->display->display_options['filters']['name_list']['relationship'] = 'field_country_iso2';
$handler->display->display_options['filters']['name_list']['exposed'] = TRUE;
$handler->display->display_options['filters']['name_list']['expose']['operator_id'] = 'name_list_op';
$handler->display->display_options['filters']['name_list']['expose']['label'] = 'País';
$handler->display->display_options['filters']['name_list']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['name_list']['expose']['operator_label'] = 'Tipo de filtro';
$handler->display->display_options['filters']['name_list']['expose']['operator'] = 'name_list_op';
$handler->display->display_options['filters']['name_list']['expose']['identifier'] = 'name_list';
$handler->display->display_options['filters']['name_list']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['name_list']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['pane_title'] = 'Usuários listados por país';
$handler->display->display_options['pane_category']['name'] = 'Views';
$handler->display->display_options['pane_category']['weight'] = '0';
$handler->display->display_options['allow']['use_pager'] = 0;
$handler->display->display_options['allow']['items_per_page'] = 0;
$handler->display->display_options['allow']['offset'] = 0;
$handler->display->display_options['allow']['link_to_view'] = 0;
$handler->display->display_options['allow']['more_link'] = 0;
$handler->display->display_options['allow']['path_override'] = 0;
$handler->display->display_options['allow']['title_override'] = 'title_override';
$handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
$handler->display->display_options['allow']['fields_override'] = 0;
$translatables['users'] = array(
  t('Master'),
  t('Usuários'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Listagem por Universidade'),
  t('Lista usuários agrupados por suas universidades'),
  t('term from field_university'),
  t('Universidade'),
  t('Tipo de filtro'),
  t('Usuários listados por universidade'),
  t('Views'),
  t('Listagem por País'),
  t('Lista os usuários agrupados por país'),
  t('country from field_country'),
  t('Continente'),
  t('País'),
  t('Usuários listados por país'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => 'user',
  2 => 'views_content',
  3 => 'taxonomy',
);
