<?php
/**
 * @file
 * permission.use_text_format_panopoly_wysiwyg_text.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'use text format panopoly_wysiwyg_text',
  'roles' => array(
    0 => 'administrator',
    1 => 'editor',
  ),
);

$dependencies = array(
  'text_format.panopoly_wysiwyg_text' => 'text_format.panopoly_wysiwyg_text',
);

$optional = array();

$modules = array(
  0 => 'filter',
);
