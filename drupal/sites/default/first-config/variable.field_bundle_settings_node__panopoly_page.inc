<?php
/**
 * @file
 * variable.field_bundle_settings_node__panopoly_page.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'field_bundle_settings_node__panopoly_page';
$strongarm->value = array(
  'view_modes' => array(
    'teaser' => array(
      'custom_settings' => TRUE,
    ),
    'search_result' => array(
      'custom_settings' => FALSE,
    ),
    'featured' => array(
      'custom_settings' => TRUE,
    ),
    'full' => array(
      'custom_settings' => FALSE,
    ),
    'rss' => array(
      'custom_settings' => FALSE,
    ),
    'search_index' => array(
      'custom_settings' => FALSE,
    ),
    'token' => array(
      'custom_settings' => FALSE,
    ),
    'stacked' => array(
      'custom_settings' => FALSE,
    ),
  ),
  'extra_fields' => array(
    'form' => array(
      'title' => array(
        'weight' => '0',
      ),
      'path' => array(
        'weight' => '5',
      ),
    ),
    'display' => array(),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
