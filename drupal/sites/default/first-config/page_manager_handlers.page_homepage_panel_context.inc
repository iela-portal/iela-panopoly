<?php
/**
 * @file
 * page_manager_handlers.page_homepage_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_homepage_panel_context';
$handler->task = 'page';
$handler->subtask = 'homepage';
$handler->handler = 'panel_context';
$handler->weight = 1;
$handler->conf = array(
  'title' => 'Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'home-page',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display();
$display->layout = 'iela-half-third';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'highlighted' => NULL,
    'header' => NULL,
    'header_second' => NULL,
    'header_third' => NULL,
    'header_fourth' => NULL,
    'content' => NULL,
    'content_second' => NULL,
    'content_third' => NULL,
    'content_fourth' => NULL,
    'content_fifth' => NULL,
    'footer' => NULL,
    'footer_second' => NULL,
    'footer_third' => NULL,
    'footer_fourth' => NULL,
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '48d3246d-c3f0-4bc4-b426-479ec2f117dd';
$display->content = array();
$display->panels = array();
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
