<?php
/**
 * @file
 * panelizer_defaults.node_panopoly_page_default_teaser.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:panopoly_page:default:teaser';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'panopoly_page';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'standard';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'teaser';
$panelizer->css_class = 'page-teaser';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = '';
$display = new panels_display();
$display->layout = 'boxton';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'contentmain' => NULL,
  ),
);
$display->cache = array();
$display->title = '%node:title';
$display->uuid = 'f76cbd0c-d182-4bd2-96f2-f1759b4e69d8';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-873d60d8-f80e-41c1-b89d-86ad219285e8';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_link' => 'content',
      'image_style' => 'panopoly_image_quarter',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '873d60d8-f80e-41c1-b89d-86ad219285e8';
  $display->content['new-873d60d8-f80e-41c1-b89d-86ad219285e8'] = $pane;
  $display->panels['contentmain'][0] = 'new-873d60d8-f80e-41c1-b89d-86ad219285e8';
  $pane = new stdClass();
  $pane->pid = 'new-9363564d-7f96-4fa2-a1a5-5b0f5f60ce68';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_summary_or_trimmed',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '600',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9363564d-7f96-4fa2-a1a5-5b0f5f60ce68';
  $display->content['new-9363564d-7f96-4fa2-a1a5-5b0f5f60ce68'] = $pane;
  $display->panels['contentmain'][1] = 'new-9363564d-7f96-4fa2-a1a5-5b0f5f60ce68';
  $pane = new stdClass();
  $pane->pid = 'new-e7b29d80-ad23-4c5d-85fd-13f7a038e3d8';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'e7b29d80-ad23-4c5d-85fd-13f7a038e3d8';
  $display->content['new-e7b29d80-ad23-4c5d-85fd-13f7a038e3d8'] = $pane;
  $display->panels['contentmain'][2] = 'new-e7b29d80-ad23-4c5d-85fd-13f7a038e3d8';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
