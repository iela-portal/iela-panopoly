<?php
/**
 * @file
 * permission.delete_own_book_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete own book content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.book' => 'content_type.book',
);

$optional = array();

$modules = array(
  0 => 'node',
);
