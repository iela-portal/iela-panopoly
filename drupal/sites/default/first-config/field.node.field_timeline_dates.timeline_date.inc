<?php
/**
 * @file
 * field.node.field_timeline_dates.timeline_date.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'value' => array(
        'mysql_type' => 'datetime',
        'not null' => FALSE,
        'pgsql_type' => 'timestamp without time zone',
        'sortable' => TRUE,
        'sqlite_type' => 'varchar',
        'sqlsrv_type' => 'smalldatetime',
        'type' => 'datetime',
        'views' => TRUE,
      ),
      'value2' => array(
        'mysql_type' => 'datetime',
        'not null' => FALSE,
        'pgsql_type' => 'timestamp without time zone',
        'sortable' => TRUE,
        'sqlite_type' => 'varchar',
        'sqlsrv_type' => 'smalldatetime',
        'type' => 'datetime',
        'views' => FALSE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_timeline_dates',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => '0',
    'module' => 'date',
    'settings' => array(
      'cache_count' => '4',
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => 'UTC',
      'todate' => 'optional',
      'tz_handling' => 'site',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_timeline_dates' => array(
              'value' => 'field_timeline_dates_value',
              'value2' => 'field_timeline_dates_value2',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_timeline_dates' => array(
              'value' => 'field_timeline_dates_value',
              'value2' => 'field_timeline_dates_value2',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'datetime',
  ),
  'field_instance' => array(
    'bundle' => 'timeline_date',
    'deleted' => '0',
    'description' => 'Selecione a data de início e, caso esteja definida, a data de término do evento.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => '1',
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'poster' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stacked' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stand' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_timeline_dates',
    'label' => 'Data',
    'required' => 1,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_popup',
      'weight' => '2',
    ),
  ),
);

$dependencies = array(
  'content_type.timeline_date' => 'content_type.timeline_date',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'date',
);
