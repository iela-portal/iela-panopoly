<?php
/**
 * @file
 * vocabulary.collections.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Vocabulário de coleções para livros e outros conteúdos',
  'hierarchy' => '0',
  'machine_name' => 'collections',
  'module' => 'taxonomy',
  'name' => 'Coleções',
  'vid' => '8',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
