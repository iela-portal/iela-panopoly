<?php
/**
 * @file
 * entityform_type.contact.inc
 */

$api = '2.0.0';

$data = entity_import('entityform_type', '{
    "type" : "contact",
    "label" : "Formul\\u00e1rio de Contato",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "panopoly_wysiwyg_text" },
      "submit_button_text" : "Enviar",
      "submit_confirm_msg" : "Formul\\u00e1rio enviado com sucesso. Entraremos em contato em breve!",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "panopoly_wysiwyg_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : { "value" : "", "format" : "panopoly_wysiwyg_text" }
    },
    "weight" : "0",
    "paths" : []
  }');

$dependencies = array();

$optional = array(
  'field.entityform.field_contact_name.contact' => 'field.entityform.field_contact_name.contact',
  'field.entityform.field_email.contact' => 'field.entityform.field_email.contact',
  'field.entityform.field_message.contact' => 'field.entityform.field_message.contact',
);

$modules = array(
  0 => 'entity',
  1 => 'entityform',
);
