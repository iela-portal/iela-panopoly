<?php
/**
 * @file
 * field.node.field_document.article.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '-1',
    'columns' => array(
      'description' => array(
        'description' => 'A description of the file.',
        'not null' => FALSE,
        'type' => 'text',
      ),
      'display' => array(
        'default' => 1,
        'description' => 'Flag to control whether this file should be displayed when viewing content.',
        'not null' => TRUE,
        'size' => 'tiny',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'fid' => array(
        'description' => 'The {file_managed}.fid being referenced in this field.',
        'not null' => FALSE,
        'type' => 'int',
        'unsigned' => TRUE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_document',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => '0',
    'module' => 'file',
    'settings' => array(
      'display_default' => 0,
      'display_field' => 0,
      'uri_scheme' => 'public',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_document' => array(
              'description' => 'field_document_description',
              'display' => 'field_document_display',
              'fid' => 'field_document_fid',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_document' => array(
              'description' => 'field_document_description',
              'display' => 'field_document_display',
              'fid' => 'field_document_fid',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'file',
  ),
  'field_instance' => array(
    'bundle' => 'article',
    'deleted' => '0',
    'description' => 'Selecione um documento para esse artigo. <small>Somente serão aceitos documentos nos formatos PDF ou DOC.</small>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file',
        'settings' => array(),
        'type' => 'file_default',
        'weight' => 14,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '2',
      ),
      'poster' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stacked' => array(
        'label' => 'hidden',
        'module' => 'file_entity',
        'settings' => array(
          'text' => 'Baixar',
        ),
        'type' => 'file_download_link',
        'weight' => '0',
      ),
      'stand' => array(
        'label' => 'inline',
        'module' => 'file_entity',
        'settings' => array(
          'text' => 'Download [file:name]',
        ),
        'type' => 'file_download_link',
        'weight' => '3',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_document',
    'label' => 'Documento',
    'required' => 1,
    'settings' => array(
      'description_field' => 1,
      'file_directory' => 'articles',
      'file_extensions' => 'pdf doc docx',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'vimeo' => 0,
          'youtube' => 0,
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 'document',
          'image' => 0,
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 'media_internet',
          'upload' => 'upload',
          'youtube' => 0,
        ),
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 0,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => 1,
        'manualcrop_maximize_default_crop_area' => 0,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => 0,
      ),
      'type' => 'media_generic',
      'weight' => '2',
    ),
  ),
);

$dependencies = array(
  'content_type.article' => 'content_type.article',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'file',
  2 => 'media',
);
