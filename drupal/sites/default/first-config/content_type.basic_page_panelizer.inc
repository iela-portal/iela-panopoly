<?php
/**
 * @file
 * content_type.basic_page_panelizer.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Criar página básica personalizavel',
  'has_title' => '1',
  'help' => '',
  'name' => 'Página Básica - Panelizer',
  'title_label' => 'Título',
  'type' => 'basic_page_panelizer',
);

$dependencies = array();

$optional = array(
  'field.node.body.basic_page_panelizer' => 'field.node.body.basic_page_panelizer',
  'permission.create_basic_page_panelizer_content' => 'permission.create_basic_page_panelizer_content',
  'permission.delete_any_basic_page_panelizer_content' => 'permission.delete_any_basic_page_panelizer_content',
  'permission.delete_own_basic_page_panelizer_content' => 'permission.delete_own_basic_page_panelizer_content',
  'permission.edit_any_basic_page_panelizer_content' => 'permission.edit_any_basic_page_panelizer_content',
  'permission.edit_own_basic_page_panelizer_content' => 'permission.edit_own_basic_page_panelizer_content',
);

$modules = array(
  0 => 'node',
);
