<?php
/**
 * @file
 * variable.panels_page_allowed_layouts.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'panels_page_allowed_layouts';
$strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:11:"panels_page";s:23:"allowed_layout_settings";a:50:{s:8:"flexible";b:0;s:14:"twocol_stacked";b:0;s:13:"twocol_bricks";b:0;s:6:"twocol";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:17:"threecol_33_34_33";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:17:"threecol_25_50_25";b:0;s:6:"onecol";b:0;s:6:"whelan";b:0;s:12:"webb_flipped";b:0;s:5:"sutro";b:0;s:13:"selby_flipped";b:0;s:5:"selby";b:0;s:4:"pond";b:0;s:15:"moscone_flipped";b:0;s:7:"moscone";b:0;s:8:"mccoppin";b:0;s:15:"hewston_flipped";b:0;s:7:"hewston";b:0;s:6:"harris";b:0;s:5:"geary";b:0;s:12:"burr_flipped";b:0;s:4:"burr";b:0;s:5:"brown";b:0;s:15:"brenham_flipped";b:0;s:7:"brenham";b:0;s:16:"bartlett_flipped";b:0;s:8:"bartlett";b:0;s:17:"sanderson_flipped";b:0;s:9:"sanderson";b:0;s:6:"boxton";b:0;s:22:"bryant_flipped_flipped";b:0;s:6:"bryant";b:0;s:6:"phelan";b:0;s:14:"taylor_flipped";b:0;s:6:"taylor";b:0;s:12:"sutro_double";b:0;s:5:"rolph";b:0;s:4:"webb";b:0;s:15:"iela-half-third";b:1;s:9:"iela-half";b:1;s:17:"iela-sidebar-left";b:1;s:23:"iela-sidebar-mixed-left";b:1;s:24:"iela-sidebar-mixed-right";b:1;s:18:"iela-sidebar-right";b:1;s:11:"iela-simple";b:1;s:15:"iela-third-half";b:1;s:10:"iela-third";b:1;s:24:"iela-sidebar-right-third";b:1;}s:10:"form_state";N;}';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
