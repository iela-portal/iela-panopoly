<?php
/**
 * @file
 * field.user.field_fullname.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'credentials' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'family' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'generational' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'given' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'middle' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'title' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_fullname',
    'foreign keys' => array(),
    'indexes' => array(
      'family' => array(
        0 => 'family',
      ),
      'given' => array(
        0 => 'given',
      ),
    ),
    'locked' => '0',
    'module' => 'name',
    'settings' => array(
      'allow_family_or_given' => 1,
      'autocomplete_separator' => array(
        'credentials' => ', ',
        'family' => ' -',
        'generational' => ' ',
        'given' => ' -',
        'middle' => ' -',
        'title' => ' ',
      ),
      'autocomplete_source' => array(
        'credentials' => array(),
        'family' => array(),
        'generational' => array(
          'generational' => 0,
        ),
        'given' => array(),
        'middle' => array(),
        'title' => array(
          'title' => 0,
        ),
      ),
      'components' => array(
        'credentials' => 0,
        'family' => 'family',
        'generational' => 0,
        'given' => 'given',
        'middle' => 'middle',
        'title' => 'title',
      ),
      'generational_options' => '-- --
Jr.
Sr.
I
II
III
IV
V
VI
VII
VIII
IX
X',
      'labels' => array(
        'credentials' => 'Credentials',
        'family' => 'Sobrenome',
        'generational' => 'Generational',
        'given' => 'Nome',
        'middle' => 'Nome do meio',
        'title' => 'Pronome',
      ),
      'max_length' => array(
        'credentials' => '255',
        'family' => '63',
        'generational' => '15',
        'given' => '63',
        'middle' => '127',
        'title' => '31',
      ),
      'minimum_components' => array(
        'credentials' => 0,
        'family' => 'family',
        'generational' => 0,
        'given' => 'given',
        'middle' => 0,
        'title' => 0,
      ),
      'sort_options' => array(
        'generational' => 0,
        'title' => 0,
      ),
      'title_options' => '-- --
Sr.
Sr.ª
Dr.
Dr.ª
Prof.
Prof.ª',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_fullname' => array(
              'credentials' => 'field_fullname_credentials',
              'family' => 'field_fullname_family',
              'generational' => 'field_fullname_generational',
              'given' => 'field_fullname_given',
              'middle' => 'field_fullname_middle',
              'title' => 'field_fullname_title',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_fullname' => array(
              'credentials' => 'field_fullname_credentials',
              'family' => 'field_fullname_family',
              'generational' => 'field_fullname_generational',
              'given' => 'field_fullname_given',
              'middle' => 'field_fullname_middle',
              'title' => 'field_fullname_title',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'name',
  ),
  'field_instance' => array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'name',
        'settings' => array(
          'format' => 'default',
          'markup' => 0,
          'multiple' => 'default',
          'multiple_and' => 'text',
          'multiple_delimiter' => ', ',
          'multiple_delimiter_precedes_last' => 'never',
          'multiple_el_al_first' => 1,
          'multiple_el_al_min' => 3,
          'output' => 'default',
        ),
        'type' => 'name_formatter',
        'weight' => 3,
      ),
      'featured' => array(
        'label' => 'hidden',
        'module' => 'name',
        'settings' => array(
          'format' => 'default',
          'markup' => 0,
          'multiple' => 'default',
          'multiple_and' => 'text',
          'multiple_delimiter' => ', ',
          'multiple_delimiter_precedes_last' => 'never',
          'multiple_el_al_first' => 1,
          'multiple_el_al_min' => 3,
          'output' => 'default',
        ),
        'type' => 'name_formatter',
        'weight' => '2',
      ),
      'full_name' => array(
        'label' => 'hidden',
        'module' => 'name',
        'settings' => array(
          'format' => 'default',
          'markup' => 0,
          'multiple' => 'default',
          'multiple_and' => 'text',
          'multiple_delimiter' => ', ',
          'multiple_delimiter_precedes_last' => 'never',
          'multiple_el_al_first' => '1',
          'multiple_el_al_min' => '3',
          'output' => 'default',
        ),
        'type' => 'name_formatter',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_fullname',
    'label' => 'Nome',
    'required' => 1,
    'settings' => array(
      'component_css' => '',
      'component_layout' => 'default',
      'credentials_inline' => 0,
      'field_type' => array(
        'credentials' => 'text',
        'family' => 'text',
        'generational' => 'select',
        'given' => 'text',
        'middle' => 'text',
        'title' => 'text',
      ),
      'generational_field' => 'select',
      'inline_css' => array(
        'credentials' => '',
        'family' => '',
        'generational' => '',
        'given' => '',
        'middle' => '',
        'title' => '',
      ),
      'name_user_preferred' => 0,
      'override_format' => 'default',
      'show_component_required_marker' => 0,
      'size' => array(
        'credentials' => '35',
        'family' => '20',
        'generational' => '5',
        'given' => '20',
        'middle' => '20',
        'title' => '6',
      ),
      'title_display' => array(
        'credentials' => 'none',
        'family' => 'description',
        'generational' => 'none',
        'given' => 'description',
        'middle' => 'description',
        'title' => 'description',
      ),
      'title_field' => 'select',
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'name',
      'settings' => array(),
      'type' => 'name_widget',
      'weight' => '5',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'name',
);
