<?php
/**
 * @file
 * variable.date_popup_authored_format_basic_page_panelizer.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'date_popup_authored_format_basic_page_panelizer';
$strongarm->value = 'j M Y - H:i';


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
