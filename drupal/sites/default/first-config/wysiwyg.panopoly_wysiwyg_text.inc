<?php
/**
 * @file
 * wysiwyg.panopoly_wysiwyg_text.inc
 */

$api = '2.0.0';

$data = (object) array(
  'editor' => 'ckeditor',
  'format' => 'panopoly_wysiwyg_text',
  'settings' => array(
    'acf_allowed_content' => '',
    'acf_mode' => '0',
    'add_to_summaries' => 1,
    'advanced__active_tab' => 'edit-basic',
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'buttons' => array(
      'default' => array(
        'BGColor' => 1,
        'Blockquote' => 1,
        'Bold' => 1,
        'BulletedList' => 1,
        'CreateDiv' => 1,
        'Font' => 1,
        'FontSize' => 1,
        'HorizontalRule' => 1,
        'Indent' => 1,
        'Italic' => 1,
        'JustifyCenter' => 1,
        'JustifyLeft' => 1,
        'JustifyRight' => 1,
        'Link' => 1,
        'Maximize' => 1,
        'NumberedList' => 1,
        'Outdent' => 1,
        'PasteFromWord' => 1,
        'Redo' => 1,
        'RemoveFormat' => 1,
        'ShowBlocks' => 1,
        'Strike' => 1,
        'Styles' => 1,
        'Table' => 1,
        'TextColor' => 1,
        'Underline' => 1,
        'Unlink' => 1,
      ),
      'drupal' => array(
        'media' => 1,
        'node_embed' => 1,
      ),
      'linkit' => array(
        'linkit' => 1,
      ),
      'templates' => array(
        'Templates' => 1,
      ),
    ),
    'css_path' => '',
    'css_setting' => 'theme',
    'default' => 1,
    'default_toolbar_grouping' => 0,
    'forcePasteAsPlainText' => 0,
    'language' => 'pt-br',
    'resize_enabled' => 1,
    'show_toggle' => 1,
    'simple_source_formatting' => 0,
    'stylesSet' => '',
    'theme' => 'advanced',
    'toolbarLocation' => 'top',
    'user_choose' => 0,
  ),
);

$dependencies = array(
  'text_format.panopoly_wysiwyg_text' => 'text_format.panopoly_wysiwyg_text',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
