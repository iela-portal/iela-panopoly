<?php
/**
 * @file
 * content_type.video.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>Vídeo</i> para cadastrar um vídeo categorizável no site.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Vídeo',
  'title_label' => 'Título',
  'type' => 'video',
);

$dependencies = array();

$optional = array(
  'field.node.field_project.video' => 'field.node.field_project.video',
  'field.node.field_teaser.video' => 'field.node.field_teaser.video',
  'field.node.field_video.video' => 'field.node.field_video.video',
  'permission.create_video_content' => 'permission.create_video_content',
  'permission.delete_any_video_content' => 'permission.delete_any_video_content',
  'permission.delete_own_video_content' => 'permission.delete_own_video_content',
  'permission.edit_any_video_content' => 'permission.edit_any_video_content',
  'permission.edit_own_video_content' => 'permission.edit_own_video_content',
);

$modules = array(
  0 => 'node',
);
