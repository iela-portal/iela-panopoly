<?php
/**
 * @file
 * panelizer_defaults.node_panopoly_page_default_featured.inc
 */

$api = '2.0.0';

$data = $panelizer = new stdClass();
$panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
$panelizer->api_version = 1;
$panelizer->name = 'node:panopoly_page:default:featured';
$panelizer->title = 'Default';
$panelizer->panelizer_type = 'node';
$panelizer->panelizer_key = 'panopoly_page';
$panelizer->no_blocks = FALSE;
$panelizer->css_id = '';
$panelizer->css = '';
$panelizer->pipeline = 'standard';
$panelizer->contexts = array();
$panelizer->relationships = array();
$panelizer->access = array();
$panelizer->view_mode = 'featured';
$panelizer->css_class = '';
$panelizer->title_element = 'H2';
$panelizer->link_to_entity = TRUE;
$panelizer->extra = '';
$display = new panels_display();
$display->layout = 'boxton';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'column1' => NULL,
    'column2' => NULL,
    'contentmain' => NULL,
  ),
);
$display->cache = array();
$display->title = '%node:title';
$display->uuid = '5b7d08f9-c351-427e-8015-14433221537b';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-af46907d-aa25-43db-995b-b38f9174221e';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_link' => 'content',
      'image_style' => 'panopoly_image_half',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'af46907d-aa25-43db-995b-b38f9174221e';
  $display->content['new-af46907d-aa25-43db-995b-b38f9174221e'] = $pane;
  $display->panels['contentmain'][0] = 'new-af46907d-aa25-43db-995b-b38f9174221e';
  $pane = new stdClass();
  $pane->pid = 'new-1ee1b165-ebd0-4115-ad5e-49df79509f82';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '1ee1b165-ebd0-4115-ad5e-49df79509f82';
  $display->content['new-1ee1b165-ebd0-4115-ad5e-49df79509f82'] = $pane;
  $display->panels['contentmain'][1] = 'new-1ee1b165-ebd0-4115-ad5e-49df79509f82';
  $pane = new stdClass();
  $pane->pid = 'new-a71dc705-b195-439c-b47b-980d8c7fa155';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_summary_or_trimmed',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '250',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a71dc705-b195-439c-b47b-980d8c7fa155';
  $display->content['new-a71dc705-b195-439c-b47b-980d8c7fa155'] = $pane;
  $display->panels['contentmain'][2] = 'new-a71dc705-b195-439c-b47b-980d8c7fa155';
  $pane = new stdClass();
  $pane->pid = 'new-1d6308c6-817d-4465-ac63-fa7acca627aa';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'featured',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = '1d6308c6-817d-4465-ac63-fa7acca627aa';
  $display->content['new-1d6308c6-817d-4465-ac63-fa7acca627aa'] = $pane;
  $display->panels['contentmain'][3] = 'new-1d6308c6-817d-4465-ac63-fa7acca627aa';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = '0';
$panelizer->display = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'panelizer',
);
