<?php
/**
 * @file
 * variable.comment_default_per_page_article.inc
 */

$api = '2.0.0';

$data = array(
  'content' => '50',
  'name' => 'comment_default_per_page_article',
);

$dependencies = array();

$optional = array();

$modules = array();
