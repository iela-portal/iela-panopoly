<?php
/**
 * @file
 * permission.delete_any_timeline_date_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete any timeline_date content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.timeline_date' => 'content_type.timeline_date',
);

$optional = array();

$modules = array(
  0 => 'node',
);
