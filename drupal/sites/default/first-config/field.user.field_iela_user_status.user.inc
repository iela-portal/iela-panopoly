<?php
/**
 * @file
 * field.user.field_iela_user_status.user.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'format' => array(
        'length' => 255,
        'not null' => FALSE,
        'type' => 'varchar',
      ),
      'value' => array(
        'length' => '255',
        'not null' => FALSE,
        'type' => 'varchar',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_iela_user_status',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => '0',
    'module' => 'text',
    'settings' => array(
      'max_length' => '255',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_iela_user_status' => array(
              'format' => 'field_iela_user_status_format',
              'value' => 'field_iela_user_status_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_iela_user_status' => array(
              'format' => 'field_iela_user_status_format',
              'value' => 'field_iela_user_status_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'text',
  ),
  'field_instance' => array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'Use esse campo para adicionar uma informação complementar que aparecerá próxima do nome do usuário nas listas. <small>Presidente, Aluno colaborador, Professor, etc.</small>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
      'featured' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_plain',
        'weight' => '3',
      ),
      'full_name' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '3',
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_iela_user_status',
    'label' => 'Breve descrição',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => '0',
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => '60',
      ),
      'type' => 'text_textfield',
      'weight' => '6',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'text',
);
