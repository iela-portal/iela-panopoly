<?php
/**
 * @file
 * variable.field_bundle_settings_taxonomy_term__panopoly_categories.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'field_bundle_settings_taxonomy_term__panopoly_categories';
$strongarm->value = array(
  'view_modes' => array(
    'full' => array(
      'custom_settings' => FALSE,
    ),
    'token' => array(
      'custom_settings' => FALSE,
    ),
  ),
  'extra_fields' => array(
    'form' => array(
      'path' => array(
        'weight' => '3',
      ),
      'name' => array(
        'weight' => '0',
      ),
      'description' => array(
        'weight' => '2',
      ),
    ),
    'display' => array(
      'description' => array(
        'default' => array(
          'weight' => '1',
          'visible' => TRUE,
        ),
      ),
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
