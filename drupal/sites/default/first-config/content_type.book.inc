<?php
/**
 * @file
 * content_type.book.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>Livro</i> para cadastrar um livro no sistema.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Livro',
  'title_label' => 'Título',
  'type' => 'book',
);

$dependencies = array();

$optional = array(
  'field.node.body.book' => 'field.node.body.book',
  'field.node.field_collection.book' => 'field.node.field_collection.book',
  'field.node.field_featured_image.book' => 'field.node.field_featured_image.book',
  'permission.create_book_content' => 'permission.create_book_content',
  'permission.delete_any_book_content' => 'permission.delete_any_book_content',
  'permission.delete_own_book_content' => 'permission.delete_own_book_content',
  'permission.edit_any_book_content' => 'permission.edit_any_book_content',
  'permission.edit_own_book_content' => 'permission.edit_own_book_content',
);

$modules = array(
  0 => 'node',
);
