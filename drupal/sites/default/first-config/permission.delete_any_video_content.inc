<?php
/**
 * @file
 * permission.delete_any_video_content.inc
 */

$api = '2.0.0';

$data = array(
  'permission' => 'delete any video content',
  'roles' => array(),
);

$dependencies = array(
  'content_type.video' => 'content_type.video',
);

$optional = array();

$modules = array(
  0 => 'node',
);
