<?php
/**
 * @file
 * field.node.field_image_gallery.news.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '-1',
    'columns' => array(
      'revision_id' => array(
        'description' => 'The field collection item revision id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
      'value' => array(
        'description' => 'The field collection item id.',
        'not null' => FALSE,
        'type' => 'int',
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_image_gallery',
    'foreign keys' => array(),
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
    ),
    'locked' => '0',
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'path' => '',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_image_gallery' => array(
              'revision_id' => 'field_image_gallery_revision_id',
              'value' => 'field_image_gallery_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_image_gallery' => array(
              'revision_id' => 'field_image_gallery_revision_id',
              'value' => 'field_image_gallery_value',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'field_collection',
  ),
  'field_instance' => array(
    'bundle' => 'news',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => 'Use esse campo pra criar uma galeria de imagens para a notícia.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'flexslider_field_collection',
        'settings' => array(
          'optionset' => 'media_gallery',
          'view_mode' => 'full',
        ),
        'type' => 'field_collection_entity_flexslider',
        'weight' => '6',
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'mini_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '2',
      ),
      'poster' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'search_result' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stacked' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'stand' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '4',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image_gallery',
    'label' => 'Galeria',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '6',
    ),
  ),
);

$dependencies = array(
  'content_type.news' => 'content_type.news',
);

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'field_collection',
);
