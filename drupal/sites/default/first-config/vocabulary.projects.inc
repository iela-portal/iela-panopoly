<?php
/**
 * @file
 * vocabulary.projects.inc
 */

$api = '2.0.0';

$data = (object) array(
  'description' => 'Projetos sob guarda-chuva do IELA',
  'hierarchy' => '1',
  'machine_name' => 'projects',
  'module' => 'taxonomy',
  'name' => 'Projetos',
  'vid' => '4',
  'weight' => '0',
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'taxonomy',
);
