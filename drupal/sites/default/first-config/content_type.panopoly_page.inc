<?php
/**
 * @file
 * content_type.panopoly_page.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>Página</i> para criar páginas aleatórias ou páginas internas de um projeto.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Página',
  'title_label' => 'Título da Página',
  'type' => 'panopoly_page',
);

$dependencies = array();

$optional = array(
  'field.node.body.panopoly_page' => 'field.node.body.panopoly_page',
  'field.node.field_featured_image.panopoly_page' => 'field.node.field_featured_image.panopoly_page',
  'field.node.field_featured_status.panopoly_page' => 'field.node.field_featured_status.panopoly_page',
  'field.node.field_project.panopoly_page' => 'field.node.field_project.panopoly_page',
  'permission.create_panopoly_page_content' => 'permission.create_panopoly_page_content',
  'permission.delete_any_panopoly_page_content' => 'permission.delete_any_panopoly_page_content',
  'permission.delete_own_panopoly_page_content' => 'permission.delete_own_panopoly_page_content',
  'permission.edit_any_panopoly_page_content' => 'permission.edit_any_panopoly_page_content',
  'permission.edit_own_panopoly_page_content' => 'permission.edit_own_panopoly_page_content',
);

$modules = array(
  0 => 'node',
);
