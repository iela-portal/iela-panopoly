<?php
/**
 * @file
 * field.field_collection_item.field_multimedia.field_image_gallery.inc
 */

$api = '2.0.0';

$data = array(
  'field_config' => array(
    'active' => '1',
    'cardinality' => '1',
    'columns' => array(
      'description' => array(
        'description' => 'A description of the file.',
        'not null' => FALSE,
        'type' => 'text',
      ),
      'display' => array(
        'default' => 1,
        'description' => 'Flag to control whether this file should be displayed when viewing content.',
        'not null' => TRUE,
        'size' => 'tiny',
        'type' => 'int',
        'unsigned' => TRUE,
      ),
      'fid' => array(
        'description' => 'The {file_managed}.fid being referenced in this field.',
        'not null' => FALSE,
        'type' => 'int',
        'unsigned' => TRUE,
      ),
    ),
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_multimedia',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => '0',
    'module' => 'file',
    'settings' => array(
      'display_default' => 0,
      'display_field' => 0,
      'uri_scheme' => 'public',
    ),
    'storage' => array(
      'active' => '1',
      'details' => array(
        'sql' => array(
          'FIELD_LOAD_CURRENT' => array(
            'field_data_field_multimedia' => array(
              'description' => 'field_multimedia_description',
              'display' => 'field_multimedia_display',
              'fid' => 'field_multimedia_fid',
            ),
          ),
          'FIELD_LOAD_REVISION' => array(
            'field_revision_field_multimedia' => array(
              'description' => 'field_multimedia_description',
              'display' => 'field_multimedia_display',
              'fid' => 'field_multimedia_fid',
            ),
          ),
        ),
      ),
      'module' => 'field_sql_storage',
      'settings' => array(),
      'type' => 'field_sql_storage',
    ),
    'translatable' => '0',
    'type' => 'file',
  ),
  'field_instance' => array(
    'bundle' => 'field_image_gallery',
    'deleted' => '0',
    'description' => 'Imagem ou vídeo para usar em um slide.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'default',
        ),
        'type' => 'file_rendered',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_multimedia',
    'label' => 'Multimedia',
    'required' => 1,
    'settings' => array(
      'description_field' => 0,
      'file_directory' => 'gallery',
      'file_extensions' => 'png jpeg jpg avi mpeg',
      'max_filesize' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'public' => 'public',
          'vimeo' => 'vimeo',
          'youtube' => 'youtube',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 'video',
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 'media_default--media_browser_1',
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 'media_internet',
          'upload' => 'upload',
          'youtube' => 'youtube',
        ),
        'manualcrop_crop_info' => 1,
        'manualcrop_default_crop_area' => 1,
        'manualcrop_enable' => 0,
        'manualcrop_inline_crop' => 0,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => 1,
        'manualcrop_keyboard' => 1,
        'manualcrop_maximize_default_crop_area' => 0,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => 0,
      ),
      'type' => 'media_generic',
      'weight' => '2',
    ),
  ),
);

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'field_sql_storage',
  1 => 'file',
  2 => 'media',
);
