<?php
/**
 * @file
 * variable.panelizer_defaults_user_user.inc
 */

$api = '2.0.0';

$data = $strongarm = new stdClass();
$strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
$strongarm->api_version = 1;
$strongarm->name = 'panelizer_defaults_user_user';
$strongarm->value = array(
  'status' => 1,
  'view modes' => array(
    'page_manager' => array(
      'status' => 1,
      'default' => 0,
      'choice' => 0,
    ),
    'default' => array(
      'status' => 0,
      'default' => 1,
      'choice' => 0,
    ),
    'full_name' => array(
      'status' => 0,
      'default' => 0,
      'choice' => 0,
    ),
    'featured' => array(
      'status' => 0,
      'default' => 1,
      'choice' => 0,
    ),
  ),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'strongarm',
  1 => 'ctools',
  2 => 'system',
);
