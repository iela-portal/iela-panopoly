<?php
/**
 * @file
 * content_type.news.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>Notícia</i> para cadastrar notícias ou publicações de blog.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Notícia',
  'title_label' => 'Título',
  'type' => 'news',
);

$dependencies = array();

$optional = array(
  'field.node.body.news' => 'field.node.body.news',
  'field.node.field_featured_image.news' => 'field.node.field_featured_image.news',
  'field.node.field_featured_status.news' => 'field.node.field_featured_status.news',
  'field.node.field_image_gallery.news' => 'field.node.field_image_gallery.news',
  'field.node.field_project.news' => 'field.node.field_project.news',
  'field.node.field_tags.news' => 'field.node.field_tags.news',
  'field.node.field_teaser.news' => 'field.node.field_teaser.news',
  'permission.create_news_content' => 'permission.create_news_content',
  'permission.delete_any_news_content' => 'permission.delete_any_news_content',
  'permission.delete_own_news_content' => 'permission.delete_own_news_content',
  'permission.edit_any_news_content' => 'permission.edit_any_news_content',
  'permission.edit_own_news_content' => 'permission.edit_own_news_content',
);

$modules = array(
  0 => 'node',
);
