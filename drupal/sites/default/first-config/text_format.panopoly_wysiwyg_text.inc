<?php
/**
 * @file
 * text_format.panopoly_wysiwyg_text.inc
 */

$api = '2.0.0';

$data = (object) array(
  'cache' => '0',
  'filters' => array(
    'caption_filter' => array(
      'settings' => array(),
      'status' => '1',
      'weight' => '-47',
    ),
    'filter_autop' => array(
      'settings' => array(),
      'status' => '1',
      'weight' => '-44',
    ),
    'filter_htmlcorrector' => array(
      'settings' => array(),
      'status' => '1',
      'weight' => '-45',
    ),
    'filter_url' => array(
      'settings' => array(
        'filter_url_length' => '72',
      ),
      'status' => '1',
      'weight' => '-43',
    ),
    'image_resize_filter' => array(
      'settings' => array(
        'image_locations' => array(
          'local' => 'local',
          'remote' => 'remote',
        ),
        'link' => 0,
        'link_class' => '',
        'link_rel' => '',
      ),
      'status' => '1',
      'weight' => '-49',
    ),
    'media_filter' => array(
      'settings' => array(),
      'status' => '1',
      'weight' => '-46',
    ),
    'node_embed' => array(
      'settings' => array(),
      'status' => '1',
      'weight' => '-50',
    ),
  ),
  'format' => 'panopoly_wysiwyg_text',
  'name' => 'WYSIWYG',
  'status' => '1',
  'weight' => '-10',
);

$dependencies = array();

$optional = array(
  'permission.use_text_format_panopoly_wysiwyg_text' => 'permission.use_text_format_panopoly_wysiwyg_text',
  'wysiwyg.panopoly_wysiwyg_text' => 'wysiwyg.panopoly_wysiwyg_text',
);

$modules = array(
  0 => 'node_embed',
  1 => 'image_resize_filter',
  2 => 'caption_filter',
  3 => 'media_wysiwyg',
  4 => 'filter',
);
