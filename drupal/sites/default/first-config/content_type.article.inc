<?php
/**
 * @file
 * content_type.article.inc
 */

$api = '2.0.0';

$data = (object) array(
  'base' => 'node_content',
  'description' => 'Use <i>artigo</i> para cadastrar artigos no formato PDF ou Word.',
  'has_title' => '1',
  'help' => '',
  'name' => 'Artigo',
  'title_label' => 'Título',
  'type' => 'article',
);

$dependencies = array();

$optional = array(
  'field.node.field_document.article' => 'field.node.field_document.article',
  'field.node.field_featured_image.article' => 'field.node.field_featured_image.article',
  'field.node.field_project.article' => 'field.node.field_project.article',
  'field.node.field_tags.article' => 'field.node.field_tags.article',
  'field.node.field_teaser.article' => 'field.node.field_teaser.article',
  'permission.create_article_content' => 'permission.create_article_content',
  'permission.delete_any_article_content' => 'permission.delete_any_article_content',
  'permission.delete_own_article_content' => 'permission.delete_own_article_content',
  'permission.edit_any_article_content' => 'permission.edit_any_article_content',
  'permission.edit_own_article_content' => 'permission.edit_own_article_content',
);

$modules = array(
  0 => 'node',
);
