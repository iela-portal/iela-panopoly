<?php
/**
 * @file
 * wysiwyg.filtered_html.inc
 */

$api = '2.0.0';

$data = (object) array(
  'editor' => 'ckeditor',
  'format' => 'filtered_html',
  'settings' => array(
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
    'buttons' => array(
      'default' => array(
        'Anchor' => 1,
        'BidiLtr' => 1,
        'BidiRtl' => 1,
        'Bold' => 1,
        'BulletedList' => 1,
        'Copy' => 1,
        'Cut' => 1,
        'Font' => 1,
        'FontSize' => 1,
        'Format' => 1,
        'Image' => 1,
        'Italic' => 1,
        'JustifyBlock' => 1,
        'JustifyCenter' => 1,
        'JustifyLeft' => 1,
        'JustifyRight' => 1,
        'Link' => 1,
        'NumberedList' => 1,
        'Paste' => 1,
        'PasteText' => 1,
        'Redo' => 1,
        'SelectAll' => 1,
        'Source' => 1,
        'Strike' => 1,
        'Subscript' => 1,
        'Superscript' => 1,
        'Table' => 1,
        'TextColor' => 1,
        'Underline' => 1,
        'Undo' => 1,
      ),
    ),
    'convert_fonts_to_spans' => 1,
    'css_path' => '',
    'css_setting' => 'theme',
    'default' => 1,
    'forcePasteAsPlainText' => 0,
    'language' => 'pt-br',
    'path_loc' => 'bottom',
    'preformatted' => 0,
    'remove_linebreaks' => 1,
    'resize_enabled' => 1,
    'show_toggle' => 1,
    'simple_source_formatting' => 0,
    'stylesSet' => '',
    'theme' => 'advanced',
    'toolbarLocation' => 'top',
    'toolbar_align' => 'left',
    'user_choose' => 0,
    'verify_html' => 1,
  ),
);

$dependencies = array(
  'text_format.filtered_html' => 'text_format.filtered_html',
);

$optional = array();

$modules = array(
  0 => 'wysiwyg',
);
