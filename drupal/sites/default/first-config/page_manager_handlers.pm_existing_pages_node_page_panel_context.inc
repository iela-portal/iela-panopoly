<?php
/**
 * @file
 * page_manager_handlers.pm_existing_pages_node_page_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'pm_existing_pages_node_page_panel_context';
$handler->task = 'pm_existing_pages';
$handler->subtask = 'node_page';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Notícia',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'body_classes_to_remove' => '',
  'body_classes_to_add' => 'news-page',
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(),
    'logic' => 'and',
  ),
);
$display = new panels_display();
$display->layout = 'iela-simple';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'highlighted' => NULL,
    'header' => NULL,
    'header_second' => array(
      'container_background' => array(
        'color' => '#eeeeee',
        'reverse' => 0,
        'use_background_images' => 0,
        'background_image' => array(
          'behavior' => 'cover',
          'position' => NULL,
          'images' => array(
            0 => 0,
          ),
        ),
      ),
      'spacing' => array(
        'fluid' => 0,
        'divisor' => array(
          'top' => array(
            'type' => '',
            'extended' => 0,
            'thickness' => 'thin',
            'color' => '',
          ),
          'bottom' => array(
            'type' => '',
            'extended' => 0,
            'thickness' => 'thin',
            'color' => '',
          ),
        ),
        'padding' => array(
          'top' => 'small',
          'right' => 'big',
          'bottom' => 'small',
          'left' => 'big',
        ),
        'margin' => array(
          'top' => '',
          'right' => '',
          'bottom' => 'medium',
          'left' => '',
        ),
      ),
    ),
    'header_third' => NULL,
    'header_fourth' => NULL,
    'content' => NULL,
    'content_second' => NULL,
    'content_third' => NULL,
    'content_fourth' => NULL,
    'content_fifth' => NULL,
    'footer' => NULL,
    'footer_second' => NULL,
    'footer_third' => NULL,
    'footer_fourth' => NULL,
  ),
  'header_second' => array(
    'style' => 'multiple',
  ),
);
$display->cache = array();
$display->title = '';
$display->uuid = '23c6bf26-d047-4053-a0c6-ae49b4010001';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f13bd2a1-cafe-4db1-84e4-47e977f52f86';
  $pane->panel = 'content';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'adaptative_pane' => array(
      'breakpoints.theme.iela_theme.lg' => array(
        'columns' => '10',
        'bleed' => 0,
        'ordering' => 'push-1',
      ),
      'breakpoints.theme.iela_theme.md' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
      'breakpoints.theme.iela_theme.sm' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
      'breakpoints.theme.iela_theme.xs' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f13bd2a1-cafe-4db1-84e4-47e977f52f86';
  $display->content['new-f13bd2a1-cafe-4db1-84e4-47e977f52f86'] = $pane;
  $display->panels['content'][0] = 'new-f13bd2a1-cafe-4db1-84e4-47e977f52f86';
  $pane = new stdClass();
  $pane->pid = 'new-a6f136e2-0d20-45cf-b4b7-77e8b84c5bb4';
  $pane->panel = 'footer';
  $pane->type = 'block';
  $pane->subtype = 'disqus-disqus_comments';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Comentários',
    'adaptative_pane' => array(
      'breakpoints.theme.iela_theme.lg' => array(
        'columns' => '10',
        'bleed' => 0,
        'ordering' => 'push-1',
      ),
      'breakpoints.theme.iela_theme.md' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
      'breakpoints.theme.iela_theme.sm' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
      'breakpoints.theme.iela_theme.xs' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
    ),
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'a6f136e2-0d20-45cf-b4b7-77e8b84c5bb4';
  $display->content['new-a6f136e2-0d20-45cf-b4b7-77e8b84c5bb4'] = $pane;
  $display->panels['footer'][0] = 'new-a6f136e2-0d20-45cf-b4b7-77e8b84c5bb4';
  $pane = new stdClass();
  $pane->pid = 'new-c40106c1-ac1b-4bc3-9858-8174922b0d18';
  $pane->panel = 'header';
  $pane->type = 'heading';
  $pane->subtype = 'heading';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'use_page_title' => 1,
    'title' => '',
    'use_authoring_information' => 1,
    'description' => '',
    'tag' => 'h1',
    'alignment' => 'center',
    'override_title' => '',
    'override_title_text' => '',
    'adaptative_pane' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c40106c1-ac1b-4bc3-9858-8174922b0d18';
  $display->content['new-c40106c1-ac1b-4bc3-9858-8174922b0d18'] = $pane;
  $display->panels['header'][0] = 'new-c40106c1-ac1b-4bc3-9858-8174922b0d18';
  $pane = new stdClass();
  $pane->pid = 'new-73371547-b673-41cf-a161-81c63ab6b961';
  $pane->panel = 'header_second';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_summary_or_trimmed',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '600',
    ),
    'context' => array(),
    'override_title' => 0,
    'override_title_text' => '',
    'adaptative_pane' => array(
      'breakpoints.theme.iela_theme.lg' => array(
        'columns' => '8',
        'bleed' => 0,
        'ordering' => 'push-2',
      ),
      'breakpoints.theme.iela_theme.md' => array(
        'columns' => '10',
        'bleed' => 0,
        'ordering' => 'push-1',
      ),
      'breakpoints.theme.iela_theme.sm' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
      'breakpoints.theme.iela_theme.xs' => array(
        'columns' => '12',
        'bleed' => 0,
        'ordering' => 'push-0',
      ),
    ),
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'featured_text',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '73371547-b673-41cf-a161-81c63ab6b961';
  $display->content['new-73371547-b673-41cf-a161-81c63ab6b961'] = $pane;
  $display->panels['header_second'][0] = 'new-73371547-b673-41cf-a161-81c63ab6b961';
$display->hide_title = PANELS_TITLE_NONE;
$display->title_pane = 'new-c40106c1-ac1b-4bc3-9858-8174922b0d18';
$handler->conf['display'] = $display;


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
