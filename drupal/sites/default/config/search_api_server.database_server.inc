<?php
/**
 * @file
 * search_api_server.database_server.inc
 */

$api = '2.0.0';

$data = entity_import('search_api_server', '{
    "name" : "Database Server",
    "machine_name" : "database_server",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "3",
      "indexes" : { "database_node_index" : {
          "nid" : {
            "table" : "search_api_db_database_node_index_nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_database_node_index_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_database_node_index_title",
            "type" : "text",
            "boost" : "8.0"
          },
          "url" : {
            "table" : "search_api_db_database_node_index_url",
            "type" : "uri",
            "boost" : "1.0"
          },
          "status" : {
            "table" : "search_api_db_database_node_index_status",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "created" : {
            "table" : "search_api_db_database_node_index_created",
            "type" : "integer",
            "boost" : "1.0"
          },
          "author" : {
            "table" : "search_api_db_database_node_index_author",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_database_node_index_search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "search_api_url" : {
            "table" : "search_api_db_database_node_index_search_api_url",
            "type" : "uri",
            "boost" : "1.0"
          },
          "search_api_access_node" : {
            "table" : "search_api_db_database_node_index_search_api_access_node",
            "type" : "list\\u003Cstring\\u003E",
            "boost" : "1.0"
          },
          "body:value" : {
            "table" : "search_api_db_database_node_index_body_value",
            "type" : "text",
            "boost" : "1.0"
          },
          "body:summary" : {
            "table" : "search_api_db_database_node_index_body_summary",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_featured_status" : {
            "table" : "search_api_db_database_node_index_field_featured_status",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_featured_image:file" : {
            "table" : "search_api_db_database_node_index_field_featured_image_file",
            "type" : "integer",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1",
    "entity_type" : "search_api_server"
  }');

$dependencies = array();

$optional = array();

$modules = array(
  0 => 'entity',
  1 => 'search_api',
);
