<?php
/**
 * @file
 * page_manager_pages.panopoly_search.inc
 */

$api = '2.0.0';

$data = $page = new stdClass();
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'panopoly_search';
$page->task = 'page';
$page->admin_title = 'Search Page';
$page->admin_description = '';
$page->path = 'busca/!keywords';
$page->access = array(
  'plugins' => array(
    0 => array(
      'name' => 'perm',
      'settings' => array(
        'perm' => 'search content',
      ),
      'context' => 'logged-in-user',
      'not' => FALSE,
    ),
  ),
  'logic' => 'and',
  'type' => 'none',
  'settings' => NULL,
);
$page->menu = array();
$page->arguments = array(
  'keywords' => array(
    'id' => 1,
    'identifier' => 'String',
    'name' => 'string',
    'settings' => array(
      'use_tail' => 0,
    ),
  ),
);
$page->conf = array(
  'admin_paths' => FALSE,
);


$dependencies = array(
  'page_manager_handlers.page_panopoly_search_panel_context' => 'page_manager_handlers.page_panopoly_search_panel_context',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
