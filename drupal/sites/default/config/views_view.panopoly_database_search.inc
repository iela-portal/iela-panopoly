<?php
/**
 * @file
 * views_view.panopoly_database_search.inc
 */

$api = '2.0.0';

$data = $view = new view();
$view->name = 'panopoly_database_search';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'search_api_index_database_node_index';
$view->human_name = 'Database Search';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'search content';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'load_more';
$handler->display->display_options['pager']['options']['items_per_page'] = '12';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '5';
$handler->display->display_options['pager']['options']['effects']['type'] = 'fade';
$handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['row_class'] = 'clearfix';
$handler->display->display_options['row_plugin'] = 'fields';
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'Empty Text';
$handler->display->display_options['empty']['area']['content'] = 'Your search did not return any results.';
$handler->display->display_options['empty']['area']['format'] = 'panopoly_html_text';
/* Field: Image */
$handler->display->display_options['fields']['field_featured_image']['id'] = 'field_featured_image';
$handler->display->display_options['fields']['field_featured_image']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['fields']['field_featured_image']['field'] = 'field_featured_image';
$handler->display->display_options['fields']['field_featured_image']['ui_name'] = 'Image';
$handler->display->display_options['fields']['field_featured_image']['label'] = '';
$handler->display->display_options['fields']['field_featured_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_featured_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_featured_image']['settings'] = array(
  'image_style' => 'panopoly_image_quarter',
  'image_link' => 'content',
);
/* Field: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['ui_name'] = 'Title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_entity'] = 1;
/* Field: Teaser */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['ui_name'] = 'Teaser';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
$handler->display->display_options['fields']['body']['settings'] = array(
  'trim_length' => '300',
);
/* Sort criterion: Search: Relevance */
$handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
$handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
$handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
/* Contextual filter: Search: Fulltext search */
$handler->display->display_options['arguments']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
$handler->display->display_options['arguments']['search_api_views_fulltext']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['arguments']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
$handler->display->display_options['arguments']['search_api_views_fulltext']['default_action'] = 'empty';
$handler->display->display_options['arguments']['search_api_views_fulltext']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['search_api_views_fulltext']['default_argument_options']['argument'] = 'vegetables';
$handler->display->display_options['arguments']['search_api_views_fulltext']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['search_api_views_fulltext']['break_phrase'] = 0;
/* Filter criterion: Indexed Node: Status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = array(
  1 => '1',
);

/* Display: Database Search Results */
$handler = $view->new_display('panel_pane', 'Database Search Results', 'search_database_results');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Indexed Node: Status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = array(
  1 => '1',
);
/* Filter criterion: Indexed Node: Content type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array();
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Tipo de conteúdo';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['type']['expose']['reduce'] = 0;
/* Filter criterion: Imagem de Destaque: The image file. (indexed) */
$handler->display->display_options['filters']['field_featured_image_file']['id'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['field_featured_image_file']['field'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_featured_image_file']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_featured_image_file']['expose']['operator_id'] = 'field_featured_image_file_op';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['label'] = 'Imagem de Destaque';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['operator'] = 'field_featured_image_file_op';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['identifier'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['pane_title'] = 'Database Search Results';
$handler->display->display_options['pane_category']['name'] = 'Search';
$handler->display->display_options['pane_category']['weight'] = '0';
$handler->display->display_options['allow']['use_pager'] = 'use_pager';
$handler->display->display_options['allow']['set_view_mode'] = 0;
$handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
$handler->display->display_options['allow']['offset'] = 0;
$handler->display->display_options['allow']['link_to_view'] = 0;
$handler->display->display_options['allow']['more_link'] = 0;
$handler->display->display_options['allow']['path_override'] = 0;
$handler->display->display_options['allow']['title_override'] = 'title_override';
$handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
$handler->display->display_options['allow']['fields_override'] = 'fields_override';
$handler->display->display_options['argument_input'] = array(
  'search_api_views_fulltext' => array(
    'type' => 'context',
    'context' => 'string.html_safe',
    'context_optional' => 0,
    'panel' => '2',
    'fixed' => '',
    'label' => 'Search: Fulltext search',
  ),
);

/* Display: Database Search Secondary Results */
$handler = $view->new_display('panel_pane', 'Database Search Secondary Results', 'search_database_secondary_results');
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '4';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '3';
$handler->display->display_options['defaults']['empty'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Indexed Node: Status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = array(
  1 => '1',
);
/* Filter criterion: Indexed Node: Content type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array();
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Tipo de conteúdo';
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['expose']['multiple'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['filters']['type']['expose']['reduce'] = 0;
/* Filter criterion: Imagem de Destaque: The image file. (indexed) */
$handler->display->display_options['filters']['field_featured_image_file']['id'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['table'] = 'search_api_index_database_node_index';
$handler->display->display_options['filters']['field_featured_image_file']['field'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['operator'] = 'not empty';
$handler->display->display_options['filters']['field_featured_image_file']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_featured_image_file']['expose']['operator_id'] = 'field_featured_image_file_op';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['label'] = 'Imagem de Destaque';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['operator'] = 'field_featured_image_file_op';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['identifier'] = 'field_featured_image_file';
$handler->display->display_options['filters']['field_featured_image_file']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
  4 => 0,
);
$handler->display->display_options['pane_title'] = 'Database Search Secondary Results';
$handler->display->display_options['pane_category']['name'] = 'Search';
$handler->display->display_options['pane_category']['weight'] = '0';
$handler->display->display_options['allow']['use_pager'] = 'use_pager';
$handler->display->display_options['allow']['set_view_mode'] = 0;
$handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
$handler->display->display_options['allow']['offset'] = 0;
$handler->display->display_options['allow']['link_to_view'] = 0;
$handler->display->display_options['allow']['more_link'] = 0;
$handler->display->display_options['allow']['path_override'] = 0;
$handler->display->display_options['allow']['title_override'] = 'title_override';
$handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
$handler->display->display_options['allow']['fields_override'] = 'fields_override';
$handler->display->display_options['argument_input'] = array(
  'search_api_views_fulltext' => array(
    'type' => 'context',
    'context' => 'string.html_safe',
    'context_optional' => 0,
    'panel' => '2',
    'fixed' => '',
    'label' => 'Search: Fulltext search',
  ),
);
$translatables['panopoly_database_search'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Load more'),
  t('Empty Text'),
  t('Your search did not return any results.'),
  t('All'),
  t('Database Search Results'),
  t('Tipo de conteúdo'),
  t('Imagem de Destaque'),
  t('Search'),
  t('Database Search Secondary Results'),
);


$dependencies = array();

$optional = array();

$modules = array(
  0 => 'views',
  1 => '',
  2 => 'image',
  3 => 'text',
  4 => 'views_content',
);
