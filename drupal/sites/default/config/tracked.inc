<?php

// This file contains the current being tracked configurations.

$tracked = array (
  'views_view.panopoly_database_search' => 'd7271c135a4bbb610586daabf6641c66c08f35ff',
  'views_view.panopoly_search' => '03d01e7c0c5eb3295acc8bcd3b294171952f163e',
  'page_manager_handlers.page_panopoly_search_panel_context' => 'a1e72b2d3f5b399aa30e0ea6ea123cec18ed0de5',
  'page_manager_pages.panopoly_search' => '74924e0e0c6e88e9f8320e7557c2495b401bc072',
  'search_api_server.database_server' => '4fb13535f13f25aa201d5c93b0d84233fc67a6ed',
  'search_api_index.database_node_index' => 'c9184843b6856c2b1fe78358552213f907dbeaf2',
);


// The following modules are required to run the configurations of this file.

$modules = array(
  'ctools',
  'page_manager',
  'entity',
  'search_api',
  'views',
  '',
  'image',
  'text',
  'views_content',
);
