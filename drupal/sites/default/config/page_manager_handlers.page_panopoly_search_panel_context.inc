<?php
/**
 * @file
 * page_manager_handlers.page_panopoly_search_panel_context.inc
 */

$api = '2.0.0';

$data = $handler = new stdClass();
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_panopoly_search_panel_context';
$handler->task = 'page';
$handler->subtask = 'panopoly_search';
$handler->handler = 'panel_context';
$handler->weight = -30;
$handler->conf = array(
  'title' => 'Solr Search Panel',
  'no_blocks' => 0,
  'pipeline' => 'ipe',
  'css_id' => 'solr-search-panel',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      1 => array(
        'name' => 'solr',
        'settings' => NULL,
        'not' => FALSE,
      ),
      2 => array(
        'name' => 'search_api_index',
        'settings' => NULL,
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  ),
  'panels_breadcrumbs_state' => 1,
  'panels_breadcrumbs_titles' => 'Search',
  'panels_breadcrumbs_paths' => 'search/site',
  'panels_breadcrumbs_home' => 1,
);
$display = new panels_display();
$display->layout = 'burr_flipped';
$display->layout_settings = array();
$display->panel_settings = array(
  'style_settings' => array(
    'default' => NULL,
    'sidebar' => NULL,
    'contentmain' => NULL,
  ),
);
$display->cache = array();
$display->title = 'Search Results';
$display->uuid = 'b369b7fe-466f-4e52-b315-5070800e6d63';
$display->content = array();
$display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-15f4a1d6-8bf1-43cd-8ef3-a8b484aabe79';
  $pane->panel = 'contentmain';
  $pane->type = 'search_current';
  $pane->subtype = 'search_current';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'node',
    'form' => 'advanced',
    'path_type' => 'default',
    'path' => '',
    'override_prompt' => FALSE,
    'prompt' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '15f4a1d6-8bf1-43cd-8ef3-a8b484aabe79';
  $display->content['new-15f4a1d6-8bf1-43cd-8ef3-a8b484aabe79'] = $pane;
  $display->panels['contentmain'][0] = 'new-15f4a1d6-8bf1-43cd-8ef3-a8b484aabe79';
  $pane = new stdClass();
  $pane->pid = 'new-9315ef28-12fb-4ed8-a491-1c628e698406';
  $pane->panel = 'contentmain';
  $pane->type = 'search_box';
  $pane->subtype = 'search_box';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'node',
    'form' => 'simple',
    'path_type' => 'default',
    'path' => '',
    'override_prompt' => 1,
    'prompt' => '',
    'context' => array(),
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '9315ef28-12fb-4ed8-a491-1c628e698406';
  $display->content['new-9315ef28-12fb-4ed8-a491-1c628e698406'] = $pane;
  $display->panels['contentmain'][1] = 'new-9315ef28-12fb-4ed8-a491-1c628e698406';
  $pane = new stdClass();
  $pane->pid = 'new-25ec26bf-8e25-4163-bf5c-406fed80259d';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'panopoly_search-search_solr_results';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'argument_string_1',
    ),
    'view_mode' => NULL,
    'widget_title' => '',
    'items_per_page' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '25ec26bf-8e25-4163-bf5c-406fed80259d';
  $display->content['new-25ec26bf-8e25-4163-bf5c-406fed80259d'] = $pane;
  $display->panels['contentmain'][2] = 'new-25ec26bf-8e25-4163-bf5c-406fed80259d';
  $pane = new stdClass();
  $pane->pid = 'new-3d1478d7-0f89-424e-824f-82483bc89651';
  $pane->panel = 'sidebar';
  $pane->type = 'facet';
  $pane->subtype = 'facet';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(),
    'override_title' => 1,
    'override_title_text' => 'Filter by Type',
    'delta' => 'Shb0Q1vWgCArrHrjHszTyhl2jaEhIjzW',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3d1478d7-0f89-424e-824f-82483bc89651';
  $display->content['new-3d1478d7-0f89-424e-824f-82483bc89651'] = $pane;
  $display->panels['sidebar'][0] = 'new-3d1478d7-0f89-424e-824f-82483bc89651';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;


$dependencies = array(
  'views_view.panopoly_search' => 'views_view.panopoly_search',
);

$optional = array();

$modules = array(
  0 => 'ctools',
  1 => 'page_manager',
);
