<p align="center">
  <a href="https://iela.ufsc.br">
    <img src="./logo.png" style="max-width: 400px" />
  </a>
</p>

<h1 align="center">
  Código-fonte e Guia de Desenvolvimento do <br /><a href="https://iela.ufsc.br">Portal do IELA</a>
</h1>

## Visão geral de tecnologías

O portal é desenvolvido por sobre o framework [Drupal 7](https://www.drupal.org/drupal-7.0), e portanto consiste de uma aplicação escrita em PHP (5.5.9) e de um banco de dados MySQL (5.5.41-0ubuntu0.14.04.1). A distribuição Drupal usada como base é o [Panopoly](https://www.drupal.org/project/panopoly), em versão específica para o Drupal 7.

São seguidos todos os padrões de desenvolvimento adotados na comunidade Drupal.

Como tema, é utilizado como base o [Radix](https://www.drupal.org/project/radix), fundamentado em Bootstrap 4. Para estilos, foi utilizado Sass, compilado para CSS com Compass (Ruby).

Para desenvolvimento local, há uma configuração de virtualização feita com Docker Compose, removendo a necessidade de instalação manual das dependencias citadas acima.

## Do que você vai precisar

Para desenvolvimento local, você precisara ter instalado:

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Make](<https://en.wikipedia.org/wiki/Make_(software)>) (optional)
- [Git](https://git-scm.com/)

> Esse guia não cobrirá a instalação sem uso de Docker.

## Como começar

### 1. Clone este repositório

```
git clone git@bitbucket.org:iela-portal/iela-panopoly.git
```

### 2. Instalação e execução do Drupal

```
make run
```

Este comando opera de duas formas diferentes:

Na primeira vez que executado, passos iniciais de setup serão realizados, como importação do banco de dados inicial (baseado no estado do site em 2018) e compilação inicial do CSS (baseado em Sass). Dessa forma, ao fim da execução, um site operacional deve estar disponível para visualização.

Nas vezes posteriores o comando deve terminar muito mais rapidamente, já que as tarefas iniciais não serão executadas. Como no anterior, ao final da execução o site deve estar disponível para visualização.

> Caso não tenha Make disponível, execute manualmente o comando `run` descrito no arquivo [`./Makefile`](`./Makefile`)

**Pegue um café** :coffee:

> Não tem SSD? Pegue dois cafés.

**Acesse o Drupal**

Accesse o endereço http://localhost:8080 no navegador.

> **Área administrativa**<br />
> [http://localhost:8080/user/login](http://localhost:8080/user/login)<br />
> Username: _admin_<br />
> Password: _password_

## Comandos disponíveis

### Makefile

Segue a descrição e objetivo dos comandos executáveis via Make:

| Command             | Description                                                |
| ------------------- | ---------------------------------------------------------- |
| `make run`          | Inicía e entra no container                                |
| `make in`           | Abre uma nova sessão no container previamente iniciado     |
| `make mysql`        | Acessa o [MySQL](https://www.mysql.com/) usado pelo Drupal |
| `make mysql--reset` | Reseta o banco de dados ao estado inicial                  |
| `make css--compile` | Compila as folhas de estilo                                |
| `make css--watch`   | Compila as folhas de estilo a cada alteração               |
| `make stop`         | Stops all containers                                       |
| `make clean`        | Removes all containers                                     |
| `make build`        | Rebuild the app image                                      |

### Ruby/Compass/CSS

Para compilar o CSS, é preciso acessar o container (`make run` ou `make in`), entrar no diretório `/iela/app/drupal/sites/all/themes/iela_theme/sass/` e executar o comando `bundle exec compass compile`. Opcionalmente, o comando `bundle exec compass watch -e development` serve para entrar em modo desenvolvimento, recompilando o CSS a cada alteração.

Essa tarefa pode ser simplificada executando os comandos `make css--compile` e `make css--watch` respectivamente.

### Drush

O Drupal possúi uma ferramenta de linha de comando robusta, [Drush](https://docs.drush.org/en/8.x/), que está disponível dentro do container. Para execução de comandos Drush, é importante estar dentro do diretório `/iela/app/drupal/`.

## HTTPS local

Execute o seguinte comando para iniciar o servidor HTTPS:

```
make https
```

> `https://local.host:8443`

Embora não seja necessário, simular o site com HTTPS local ajuda a aproximar o ambiente de desenvolvimento do ambiente de produção. O ambiente de desenvolvimento já provê um certificado e script de execução de um proxy (em `./https-proxy/server.js`) para possibilitar o uso de HTTPS localmente. O certificado funciona para o domain `local.host`, portanto é necessário garantir que esse domain aponte para `localhost`. Para iniciar o proxy HTTPS, execute o comando `make https` em outro terminal, após já ter iniciado o servidor com `make run`.
